/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.cg;

import com.darkhonor.rage.libs.listmodel.CourseComboBoxModel;
import com.darkhonor.rage.libs.GraderThread;
import com.darkhonor.rage.libs.listmodel.InstructorComboBoxModel;
import com.darkhonor.rage.libs.RAGEConst;
import com.darkhonor.rage.libs.RageLib;
import com.darkhonor.rage.libs.listmodel.SectionComboBoxModel;
import com.darkhonor.rage.libs.ZipFileFilter;
import com.darkhonor.rage.libs.dataaccess.CourseDAO;
import com.darkhonor.rage.libs.dataaccess.GradedEventDAO;
import com.darkhonor.rage.libs.dataaccess.InstructorDAO;
import com.darkhonor.rage.libs.dataaccess.SectionDAO;
import com.darkhonor.rage.libs.dataaccess.VersionDAO;
import com.darkhonor.rage.libs.listmodel.GradedEventComboBoxModel;
import com.darkhonor.rage.libs.listmodel.StudentComboBoxModel;
import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.GradedEvent;
import com.darkhonor.rage.model.Instructor;
import com.darkhonor.rage.model.Section;
import com.darkhonor.rage.model.SectionReport;
import com.darkhonor.rage.model.Student;
import com.darkhonor.rage.model.StudentReport;
import com.darkhonor.rage.model.Version;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

/**
 * The application's main frame.
 */
public class CourseGraderView extends FrameView
{

    public CourseGraderView(SingleFrameApplication app)
    {
        super(app);

        initComponents();
        LOGGER.debug("Components initialized.");
        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++)
        {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener()
        {

            @Override
            public void propertyChange(java.beans.PropertyChangeEvent evt)
            {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName))
                {
                    if (!busyIconTimer.isRunning())
                    {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName))
                {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName))
                {
                    String text = (String) (evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName))
                {
                    int value = (Integer) (evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });
        LOGGER.debug("Setting buttons");
        btnGrade.setEnabled(false);
        gradeAssignmentsMenuItem.setEnabled(false);
        btnCancel.setEnabled(false);
        btnUnzip.setEnabled(false);
        unzipAssignmentMenuItem.setEnabled(false);
        statusMessageLabel.setText("Welcome to RAGE CourseGrader");
        if (node.get("RootDir", "").equals(""))
        {
            JOptionPane.showMessageDialog(CourseGraderApp.getApplication().getMainFrame(),
                    "Preferences not set.  Please set preferences.");
            showOptionDialog();
            // If Ok was pressed and preferences set, continue...otherwise exit program
            if (optionDialog.getReturnStatus() == OptionsDialog.RET_CANCEL)
            {
                JOptionPane.showMessageDialog(null, "Preferences not set.  "
                        + "Exiting CourseGrader.", "Error", JOptionPane.ERROR_MESSAGE);
                System.exit(1);
            }
        }

        disconnectMenuItem.setEnabled(false);
        LOGGER.debug("Setting Course, Instructor, and Section ComboBox Models");
        cboCourse.setEnabled(false);
        cboCourse.setModel(courseComboBoxModel);
        cboInstructor.setModel(instructorComboBoxModel);
        cboSection.setModel(sectionComboBoxModel);
        cboAssignment.setModel(gradedEventComboBoxModel);
        cboStudent.setModel(studentComboBoxModel);
        LOGGER.debug("Constructor complete");

        // Set default option for DDL Generation to false
        node.put("DDLGen", RAGEConst.DEFAULT_DDL_OPTION);
    }

    @Action
    public void showAboutBox()
    {
        if (aboutBox == null)
        {
            JFrame mainFrame = CourseGraderApp.getApplication().getMainFrame();
            aboutBox = new CourseGraderAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        LOGGER.debug("Showing About Box");
        CourseGraderApp.getApplication().show(aboutBox);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        lblCourse = new javax.swing.JLabel();
        lblAssignment = new javax.swing.JLabel();
        lblInstructor = new javax.swing.JLabel();
        lblSection = new javax.swing.JLabel();
        lblStudent = new javax.swing.JLabel();
        cboCourse = new javax.swing.JComboBox();
        cboAssignment = new javax.swing.JComboBox();
        cboInstructor = new javax.swing.JComboBox();
        cboSection = new javax.swing.JComboBox();
        cboStudent = new javax.swing.JComboBox();
        btnGrade = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        lblStatus = new javax.swing.JLabel();
        btnUnzip = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        rBtnRaptor = new javax.swing.JRadioButton();
        rBtnProcessing = new javax.swing.JRadioButton();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        connectMenuItem = new javax.swing.JMenuItem();
        disconnectMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        toolsMenu = new javax.swing.JMenu();
        gradeAssignmentsMenuItem = new javax.swing.JMenuItem();
        unzipAssignmentMenuItem = new javax.swing.JMenuItem();
        optionsMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();
        langButtonSelect = new javax.swing.ButtonGroup();

        mainPanel.setMinimumSize(new java.awt.Dimension(490, 500));
        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setPreferredSize(new java.awt.Dimension(490, 500));

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(com.darkhonor.rage.cg.CourseGraderApp.class).getContext().getResourceMap(CourseGraderView.class);
        lblCourse.setText(resourceMap.getString("lblCourse.text")); // NOI18N
        lblCourse.setName("lblCourse"); // NOI18N

        lblAssignment.setText(resourceMap.getString("lblAssignment.text")); // NOI18N
        lblAssignment.setName("lblAssignment"); // NOI18N

        lblInstructor.setText(resourceMap.getString("lblInstructor.text")); // NOI18N
        lblInstructor.setName("lblInstructor"); // NOI18N

        lblSection.setText(resourceMap.getString("lblSection.text")); // NOI18N
        lblSection.setName("lblSection"); // NOI18N

        lblStudent.setText(resourceMap.getString("lblStudent.text")); // NOI18N
        lblStudent.setName("lblStudent"); // NOI18N

        cboCourse.setName("cboCourse"); // NOI18N
        cboCourse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCourseActionPerformed(evt);
            }
        });

        cboAssignment.setEnabled(false);
        cboAssignment.setName("cboAssignment"); // NOI18N
        cboAssignment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboAssignmentActionPerformed(evt);
            }
        });

        cboInstructor.setEnabled(false);
        cboInstructor.setName("cboInstructor"); // NOI18N
        cboInstructor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboInstructorActionPerformed(evt);
            }
        });

        cboSection.setEnabled(false);
        cboSection.setName("cboSection"); // NOI18N
        cboSection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboSectionActionPerformed(evt);
            }
        });

        cboStudent.setEnabled(false);
        cboStudent.setName("cboStudent"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(com.darkhonor.rage.cg.CourseGraderApp.class).getContext().getActionMap(CourseGraderView.class, this);
        btnGrade.setAction(actionMap.get("gradeAssignment")); // NOI18N
        btnGrade.setText(resourceMap.getString("btnGrade.text")); // NOI18N
        btnGrade.setName("btnGrade"); // NOI18N

        btnCancel.setAction(actionMap.get("cancelThreadAction")); // NOI18N
        btnCancel.setText(resourceMap.getString("btnCancel.text")); // NOI18N
        btnCancel.setName("btnCancel"); // NOI18N

        lblStatus.setText(resourceMap.getString("lblStatus.text")); // NOI18N
        lblStatus.setName("lblStatus"); // NOI18N

        btnUnzip.setAction(actionMap.get("unzipAssignments")); // NOI18N
        btnUnzip.setText(resourceMap.getString("btnUnzip.text")); // NOI18N
        btnUnzip.setName("btnUnzip"); // NOI18N

        jLabel1.setFont(resourceMap.getFont("jLabel1.font")); // NOI18N
        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N

        langButtonSelect.add(rBtnRaptor);
        rBtnRaptor.setSelected(true);
        rBtnRaptor.setText(resourceMap.getString("rBtnRaptor.text")); // NOI18N
        rBtnRaptor.setEnabled(false);
        rBtnRaptor.setName("rBtnRaptor"); // NOI18N

        langButtonSelect.add(rBtnProcessing);
        rBtnProcessing.setText(resourceMap.getString("rBtnProcessing.text")); // NOI18N
        rBtnProcessing.setEnabled(false);
        rBtnProcessing.setName("rBtnProcessing"); // NOI18N

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGap(142, 142, 142)
                        .addComponent(jLabel1))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCourse)
                            .addComponent(lblAssignment)
                            .addComponent(lblInstructor)
                            .addComponent(lblSection)
                            .addComponent(lblStudent))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cboCourse, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboAssignment, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboSection, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboInstructor, 0, 373, Short.MAX_VALUE)
                            .addComponent(cboStudent, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rBtnProcessing)
                            .addComponent(rBtnRaptor))
                        .addGap(185, 185, 185))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnUnzip)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGrade)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancel)))
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCourse)
                    .addComponent(cboCourse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAssignment)
                    .addComponent(cboAssignment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblInstructor)
                    .addComponent(cboInstructor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSection)
                    .addComponent(cboSection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStudent)
                    .addComponent(cboStudent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(rBtnRaptor))
                .addGap(8, 8, 8)
                .addComponent(rBtnProcessing)
                .addGap(18, 18, 18)
                .addComponent(lblStatus)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGrade)
                    .addComponent(btnCancel)
                    .addComponent(btnUnzip))
                .addContainerGap(145, Short.MAX_VALUE))
        );

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setAction(actionMap.get("connectAction")); // NOI18N
        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        connectMenuItem.setAction(actionMap.get("connectAction")); // NOI18N
        connectMenuItem.setText(resourceMap.getString("connectMenuItem.text")); // NOI18N
        connectMenuItem.setName("connectMenuItem"); // NOI18N
        fileMenu.add(connectMenuItem);

        disconnectMenuItem.setAction(actionMap.get("disconnectAction")); // NOI18N
        disconnectMenuItem.setText(resourceMap.getString("disconnectMenuItem.text")); // NOI18N
        disconnectMenuItem.setName("disconnectMenuItem"); // NOI18N
        fileMenu.add(disconnectMenuItem);

        jSeparator1.setName("jSeparator1"); // NOI18N
        fileMenu.add(jSeparator1);

        exitMenuItem.setAction(actionMap.get("exitApplication")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        toolsMenu.setText(resourceMap.getString("toolsMenu.text")); // NOI18N
        toolsMenu.setName("toolsMenu"); // NOI18N

        gradeAssignmentsMenuItem.setAction(actionMap.get("gradeAssignment")); // NOI18N
        gradeAssignmentsMenuItem.setText(resourceMap.getString("gradeAssignmentsMenuItem.text")); // NOI18N
        gradeAssignmentsMenuItem.setName("gradeAssignmentsMenuItem"); // NOI18N
        toolsMenu.add(gradeAssignmentsMenuItem);

        unzipAssignmentMenuItem.setAction(actionMap.get("unzipAssignments")); // NOI18N
        unzipAssignmentMenuItem.setName("unzipAssignmentMenuItem"); // NOI18N
        toolsMenu.add(unzipAssignmentMenuItem);

        optionsMenuItem.setAction(actionMap.get("showOptionDialog")); // NOI18N
        optionsMenuItem.setText(resourceMap.getString("optionsMenuItem.text")); // NOI18N
        optionsMenuItem.setName("optionsMenuItem"); // NOI18N
        toolsMenu.add(optionsMenuItem);

        menuBar.add(toolsMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 498, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 302, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents

private void cboCourseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCourseActionPerformed
    btnGrade.setEnabled(false);
    gradeAssignmentsMenuItem.setEnabled(false);
    btnUnzip.setEnabled(false);
    unzipAssignmentMenuItem.setEnabled(false);
    LOGGER.debug("In cboCourseActionPerformed.  cboCourseActive: " + cboCourseActive);
    if (!node.get("Term", "").equals("") && cboCourseActive)
    {
        LOGGER.debug("Course ComboBox selected");
        try
        {
            cboAssignActive = false;
            cboInstActive = false;
            gradedEventComboBoxModel.removeAll();
            instructorComboBoxModel.removeAll();
            sectionComboBoxModel.removeAll();
            studentComboBoxModel.removeAll();
        } catch (Exception ex)
        {
            LOGGER.error("Unknown Exception Clearing ComboBoxes: "
                    + ex.getLocalizedMessage());
        }
        LOGGER.debug("Selected Index: " + cboCourse.getSelectedIndex());
        CourseDAO courseDAO = new CourseDAO(emf.createEntityManager());
        GradedEventDAO gradedEventDAO = new GradedEventDAO(emf.createEntityManager());
        LOGGER.debug("Created CourseDAO and GradedEventDAO instances");
        Course selectedCourse = (Course) cboCourse.getSelectedItem();
        try
        {
            LOGGER.debug("Course Selected: " + selectedCourse);
            LOGGER.debug("Finding Course in DB based on selected Course id");
            Course c = courseDAO.find(selectedCourse.getId());
            LOGGER.debug("Course (" + c.getName() + ") found.  Course has "
                    + c.getInstructors().size() + " instructors and "
                    + c.getSections().size() + " sections.");

            List<GradedEvent> result =
                    gradedEventDAO.getGradedEventsForCourseAndTerm(c, node.get("Term", ""));
            /**
             * List to keep track of which events are already added to the Combo Box
             */
            List<String> events = new ArrayList<>();
            /**
             * Loop through and only show a single graded event (no duplicate versions)
             */
            int count = 0;
            for (GradedEvent ev : result)
            {
                // This is done since multiple versions of a GradedEvent should
                // not be shown.  The version identification will happen when
                // the grading commences.
                if (!events.contains(ev.getAssignment()))
                {
                    events.add(ev.getAssignment());
                    gradedEventComboBoxModel.add(ev);
                    count++;
                }
            }
            LOGGER.debug(count + " assignments found for " + c.getName());

            // Add a new Instructor so that all instructors can be graded in
            // course
            instructorComboBoxModel.add(new Instructor("All", "000 - Instructors",
                    "All.Instructors"));
            count = 0;
            for (Instructor i : c.getInstructors())
            {
                instructorComboBoxModel.add(i);
                count++;
            }
            instructorComboBoxModel.sort();
            LOGGER.debug(c.getInstructors().size() + " instructors added to "
                    + "Instructor ComboBox");
            cboAssignment.setEnabled(true);
            cboInstructor.setEnabled(false);
            cboAssignment.setSelectedIndex(-1);
            cboInstructor.setSelectedIndex(-1);
            cboStudent.setSelectedIndex(-1);  // Blank out the student name as well
            cboStudent.setEnabled(false);
            cboSection.setSelectedIndex(-1);  // Blank out the section
            cboSection.setEnabled(false);
        } catch (IllegalArgumentException ex)
        {
            LOGGER.error("Illegal Argument for Query Parameter: "
                    + ex.getLocalizedMessage());
        } catch (NullPointerException ex)
        {
            LOGGER.error("Null Pointer Exception: " + ex.getLocalizedMessage());
        } catch (Exception ex)
        {
            LOGGER.error("Unknown Exception Caught: "
                    + ex.getLocalizedMessage());
        } finally
        {
            LOGGER.debug("Setting cboAssignActive to true");
            cboAssignActive = true;
            courseDAO.closeConnection();
            gradedEventDAO.closeConnection();
        }
        LOGGER.debug("Finished with cboCourseActionPerformed");
    }
}//GEN-LAST:event_cboCourseActionPerformed

private void cboInstructorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboInstructorActionPerformed
    if (cboInstActive)
    {
        LOGGER.debug("In cboInstructorActionPerformed");
        cboSectActive = false;
        sectionComboBoxModel.removeAll();
        studentComboBoxModel.removeAll();

        LOGGER.debug("Getting selected Instructor");
        Instructor selectedInstructor = (Instructor) cboInstructor.getSelectedItem();

        LOGGER.debug("Getting selected Course");
        Course selectedCourse = (Course) cboCourse.getSelectedItem();
        LOGGER.debug("Creating Section query");
        SectionDAO sectionDAO = new SectionDAO(emf.createEntityManager());
        List<Section> result =
                sectionDAO.getSectionsForCourseAndInstructor(selectedCourse, selectedInstructor);
        if (result == null || result.isEmpty())
        {
            LOGGER.error("No Sections Found");
        } else
        {
            LOGGER.debug("Adding Sections to ComboBox");
            try
            {
                sectionComboBoxModel.add(new Section(selectedCourse, "000 - All Sections",
                        selectedInstructor));
                LOGGER.debug("Added All Sections option");
                int count = 0;
                for (Section section : result)
                {
                    sectionComboBoxModel.add(section);
                    count++;
                }
                LOGGER.debug(count + " Sections added");
            } catch (Exception ex)
            {
                LOGGER.error("Exception Caught: " + ex.getLocalizedMessage());
            } finally
            {
                sectionDAO.closeConnection();
            }
            cboStudent.setSelectedIndex(-1);  // Blank out the student name as well
            cboStudent.setEnabled(false);
            cboSection.setSelectedIndex(-1);
            cboSection.setEnabled(true);
            cboSectActive = true;
            btnGrade.setEnabled(true);
            btnUnzip.setEnabled(true);
            unzipAssignmentMenuItem.setEnabled(true);
        }
        LOGGER.debug("Finished with cboInstructorActionPerformed");
    }
}//GEN-LAST:event_cboInstructorActionPerformed

private void cboSectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboSectionActionPerformed
    if (cboSectActive)
    {
        LOGGER.debug("In cboSectionActionPerformed");
        studentComboBoxModel.removeAll();
        SectionDAO sectionDAO = new SectionDAO(emf.createEntityManager());
        LOGGER.debug("Getting selected Section");
        Section selectedSection = (Section) cboSection.getSelectedItem();
        try
        {
            /**
             * Instead of just grabbing the Set of students in the section, we will
             * query the database for the list.  This is done in order to provide
             * and Ordered list to the user.  If the structure of Section is ever
             * changed to support an ordered collection, this query can be dropped.
             */
            LOGGER.debug("Finding list of students (ordered) in section "
                    + selectedSection.getName());
            Section section = sectionDAO.find(selectedSection.getId());
            LOGGER.debug("Found " + section.getStudents().size() + " Students");
            LOGGER.debug("Adding list of Students in Section "
                    + section.getName() + " to ComboBox");
            studentComboBoxModel.add(new Student(" ", "000 - All Students", "All.Students",
                    new Integer(2010)));
            int count = 0;
            for (int i = 0; i < section.getStudents().size(); i++)
            {
                studentComboBoxModel.add(section.getStudent(i));
                count++;
            }
            studentComboBoxModel.sort();
            LOGGER.debug(count + " students added to ComboBox");
            cboStudent.setSelectedIndex(0);
            cboStudent.setEnabled(true);
            btnGrade.setEnabled(true);
        } catch (IllegalStateException ex)
        {
            LOGGER.error("The EntityManager has been closed already. "
                    + ex.getLocalizedMessage());
        } catch (IllegalArgumentException ex)
        {
            LOGGER.error("Illegal Argument to command.  " + ex.getLocalizedMessage());
        } finally
        {
            sectionDAO.closeConnection();
        }
        LOGGER.debug("Finished with cboSectionActionPerformed");
        gradeAssignmentsMenuItem.setEnabled(true);
    }
}//GEN-LAST:event_cboSectionActionPerformed

private void cboAssignmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboAssignmentActionPerformed
    LOGGER.debug("In cboAssignmentActionPerformed");
    if (cboAssignActive)
    {
        LOGGER.debug("In cboAssignmentActionPerformed--cboAssignActive = true");
        cboInstActive = false;
        cboSectActive = false;
        cboInstructor.setSelectedIndex(0);
        cboInstructor.setEnabled(true);
        cboSection.setSelectedIndex(-1);
        cboSection.setEnabled(false);
        cboStudent.setSelectedIndex(-1);
        cboStudent.setEnabled(false);
        cboInstActive = true;
        btnGrade.setEnabled(true);
        gradeAssignmentsMenuItem.setEnabled(true);
        btnUnzip.setEnabled(true);
        rBtnRaptor.setEnabled(true);
        rBtnProcessing.setEnabled(true);
        LOGGER.debug("Finished enabling other options");
    }
}//GEN-LAST:event_cboAssignmentActionPerformed

    @Action
    public void showOptionDialog()
    {
        LOGGER.debug("Showing Options dialog");
        if (optionDialog == null)
        {
            JFrame mainFrame = CourseGraderApp.getApplication().getMainFrame();
            optionDialog = new OptionsDialog(mainFrame, true);
            optionDialog.setPreferences(node);
            optionDialog.setLocationRelativeTo(mainFrame);
        }
        CourseGraderApp.getApplication().show(optionDialog);
    }

    @Action
    public void exitApplication()
    {
        LOGGER.info("Exiting the application");
        try    // Close the EntityManagerFactory before we quit
        {
            if (emf.isOpen())
            {
                LOGGER.debug("EntityManagerFactory is open.  Closing.");
                emf.close();
            }
        } catch (Exception ex)
        {
            // Do nothing since we are exiting anyway...there was no connection
        }
        CourseGraderApp.getApplication().exit();
    }

    @Action
    public void gradeAssignment()
    {
        btnGrade.setEnabled(false);
        gradeAssignmentsMenuItem.setEnabled(false);
        btnCancel.setEnabled(true);
        multVersions = false;

        // Define the type of Questions to grade.  Default to RAPTOR
        int type = RAGEConst.RAPTOR_QUESTION;
        if (rBtnRaptor.isSelected())
        {
            type = RAGEConst.RAPTOR_QUESTION;
            LOGGER.debug("Grading RAPTOR Questions");
        } else if (rBtnProcessing.isSelected())
        {
            LOGGER.debug("Grading Processing Questions");
            type = RAGEConst.PROCESSING_QUESTION;
        }

        // Minimum requirement: Course and Assignment MUST be selected to continue
        if ((cboCourse.getSelectedIndex() >= 0) && (cboAssignment.getSelectedIndex() >= 0))
        {
            GradedEventDAO gradedEventDAO = new GradedEventDAO(emf.createEntityManager());
            CourseDAO courseDAO = new CourseDAO(emf.createEntityManager());
            String[] name;

            Course selectedCourse = (Course) cboCourse.getSelectedItem();
            Course course = courseDAO.find(selectedCourse.getId());
            GradedEvent selectedAssignment = (GradedEvent) cboAssignment.getSelectedItem();
            LOGGER.debug("Querying database for the selected GradedEvent");

            int numVersions = gradedEventDAO.getNumVersionsOfGradedEvent(selectedAssignment);

            GradedEvent gevent = null;
            List<GradedEvent> gevents = null;

            // If more than 1 result comes up for the GradedEvent query (not 
            // including the version).  We can't set gevent until we know which
            // section is to be graded
            if (numVersions == 1)
            {
                gevent = gradedEventDAO.find(selectedAssignment.getId());
            } else
            {   // Set it to the first GradedEvent so we can get the name
                gevents = gradedEventDAO.getAllVersionsOfGradedEvent(selectedAssignment);
                gevent = gevents.get(0);
                multVersions = true;
            }

            String path = this.buildBasePath();

            Instructor selectedInstructor = null;
            Section selectedSection = null;
            Student selectedStudent = null;
            if (cboInstructor.getSelectedIndex() != -1)
            {
                selectedInstructor = (Instructor) cboInstructor.getSelectedItem();
            }
            if (cboSection.getSelectedIndex() != -1)
            {
                selectedSection = (Section) cboSection.getSelectedItem();
            }
            if (cboStudent.getSelectedIndex() != -1)
            {
                selectedStudent = (Student) cboStudent.getSelectedItem();
            }

//            // Derive the grading path from the selected comboboxes
//            String path = node.get("RootDir", "");
//            path = path.concat(File.separator);
//            path = path.concat(selectedCourse.getName());
//            path = path.concat(File.separator);
//            path = path.concat(selectedAssignment.getAssignment());

            /**
             * Get the selected Instructor and Section objects.  If either is
             * not selected, getSelectedIndex will return -1 and the object will
             * stay null.
             */
//            Instructor selectedInstructor = null;
//            Section selectedSection = null;
//            if (cboInstructor.getSelectedIndex() != -1)
//            {
//                selectedInstructor = (Instructor) cboInstructor.getSelectedItem();
//            }
//            if (cboSection.getSelectedIndex() != -1)
//            {
//                selectedSection = (Section) cboSection.getSelectedItem();
//            }
//
//            // Add the Instructor if one is selected
//            if (cboInstructor.getSelectedIndex() > 0)
//            {
//                path = path.concat(File.separator);
//
//                //InstructorDAO instructorDAO = new InstructorDAO(emf.createEntityManager());
//                //instructor = instructorDAO.find(selectedInstructor.getId());
//                path = path.concat(selectedInstructor.getWebID());
//
//                // Add the section if one is selected
//                if (cboSection.getSelectedIndex() > 0)
//                {
//                    //SectionDAO sectionDAO = new SectionDAO(emf.createEntityManager());
//                    //section = sectionDAO.find(selectedSection.getId());
//
//                    path = path.concat(File.separator);
//                    path = path.concat(selectedSection.getName());
//
//                    // Add the Student if one is selected
//                    if (cboStudent.getSelectedIndex() > 0)
//                    {
//                        path = path.concat(File.separator);
//                        name = cboStudent.getSelectedItem().toString().split(",");
//                        query = em.createQuery("SELECT p FROM Person p WHERE "
//                                + "p.firstName = :fname AND p.lastName = :lname");
//                        query.setParameter("fname", name[1].trim());
//                        query.setParameter("lname", name[0].trim());
//                        student = (Student) query.getSingleResult();
//                        path = path.concat(student.getWebID());
//                    }
//                }
//            }
            // Build the thread environment regardless of whether doing student or 
            ProcessBuilder launcher = new ProcessBuilder();
            Map<String, String> environment = launcher.environment();
            launcher.redirectErrorStream(true);
            File outputFile = null;
            SectionReport sectionReport = null;
            Date dateStamp = new Date();
            String dateStampString = String.format("%tY%tm%td%tH%tM", dateStamp);

            /**
             * Grade All Instructors.  Section and Student are not selected
             */
            if ((cboAssignment.getSelectedIndex() >= 0)
                    && (cboInstructor.getSelectedIndex() == 0)
                    && (cboSection.getSelectedIndex() < 0)
                    && (cboStudent.getSelectedIndex() < 0))
            {
                LOGGER.debug("Grading All Instructors.  Section and Student are "
                        + "not selected");
                for (Instructor insIter : course.getInstructors())
                {
                    String tempPath;
                    tempPath = path.concat(File.separator);
                    tempPath = tempPath.concat(insIter.getWebID());
                    for (Section sectionIter : insIter.getSections())
                    {
                        sectionReport = new SectionReport(course.getName(),
                                insIter, sectionIter.getName(),
                                selectedAssignment.getAssignment());
                        try
                        {
                            outputFile = new File(tempPath + File.separator
                                    + sectionIter.getName() + File.separator
                                    + sectionIter.getName() + dateStampString + ".html");
                            LOGGER.debug("Output File: " + outputFile.getCanonicalPath());
                            outputFile.createNewFile();
                        } catch (IOException ex)
                        {
                            LOGGER.error("ERROR: IOException caught.");
                            LOGGER.error(ex.getLocalizedMessage());
                        }
                        /**
                         * If a section is selected and there are multiple versions,
                         * pull the right one for the gevent variable...can we 
                         * search the list and return the right one?
                         */
                        if (multVersions)
                        {
                            /**
                             * If there are multiple versions of the GradedEvent,
                             * pull only the one for this section
                             */
                            gevent = RageLib.getGradedEventFromSectionName(gevents,
                                    sectionIter.getName());
                        }
                        // No else needed since if there are no multiple versions,
                        // gevent is already set above

                        for (Student studentIter : sectionIter.getStudents())
                        {
                            StudentReport studentReport = new StudentReport(studentIter,
                                    sectionIter);
                            String newPath;
                            newPath = tempPath.concat(File.separator);
                            newPath = newPath.concat(sectionIter.getName());
                            newPath = newPath.concat(File.separator);
                            newPath = newPath.concat(studentIter.getWebID());
                            LOGGER.debug("Path to grade: " + newPath);
                            statusMessageLabel.setText("Grading: "
                                    + studentIter.getLastName()
                                    + ", " + studentIter.getFirstName());
                            LOGGER.info("Grading: " + studentIter.getLastName()
                                    + ", " + studentIter.getFirstName());
                            try
                            {
                                File directory = new File(newPath);
                                LOGGER.debug("Grading path: " + directory.getCanonicalPath());
                                launcher.directory(directory);
                                Runnable r = new GraderThread(node, launcher, directory,
                                        gevent, studentReport, type);
                                grader_thread = new Thread(r);
                                grader_thread.start();
                                while (grader_thread.isAlive())
                                {
                                }
                                // After grader is complete, add StudentReport to
                                // SectionReport
                                sectionReport.addStudentReport(studentReport);
                            } catch (IOException ex)
                            {
                                LOGGER.error("IO Exception: " + ex.getLocalizedMessage());
                            }
                        }
                        sectionReport.sortStudentReports();
                        RageLib.printSectionReport(outputFile, sectionReport);
                    }
                }
            } /**
             * Grade a single instructor, but All Sections is selected
             */
            else if ((cboInstructor.getSelectedIndex() > 0)
                    && (cboSection.getSelectedIndex() == 0)
                    && (cboStudent.getSelectedIndex() < 0))
            {
                LOGGER.debug("Grading all sections for Instructor "
                        + selectedInstructor.getLastName() + ", "
                        + selectedInstructor.getFirstName());

                // Pull the Instructor from the Data Source to be able to grab Sections
                InstructorDAO instructorDAO = new InstructorDAO(emf.createEntityManager());
                Instructor instructor = instructorDAO.find(selectedInstructor.getId());
                for (Section sectionIter : instructor.getSections())
                {
                    sectionReport = new SectionReport(course.getName(),
                            instructor, sectionIter.getName(),
                            selectedAssignment.getAssignment());
                    try
                    {
                        outputFile = new File(path + File.separator
                                + sectionIter.getName() + File.separator
                                + sectionIter.getName() + dateStampString + ".html");
                        LOGGER.debug("Output File: " + outputFile.getCanonicalPath());
                        outputFile.createNewFile();
                    } catch (IOException ex)
                    {
                        LOGGER.error("IOException caught: "
                                + ex.getLocalizedMessage());
                    }
                    /**
                     * If a section is selected and there are multiple versions,
                     * pull the right one for the gevent variable...can we
                     * search the list and return the right one?
                     */
                    if (multVersions)
                    {
                        /**
                         * If there are multiple versions of the GradedEvent,
                         * pull only the one for this section
                         */
                        gevent = RageLib.getGradedEventFromSectionName(gevents,
                                sectionIter.getName());
                    }
                    // No else needed since if there are no multiple versions,
                    // gevent is already set above

                    for (Student studentIter : sectionIter.getStudents())
                    {
//                        StudentReport studentReport = new StudentReport(studentIter);
                        StudentReport studentReport = new StudentReport(studentIter,
                                sectionIter);
                        String newPath;
                        newPath = path.concat(File.separator);
                        newPath = newPath.concat(sectionIter.getName());
                        newPath = newPath.concat(File.separator);
                        newPath = newPath.concat(studentIter.getWebID());
                        LOGGER.debug("Path to grade: " + newPath);
                        statusMessageLabel.setText("Grading: " + studentIter.getLastName()
                                + ", " + studentIter.getFirstName());
                        LOGGER.info("Grading: " + studentIter.getLastName()
                                + ", " + studentIter.getFirstName());
                        File directory = new File(newPath);
                        launcher.directory(directory);
                        Runnable r = new GraderThread(node, launcher, directory,
                                gevent, studentReport, type);
                        grader_thread = new Thread(r);
                        grader_thread.start();
                        while (grader_thread.isAlive())
                        {
                        }
                        sectionReport.addStudentReport(studentReport);
                    }
                    sectionReport.sortStudentReports();
                    RageLib.printSectionReport(outputFile, sectionReport);
                }
                // Close the connection to the Data Source
                instructorDAO.closeConnection();
            } /**
             * Grade all students in a single section
             */
            else if ((cboStudent.getSelectedIndex() == 0)
                    && (cboSection.getSelectedIndex() > 0))
            {
                LOGGER.debug("Grading all students in section "
                        + selectedSection.getName());

                // Pull the Section from the Data Source to get the full list of
                // Students
                SectionDAO sectionDAO = new SectionDAO(emf.createEntityManager());
                Section section = sectionDAO.find(selectedSection.getId());
                sectionReport = new SectionReport(course.getName(), selectedInstructor,
                        section.getName(), selectedAssignment.getAssignment());
                try
                {
                    outputFile = new File(path + File.separator
                            + selectedSection.getName() + dateStampString + ".html");
                    LOGGER.debug("Output File: " + outputFile.getCanonicalPath());
                    outputFile.createNewFile();
                    /**
                     * If a section is selected and there are multiple versions,
                     * pull the right one for the gevent variable...can we
                     * search the list and return the right one?
                     */
                    if (multVersions)
                    {
                        /**
                         * If there are multiple versions of the GradedEvent,
                         * pull only the one for this section
                         */
                        gevent = RageLib.getGradedEventFromSectionName(gevents,
                                selectedSection.getName());
                    }
                    // No else needed since if there are no multiple versions,
                    // gevent is already set above

                } catch (FileNotFoundException ex)
                {
                    // Do nothing...the file should be created if it's not there
                } catch (IOException ex)
                {
                    LOGGER.error("ERROR: IOException caught.");
                    LOGGER.error(ex.getLocalizedMessage());
                }
                for (Student studentIter : section.getStudents())
                {
                    StudentReport studentReport = new StudentReport(studentIter, section);
                    String newPath;
                    newPath = path.concat(File.separator);
                    newPath = newPath.concat(studentIter.getWebID());
                    LOGGER.debug("Path to grade: " + newPath);
                    statusMessageLabel.setText("Grading: " + studentIter.getLastName()
                            + ", " + studentIter.getFirstName());
                    LOGGER.info("Grading: " + studentIter.getLastName() + ", "
                            + studentIter.getFirstName());
                    File directory = new File(newPath);
                    launcher.directory(directory);
                    Runnable r = new GraderThread(node, launcher, directory, gevent,
                            studentReport, type);
                    grader_thread = new Thread(r);
                    grader_thread.start();

                    while (grader_thread.isAlive())
                    {
                    }
                    sectionReport.addStudentReport(studentReport);
                }
                sectionReport.sortStudentReports();
                RageLib.printSectionReport(outputFile, sectionReport);

                // Close the connection to the Data Source
                sectionDAO.closeConnection();
            } /**
             * Just grade a single student
             */
            else
            {
                LOGGER.debug("Grading a single Student:" + selectedStudent.getWebID());
                LOGGER.debug("Path to grade: " + path);
                File directory = null;
                try
                {
                    directory = new File(path);
                    LOGGER.debug("Creating Output File");
                    outputFile = new File(directory.getParent()
                            + File.separator + selectedStudent.getWebID()
                            + dateStampString + ".html");
                    LOGGER.debug("Output File: " + outputFile.getCanonicalPath());
                    outputFile.createNewFile();
                    /**
                     * If a section is selected and there are multiple versions,
                     * pull the right one for the gevent variable...can we
                     * search the list and return the right one?
                     */
                    if (multVersions)
                    {
                        /**
                         * If there are multiple versions of the GradedEvent,
                         * pull only the one for this section
                         */
                        gevent = RageLib.getGradedEventFromSectionName(gevents,
                                selectedSection.getName());
                    }
                    // No else needed since if there are no multiple versions,
                    // gevent is already set above
                } catch (FileNotFoundException ex)
                {
                    // Do nothing...the file should be created if it's not there
                } catch (IOException ex)
                {
                    LOGGER.error("IOException caught.");
                    LOGGER.error(ex.getLocalizedMessage());
                } catch (NullPointerException ex)
                {
                    LOGGER.error("Null Pointer Exception: " + ex.getLocalizedMessage());
                } catch (Exception ex)
                {
                    LOGGER.error("Exception caught: " + ex.getLocalizedMessage());
                }
                LOGGER.info("Grading: " + selectedStudent.getLastName() + ", "
                        + selectedStudent.getFirstName());
                try
                {
                    LOGGER.debug("Grading Directory: " + directory.getCanonicalPath());
                    launcher.directory(directory);
                    StudentReport studentReport = new StudentReport(selectedStudent,
                            selectedSection);
                    Runnable r = new GraderThread(node, launcher, directory, gevent,
                            studentReport, type);
                    grader_thread = new Thread(r);
                    grader_thread.start();

                    statusMessageLabel.setText("");
                    while (grader_thread.isAlive())
                    {
                    }

                    sectionReport = new SectionReport(course.getName(), selectedInstructor,
                            selectedSection.getName(), gevent.getAssignment());
                    sectionReport.addStudentReport(studentReport);
                } catch (IOException ex)
                {
                    LOGGER.error("IO Exception: " + ex.getLocalizedMessage());
                }
                RageLib.printSectionReport(outputFile, sectionReport);
            }

            statusMessageLabel.setText("Grading complete");
            JOptionPane.showMessageDialog(null, "Grading complete.",
                    "Information", JOptionPane.INFORMATION_MESSAGE);
            LOGGER.info("Grading Complete");

            // Release the EntityManager objects.
            gradedEventDAO.closeConnection();
            courseDAO.closeConnection();
        }
        btnGrade.setEnabled(true);
        gradeAssignmentsMenuItem.setEnabled(true);
        btnCancel.setEnabled(false);

    }

    @Action
    public void cancelThreadAction()
    {
        grader_thread.stop();
    }

    @Action
    public void unzipAssignments()
    {
        if ((cboCourse.getSelectedIndex() >= 0) && (cboAssignment.getSelectedIndex() >= 0))
        {
            LOGGER.debug("Unzipping assignments");
            EntityManager em = emf.createEntityManager();
            // TODO: Remove Query once StudentDAO is finished.  All others clear
            Query query;
            String[] name;
            Student student;
            Course course;
            Instructor instructor = null;
            Section section = null;
            int counter = 0;
            File rootDir;
            File[] files;

            CourseDAO courseDAO = new CourseDAO(emf.createEntityManager());
            
            Course selectedCourse = (Course) cboCourse.getSelectedItem();
            course = courseDAO.find(selectedCourse.getId());

            // Derive the grading path from the selected comboboxes
            //String path = buildBasePath(instructor, section, student);
            String path = node.get("RootDir", "");
            path = path.concat(File.separator);
            path = path.concat(course.getName());
            path = path.concat(File.separator);
            path = path.concat((String) cboAssignment.getSelectedItem());

            Instructor selectedInstructor = null;
            Section selectedSection = null;
            if (cboInstructor.getSelectedIndex() != -1)
            {
                selectedInstructor = (Instructor) cboInstructor.getSelectedItem();
            }
            if (cboSection.getSelectedIndex() != -1)
            {
                selectedSection = (Section) cboSection.getSelectedItem();
            }
            // Add the Instructor if one is selected
            if (cboInstructor.getSelectedIndex() > 0)
            {
                path = path.concat(File.separator);
                InstructorDAO instructorDAO = new InstructorDAO(emf.createEntityManager());
                instructor = instructorDAO.find(selectedInstructor.getId());
                path = path.concat(instructor.getWebID());

                // Add the section if one is selected
                if (cboSection.getSelectedIndex() > 0)
                {
                    SectionDAO sectionDAO = new SectionDAO(emf.createEntityManager());
                    section = sectionDAO.find(selectedSection.getId());
                    path = path.concat(File.separator);
                    path = path.concat(section.getName());

                    // Add the Student if one is selected
                    if (cboStudent.getSelectedIndex() > 0)
                    {
                        path = path.concat(File.separator);
                        name = cboStudent.getSelectedItem().toString().split(",");
                        query = em.createQuery("SELECT p FROM Person p WHERE "
                                + "p.firstName = :fname AND p.lastName = :lname");
                        query.setParameter("fname", name[1].trim());
                        query.setParameter("lname", name[0].trim());
                        student = (Student) query.getSingleResult();
                        path = path.concat(student.getWebID());
                    }
                    sectionDAO.closeConnection();
                }
                instructorDAO.closeConnection();
            }
            // Figure out what is selected based on the path info above
            if ((cboInstructor.getSelectedIndex() == 0) && (cboSection.getSelectedIndex() < 0))
            {  // Assignment and All Instructors is selected, no section or student
                // selected
                LOGGER.debug("Unzipping all assignments in course: "
                        + course.getName());
                for (Instructor insIter : course.getInstructors())
                {
                    String newPath;
                    newPath = path.concat(File.separator);
                    newPath = newPath.concat(insIter.getWebID());
                    for (Section sectIter : insIter.getSections())
                    {
                        String nPath;
                        nPath = newPath.concat(File.separator);
                        nPath = nPath.concat(sectIter.getName());
                        for (Student stuIter : sectIter.getStudents())
                        {
                            String tempPath;
                            tempPath = nPath.concat(File.separator);
                            tempPath = tempPath.concat(stuIter.getWebID());
                            try
                            {
                                rootDir = new File(tempPath).getCanonicalFile();
                                files = rootDir.listFiles(new ZipFileFilter());
                                for (File file : files)
                                {
                                    RageLib.unZip(file.getParent(), file.getName(), true);
                                    counter++;
                                }
                            } catch (IOException ex)
                            {
                                LOGGER.error("Invalid Path: " + newPath);
                            }
                        }
                    }
                    JOptionPane.showMessageDialog(null, "Unzipped " + counter
                            + " file(s).",
                            "Information", JOptionPane.INFORMATION_MESSAGE);
                    LOGGER.info("Unzipped " + counter + " files.");
                }
            } else if ((cboInstructor.getSelectedIndex() > 0)
                    && (cboSection.getSelectedIndex() == 0)
                    && (cboStudent.getSelectedIndex() < 0))
            {  // Instructor and All Sections is selected, no student selected
                LOGGER.debug("Unzipping all assignments for instructor: "
                        + instructor.getLastName() + ", " + instructor.getFirstName());
                for (Section sectIter : instructor.getSections())
                {
                    LOGGER.debug("Unzipping section: " + sectIter.getName());
                    String newPath;
                    newPath = path.concat(File.separator);
                    newPath = newPath.concat(sectIter.getName());
                    for (Student stuIter : sectIter.getStudents())
                    {
                        String tempPath;
                        tempPath = newPath.concat(File.separator);
                        tempPath = tempPath.concat(stuIter.getWebID());
                        try
                        {
                            rootDir = new File(tempPath).getCanonicalFile();
                            files = rootDir.listFiles(new ZipFileFilter());
                            for (File file : files)
                            {
                                RageLib.unZip(file.getParent(), file.getName(), true);
                                counter++;
                            }
                        } catch (IOException ex)
                        {
                            LOGGER.error("Invalid Path: " + newPath);
                        }
                    }
                }
                JOptionPane.showMessageDialog(null, "Unzipped " + counter + " file(s).",
                        "Information", JOptionPane.INFORMATION_MESSAGE);
                LOGGER.info("Unzipped " + counter + " files.");
            } else if ((cboSection.getSelectedIndex() > 0)
                    && (cboStudent.getSelectedIndex() == 0))
            {  // Section is selected and All Students is selected
                // sect is my Section
                LOGGER.debug("Unzipping all students in section " + section.getName());
                for (Student stuIter : section.getStudents())
                {
                    String newPath;
                    newPath = path.concat(File.separator);
                    newPath = newPath.concat(stuIter.getWebID());
                    try
                    {
                        rootDir = new File(newPath).getCanonicalFile();
                        files = rootDir.listFiles(new ZipFileFilter());
                        for (File file : files)
                        {
                            RageLib.unZip(file.getParent(), file.getName(), true);
                            counter++;
                        }
                    } catch (IOException ex)
                    {
                        LOGGER.error("Invalid Path: " + newPath);
                    }
                }
                JOptionPane.showMessageDialog(null, "Unzipped " + counter + " file(s).",
                        "Information", JOptionPane.INFORMATION_MESSAGE);
                LOGGER.info("Unzipped " + counter + " files.");
            } else if (cboStudent.getSelectedIndex() > 0)
            {  // An individual student is selected
                LOGGER.debug("Unzipping a single student");
                try
                {
                    rootDir = new File(path).getCanonicalFile();
                    files = rootDir.listFiles(new ZipFileFilter());
                    for (File file : files)
                    {
                        RageLib.unZip(file.getParent(), file.getName(), true);
                        counter++;
                    }
                } catch (IOException ex)
                {
                    LOGGER.error("Invalid Path: " + path);
                }
                JOptionPane.showMessageDialog(null, "Unzipped " + counter + " file(s).",
                        "Information", JOptionPane.INFORMATION_MESSAGE);
                LOGGER.info("Unzipped " + counter + " files.");

            } else
            {
                LOGGER.error("Unknown selection");
            }
            LOGGER.debug("Finished unzipAssignments");
        }
    }

    @Action
    public void connectAction()
    {
        LOGGER.debug("(connectAction) Entering connectAction");
        if (JOptionPane.showConfirmDialog(CourseGraderApp.getApplication().getMainFrame(),
                "Connect to the database?", "Confirmation",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
        {
            LOGGER.debug("(connectAction) Setting database properties map based "
                    + "upon current Preferences");
            // Set the database properties from those in the Preferences
            dbProperties.put("javax.persistence.jdbc.driver",
                    node.get("DBDriver", RAGEConst.DEFAULT_DBDRIVER));
            dbProperties.put("javax.persistence.jdbc.url",
                    node.get("DBURI", RAGEConst.DEFAULT_DBURI));
            dbProperties.put("javax.persistence.jdbc.user",
                    node.get("DBUsername", RAGEConst.DEFAULT_DBUSER));
            dbProperties.put("javax.persistence.jdbc.password",
                    node.get("DBPassword", RAGEConst.DEFAULT_DBPASS));

            LOGGER.debug("(connectAction) Connecting to the database");
            // Log Database connection info (sans password)
            LOGGER.info("(connectAction) URL: "
                    + dbProperties.get("javax.persistence.jdbc.url"));
            LOGGER.info("(connectAction) User: "
                    + dbProperties.get("javax.persistence.jdbc.user"));
            LOGGER.debug("(connectAction) Driver: "
                    + dbProperties.get("javax.persistence.jdbc.driver"));

            LOGGER.debug("(connectAction) Connecting to the database");
            emf = Persistence.createEntityManagerFactory("rage-cg", dbProperties);
            try
            {
                EntityManager em = emf.createEntityManager();
                em.close();
            } catch (Exception ex)
            {
                LOGGER.error("(connectAction) Caught Exception: " +
                        ex.getLocalizedMessage());
                LOGGER.error("(connectAction) Not connected to the database");
                JOptionPane.showMessageDialog(CourseGraderApp.getApplication()
                        .getMainFrame(),
                        "ERROR: Unable to connect to the database.  Check your "
                        + "database connection details.", "ERROR",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            LOGGER.debug("(connectAction) Connected to the database");

            LOGGER.debug("(connectAction) Checking the version of the database "
                    + "against the version this application requires.");
            checkVersion();
//            try
//            {
//                LOGGER.debug("(connectAction) Creating VersionDAO object");
//                VersionDAO versionDAO = new VersionDAO(emf.createEntityManager());
//                LOGGER.debug("(connectAction) VersionDAO object created");
//                if (versionDAO.checkDbVersion(version))
//                {
//                    LOGGER.debug("(connectAction) Database version matches "
//                            + "application version.");
//                } else
//                {
//                    LOGGER.error("(connectAction) Database version does not match. "
//                            + "Exiting program.");
//                    JOptionPane.showMessageDialog(CourseGraderApp.getApplication()
//                            .getMainFrame(),
//                            "ERROR: This version of CourseGrader is "
//                            + "not compatible with the RAGE database. Exiting program.",
//                            "ERROR", JOptionPane.ERROR_MESSAGE);
//                    this.exitApplication();
//                }
//                LOGGER.debug("(connectAction) Closing VersionDAO connection");
//                versionDAO.closeConnection();
//            } catch (IllegalStateException ex)
//            {
//                LOGGER.warn("(connectAction) Problem connecting to the database "
//                        + "with the VersionDAO object");
//            } 

            LOGGER.debug("(connectAction) Validating connection authorization");
            String userName = System.getProperty("user.name");
            LOGGER.debug("(connectAction) Finding Instructor from userid: "
                    + userName);
            InstructorDAO instructorDAO = new InstructorDAO(emf.createEntityManager());
            try
            {
                Instructor instructor =
                        instructorDAO.findInstructorByDomainAccount(userName);
                LOGGER.debug("(connectAction) Instructor found.");
                if (node.getBoolean("UsePassword", true))
                {
                    // TODO: Add Password option
                    // Prompt the user for a password and check the response with what
                    // is now stored in appUser.password.  If it's ok, move on.  Otherwise
                    // exit the program with an error message stating the user is not
                    // authorized.  Perhaps give them a couple of tries.
                }
                statusMessageLabel.setText("Welcome " + instructor.getFirstName()
                        + " " + instructor.getLastName());
                LOGGER.debug("(connectAction) Closing InstructorDAO connection");
                instructorDAO.closeConnection();
            } catch (NoResultException ex)
            {
                LOGGER.error("(connectAction) Instructor not found in the data "
                        + "source");
                JOptionPane.showMessageDialog(CourseGraderApp.getApplication()
                        .getMainFrame(), "Instructor " + userName + " not found "
                        + "in the database.  Exiting CourseGrader.", "Error",
                        JOptionPane.ERROR_MESSAGE);
                instructorDAO.closeConnection();
                this.exitApplication();
            } catch (IllegalStateException ex)
            {
                LOGGER.warn("(connectAction) Problem connecting to the database "
                        + "with the InstructorDAO object");
            }

            LOGGER.debug("(connectAction) Getting list of courses");
            try
            {
                CourseDAO courseDAO = new CourseDAO(emf.createEntityManager());
                List<Course> courseList = courseDAO.getAllCoursesByName();
                if (courseList.size() > 0)
                {
                    LOGGER.debug("(connectAction) Found " + courseList.size()
                            + " courses");
                    for (int i = 0; i < courseList.size(); i++)
                    {
                        courseComboBoxModel.add(courseList.get(i));
                        LOGGER.debug("(connectAction) - Added "
                                + courseList.get(i).getName());
                    }
                } else
                {
                    JOptionPane.showMessageDialog(CourseGraderApp.getApplication()
                            .getMainFrame(), "Error: No Courses Found.", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    LOGGER.warn("(connectAction) No Courses found");
                }
                LOGGER.debug("(connectAction) Closing CourseDAO connection");
                courseDAO.closeConnection();
            } catch (IllegalStateException ex)
            {
                LOGGER.error("(connectAction) Error connecting to the database "
                        + "when trying to retrieve course listing");
            } catch (Exception ex)
            {
                LOGGER.error("(connectAction) Caught Exception: "
                        + ex.getLocalizedMessage());
            }
//            query = em.createQuery("SELECT c FROM Course c ORDER BY c.name");
//            try
//            {
//                result = query.getResultList();
//                LOGGER.debug("Found " + result.size() + " Courses.");
//                for (int i = 0; i < result.size(); i++)
//                {
//                    Course crs = (Course) result.get(i);
//                    courseComboBoxModel.add(crs);
//                    LOGGER.debug("- Added " + crs.getName());
//                }
//                cboCourse.setEnabled(true);
//            } catch (NoResultException ex)
//            {
//                JOptionPane.showMessageDialog(CourseGraderApp.getApplication()
//                .getMainFrame(), "Error: No Courses Found.", "Error",
//                        JOptionPane.ERROR_MESSAGE);
//                LOGGER.warn("No Courses found");
//            } catch (Exception ex)
//            {
//                LOGGER.error("Caught Exception: " + ex.getLocalizedMessage());
//            }

//            em = emf.createEntityManager();
//            Query query = em.createQuery("select ins FROM Instructor ins WHERE "
//                    + "ins.domainAccount = :dname");
//            LOGGER.debug("Query created.  Setting parameter.");
//            query.setParameter("dname", System.getProperty("user.name"));
//            List result = null;
//            try
//            {
//                LOGGER.debug("Executing Instructor query");
//                result = query.getResultList();
//                LOGGER.debug("Getting first result");
//
//                Instructor appUser = (Instructor) result.get(0);
//                LOGGER.debug("Instructor found: " + appUser.getFirstName() + " "
//                        + appUser.getLastName());
//                if (node.getBoolean("UsePassword", true))
//                {
//                    // TODO: Add Password option
//                    // Prompt the user for a password and check the response with what
//                    // is now stored in appUser.password.  If it's ok, move on.  Otherwise
//                    // exit the program with an error message stating the user is not
//                    // authorized.  Perhaps give them a couple of tries.
//                }
//                statusMessageLabel.setText("Welcome " + appUser.getFirstName() + " "
//                        + appUser.getLastName());
//            } catch (NoResultException ex)
//            {
//                statusMessageLabel.setText("User not found in the database");
//                /**
//                 * Provide a modal Dialog for an Instructor to add themselves to
//                 * the database
//                 */
//                JOptionPane.showMessageDialog(CourseGraderApp.getApplication()
//                        .getMainFrame(),
//                        "Instructor UserID not found in the "
//                        + "database.  Exiting CourseGrader.", "Error",
//                        JOptionPane.ERROR_MESSAGE);
//                LOGGER.error("Instructor UserID not found in the database.  Exiting");
//                this.exitApplication();
//            }

            disconnectMenuItem.setEnabled(true);
            connectMenuItem.setEnabled(false);
            cboCourseActive = true;
            cboCourse.setEnabled(true);
        } else
        {
            LOGGER.debug("(connectAction) Declined connection to database");
        }
        LOGGER.debug("(connectAction) connectAction complete");
    }

    @Action
    public void disconnectAction()
    {
        LOGGER.debug("Clearing all menu items");
        cboAssignActive = false;
        cboInstActive = false;
        cboSectActive = false;
        cboCourseActive = false;
        cboAssignment.setEnabled(false);
        cboAssignment.removeAllItems();
        cboAssignment.setSelectedIndex(-1);
        //cboInstructor.removeAllItems();
        cboInstructor.setEnabled(false);
        instructorComboBoxModel.removeAll();
        cboInstructor.setSelectedIndex(-1);
        cboSection.setEnabled(false);
        sectionComboBoxModel.removeAll();
        cboSection.setSelectedIndex(-1);
        //cboSection.removeAllItems();
        cboStudent.setEnabled(false);
        cboStudent.removeAllItems();
        cboStudent.setSelectedIndex(-1);
        //cboCourse.removeAllItems();
        cboCourse.setEnabled(false);
        courseComboBoxModel.removeAll();
        cboCourse.setSelectedIndex(-1);
        // Disable Buttons and Radio Buttons
        btnGrade.setEnabled(false);
        btnCancel.setEnabled(false);
        btnUnzip.setEnabled(false);
        rBtnProcessing.setEnabled(false);
        rBtnRaptor.setEnabled(false);
        LOGGER.debug("Disconnecting from the database");
        if (emf.isOpen())
        {
            emf.close();
            disconnectMenuItem.setEnabled(false);
            connectMenuItem.setEnabled(true);
            LOGGER.info("Disconnected from the database");
        }
    }

    /**
     * Builds the base path for grading.  The base path is defined as the lowest
     * level where an individual Course, Instructor, Section, or Student is
     * chosen by the user.  If the Course or Assignment are not selected,
     * <code>null</code> is returned.
     *
     * @return      The base path to grade or <code>null</code>
     */
    private String buildBasePath()
    {
        if (cboCourse.getSelectedIndex() != -1
                && cboAssignment.getSelectedIndex() != 1)
        {
            Course selectedCourse = (Course) cboCourse.getSelectedItem();
            GradedEvent selectedAssignment = (GradedEvent) cboAssignment.getSelectedItem();

            // Derive the grading path from the selected comboboxes
            String path = node.get("RootDir", "");
            path = path.concat(File.separator);
            path = path.concat(selectedCourse.getName());
            path = path.concat(File.separator);
            path = path.concat(selectedAssignment.getAssignment());

            /**
             * Get the selected Instructor and Section objects.  If either is
             * not selected, getSelectedIndex will return -1 and the object will
             * stay null.
             */
            Instructor selectedInstructor = null;
            Section selectedSection = null;
            if (cboInstructor.getSelectedIndex() != -1)
            {
                selectedInstructor = (Instructor) cboInstructor.getSelectedItem();
            }
            if (cboSection.getSelectedIndex() != -1)
            {
                selectedSection = (Section) cboSection.getSelectedItem();
            }

            // Add the Instructor if one is selected
            if (selectedInstructor != null)
            {
                path = path.concat(File.separator);
                path = path.concat(selectedInstructor.getWebID());
                LOGGER.debug("Instructor Selected: " + selectedInstructor.getLastName()
                        + ", " + selectedInstructor.getFirstName());
                // Add the section if one is selected
                if (selectedSection != null)
                {
                    path = path.concat(File.separator);
                    path = path.concat(selectedSection.getName());
                    LOGGER.debug("Section Selected: " + selectedSection.getName());

                    // Add the Student if one is selected
                    if (cboStudent.getSelectedIndex() > 0)
                    {
                        path = path.concat(File.separator);
                        Student selectedStudent = (Student) cboStudent.getSelectedItem();
                        LOGGER.debug("Student Selected: " + selectedStudent.getLastName()
                                + ", " + selectedStudent.getFirstName());
                        path = path.concat(selectedStudent.getWebID());
                    }
                }
            }
            LOGGER.debug("Path: " + path);
            return path;
        } else
        {
            LOGGER.error("No Course and/or Assignment selected.");
            return null;
        }
    }
    
    private void checkVersion()
    {
        try
        {
            LOGGER.debug("(checkVersion) Creating VersionDAO object");
            VersionDAO versionDAO = new VersionDAO(emf.createEntityManager());
            LOGGER.debug("(checkVersion) VersionDAO object created");
            if (versionDAO.checkDbVersion(version))
            {
                LOGGER.debug("(checkVersion) Database version matches "
                        + "application version.");
            } else
            {
                LOGGER.error("(checkVersion) Database version does not match. "
                        + "Exiting program.");
                JOptionPane.showMessageDialog(CourseGraderApp.getApplication()
                        .getMainFrame(),
                        "ERROR: This version of CourseGrader is "
                        + "not compatible with the RAGE database. Exiting program.",
                        "ERROR", JOptionPane.ERROR_MESSAGE);
                this.exitApplication();
            }
            LOGGER.debug("(checkVersion) Closing VersionDAO connection");
            versionDAO.closeConnection();
        } catch (IllegalStateException ex)
        {
            LOGGER.warn("(checkVersion) Problem connecting to the database "
                    + "with the VersionDAO object");
        } 
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnGrade;
    private javax.swing.JButton btnUnzip;
    private javax.swing.JComboBox<GradedEvent> cboAssignment;
    private javax.swing.JComboBox<Course> cboCourse;
    private javax.swing.JComboBox<Instructor> cboInstructor;
    private javax.swing.JComboBox<Section> cboSection;
    private javax.swing.JComboBox<Student> cboStudent;
    private javax.swing.JMenuItem connectMenuItem;
    private javax.swing.JMenuItem disconnectMenuItem;
    private javax.swing.JMenuItem gradeAssignmentsMenuItem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.ButtonGroup langButtonSelect;
    private javax.swing.JLabel lblAssignment;
    private javax.swing.JLabel lblCourse;
    private javax.swing.JLabel lblInstructor;
    private javax.swing.JLabel lblSection;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblStudent;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem optionsMenuItem;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JRadioButton rBtnProcessing;
    private javax.swing.JRadioButton rBtnRaptor;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JMenu toolsMenu;
    private javax.swing.JMenuItem unzipAssignmentMenuItem;
    // End of variables declaration//GEN-END:variables
    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;
    private JDialog aboutBox;
    private OptionsDialog optionDialog;
    private EntityManagerFactory emf;
    private final Preferences root = Preferences.userRoot();
    private final Preferences node = root.node("/com/darkhonor/rage");
    private final Map<String,String> dbProperties = new HashMap<>();
    private boolean cboCourseActive = false;
    private final CourseComboBoxModel courseComboBoxModel = new CourseComboBoxModel();
    private final InstructorComboBoxModel instructorComboBoxModel =
            new InstructorComboBoxModel();
    private final SectionComboBoxModel sectionComboBoxModel = new SectionComboBoxModel();
    private final StudentComboBoxModel studentComboBoxModel = new StudentComboBoxModel();
    private final GradedEventComboBoxModel gradedEventComboBoxModel = new GradedEventComboBoxModel();
    private boolean cboAssignActive = false;
    private boolean cboInstActive = false;
    private boolean cboSectActive = false;
    private boolean multVersions = false;
    private Thread grader_thread;
    private static final Logger LOGGER = Logger.getLogger(CourseGraderView.class);
    private static final Version version = new Version(2.1);

    public class RageWindowAdapter extends WindowAdapter
    {

        @Override
        public void windowClosing(WindowEvent e)
        {
            try
            {
                if ((emf != null) && (emf.isOpen()))
                {
                    LOGGER.debug("Disconnecting from the database");
                    emf.close();
                    LOGGER.info("Disconnected from the database.  Exiting program.");
                }
            } catch (Exception ex)
            {
            }
            System.exit(0);
        }
    }
}
