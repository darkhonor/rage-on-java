/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import com.darkhonor.rage.model.Category;
import com.darkhonor.rage.model.Question;
import java.util.List;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;

/**
 * Class to handle all database queries for {@link com.darkhonor.rage.model.Question}
 * objects.
 *
 * @author Alex Ackerman
 */
public class QuestionDAO
{
    /**
     * Default constructor.
     *
     * @param em    EntityManager connection to the database.
     * @throws IllegalArgumentException Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager is
     *                                  <code>null</code>
     */
    public QuestionDAO(EntityManager em) throws IllegalArgumentException,
            NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("EntityManager is closed.  Throwing IllegalArgumentException");
            throw new IllegalArgumentException("Invalid EntityManager state");
        } else
        {
            this.em = em;
        }
        this.em = em;
    }

    /**
     * Closes the connection to the database.
     *
     * @throws IllegalStateException    Thrown if the database connection is
     *                                  already closed.
     */
    public void closeConnection() throws IllegalStateException, NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        }
        em.close();
    }

    /**
     * Returns whether or not the connection to the data source is open.
     * 
     * @return      <code>true</code> if the connection is open, <code>false</code>
     *              otherwise
     * @throws NullPointerException Thrown when the EntityManager is null
     */
    public boolean isOpen() throws NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        }
        return em.isOpen();
    }
    
    /**
     * Saves the provided Question to the data source.  The connection to the
     * data source remains open at the end of this call.
     * 
     * @param question          The Question to save to the data source
     * @return                  The Id of the created Question object
     * @throws IllegalStateException    Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager or the 
     *                                  Question is <code>null</code>
     * @throws IllegalArgumentException     Thrown if the Question is invalid
     */
    public Long create(Question question) throws IllegalStateException,
            NullPointerException, IllegalArgumentException
    {
        return null;
    }
    
    /**
     * Finds a Question object in the database with the given Id value.  The database
     * connection will still be active at the end of this method call.
     *
     * @param id                        The Id of the Question to find
     * @return                          The selected Question if found or
     *                                  <code>null</code> otherwise
     * @throws IllegalArgumentException Thrown when the Id is an invalid value--
     *                                  negative or 0.
     * @throws NullPointerException     Thrown when the Id is <code>null</code>
     * @throws IllegalStateException    Thrown if the connection to data source is not
     *                                  open
     */
    public Question find(Long id) throws IllegalArgumentException, NullPointerException,
            IllegalStateException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (id == null)
        {
            LOGGER.error("The provided Question Id is null");
            throw new NullPointerException("The provided Question Id is null");
        } else if (id < 1)
        {
            LOGGER.error("The provided Question Id is less than 1");
            throw new IllegalArgumentException("The provided Question Id is less than 1");
        } else if (!em.isOpen())
        {
            LOGGER.error("The connection to the data source is closed");
            throw new IllegalStateException("The connection to the data source is closed");
        }
        return em.find(Question.class, id);
    }

    /**
     * Returns the Question from the data source with the given name.  The data source 
     * connection will still be active at the end of the call.
     * 
     * @param name      The name of the Question to retrieve
     * @return          The Question in the data source
     * @throws IllegalArgumentException Thrown when an invalid name is supplied
     * @throws NullPointerException     Thrown when the name is <code>null</code>
     * @throws IllegalStateException    Thrown when the data source is closed
     */
    public Question find(String name) throws IllegalArgumentException, 
            NullPointerException, IllegalStateException
    {
        return null;
    }
    
    /**
     * Updates the provided Question in the data source.  The updated Question
     * is returned to the user.  An error will be raised if the Question doesn't
     * already exist in the data source.  The connection will still be active
     * at the end of the call.
     * 
     * @param question          The Question to update in the data source
     * @return                  The updated Question saved to the data source
     * @throws IllegalArgumentException Thrown if the Question doesn't exist
     * @throws NullPointerException     Thrown if the EntityManager or Question
     *                                  is <code>null</code>
     * @throws IllegalStateException    Thrown if the EntityManager is closed
     */
    public Question update(Question question) throws IllegalArgumentException,
            NullPointerException, IllegalStateException
    {
        return null;
    }
    
    /**
     * Deletes the Question with the provided Id from the data source.  If the
     * Question cannot be found in the data source, an error will be raised.  The
     * connection to the data source will still be open at the end of the call.
     * 
     * @param id            The Id of the Question to delete
     * @throws IllegalArgumentException Thrown if the Question cannot be found
     *                                  in the data source or if the Id contains
     *                                  an invalid value
     * @throws NullPointerException     Thrown if the EntityManager or the Id
     *                                  is <code>null</code>
     * @throws IllegalStateException    Thrown if the EntityManager is closed
     */
    public void delete(Long id) throws IllegalArgumentException, 
            NullPointerException, IllegalStateException
    {
        
    }
    
    /**
     * Deletes the selected Question from the data source.  If the Question 
     * cannot be found in the data source, an error will be raised.  The 
     * connection to the data source will still be open at the end of the call.
     * 
     * @param question          The Question to delete
     * @throws IllegalArgumentException Thrown if the Question cannot be found
     *                                  in the data source or if the Question 
     *                                  is invalid
     * @throws NullPointerException     Thrown if the EntityManager or the 
     *                                  Question is <code>null</code>
     * @throws IllegalStateException    Thrown if the EntityManager is closed
     */
    public void delete(Question question) throws IllegalArgumentException,
            NullPointerException, IllegalStateException
    {
        
    }
    
    /**
     *  Hint: See equivalent Query in the CourseDAO interface.
     * 
     *  Query Logic:
     *      cb = em.getCriteriaBuilder();
            cq = cb.createQuery(Question.class);
            Root<Question> questionRoot = cq.from(Question.class);
            cq.orderBy(cb.asc(questionRoot.get("name")));
            TypedQuery<Question> questionQuery = em.createQuery(cq);
            List<Question> questionResult;
            try
            {
                count = 0;
                questionResult = questionQuery.getResultList();

     */
    /**
     *  Query: GetAllQuestionsByName
     *  Description: Returns a list of all Questions stored in the data source.
     *               If there are no Questions in the data source, then an empty
     *               List of Question objects will be returned.  The list will be sorted
     *               in alphabetical order by Question name.
     * @return      The sorted List of Questions in the data source 
     * @throws IllegalStateException    Thrown if the data source connection is
     *                                  closed
     * @throws NullPointerException     Thrown if the EntityManager is 
     *                                  <code>null</code>
     *          
     */
    public List<Question> getAllQuestionsByName() throws IllegalStateException,
            NullPointerException
    {
        return null;
    }
    
    /**
     * Usage: JTestGen -> SelectQuestionDialog.initialize()
     * 
     * Query Logic:
     *      CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery(Question.class);
            Root<Question> questionRoot = cq.from(Question.class);
            // Restrict result to those not found in the list of Question Id's
            cq.where(cb.not(questionRoot.get("id").in(questionIds)));
            cq.orderBy(cb.asc(questionRoot.get("name")));
            TypedQuery<Question> questionQuery = em.createQuery(cq);
            try
            {
                LOGGER.debug("Getting list of Questions");
                List<Question> result = questionQuery.getResultList();

     */
    /**
     * Query: GetAllQuestionsNotInIdList
     * Description: Return a list of Questions that does not include those 
     *              identified by the list of Ids.  If there are no Questions
     *              that meet the criteria, an empty List of Question objects will be 
     *              returned.  The returned list will be sorted in alphabetical
     *              order.
     * 
     * @param idList    The List of questions to query
     * @return      List of questions not in list
     * @throws IllegalStateException        Thrown if the connection is already closed
     * @throws IllegalArgumentException     Thrown if the list is invalid
     * @throws NullPointerException         Thrown if the list is <code>null</code>
     */
    public List<Question> getAllQuestionsNotInIdList(List<Long> idList) throws
            IllegalStateException, IllegalArgumentException, NullPointerException
    {
        return null;
    }

    /**
     * Return a list of Questions that are of the Category provided.  If there
     * are no Questions that meet the criteria, an empty list will be returned.
     * The returned list will be sorted in alphabetical order.
     * 
     * @param category      The Category of Questions to return
     * @return              The List of Questions with the given Category
     * @throws IllegalStateException    Thrown when the connection to the data
     *                                  source is closed
     * @throws IllegalArgumentException Thrown if the Category is invalid
     * @throws NullPointerException     Thrown if the Category or the EntityManager
     *                                  is <code>null</code>
     */
    public List<Question> getAllQuestionsInCategory(Category category) throws
            IllegalStateException, IllegalArgumentException, NullPointerException
    {
        return null;
    }
    
    /**
     * Returns a list of Questions that either require or don't require verbatim
     * output based upon the input.  If there are no questions that meet the 
     * criteria, an empty list will be returned.  The returned list will be
     * sorted in alphabetical order.
     * 
     * @param verbatim      Whether or not the Verbatim Questions are returned
     * @return              List of Questions that meet the criteria
     * @throws IllegalStateException    Thrown if the EntityManager is closed
     * @throws NullPointerException     Thrown if the EntityManager is null
     */
    public List<Question> getAllVerbatimQuestions(boolean verbatim) throws
            IllegalStateException, NullPointerException
    {
        return null;
    }
    
    /**
     * Returns the number of Questions stored in the data source.  The number will
     * always return greater than or equal to zero.  
     * 
     * @return          The number of the Questions in the data source
     * @throws IllegalStateException    Thrown when the connection to the data
     *                                  source is closed
     * @throws NullPointerException     Thrown when the EntityManager is 
     *                                  <code>null</code>
     */
    public int getNumberOfQuestions() throws IllegalStateException, 
            NullPointerException
    {
        return -1;
    }
    
    private final Logger LOGGER = Logger.getLogger(QuestionDAO.class);
    private EntityManager em;
}
