/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import com.darkhonor.rage.model.Version;
import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;

/**
 * Class to handle all database queries for {@link com.darkhonor.rage.model.Version}
 * objects.  This class will also handle any version check methods that will be used
 * by the various calling methods.
 *
 * @author Alex Ackerman
 */
public class VersionDAO
{

    /**
     * Default constructor.
     *
     * @param em    EntityManager connection to the database.
     * @throws IllegalArgumentException Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager is
     *                                  <code>null</code>
     */
    public VersionDAO(EntityManager em) throws IllegalArgumentException,
            NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("EntityManager is closed.  Throwing IllegalArgumentException");
            throw new IllegalArgumentException("Invalid EntityManager state");
        } else
        {
            this.em = em;
        }
        this.em = em;
    }

    /**
     * Checks the version of the Database an application is compatible with the
     * value stored in the database.  The EntityManager will not be closed at the
     * end of the method.
     *
     * @param appVersion    The Version from the application
     * @return              <code>true</code> if the application version equals
     *                      the stored version, <code>false</code> otherwise.
     */
    public boolean checkDbVersion(Version appVersion) throws IllegalStateException
    {
        if (!em.isOpen())
        {
            LOGGER.error("(checkDbVersion) Illegal Database State: EntityManager "
                    + "is closed");
            throw new IllegalStateException("EntityManager is closed");
        } else if (appVersion == null)
        {
            LOGGER.error("(checkDbVersion) Version cannot be null.");
            throw new NullPointerException("Version cannot be null");
        } else if (appVersion.getId() == null)
        {
            LOGGER.error("(checkDbVersion) Malformed Version.");
            throw new IllegalArgumentException("Malformed Version");
        } else
        {
            try
            {
                LOGGER.debug("(checkDbVersion) Creating version query");
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery<Version> cq = cb.createQuery(Version.class);
                Root<Version> versionRoot = cq.from(Version.class);
                cq.orderBy(cb.desc(versionRoot.get("id")));
                TypedQuery<Version> versionQuery = em.createQuery(cq);
                LOGGER.debug("(checkDbVersion) Query created");
                List<Version> versions = versionQuery.getResultList();
                LOGGER.debug("(checkDbVersion) Returned " + versions.size() +
                        " results.");
                if (versions.size() > 1)
                {
                    for (int i = 0; i < versions.size(); i++)
                    {
                        LOGGER.debug("(checkDbVersion) Version " + i + ": " +
                                versions.get(i).getId());
                    }
                }
                LOGGER.debug("(checkDbVersion) Version from Database: " +
                        versions.get(0).getId());
                return appVersion.equals(versions.get(0));
            } catch (NoResultException ex)
            {
                LOGGER.error("(checkDbVersion) No result found");
                return false;
            } catch (Exception ex)
            {
                LOGGER.error("(checkDbVersion) Exception caught: " +
                        ex.getLocalizedMessage());
                return false;
            }
        }
    }

    /**
     * Closes the connection to the database.
     * 
     * @throws IllegalStateException    Thrown if the database connection is
     *                                  already closed.
     */
    public void closeConnection() throws IllegalStateException
    {
        em.close();
    }

    public boolean isOpen()
    {
        return em.isOpen();
    }

    public Double create(Version version) throws IllegalStateException, NullPointerException,
            IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(create) EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(create) EntityManager is closed.  Throwing IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (version == null)
        {
            LOGGER.error("(create) Version is null.  Throwing NullPointerException.");
            throw new NullPointerException("Version is null");
        } else if (version.getId() == null || version.getId() < 0.0)
        {
            LOGGER.error("(create) Version is either null or invalid.");
            throw new IllegalArgumentException("Version is either null or invalid");
        } else
        {
            EntityTransaction tx = null;
            // Don't need to put this into a Transaction since the transaction will
            // be created in the find method
            Version testVersion = this.find(version.getId());
            if (testVersion != null)
            {
                LOGGER.warn("(create) Version already exists in the data source");
                throw new IllegalArgumentException("Version alread exists in the data source");
            }
            try
            {
                tx = em.getTransaction();
                tx.begin();
                em.persist(version);
                tx.commit();
                return version.getId();
            } catch (EntityExistsException ex)
            {
                LOGGER.warn("(create) Version already exists in the data source");
                tx.rollback();
                throw new IllegalArgumentException("Version already exists in the data source");
            }
        }

    }

    public Version find(Double versionNumber) throws IllegalStateException, NullPointerException,
            IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(find) EntityManager is null.  "
                    + "Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(find) EntityManager is closed.  "
                    + "Throwing IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (versionNumber == null )
        {
            LOGGER.error("(find) Version number is null.");
            throw new NullPointerException("Version number is null");
        } else if (versionNumber <= 0.0)
        {
            LOGGER.error("(find) Version number is less than or equal to 0.0");
            throw new IllegalArgumentException("Version number is less than or "
                    + "equal to 0.0");
        } else
        {
            Version version;
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Version> cq = cb.createQuery(Version.class);
            Root<Version> categoryRoot = cq.from(Version.class);
            cq.where(cb.equal(categoryRoot.get("id"), versionNumber));
            TypedQuery<Version> versionQuery = em.createQuery(cq);
            EntityTransaction tx = null;
            try
            {
                tx = em.getTransaction();
                tx.begin();
                version = versionQuery.getSingleResult();
                tx.commit();
            } catch (NoResultException ex)
            {
                LOGGER.info("(find) No version found with the number " +
                        versionNumber);
                tx.rollback();
                version = null;
            }
            return version;
        }
    }

    public void delete(Version version) throws IllegalStateException, NullPointerException,
            IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(delete) EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(delete) EntityManager is closed.  Throwing IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (version == null)
        {
            LOGGER.error("(delete) Version is null.  Throwing NullPointerException.");
            throw new NullPointerException("Version is null");
        } else if (version.getId() == null || version.getId() <= 0.0)
        {
            LOGGER.error("(delete) Version number is either null or invalid.");
            throw new IllegalArgumentException("Version number is either null or blank");
        } else
        {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Version deletedVersion = em.find(Version.class, version.getId());
            tx.commit();
            if (deletedVersion == null)
            {
                LOGGER.error("(delete) Provided Version does not exist");
                throw new IllegalArgumentException("Provided Version does not exist");
            }
            tx.begin();
            em.remove(deletedVersion);
            tx.commit();
        }
    }

    public void delete(Double versionNumber) throws IllegalStateException, NullPointerException,
            IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(delete) EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(delete) EntityManager is closed.  Throwing IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (versionNumber == null)
        {
            LOGGER.error("(delete) Version number is null.  Throwing NullPointerException.");
            throw new NullPointerException("Version number is null");
        } else if (versionNumber <= 0.0)
        {
            LOGGER.error("(delete) Version number is invalid.  Throwing InvalidArgumentException");
            throw new IllegalArgumentException("Version number is invalid");
        } else
        {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Version deletedCategory = em.find(Version.class, versionNumber);
            tx.commit();
            if (deletedCategory == null)
            {
                LOGGER.error("(delete) Provided Version number does not exist");
                throw new IllegalArgumentException("Provided Version number does not exist");
            }
            tx.begin();
            em.remove(deletedCategory);
            tx.commit();
        }
    }

    private Logger LOGGER = Logger.getLogger(VersionDAO.class);
    private EntityManager em;
}
