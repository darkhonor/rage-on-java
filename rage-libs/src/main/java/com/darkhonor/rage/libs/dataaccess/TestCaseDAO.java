/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import com.darkhonor.rage.model.TestCase;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.apache.log4j.Logger;

/**
 * Class to handle all database queries for {@link com.darkhonor.rage.model.TestCase}
 * objects. 
 *
 * @author Alex Ackerman
 */
public class TestCaseDAO
{
    /**
     * Default constructor.
     *
     * @param em    EntityManager connection to the database.
     * @throws IllegalArgumentException Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager is
     *                                  <code>null</code>
     */
    public TestCaseDAO(EntityManager em) throws IllegalArgumentException,
            NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("EntityManager is closed.  Throwing IllegalArgumentException");
            throw new IllegalArgumentException("Invalid EntityManager state");
        } else
        {
            this.em = em;
        }
        this.em = em;
    }

    /**
     * Closes the connection to the database.
     *
     * @throws IllegalStateException    Thrown if the database connection is
     *                                  already closed.
     */
    public void closeConnection() throws IllegalStateException
    {
        em.close();
    }

    /**
     * Returns whether or not the connection to the data source is open.
     *
     * @return  <code>true</code> if the connection is open, <code>false</code>
     *          otherwise.
     */
    public boolean isOpen()
    {
        return em.isOpen();
    }

    /**
     * Returns the TestCase identified by the provided id or <code>null</code>
     * if it cannot be found in the data source.
     * 
     * @param id        The id of the TestCase
     * @return          The TestCase from the data source or <code>null</code>
     * @throws IllegalStateException        Thrown when the EntityManager is closed
     * @throws NullPointerException         Thrown when the EntityManager or id
     *                                      is <code>null</code>
     * @throws IllegalArgumentException     Thrown when the id is less than 1
     */
    public TestCase find(Long id) throws IllegalStateException, NullPointerException,
            IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(find) EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(find) EntityManager is closed.  Throwing "
                    + "IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (id == null)
        {
            LOGGER.error("(find) Provided Test Case Id is null");
            throw new NullPointerException("Provided Test Case Id is null");
        } else if (id < 1L)
        {
            LOGGER.error("(find) Provided Test Case Id is less than 1");
            throw new IllegalArgumentException("Provided Test Case Id is less than 1");
        } else
        {
            EntityTransaction tx = em.getTransaction();
            TestCase testCase;
            tx.begin();
            testCase = em.find(TestCase.class, id);
            tx.commit();
            return testCase;
        }
    }

    /**
     * Saves the given TestCase to the data source
     * 
     * @param testCase      The TestCase to save to the data source
     * @return              The id of the TestCase after it has been saved
     * @throws IllegalStateException    Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager or the 
     *                                  TestCase is <code>null</code>
     * @throws IllegalArgumentException     Thrown when the TestCase has an
     *                                      invalid id (less than 1), if the 
     *                                      TestCase already exists in the data 
     *                                      source, or the TestCase value is 
     *   
     */
    public Long create(TestCase testCase) throws IllegalStateException, 
            NullPointerException, IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(create) EntityManager is null.  Throwing "
                    + "NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(create) EntityManager is closed.  Throwing "
                    + "IllegalStateException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (testCase == null)
        {
            LOGGER.error("(create) Test Case is null.  Throwing NullPointerException.");
            throw new NullPointerException("Test Case is null");
        } else if (testCase.getValue() == null)
        {
            LOGGER.error("(create) Test Case value is null");
            throw new IllegalArgumentException("Test Case value is null");
        } else
        {
            EntityTransaction tx;
            try
            {
                tx = em.getTransaction();
                tx.begin();
                em.persist(testCase);
                tx.commit();
                return testCase.getId();
            } catch (EntityExistsException ex)
            {
                LOGGER.warn("(create) Test Case already exists in the data source");
                throw new IllegalArgumentException("Test Case already exists in "
                        + "the data source");
            } catch (PersistenceException ex)
            {
                LOGGER.warn("(create) Another Persistence Exception was thrown");
                throw new IllegalArgumentException("Problem saving the Test Case "
                        + "to the data source");
            }
        }
    }

    /**
     * Update the provided TestCase in the database with the new values.  The
     * TestCase must exist in the data source or an exception will be raised.
     * The updated TestCase from the data source will be returned.
     * 
     * @param testCase                  The new TestCase values to update
     * @return                          The updated TestCase
     * @throws IllegalStateException    Thrown when EntityManager is closed
     * @throws NullPointerException     Thrown when EntityManager, the TestCase,
     *                                  or the TestCase id is <code>null</code>
     * @throws IllegalArgumentException     Thrown if the TestCase doesn't exist 
     *                                      in the data source or the id is less than 1
     */
    public TestCase update(TestCase testCase) throws IllegalStateException, 
            NullPointerException, IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(find) EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(find) EntityManager is closed.  Throwing "
                    + "IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (testCase == null)
        {
            LOGGER.error("(find) Provided Test Case is null");
            throw new NullPointerException("Provided Test Case is null");
        } else if (testCase.getId() == null)
        {
            LOGGER.error("(find) Provided Test Case Id is null");
            throw new NullPointerException("Provided Test Case Id is null");

        } else if (testCase.getId() < 1L)
        {
            LOGGER.error("(find) Provided Test Case Id is less than 1");
            throw new IllegalArgumentException("Provided Test Case Id is less than 1");
        } else
        {
            EntityTransaction tx = em.getTransaction();
            TestCase updatedTestCase = em.find(TestCase.class, testCase.getId());
            if (updatedTestCase == null)
            {
                LOGGER.error("(update) Provided TestCase does not exist");
                throw new IllegalArgumentException("Provided TestCase does not exist");
            }
            updatedTestCase.setValue(testCase.getValue());
            updatedTestCase.setInputs(new ArrayList<String>());
            updatedTestCase.setOutputs(new ArrayList<String>());
            updatedTestCase.setExcludes(new ArrayList<String>());
            // Clear current inputs, outputs, and excludes
            tx.begin();
            updatedTestCase = em.merge(updatedTestCase);
            tx.commit();
            // Add current inputs, outputs, and excludes
            for (int i = 0; i < testCase.getInputs().size(); i++)
            {
                updatedTestCase.addInput(testCase.getInputs().get(i));
            }
            for (int i = 0; i < testCase.getOutputs().size(); i++)
            {
                updatedTestCase.addOutput(testCase.getOutputs().get(i));
            }
            for (int i = 0; i < testCase.getExcludes().size(); i++)
            {
                updatedTestCase.addExclusion(testCase.getExcludes().get(i));
            }
            tx.begin();
            updatedTestCase = em.merge(updatedTestCase);
            tx.commit();
            return updatedTestCase;
        }        
    }

    /**
     * Deletes the given TestCase from the data source.  If the TestCase doesn't
     * exist, an exception will be thrown.
     * 
     * @param testCase                  The TestCase to delete
     * @throws IllegalStateException    Thrown when EntityManager is closed
     * @throws NullPointerException     Thrown when EntityManager, TestCase, or
     *                                  id is <code>null</code>
     * @throws IllegalArgumentException     Thrown if the TestCase doesn't exist
     *                                      in the data source or id is less than 1
     */
    public void delete(TestCase testCase) throws IllegalStateException, 
            NullPointerException, IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(find) EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(find) EntityManager is closed.  Throwing "
                    + "IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (testCase == null)
        {
            LOGGER.error("(find) Provided Test Case is null");
            throw new NullPointerException("Provided Test Case is null");
        } else if (testCase.getId() == null)
        {
            LOGGER.error("(find) Provided Test Case Id is null");
            throw new NullPointerException("Provided Test Case Id is null");

        } else if (testCase.getId() < 1L)
        {
            LOGGER.error("(find) Provided Test Case Id is less than 1");
            throw new IllegalArgumentException("Provided Test Case Id is less than 1");
        } else
        {
            EntityTransaction tx = em.getTransaction();
            TestCase deletedTestCase = em.find(TestCase.class, testCase.getId());
            if (deletedTestCase == null)
            {
                LOGGER.error("(delete) Provided TestCase does not exist");
                throw new IllegalArgumentException("Provided TestCase does not "
                        + "exist");
            }
            tx.begin();
            em.remove(deletedTestCase);
            tx.commit();
        }        
    }

    /**
     * Deletes the TestCase with the provided id from the data source.  If the 
     * TestCase doesn't exist, an exception will be thrown.
     * 
     * @param id                        The id of the TestCase to delete
     * @throws IllegalStateException    Thrown when EntityManager is closed
     * @throws NullPointerException     Thrown when EntityManager or id is 
     *                                  <code>null</code>
     * @throws IllegalArgumentException     Thrown if the TestCase doesn't exist
     *                                      in the data source or id is less than 1
     */
    public void delete(Long id) throws IllegalStateException, NullPointerException,
            IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(find) EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(find) EntityManager is closed.  Throwing "
                    + "IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (id == null)
        {
            LOGGER.error("(find) Provided Test Case Id is null");
            throw new NullPointerException("Provided Test Case Id is null");
        } else if (id < 1L)
        {
            LOGGER.error("(find) Provided Test Case Id is less than 1");
            throw new IllegalArgumentException("Provided Test Case Id is less than 1");
        } else
        {
            EntityTransaction tx = em.getTransaction();
            TestCase deletedTestCase = em.find(TestCase.class, id);
            if (deletedTestCase == null)
            {
                LOGGER.error("(delete) Provided TestCase ID does not exist");
                throw new IllegalArgumentException("Provided TestCase Id does not "
                        + "exist");
            }
            tx.begin();
            em.remove(deletedTestCase);
            tx.commit();
        }
    }
    
    /**
     * Returns a List of all TestCase objects in the data source.  If there are 
     * no TestCases in the data source, an empty (but not <code>null</code> List
     * will be returned.
     * 
     * @return                          A List of all TestCase objects in the data source
     * @throws IllegalStateException    Thrown when EntityManager is closed
     * @throws NullPointerException     Thrown when EntityManager is null
     */
    List<TestCase> findAllTestCases() throws IllegalStateException, NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("(find) EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(find) EntityManager is closed.  Throwing "
                    + "IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else
        {
            List<TestCase> testCaseList;
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<TestCase> cq = cb.createQuery(TestCase.class);
            TypedQuery<TestCase> testCaseQuery = em.createQuery(cq);
            testCaseList = testCaseQuery.getResultList();
            return testCaseList;
        }
    }

    private final Logger LOGGER = Logger.getLogger(TestCaseDAO.class);
    private EntityManager em;
}
