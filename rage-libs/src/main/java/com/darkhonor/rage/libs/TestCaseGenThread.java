/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs;

import com.darkhonor.rage.model.Question;
import com.darkhonor.rage.model.TestCase;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.prefs.Preferences;
import org.apache.log4j.Logger;

/**
 * The TestCaseGenThread runs a Raptor program for a given test file and saves
 * the output of the program to the {@link Question} object for each {@link TestCase}.
 *
 * @author Alexander.Ackerman 
 *
 * @version 1.0.0
 *
 */
public class TestCaseGenThread implements Runnable
{

    /**
     * No argument constructor.  Initializes each of the object elements.
     */
    public TestCaseGenThread()
    {
        processBuilder = new ProcessBuilder();
        testFile = null;
        question = new Question();
        node = null;
        type = 0;
    }

    /**
     * Constructor for TestCaseGenThread object.
     *
     * @param prefs The stored preferences for the user
     * @param pb    The process builder used to create the thread
     * @param file  The test program to use
     * @param question  The question object to store the results to
     * @param type  The type of test case. Default is 0
     */
    public TestCaseGenThread(Preferences prefs, ProcessBuilder pb, File file,
            Question question, int type)
    {
        this.processBuilder = pb;
        this.testFile = file;
        this.question = question;
        this.node = prefs;
        this.type = type;
    }

    @Override
    public void run()
    {
        try
        {
            // Create a 10KB buffer to write the input and output
            ByteBuffer buffer = ByteBuffer.allocate(1024 * 10);

            // Run all test cases for the question
            Iterator testCaseIterator = question.getTestCases().iterator();
            while (testCaseIterator.hasNext())
            {
                TestCase testCase = (TestCase) testCaseIterator.next();

                // Create temp files with response information from the database
                File tempIn = File.createTempFile("rage", ".tmp");
                File tempOut = File.createTempFile("rage", ".tmp");

                FileOutputStream outFile = new FileOutputStream(tempIn);
                FileChannel outChannel = outFile.getChannel();
                outChannel.lock();
                for (int i = 0; i < testCase.getInputs().size(); i++)
                {
                    buffer.put(testCase.getInputs().get(i).getBytes());
                    buffer.put(System.getProperty("line.separator").getBytes());
                    buffer.flip();
                    outChannel.write(buffer);
                    buffer.clear();
                }
                outFile.close();
                outChannel.close();

                List<String> cmd = new ArrayList<String>();
                if (type == RAGEConst.RAPTOR_QUESTION)  // It's a RAPTOR algorithm
                {
                    cmd.add(node.get("RaptorExecutable",
                            RAGEConst.DEFAULT_RAPTOR_EXECUTABLE));
                    cmd.add("\"" + testFile.getCanonicalPath() + "\"");
                    cmd.add("/run");
                    cmd.add("\"" + tempIn.getCanonicalPath() + "\"");
                    cmd.add("\"" + tempOut.getCanonicalPath() + "\"");
                } else if (type == RAGEConst.PROCESSING_QUESTION)    // It's a Processing sketch
                {
                    // TODO: Change from runme to full native Java call
                    cmd.add(node.get("RunmeExecutable", 
                            RAGEConst.DEFAULT_PROCESSING_RUNNER));
                    cmd.add(question.getName());
                    for (int i = 0; i < testCase.getInputs().size(); i++)
                    {
                        cmd.add(testCase.getInputs().get(i));
                    }
                    cmd.add(">" + tempOut.getCanonicalPath());

                } else
                {
                    LOGGER.error("Unsupported Question Type");
                    throw new UnsupportedOperationException();
                }
                LOGGER.debug("Command: " + cmd);
                processBuilder.command(cmd);

                // Add thread protection
                Process p = processBuilder.start();
                Long startTimeInNanoSec = System.nanoTime();
                Long delayInNanoSec;

                // If Inifinite Loop protection is enabled, default to true
                if (node.getBoolean("InfiniteLoopDetection", true))
                {
                    LOGGER.debug("Infinite loop detection is enabled");
                    try
                    {
                        delayInNanoSec =
                                Long.parseLong(node.get("Threshold", "10")) * 1000000000;
                    } catch (NumberFormatException e)
                    {
                        LOGGER.warn("Invalid Threshold value.  Defaulting to 10");
                        delayInNanoSec = new Long(10 * 1000000000);
                    }
                    boolean timeFlag = true;
                    while (timeFlag)
                    {
                        try
                        {
                            int val = p.exitValue();
                            timeFlag = false;
                        } // This should come back with an exception
                        // if the process is still going
                        catch (IllegalThreadStateException e)
                        {
                            Long elapsedTime = System.nanoTime()
                                    - startTimeInNanoSec;
                            if (elapsedTime > delayInNanoSec)
                            {
                                LOGGER.warn("Threshold time exceeded.");
                                p.destroy();
                                timeFlag = false;
                            }
                            Thread.sleep(50);
                        }
                    }
                } else
                {
                    LOGGER.debug("Infinite loop detection is not enabled");
                    p.waitFor();
                }

                File newTemp = null;
                BufferedReader inFile;
                // Read the results from the user and store
                try
                {
                    inFile = new BufferedReader(new FileReader(tempOut));
                } catch (FileNotFoundException ex)
                {   /* Assume since the file is locked by another process
                     * that no good output is available...come up with a
                     * dummy file to pass to the grader.
                     */
                    LOGGER.error("The file is in use by another process.");
                    newTemp = File.createTempFile("rage", ".tmp");
                    inFile = new BufferedReader(new FileReader(newTemp));
                }
                String line = null;
                while ((line = inFile.readLine()) != null)
                {
                    // Add to list of output for the TestCase
                    testCase.addOutput(line);
                }
                inFile.close();
                tempIn.delete();
                tempOut.delete();
                if (newTemp != null)
                {
                    newTemp.delete();
                }
            }
        } catch (IOException ex)
        {
            LOGGER.error("Exception: " + ex.getLocalizedMessage());
        } catch (InterruptedException ex)
        {
            LOGGER.warn("Thread interrupted");
        } catch (ThreadDeath td)
        {
            throw td;
        }
    }
    
    private ProcessBuilder processBuilder;
    private File testFile;
    private Question question;
    private Preferences node;
    private int type;
    private static final Logger LOGGER = Logger.getLogger(TestCaseGenThread.class);
}
