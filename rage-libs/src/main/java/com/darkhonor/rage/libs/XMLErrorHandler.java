/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Implementation of XML Error Handler to validate XML documents against a known DTD.
 * 
 * Source is Dr Herong Yang, <a href="http://www.herongyang.com/jdk/dtd_validation.html">
 * http://www.herongyang.com/jdk/dtd_validation.html</a>.
 * 
 * Overrides warning, error, and fatalError methods of the {@link org.xml.sax.helpers.DefaultHandler}.
 * 
 * @author Alexander Ackerman 
 * @author Dr Herong Yang
 * 
 * @version 1.0.0
 * 
 * @see <a href="http://java.sun.com/javase/6/docs/api/org/xml/sax/helpers/DefaultHandler.html">DefaultHandler</a>
 */
public class XMLErrorHandler extends DefaultHandler
{

    public XMLErrorHandler()
    {
    }

    @Override
    public void warning(SAXParseException e) throws SAXException
    {
        System.out.println("Warning: ");
        printInfo(e);
    }

    @Override
    public void error(SAXParseException e) throws SAXException
    {
        System.out.println("Error: ");
        printInfo(e);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException
    {
        System.out.println("Fatal Error: ");
        printInfo(e);
    }

    private void printInfo(SAXParseException e) throws SAXException
    {
        System.out.println("\tPublic ID: " + e.getPublicId());
        System.out.println("\tSystem ID: " + e.getSystemId());
        System.out.println("\tLine number: " + e.getLineNumber());
        System.out.println("\tColumn number: " + e.getColumnNumber());
        System.out.println("\tMessage: " + e.getMessage());
        throw new SAXException();
    }
}
