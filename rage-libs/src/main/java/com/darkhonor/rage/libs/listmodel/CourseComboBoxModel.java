/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.listmodel;

import com.darkhonor.rage.model.Course;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 * Implementation of the {@link ComboBoxModel} for the Course selection ComboBox
 * in CourseGrader.  This implementation stores the actual {@link Course} in the
 * list.
 * 
 * @author Alex Ackerman
 */
public class CourseComboBoxModel extends AbstractListModel<Course> 
    implements ComboBoxModel<Course>
{

    /**
     * No argument constructor.
     */
    public CourseComboBoxModel()
    {
        data = new ArrayList<Course>();
    }

    /**
     * Returns the number of elements in the ComboBox list.
     *
     * @return  The number of elements in the list
     */
    @Override
    public int getSize()
    {
        return data.size();
    }

    /**
     * Returns the {@link Course} stored at the given index in the ComboBox
     * (starting at 0).
     *
     * @param index     The index of the item to retrieve
     * @return          The Course stored in the list
     * @throws IllegalArgumentException Thrown when an invaid index is spefified
     */
    @Override
    public Course getElementAt(int index) throws IllegalArgumentException
    {
        if (index < 0 || index >= data.size())
        {
            throw new IllegalArgumentException("Invalid index specified");
        } else
        {
            return data.get(index);
        }
    }

    /**
     * Sets the selected {@link Course} in the ComboBox.
     *
     * @param anItem    The Course that is selected
     */
    @Override
    public void setSelectedItem(Object anItem)
    {
        selectedItem = (Course) anItem;
    }

    /**
     * Returns the selected {@link Course} from the ComboBox.
     *
     * @return  The selected Course
     */
    @Override
    public Object getSelectedItem()
    {
        return selectedItem;
    }

    /**
     * Add a {@link Course} to the ComboBox.
     *
     * @param course    The Course to add
     * @throws IllegalArgumentException Thrown when the specified Course is
     *      {@code null}
     */
    public void add(Course course) throws IllegalArgumentException
    {
        if (course == null)
        {
            throw new IllegalArgumentException("Invalid Argument - Course "
                    + "cannot be null");
        } else
        {
            data.add(course);
            this.fireContentsChanged(data, data.indexOf(course),
                    data.lastIndexOf(course));
        }
    }

    /**
     * Adds a {@link Set} of courses to the ComboBox Model.  After the set is
     * added, the List of {@link Course} objects will be sorted.
     *
     * @param courses      The Set of Courses to add to the Model
     * @throws IllegalArgumentException Thrown when the set of courses is
     *          {@code null}
     */
    public void addSet(Set<Course> courses) throws IllegalArgumentException
    {
        if (courses == null)
        {
            throw new IllegalArgumentException("Invalid argument.  Courses  "
                    + "cannot be a null set");
        } else
        {
            for (Course c : courses)
            {
                data.add(c);
            }
            Collections.sort(data);
        }
    }

    /**
     * Adds a {@link List} of courses to the ComboBox model.  After the list is
     * added, the List of {@link Course} objects will be sorted.
     *
     * @param courses   The List of Courses
     * @throws IllegalArgumentException Thrown when the list of courses is
     *      {@code null}
     */
    public void addList(List<Course> courses) throws IllegalArgumentException
    {
        if (courses == null)
        {
            throw new IllegalArgumentException("Invalid argument.  Courses "
                    + "cannot be a null list");
        } else
        {
            for (int i = 0; i < courses.size(); i++)
            {
                data.add(courses.get(i));
            }
            Collections.sort(data);
        }
    }

    /**
     * Clear the ComboBox of any {@link Course} objects.  After this method has
     * been run, the ComboBox list will be empty.
     */
    public void removeAll()
    {
        data.clear();
        this.fireIntervalRemoved(data, 0, 0);
    }

    /**
     * Remove the {@link Course} at the specified index in the list.  The list
     * is 0-based.  The Course that is removed is returned to the calling method.
     *
     * @param index     The index of the Course to remove
     * @return          The removed Course
     * @throws IllegalArgumentException Thrown when an invalid index is specified
     */
    public Course remove(int index) throws IllegalArgumentException
    {
        if (index < 0 || index >= data.size())
        {
            throw new IllegalArgumentException("Invalid index specified");
        } else
        {
            this.fireIntervalRemoved(data, index, index);
            return data.remove(index);
        }
    }

    /**
     * Sorts the {@link Course} objects stored in the list using the natural
     * sorting order.
     */
    public void sort()
    {
        Collections.sort(data);
    }
    
    private final ArrayList<Course> data;
    private Course selectedItem;
}
