/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import com.darkhonor.rage.model.Student;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;

/**
 * Class to handle all database queries for {@link com.darkhonor.rage.model.Student}
 * objects.
 *
 * @author Alex Ackerman
 */
public class StudentDAO
{
    /**
     * Default constructor.
     *
     * @param em    EntityManager connection to the database.
     * @throws IllegalArgumentException Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager is
     *                                  <code>null</code>
     */
    public StudentDAO(EntityManager em) throws IllegalArgumentException,
            NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("EntityManager is closed.  Throwing IllegalArgumentException");
            throw new IllegalArgumentException("Invalid EntityManager state");
        } else
        {
            this.em = em;
        }
        this.em = em;
    }

    public Student findStudentByWebId(String webId) throws NoResultException
    {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Student> cq = cb.createQuery(Student.class);
        Root<Student> studentRoot = cq.from(Student.class);
        cq.where(cb.equal(studentRoot.get("webID"), webId));
        TypedQuery<Student> studentQuery = em.createQuery(cq);
        return studentQuery.getSingleResult();
    }

    /**
     * Closes the connection to the database.
     *
     * @throws IllegalStateException    Thrown if the database connection is
     *                                  already closed.
     */
    public void closeConnection() throws IllegalStateException
    {
        em.close();
    }

    private Logger LOGGER = Logger.getLogger(TestCaseDAO.class);
    private EntityManager em;

}
