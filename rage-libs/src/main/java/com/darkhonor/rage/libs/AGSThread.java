/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs;

import com.darkhonor.rage.model.Question;
import com.darkhonor.rage.model.TestCase;
import com.darkhonor.rage.model.Response;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;

/**
 * The AGSThread class implements an AutoGraderServer thread to process
 * requests from a client.  The class implements the RAPTOR server protocol
 * as outlined at <a
 * href="http://raptor.martincarlisle.com/Implementing%20a%20RAPTOR%20test%20server.doc">
 * "Implementing a RAPTOR Test Server"</a>.  
 * 
 * @author Alexander Ackerman
 * 
 * @version 1.1.0
 * 
 * @see <a
 *   href="http://java.sun.com/javase/6/docs/api/java/lang/Runnable.html">Runnable</a>
 * @see <a
 *   href="http://java.sun.com/javase/6/docs/api/java/lang/Thread.html">Thread</a>
 */
public class AGSThread implements Runnable
{

    /**
     * Creates an AGSThread with the given {@link Socket} and {@link EntityManager}.
     * The {@link InputStream} and {@link OutputStream} variables are set from the
     * {@link Socket}.  This constructor is designed to work in the AutoGraderServer
     * (network version).
     * 
     * @param incoming The <code>Socket</code> with the client request
     * @param em The <code>EntityManager</code> specifying the location of the data source
     * 
     * @throws java.io.IOException Thrown if the Socket cannot return an InputStream
     *                              or OutputStream
     * 
     * @see <a href="http://java.sun.com/javase/6/docs/api/java/net/Socket.html">Socket</a>
     * @see <a
     *   href="http://java.sun.com/javaee/6/docs/api/javax/persistence/EntityManager.html">
     *   EntityManager</a>
     * 
     * @since 1.0.0
     */
    public AGSThread(Socket incoming, EntityManager em) throws IOException
    {
        this.incoming = incoming;
        this.em = em;
        try
        {
            this.inStream = incoming.getInputStream();
            this.outStream = incoming.getOutputStream();
        } catch (IOException ex)
        {
            // Let the calling procedure deal with this
            throw new IOException();
        }
    }

    /**
     * Creates an AGSThread with the given {@link InputStream}, {@link OutputStream}, and
     * {@link EntityManager}.  This constructor is designed to work in the AutoGraderServer
     * Test Environment.
     * 
     * @param inStream The <code>InputStream</code> for the thread
     * @param outStream The <code>OutputStream</code> for the thread
     * @param em The <code>EntityManager</code> specifying the location of the data source
     * 
     * @see <a 
     *   href="http://java.sun.com/javase/6/docs/api/java/io/InputStream.html">
     *   InputStream</a>
     * @see <a 
     *   href="http://java.sun.com/javase/6/docs/api/java/io/OutputStream.html">
     *   OutputStream</a>
     * @see <a 
     *   href="http://java.sun.com/javaee/6/docs/api/javax/persistence/EntityManager.html">
     *   EntityManager</a>
     * 
     * @since 1.1.0
     */
    public AGSThread(InputStream inStream, OutputStream outStream, EntityManager em)
    {
        this.em = em;
        this.inStream = inStream;
        this.outStream = outStream;
        this.incoming = null;
    }

    /**
     * Runs the thread and implements the RAPTOR Test Server communications protocol.
     * <pre>
     * Case 1:  RAPTOR requests a list of available tests
     * 
     *      RAPTOR client			Server
     *      DIRECTORY
     *                                          FILE1
     *                                          FILE2
     *                                          …
     *                                          EOF
     *
     * Case 2: RAPTOR requests unavailable test (RAPTOR sends the filename, 
     *         without the .rap extension)
     * 
     *      RAPTOR client			Server
     *      Filename
     *                                          INVALID COMMAND OR ASSIGNMENT
     *
     * Case 3: RAPTOR requests valid test file.  First RAPTOR sends filename, 
     *         then server responds with # of test files, followed by first test 
     *         file, followed by EOF.  RAPTOR then runs program and sends outputs.  
     *         When RAPTOR program finishes, EOF is sent.  Server then responds 
     *         with CORRECT or INCORRECT.  This is repeated for each test file.
     * 
     *      RAPTOR client			Server
     *      Filename
     *                                          ## (number of tests)
     *                                          Test1, Input1
     *                                          Test1, Input2
     *                                          …
     *                                          EOF
     *      Test1, Output1
     *      Test1, Output2
     *      …
     *      EOF
     *                                          CORRECT (or INCORRECT)
     *                              …
     *                                          TestN, Input1
     *                                          TestN, Input2
     *                                          …
     *                                          EOF
     *      TestN, Output1
     *      TestN, Output2
     *      …
     *      EOF
     *                                          CORRECT (or INCORRECT)
     * </pre>
     * 
     * @see <a
     * href="http://java.sun.com/javase/6/docs/api/java/lang/Runnable.html#run()">
     * run()</a>
     */
    @Override
    public void run()
    {
        try
        {

            OutputStreamWriter outWriter = new OutputStreamWriter(outStream, "utf-8");
            Scanner in = new Scanner(inStream);
            PrintWriter out = new PrintWriter(outWriter, true /* Auto Flush */);

            Response response = new Response();
            List<String> question_names = new ArrayList<String>();

            boolean done = false;
            boolean input_sent = false;
            boolean exclusionsAvailable = false;
            int numTestCases = 0;
            int tcMarker = 0;

            // Pre-load the list of available questions
            EntityTransaction tx = em.getTransaction();
            if (tx.isActive())
            {
                LOGGER.error("ERROR: Transaction is active");
            }

            /**
             * Database Query that pulls a list of all Questions in the database
             * ordered by Question name
             */
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Question> cq = cb.createQuery(Question.class);
            Root<Question> questionRoot = cq.from(Question.class);
            cq.orderBy(cb.asc(questionRoot.get("name")));
            TypedQuery<Question> nameQuery = em.createQuery(cq);
            tx.begin();
            List<Question> availQuestions = nameQuery.getResultList();
            tx.commit();

            LOGGER.info("Loaded " + availQuestions.size() + " questions.");

            for (Iterator i = availQuestions.iterator(); i.hasNext();)
            {
                Question question = (Question) i.next();
                question_names.add(question.getName().toUpperCase());
            }

            // Thread variables to track the Question and our location in it
            Question question = new Question();
            // Initialize the iterator with some dummy (aka empty) data
            // Iterator will be properly initialized when the question has been picked
            Iterator tcIterator = question.getTestCases().iterator();
            TestCase tc = new TestCase();

            while (!done)
            {
                String line;
                try
                {
                    line = in.nextLine();
                } catch (NoSuchElementException exc)
                {
                    line = null;
                }
                if (line == null)
                {
                    done = true;
                } else
                {
                    if (!input_sent && line.trim().equalsIgnoreCase("DIRECTORY"))
                    {
                        for (int i = 0; i < question_names.size(); i++)
                        {
                            out.print(question_names.get(i) + "\r\n");
                            out.flush();
                        }
                        out.print("EOF\r\n");
                        out.flush();
                        done = true;
                    } else if (!input_sent
                            && question_names.contains(line.trim().toUpperCase()))
                    {
                        for (int i = 0; i < availQuestions.size(); i++)
                        {
                            Question q = availQuestions.get(i);
                            // We can set the question here because we know that
                            // the question exists on the server--we're in this
                            // block
                            if (q.getName().equalsIgnoreCase(line.trim()))
                            {
                                question = q;
                            }
                        }
                        /**
                         * The following code must be wrapped in a transaction
                         * since the application will query the database to return
                         * the list of test cases associated with the Question.
                         */
                        tx.begin();
                        LOGGER.debug("Found Question: " + question.getName());
                        numTestCases = question.getTestCases().size();
                        out.print(numTestCases + "\r\n");
                        out.flush();
                        tcMarker = 1;
                        // Reinitialize iterator with actual data
                        tcIterator = question.getTestCases().iterator();
                        tc = (TestCase) tcIterator.next();
                        for (int i = 0; i < tc.getInputs().size(); i++)
                        {
                            out.print(tc.getInputs().get(i) + "\r\n");
                            out.flush();
                        }
                        tx.commit();
                        /**
                         * End of Transaction
                         */
                        out.print("EOF\r\n");
                        out.flush();
                        input_sent = true;
                        if (tc.getExcludes().size() > 0)
                        {
                            exclusionsAvailable = true;
                        }
                    } else if ((input_sent) && !line.trim().equals("EOF"))
                    {
                        // Save the responses received to an array (don't 
                        // persist at this time since these are not graded
                        if (!line.equals(""))
                        {
                            LOGGER.debug("Received: " + line.trim());
                            response.addAnswer(line.trim());
                        }
                    } else if ((input_sent) && line.trim().equals("EOF"))
                    {
                        // Evaluate the inputs received (stored in array) against
                        // the expected values stored in the database and send
                        // the appropriate response back to client
                        response.setResult(RageLib.gradeTestCase(tc, response,
                                question.getVerbatim(), exclusionsAvailable));
                        if (response.getResult())
                        {
                            out.print("CORRECT\r\n");
                            out.flush();
                            LOGGER.debug("Correct response received for "
                                    + question.getName());
                        } else
                        {
                            out.print("INCORRECT\r\n");
                            out.flush();
                            LOGGER.debug("Incorrect response received for "
                                    + question.getName());
                        }
                        if (numTestCases == tcMarker)
                        {
                            done = true;
                        } else
                        {
                            response.setResult(true);
                            tcMarker++;
                            input_sent = false;
                            /**
                             * The following code must be wrapped in a transaction
                             * since the application will query the database to return
                             * the list of inputs associated with the Test Case.
                             */
                            tx.begin();
                            tc = (TestCase) tcIterator.next();
                            for (int i = 0; i < tc.getInputs().size(); i++)
                            {
                                out.print(tc.getInputs().get(i) + "\r\n");
                                out.flush();
                            }
                            tx.commit();
                            /**
                             * End of Transaction
                             */
                            out.print("EOF\r\n");
                            out.flush();
                            input_sent = true;
                            response.clearAnswers();
                            exclusionsAvailable = false;
                            if (tc.getExcludes().size() > 0)
                            {
                                exclusionsAvailable = true;
                            }
                        }
                    } else
                    {
                        LOGGER.debug("Received from Client: " + line);
                        out.print("INVALID COMMAND OR ASSIGNMENT\r\n");
                        out.flush();
                        done = true;
                    }
                }
            }
            em.close();
            if (incoming != null)
            {
                LOGGER.info("Disconnect: " + incoming.getInetAddress().toString());
                incoming.close();
            }
        } catch (Exception e)
        {
            LOGGER.error("Error Caught: " + e.getLocalizedMessage());
        }
    }
    private Socket incoming;
    private InputStream inStream;
    private OutputStream outStream;
    private EntityManager em;
    private static final Logger LOGGER = Logger.getLogger(AGSThread.class);
}   

