/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.listmodel;

import com.darkhonor.rage.model.Instructor;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 * An implementation of an {@link AbstractListModel} to hold {@link Instructor}
 * objects.
 * 
 * @author Alexander Ackerman
 */
public class InstructorListModel extends AbstractListModel<Instructor>
{

    public InstructorListModel()
    {
        data = new ArrayList<Instructor>();
    }

    /**
     * Returns the number of Instructor objects in the ListModel.
     *
     * @return  The number of Instructor in the ListModel object
     */
    @Override
    public int getSize()
    {
        return data.size();
    }

    /**
     * Returns a generic Object from the ListModel.  Since the model contains
     * Instructor objects, it is safe to cast the result of this to a Instructor
     * object.
     *
     * @param index     The item in the list to return
     * @return          The identified Object.  This is castable to Student.
     */
    @Override
    public Instructor getElementAt(int index)
    {
        return data.get(index);
    }

    /**
     * Adds the provided Instructor to the ListModel object.
     *
     * @param instructor       The Instructor object to add to the ListModel
     */
    public void add(Instructor instructor)
    {
        data.add(instructor);
        this.fireContentsChanged(data, data.indexOf(instructor),
                data.lastIndexOf(instructor));
    }

    /**
     * Adds all elements of the provided List of Instructors to the ListModel object.
     * Since the model is a typed Object, an IllegalArgumentException will be
     * thrown if students is not a List of instructor objects.
     *
     * @param instructors      The List of Instructor objects
     * @throws IllegalArgumentException     Thrown if the first object in the
     *                              List is not a Instructor object.
     */
    public void addAll(List<Instructor> instructors) throws IllegalArgumentException
    {
        if (instructors == null
                || instructors.get(0).getClass() != Instructor.class)
        {
            throw new IllegalArgumentException();
        }
        data.addAll(instructors);
        this.fireContentsChanged(data, 0, data.size());
    }

    /**
     * Removes all Instructor objects in the ListModel object.
     */
    public void removeAll()
    {
        data.clear();
        this.fireIntervalRemoved(data, 0, 0);
    }

    /**
     * Removes the Instructor object from the list at the index identified.
     *
     * @param index     The index of the Instructor to remove from the model.
     * @return          The removed Instructor object
     */
    public Instructor remove(int index)
    {
        this.fireIntervalRemoved(data, index, index);
        return data.remove(index);
    }
    private final List<Instructor> data;
}
