/*
 * Copyright 2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataimport;

import com.darkhonor.rage.model.Course;
import org.apache.log4j.Logger;

/**
 * Import RAGE Data from a properly formatted XML file.
 * 
 * @author Alex Ackerman
 */
public class ImportXML implements ImportData
{
    @Override
    public Course importCourse(String filename)
    {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    private final Logger LOGGER = Logger.getLogger(ImportXML.class);
}
