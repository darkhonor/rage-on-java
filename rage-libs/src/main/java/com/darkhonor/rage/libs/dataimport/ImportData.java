/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataimport;

import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.Instructor;
import com.darkhonor.rage.model.Section;
import com.darkhonor.rage.model.Student;
import java.util.List;

/**
 * Public Interface methods to import data into the RAGE Database.  This class
 * can be implemented for the various import sources.
 *
 * @author Alex Ackerman
 */
public interface ImportData
{

    /**
     * Abstract method to import a list of Instructors.  The imported instructors
     * are returned as a List.
     *
     * @param filename      The filename of the source file
     * @return              The List of Instructor objects
     */
//    public List<Instructor> importInstructors(String filename);
    /**
     * Abstract method to import a list of Students.  The imported students are
     * returned as a List.
     *
     * @param filename      The filename of the source file
     * @return              The List of Instructor objects
     */
//    public List<Student> importStudents(String filename);
    /**
     * Abstract method to import a Course.  The items in the import include the
     * full list of Sections, Instructors, and Students.  The Graded Events may
     * be imported if the data is available.
     *
     * @param filename      The filename of the source file
     * @return              The List of Instructor objects
     */
    public Course importCourse(String filename);
    /**
     * Abstract method to import a list of Sections.  The imported sections are
     * returned as a List.
     *
     * @param filename      The filename of the source file
     * @return              The List of Instructor objects
     */
//    public List<Section> importSections(String filename);
}
