/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.GradedEvent;
import com.darkhonor.rage.model.Instructor;
import com.darkhonor.rage.model.Section;
import com.darkhonor.rage.model.Student;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;

/**
 * Class to persist the {@link com.darkhonor.rage.model.Course} to a data source.
 * Since the class uses a standard {@link EntityManager} connection to the data
 * source, the Course information can be saved in any format that is supported
 * by the JPA provider.
 *
 * @author Alex Ackerman
 */
public class CourseDAO
{

    /**
     * Default constructor.  At a minimum, an EntityManager must be provided.
     *
     * @param em        The EntityManager connection to the data source
     * @throws IllegalArgumentException     Thrown if the EntityManager is closed
     * @throws NullPointerException         Thrown if the EntityManager is
     *                                      <code>null</code>
     */
    public CourseDAO(EntityManager em) throws IllegalArgumentException,
            NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("EntityManager is closed.  Throwing IllegalArgumentException");
            throw new IllegalArgumentException("Invalid EntityManager state");
        } else
        {
            this.entityManager = em;
        }
    }

    /**
     * Persist the {@link com.darkhonor.rage.model.Course} to the data source.
     * All {@link com.darkhonor.rage.model.Instructor},
     * {@link com.darkhonor.rage.model.Student},
     * and {@link com.darkhonor.rage.model.Section} subelements will be persisted
     * at the same time.  If the Course exists in the data source and
     * IllegalStateException will be thrown.
     *
     * @param course    Create the Course in the data source
     * @return  <code>true</code> if the Course is saved successfully, <code>false</code>
     *          otherwise.
     * @throws IllegalStateException    If the EntityManager is closed or the
     *                                  Course already exists in the data source
     */
    public Long create(Course course) throws IllegalStateException
    {
        if (entityManager == null)
        {
            LOGGER.error("(create) EntityManager is null");
            throw new NullPointerException("EntityManager is null");
        } else if (!entityManager.isOpen())
        {
            LOGGER.error("(create) EntityManager is closed. Throwing new "
                    + "IllegalStateException");
            throw new IllegalStateException("EntityManager is not open");
        } else if (course == null)
        {
            LOGGER.error("(create) Course is null");
            throw new NullPointerException("Course is null");
        } else if (course.getId() != null && course.getId() <= 0)
        {
            LOGGER.error("(create) Course ID is an invalid value");
            throw new IllegalArgumentException("Course ID is an invalid value");
        } else if (course.getName() == null)
        {
            LOGGER.error("(create) Course Name is null");
            throw new NullPointerException("Course name is null");
        } else if (course.getName().isEmpty())
        {
            LOGGER.error("(create) Course Name is blank");
            throw new IllegalArgumentException("Course name is blank");
        } else
        {
            EntityTransaction tx = this.entityManager.getTransaction();
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();

            LOGGER.debug("(create) Searching for Course: " + course.getName());
            Course dbCourse = null;
            if (course.getId() != null)
            {
                // Lookup the Course in the DB by it's ID
                dbCourse = entityManager.find(Course.class, course.getId());
            } else
            {
                CriteriaQuery<Course> cq = cb.createQuery(Course.class);
                Root<Course> courseRoot = cq.from(Course.class);
                cq.where(cb.equal(courseRoot.get("name"), course.getName()));
                TypedQuery<Course> courseQuery = entityManager.createQuery(cq);
                try
                {
                    dbCourse = courseQuery.getSingleResult();
                } catch (NoResultException ex)
                {
                    dbCourse = null;
                }
            }
            if (dbCourse != null)
            {
                LOGGER.info("(create) Course exists in the data source.");
                if (dbCourse.getInstructors().size() > 0
                        || dbCourse.getSections().size() > 0)
                {
                    LOGGER.error("(create) Course has not been cleared.  It has "
                            + dbCourse.getInstructors().size() + " Instructors and "
                            + dbCourse.getSections().size() + " Sections");
                    throw new IllegalArgumentException("Course exists in the database");
                } else
                {
                    LOGGER.info("(create) Course has been cleared previously");
                }
            } else
            {
                LOGGER.info("(create) Course doesn't exist in the data source");
                dbCourse = new Course();
                if (course.getId() != null)
                {
                    dbCourse.setId(course.getId());
                }
                LOGGER.debug("(create) Persisting new Course");
                tx.begin();
                entityManager.persist(dbCourse);
                tx.commit();
                LOGGER.debug("(create) Course ID (Created): " + dbCourse.getId());
            }

            LOGGER.debug("(create) Copying name and Course Director");
            dbCourse.setName(course.getName());
            dbCourse.setCourseDirector(course.getCourseDirector());

            LOGGER.debug("(create) Looping through " + course.getInstructors().size()
                    + " instructors");
            for (Instructor instructor : course.getInstructors())
            {
                Instructor dbInstructor = null;
                if (instructor.getId() != null)
                {
                    dbInstructor = entityManager.find(Instructor.class,
                            instructor.getId());
                } else
                {
                    CriteriaQuery<Instructor> cq = cb.createQuery(Instructor.class);
                    Root<Instructor> instructorRoot = cq.from(Instructor.class);
                    cq.where(cb.and(cb.equal(instructorRoot.get("firstName"),
                            instructor.getFirstName()),
                            cb.equal(instructorRoot.get("lastName"),
                            instructor.getLastName()),
                            cb.equal(instructorRoot.get("domainAccount"),
                            instructor.getDomainAccount())));
                    TypedQuery<Instructor> instructorQuery =
                            entityManager.createQuery(cq);
                    try
                    {
                        dbInstructor = instructorQuery.getSingleResult();
                    } catch (NoResultException ex)
                    {
                        dbInstructor = null;
                    }
                }
                if (dbInstructor != null)
                {
                    LOGGER.debug("(create) Instructor " + dbInstructor.getLastName()
                            + ", " + dbInstructor.getFirstName()
                            + " exists in DB.  Adding to course");
                    LOGGER.debug("(create) Instructor ID: " + dbInstructor.getId());
                } else
                {
                    LOGGER.debug("(create) Instructor " + instructor.getLastName()
                            + ", " + instructor.getFirstName()
                            + " doesn't exist in DB.  Adding to course");
                    dbInstructor = new Instructor(instructor.getFirstName(),
                            instructor.getLastName(), instructor.getWebID());
                    dbInstructor.setDomainAccount(instructor.getDomainAccount());
                    if (instructor.getId() != null)
                    {
                        dbInstructor.setId(instructor.getId());
                    }
                    try
                    {
                        tx.begin();
                        this.entityManager.persist(dbInstructor);
                        tx.commit();
                    } catch (Exception ex)
                    {
                        LOGGER.error("(create) Exception Caught: "
                                + ex.getLocalizedMessage());
                        throw new IllegalStateException(ex.getLocalizedMessage());
                    }
                    LOGGER.debug("(create) Instructor ID: " + dbInstructor.getId());
                }
                dbCourse.addInstructor(dbInstructor);
                tx.begin();
                dbCourse = entityManager.merge(dbCourse);
                tx.commit();
                LOGGER.debug("(create) Added Instructor to Course: "
                        + dbCourse.getId());
            }  // Loop through Course Instructors

            LOGGER.debug("(create) Looping through " + course.getSections().size()
                    + " sections");
            for (Section section : course.getSections())
            {
                LOGGER.debug("(create) Section: " + section);
                Instructor dbInstructor = null;
                CriteriaQuery<Instructor> cq = cb.createQuery(Instructor.class);
                Root<Instructor> instructorRoot = cq.from(Instructor.class);
                cq.where(cb.and(cb.equal(instructorRoot.get("firstName"),
                        section.getInstructor().getFirstName()),
                        cb.equal(instructorRoot.get("lastName"),
                        section.getInstructor().getLastName()),
                        cb.equal(instructorRoot.get("domainAccount"),
                        section.getInstructor().getDomainAccount())));
                TypedQuery<Instructor> instructorQuery =
                        entityManager.createQuery(cq);
                try
                {
                    dbInstructor = instructorQuery.getSingleResult();
                    LOGGER.debug("(create) Instructor for Section " + section
                            + " found: " + dbInstructor.getLastName() + ", "
                            + dbInstructor.getFirstName());
                    LOGGER.debug("(create) Instructor ID: " + dbInstructor.getId());
                } catch (NoResultException ex)
                {
                    dbInstructor = null;
                    LOGGER.error("(create) Section " + section
                            + " Instructor not found in DB");
                    throw new IllegalStateException("Instructors not added to DB "
                            + "properly");
                }

                LOGGER.debug("(create) Determining if Section is already in DB");
                Section dbSection = null;
                CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
                Root<Section> sectionRoot = cqSection.from(Section.class);
                cqSection.where(cb.and(cb.equal(sectionRoot.get("course"), dbCourse)),
                        cb.equal(sectionRoot.get("name"), section.getName()));
                TypedQuery<Section> sectionQuery =
                        entityManager.createQuery(cqSection);
                try
                {
                    dbSection = sectionQuery.getSingleResult();
                    LOGGER.debug("(create) Section found.  ID: " + dbSection.getId());
                } catch (NoResultException ex)
                {
                    LOGGER.debug("(create) Section " + section.getName()
                            + " doesn't exist in the database.  Adding.");
                    dbSection = new Section(dbCourse, section.getName(), dbInstructor);
                    if (section.getId() != null)
                    {
                        dbSection.setId(section.getId());
                    }
                    tx.begin();
                    entityManager.persist(dbSection);
                    dbCourse = entityManager.merge(dbCourse);
                    dbInstructor = entityManager.merge(dbInstructor);
                    tx.commit();
                    LOGGER.debug("(create) Section added.  ID: " + dbSection.getId());
                }

                /**
                 * All sections for this course were removed previously, so we don't
                 * need to query the database for potential merged sections.  We
                 * do need to look for potential existing students and update the
                 * students accordingly.
                 */
                LOGGER.debug("(create) Looping through " + section.getStudents().size()
                        + " students");
                for (Student student : section.getStudents())
                {
                    LOGGER.debug("(create) Querying for Student: " + student.getWebID()
                            + ", " + student.getFirstName() + ", "
                            + student.getLastName() + " (" + student.getClassYear()
                            + ")");
                    Student dbStudent = null;
                    CriteriaQuery<Student> cqStudent = cb.createQuery(Student.class);
                    Root<Student> studentRoot = cqStudent.from(Student.class);
                    cqStudent.where(cb.and(
                            cb.equal(studentRoot.get("webID"), student.getWebID()),
                            cb.equal(studentRoot.get("firstName"),
                            student.getFirstName()),
                            cb.equal(studentRoot.get("lastName"),
                            student.getLastName()),
                            cb.equal(studentRoot.get("classYear"),
                            student.getClassYear())));
                    TypedQuery<Student> studentQuery =
                            entityManager.createQuery(cqStudent);

                    try
                    {
                        dbStudent = studentQuery.getSingleResult();
                        LOGGER.debug("(create) Student " + dbStudent.getLastName()
                                + ", " + dbStudent.getFirstName() + " ("
                                + dbStudent.getClassYear() + ") does exist in DB.  "
                                + "Adding to Section " + dbSection);
                        LOGGER.debug("(create) Student ID: " + dbStudent.getId());
                    } // Student in Database
                    catch (NoResultException ex)
                    {
                        LOGGER.debug("(create) Student " + student.getLastName()
                                + ", " + student.getFirstName() + " ("
                                + student.getClassYear() + ") doesn't exist in DB.  "
                                + "Adding to Section " + dbSection);
                        dbStudent = new Student(student.getFirstName(),
                                student.getLastName(), student.getWebID(),
                                student.getClassYear());
                        if (student.getId() != null)
                        {
                            dbStudent.setId(student.getId());
                        }
                        tx.begin();
                        entityManager.persist(dbStudent);
                        tx.commit();
                        LOGGER.debug("(create) Student ID: " + dbStudent.getId());
                    } // Student not in Database
                    dbSection.addStudent(dbStudent);
                    tx.begin();
                    dbSection = entityManager.merge(dbSection);
                    tx.commit();
                }  // Loop through Students
                dbCourse.addSection(dbSection);
                tx.begin();
                dbCourse = entityManager.merge(dbCourse);
                //entityManager.persist(dbSection);
                //dbSection = entityManager.merge(dbSection);
                tx.commit();
                LOGGER.debug("(create) Course ID (Added Section " + dbSection
                        + "): " + dbCourse.getId() + "; Section Course ID: "
                        + dbSection.getCourse().getId());
            }  // Loop through Course Sections

            LOGGER.debug("- Instructors: " + dbCourse.getInstructors().size());
            LOGGER.debug("- Sections: " + dbCourse.getSections().size());

            tx.begin();
            dbCourse = entityManager.merge(dbCourse);
            tx.commit();
            LOGGER.debug("(create) Course " + dbCourse.getName() + " ("
                    + dbCourse.getId() + ") saved to database");

            return dbCourse.getId();
        }
    } // public Long create(Course);

    /**
     * Removes the {@link com.darkhonor.rage.model.Course} from the data source.
     * It will remove the connection to any {@link com.darkhonor.rage.model.Instructor}
     * assigned to the course, but the Instructors will remain in the database.
     * All {@link com.darkhonor.rage.model.Section} will be removed from the
     * database, but the related {@link com.darkhonor.rage.model.Student} objects
     * will be unlinked and not deleted.  All {@link com.darkhonor.rage.model.GradedEvent}
     * objects will be removed from the database, but any
     * {@link com.darkhonor.rage.model.Question} that were associated with the
     * GradedEvent will not be deleted.
     *
     * @param course        The Course to delete from the data source
     * @throws IllegalStateException    If the EntityManager is closed
     */
    public void delete(Course course) throws IllegalStateException, NullPointerException,
            IllegalArgumentException
    {
        if (entityManager == null)
        {
            LOGGER.error("(delete) EntityManager is null.  Throwing "
                    + "NullPointerException.");


            throw new NullPointerException("EntityManager is null");


        } else if (!entityManager.isOpen())
        {
            LOGGER.error("(delete) EntityManager is closed.  Throwing "
                    + "IllegalStateException");


            throw new IllegalStateException("Invalid EntityManager state");


        } else if (course == null)
        {
            LOGGER.error("(delete) Course is null.  Throwing NullPointerException.");


            throw new NullPointerException("Course is null");


        } else if (course.getName() == null)
        {
            LOGGER.error("(delete) Course name is null.");


            throw new NullPointerException("Course name is null");


        } else if (course.getName().isEmpty())
        {
            LOGGER.error("(delete) Course name is blank.");


            throw new IllegalArgumentException("Course name is blank");


        } else if (course.getId() != null && course.getId() <= 0)
        {
            LOGGER.error("(delete) Course ID contains an invalid value");


            throw new IllegalArgumentException("Course ID contains an invalid value");


        } else
        {
            LOGGER.debug("(delete) Provided Course ID:Name: " + course.getId() + ":"
                    + course.getName());
            EntityTransaction tx = entityManager.getTransaction();

            Course deletedCourse = null;


            if (course.getId() == null)
            {
                // Need to look up by name since Id is blank
                CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                CriteriaQuery<Course> cq = cb.createQuery(Course.class);
                Root<Course> courseRoot = cq.from(Course.class);
                cq.where(cb.equal(courseRoot.get("name"), course.getName()));
                TypedQuery<Course> courseQuery = entityManager.createQuery(cq);








                try
                {
                    tx.begin();
                    deletedCourse = courseQuery.getSingleResult();
                    tx.commit();
                } catch (NoResultException ex)
                {
                    deletedCourse = null;
                }
            } else  // Course Id isn't null, go ahead and look it up the easy way
            {
                tx.begin();
                deletedCourse = entityManager.find(Course.class, course.getId());
                tx.commit();
            }


            if (deletedCourse == null)
            {
                LOGGER.error("(delete) Provided Course does not exist in the data "
                        + "source");


                throw new IllegalArgumentException("Provided course does not exist "
                        + "in the data source");


            }

            LOGGER.debug("(delete) Clearing association of Instructors, Students, "
                    + "and Sections");
            clearCourse(
                    deletedCourse);

            /**
             * Remove all GradedEvents associated with the course, but ensure all
             * Questions that were associated with those GradedEvents remain in the
             * database.
             */
            LOGGER.debug("(delete) Find all GradedEvents associated with "
                    + deletedCourse);
            GradedEventDAO gradedEventDAO = new GradedEventDAO(entityManager);
            List<GradedEvent> gradedEvents =
                    gradedEventDAO.getGradedEventsForCourse(deletedCourse);


            if (gradedEvents.size() > 0)
            {
                for (GradedEvent event : gradedEvents)
                {
                    gradedEventDAO.delete(event);


                }
            }
            /**
             * Don't close the gradedEventDAO connection since it's sharing the
             * connection with the CourseDAO instance.
             */
            /**
             * All data with course has been updated.  Removing course
             */
            LOGGER.debug("(delete) Removing Course " + deletedCourse + " from the "
                    + "data source");
            tx.begin();
            entityManager.remove(deletedCourse);
            tx.commit();
            LOGGER.info("(delete) Course Removed");


        }
    }  // public boolean delete(Course)

    public void delete(Long id) throws NullPointerException, IllegalStateException,
            IllegalArgumentException
    {
        if (entityManager == null)
        {
            LOGGER.error("(delete) EntityManager is null.  Throwing "
                    + "NullPointerException.");


            throw new NullPointerException("EntityManager is null");


        } else if (!entityManager.isOpen())
        {
            LOGGER.error("(delete) EntityManager is closed.  Throwing "
                    + "IllegalStateException");


            throw new IllegalStateException("Invalid EntityManager state");


        } else if (id == null)
        {
            LOGGER.error("(delete) Id is null.  Throwing NullPointerException.");


            throw new NullPointerException("Id is null");


        } else if (id <= 0)
        {
            LOGGER.error("(delete) Course ID contains an invalid value");


            throw new IllegalArgumentException("Course ID contains an invalid value");


        } else
        {
            LOGGER.debug("(delete) Id (" + id + ") is a valid value.  Checking "
                    + "for existence");
            EntityTransaction tx = entityManager.getTransaction();
            tx.begin();
            Course deletedCourse = entityManager.find(Course.class, id);
            tx.commit();





            if (deletedCourse == null)
            {
                LOGGER.error("(delete) Provided Course Id does not exist in the data "
                        + "source");
                throw new IllegalArgumentException("Provided course Id does not exist "
                        + "in the data source");
            }

            LOGGER.debug("(delete) Found course: "
                    + deletedCourse.getName());
            LOGGER.debug(
                    "(delete) Clearing association of Instructors, Students, "
                    + "and Sections");
            clearCourse(deletedCourse);

            /**
             * Remove all GradedEvents associated with the course, but ensure all
             * Questions that were associated with those GradedEvents remain in the
             * database.
             */
            LOGGER.debug(
                    "(delete) Find all GradedEvents associated with "
                    + deletedCourse);
            GradedEventDAO gradedEventDAO = new GradedEventDAO(entityManager);
            List<GradedEvent> gradedEvents =
                    gradedEventDAO.getGradedEventsForCourse(deletedCourse);










            if (gradedEvents.size() > 0)
            {
                for (GradedEvent event : gradedEvents)
                {
                    gradedEventDAO.delete(event);
                }
            }
            /**
             * Don't close the gradedEventDAO connection since it's sharing the
             * connection with the CourseDAO instance.
             */
            /**
             * All data with course has been updated.  Removing course
             */
            LOGGER.debug(
                    "(delete) Removing Course " + deletedCourse + " from the "
                    + "data source");
            tx.begin();
            entityManager.remove(deletedCourse);
            tx.commit();
            LOGGER.info(
                    "(delete) Course Removed");
        }


    }

    /**
     * Closes the connection to the data source.
     *
     * @throws IllegalStateException    If the EntityManager is <code>null</code>
     *                                  or not open.
     */
    public void closeConnection() throws IllegalStateException
    {
        this.entityManager.close();


    }  // public boolean closeConnection()

    /**
     * Opens a new connection to the database.  This is provided in the event
     * the closeConnection() method is called prematurely.
     *
     * @param em                        New EntityManager connection to the data source
     * @return      <code>true</code> if the EntityManager is opened successfully.
     *              An exception will be thrown otherwise.
     * @throws NullPointerException     If the EntityManager is <code>null</code>
     * @throws IllegalArgumentException If the EntityManager is not open.
     */
    public boolean openConnection(EntityManager em) throws NullPointerException,
            IllegalArgumentException
    {
        /**
         * Check for validity of the provided connection.
         */
        if (em == null)
        {
            LOGGER.error("(openConnection) EntityManager is null.  Throwing "
                    + "NullPointerException.");


            throw new NullPointerException("EntityManager is null");


        } else if (!em.isOpen())
        {
            LOGGER.error("(openConnection) EntityManager is closed.  Throwing "
                    + "IllegalArgumentException");


            throw new IllegalArgumentException("Invalid EntityManager state");


        } else
        {
            /**
             * Close any existing connection to the database before opening a new
             * one.
             */
            try
            {
                closeConnection();
                LOGGER.debug("(openConnection) EntityManager was open.  Setting null.");


                this.entityManager = null;


            } catch (IllegalStateException ex)
            {
                LOGGER.debug("(openConnection) EntityManager was already closed.");


            }
            this.entityManager = em;


        }
        return true;


    }

    /**
     * Inspector used to determine whether or not the EntityManager is open.
     *
     * @return  <code>true</code> if the EntityManager is open, <code>false</code>
     *          otherwise.
     */
    public boolean isOpen()
    {
        return this.entityManager.isOpen();


    }

    /**
     * Helper function to clear the following elements of the course:
     * {@link com.darkhonor.rage.model.Instructor}, 
     * {@link com.darkhonor.rage.model.Student}, and
     * {@link com.darkhonor.rage.model.Section}.  All
     * {@link com.darkhonor.rage.model.GradedEvent} assigned to the Course
     * will remain.
     * 
     * @param course        Clear the Course information in the data source
     * @return  <code>true</code> if the elements are cleared, <code>false</code>
     *          otherwise.
     * @throws IllegalStateException    If the EntityManager is closed
     * @throws IllegalArgumentException Thrown if the Course does not exist in the
     *                                  data source
     */
    public boolean clearCourse(Course course) throws IllegalStateException
    {
        if (entityManager == null)
        {
            LOGGER.error("(clearCourse) EntityManager is null");


            throw new NullPointerException("EntityManager is null");


        } else if (!entityManager.isOpen())
        {
            LOGGER.error("(clearCourse) EntityManager is closed. Throwing new "
                    + "IllegalStateException");


            throw new IllegalStateException("EntityManager is not open");


        } else if (course == null)
        {
            LOGGER.error("(clearCourse) Course is null");


            throw new NullPointerException("Course is null");


        } else if (course.getId() == null || course.getId() < 0)
        {
            LOGGER.error("(clearCourse) Course ID is null or an invalid value");


            throw new IllegalArgumentException("Course ID is null or an invalid value");


        } else
        {
            EntityTransaction tx = this.entityManager.getTransaction();

            // Find the Course in the Database
            Course dbCourse = entityManager.find(Course.class, course.getId());







            if (dbCourse == null)
            {
                LOGGER.error("(clearCourse) Course does not exist in the data source");
                throw new IllegalArgumentException("Course does not exist in the "
                        + "data source");
            }
            int dbSectSize = dbCourse.getSections().size();
            int crsSectSize = course.getSections().size();

            LOGGER.debug("(clearCourse) # of Sections in Course: "
                    + dbSectSize + " / " + crsSectSize);

            /**
             * Remove the link between all courses and their instructors, but keep
             * the instructors in the database
             */
            int insSize = dbCourse.getInstructors().size();
            LOGGER.debug(
                    "(clearCourse) # of Instructors in Course: "
                    + insSize);
            dbCourse.clearInstructors();

            LOGGER.debug(
                    "(clearCourse) Merging Course with Database");
            tx.begin();
            dbCourse = this.entityManager.merge(dbCourse);
            tx.commit();
            LOGGER.debug(
                    "(clearCourse)(Post) # of Instructors in Course: "
                    + dbCourse.getInstructors().size());

            /**
             * Remove all sections from the course, but ensure all Students stay
             * in the database
             */
            LOGGER.debug(
                    "(clearCourse) # of Sections in Course: "
                    + dbCourse.getSections().size() + " / "
                    + course.getSections().size());
            /**
             * For some reason, we have to go backwards through this list.  When
             * you go forwards, you only get 1 iteration through.  This way we are
             * moving towards the front of the list.  Odd, but I'll take it...
             */
            for (int i = course.getSections().size() - 1;
                    i
                    >= 0; i--)
            {
                LOGGER.debug("- Iteration " + i);
                Section section = course.getSection(i);
                LOGGER.debug("- Section: " + section + ", Id = " + section.getId());
                Section dbSection = entityManager.find(Section.class, section.getId());
                if (dbSection == null)
                {
                    LOGGER.error("(clearCourse) Section not found in data source");
                    throw new IllegalStateException("Section not found in data source");
                }
                LOGGER.debug("(clearCourse) Section " + dbSection + " found in the DB");
                LOGGER.debug("(clearCourse) Section ID: " + dbSection.getId());
                LOGGER.debug("(clearCourse) # Students in Section (Pre): "
                        + dbSection.getStudents().size());

                dbSection.clearStudents();
                tx.begin();
                dbSection = entityManager.merge(dbSection);
                entityManager.flush();
                tx.commit();
                LOGGER.debug("(clearCourse) # Students in Section (Post): "
                        + dbSection.getStudents().size());

                // Find the Instructor for the Section in the Database
                Instructor dbInstructor = entityManager.find(Instructor.class,
                        dbSection.getInstructor().getId());
                if (dbInstructor == null)
                {
                    LOGGER.error("(clearCourse) Section " + dbSection
                            + " Instructor not found in DB");
                    throw new IllegalStateException("Section Instructor not "
                            + "found in DB");
                }  // Instructor not found in Database

                LOGGER.debug("(clearCourse) Removing Instructor from Section "
                        + dbSection + ": " + dbSection.getInstructor().getDomainAccount());
                dbSection.removeInstructor();

                dbCourse.removeSection(dbSection);
                dbSection.removeCourse();

                try
                {
                    tx.begin();
                    LOGGER.debug("(clearCourse) Merging Instructor with Database");
                    dbInstructor = this.entityManager.merge(dbInstructor);
                    LOGGER.debug("(clearCourse) Merging Course with Database");
                    dbCourse = this.entityManager.merge(dbCourse);
                    dbSection = this.entityManager.merge(dbSection);
                    entityManager.flush();
                    tx.commit();
                    tx.begin();
                    LOGGER.debug("(clearCourse) Removing Section from Database");
                    this.entityManager.remove(dbSection);
                    LOGGER.debug("(clearCourse) Committing Transaction");
                    tx.commit();
                    LOGGER.debug("(clearCourse) Transaction Committed");
                } catch (Exception ex)
                {
                    LOGGER.error("Exception Caught: " + ex.getLocalizedMessage());
                }
            }  // Loop through Sections in Course

            LOGGER.debug("(clearCourse) Merging Course with Database");

            tx.begin();
            dbCourse = this.entityManager.merge(dbCourse);

            entityManager.flush();

            tx.commit();

            LOGGER.debug("(clearCourse)(Post) # of Sections in Course: "
                    + dbCourse.getSections().size());



            return true;
        }


    }  // public boolean clearCourse(Course)

    /**
     * Finds a Course object in the database with the given Id value.  The database
     * connection will still be active at the end of this method call.
     * 
     * @param id                        The Id of the Course to find
     * @return                          The selected Course if found or
     *                                  <code>null</code> otherwise
     * @throws IllegalArgumentException Thrown when the Id is an invalid value--
     *                                  negative or 0.
     * @throws NullPointerException     Thrown when the Id is <code>null</code>
     * @throws IllegalStateException    Thrown if the connection to data source is not
     *                                  open
     */
    public Course find(Long id) throws IllegalArgumentException, NullPointerException,
            IllegalStateException
    {
        if (id == null)
        {
            LOGGER.error("(find) The provided Course Id is null");


            throw new NullPointerException("The provided Course Id is null");


        } else if (id < 1)
        {
            LOGGER.error("(find) The provided Course Id is less than 1");


            throw new IllegalArgumentException("The provided Course Id is less than 1");


        } else if (!entityManager.isOpen())
        {
            LOGGER.error("(find) The connection to the data source is closed");


            throw new IllegalStateException("The connection to the data source is closed");


        } else
        {
            EntityTransaction tx = entityManager.getTransaction();
            tx.begin();
            Course course = entityManager.find(Course.class, id);
            tx.commit();




            return course;
        }


    }

    public Course find(String name) throws IllegalArgumentException, NullPointerException,
            IllegalStateException
    {
        if (entityManager == null)
        {
            LOGGER.error("(find) EntityManager is null");


            throw new NullPointerException("EntityManager is null");


        } else if (!entityManager.isOpen())
        {
            LOGGER.error("(find) EntityManager is closed. Throwing new "
                    + "IllegalStateException");


            throw new IllegalStateException("EntityManager is not open");


        } else if (name == null)
        {
            LOGGER.error("(find) Course name is null");


            throw new NullPointerException("Course name is null");


        } else if (name.isEmpty())
        {
            LOGGER.error("(find) Course Name is blank");


            throw new IllegalArgumentException("Course name is blank");


        } else
        {
            EntityTransaction tx = entityManager.getTransaction();

            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Course> cq = cb.createQuery(Course.class);
            Root<Course> courseRoot = cq.from(Course.class);
            cq.where(cb.equal(courseRoot.get("name"), name));
            TypedQuery<Course> courseQuery = entityManager.createQuery(cq);

            Course course = null;
            try
            {
                course = courseQuery.getSingleResult();
            } catch (NoResultException ex)
            {
                course = null;
            }
            return course;
        }

    }

    /**
     * Searches the data source for a list of all Courses and returns a List
     * of those courses sorted by Course name in ascending order.
     *
     * @return                          The List of Courses in the data source
     * @throws NullPointerException     Thrown when the EntityManager is <code>null</code>
     * @throws IllegalStateException    Thrown when the EntityManager is closed
     */
    public List<Course> getAllCoursesByName() throws NullPointerException,
            IllegalStateException
    {
        if (entityManager == null)
        {
            LOGGER.error("(getAllCoursesByName) EntityManager is null.  Throwing "
                    + "NullPointerException.");


            throw new NullPointerException("EntityManager is null");


        } else if (!entityManager.isOpen())
        {
            LOGGER.error("(getAllCoursesByName) Connection to data source is closed");


            throw new IllegalStateException("Connection to data source is closed");


        } else
        {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Course> cq = cb.createQuery(Course.class);
            Root<Course> courseRoot = cq.from(Course.class);
            cq.orderBy(cb.asc(courseRoot.get("name")));
            TypedQuery<Course> courseQuery = entityManager.createQuery(cq);
            return courseQuery.getResultList();
        }


    }

    public Course update(Course course) throws NullPointerException, IllegalStateException,
            IllegalArgumentException
    {
        if (entityManager == null)
        {
            LOGGER.error("(update) EntityManager is null");


            throw new NullPointerException("EntityManager is null");


        } else if (!entityManager.isOpen())
        {
            LOGGER.error("(update) EntityManager is closed. Throwing new "
                    + "IllegalStateException");


            throw new IllegalStateException("EntityManager is not open");


        } else if (course == null)
        {
            LOGGER.error("(update) Course is null");


            throw new NullPointerException("Course is null");


        } else if (course.getId() == null)
        {
            LOGGER.error("(update) Course Id is null.");


            throw new NullPointerException("Course Id is null");


        } else if (course.getId() <= 0)
        {
            LOGGER.error("(update) Course ID is an invalid value");


            throw new IllegalArgumentException("Course ID is an invalid value");


        } else if (course.getName() == null)
        {
            LOGGER.error("(update) Course Name is null");


            throw new NullPointerException("Course name is null");


        } else if (course.getName().isEmpty())
        {
            LOGGER.error("(update) Course Name is blank");


            throw new IllegalArgumentException("Course name is blank");


        } else
        {
            // Find the Course in the Data Source
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            EntityTransaction tx = entityManager.getTransaction();

            LOGGER.debug("(update) Finding Course: " + course.getId());
            Course dbCourse = entityManager.find(Course.class, course.getId());
            LOGGER.debug(
                    "(update) After Searching");





            if (dbCourse == null)
            {
                LOGGER.error("(update) Provided Course does not exist in the data "
                        + "source");
                throw new IllegalArgumentException("Provided course does not exist "
                        + "in the data source");
            }

            LOGGER.debug("(update) Found course ("
                    + dbCourse.getName() + ") in the "
                    + "data source.  Updating fields:");
            // Make the changes to the Course
            LOGGER.debug(
                    "- New Name: " + course.getName());
            dbCourse.setName(course.getName());




            if (course.getCourseDirector() != null)
            {
                LOGGER.debug("- Course Director not null");
                LOGGER.debug("- New Course Director: "
                        + course.getCourseDirector().getDomainAccount());
                Instructor dbCourseDir = null;
                if (course.getCourseDirector().getId() != null)
                {
                    dbCourseDir = entityManager.find(Instructor.class,
                            course.getCourseDirector().getId());
                } else  // Course Director doesn't have an Id...search by Domain Acct
                {
                    CriteriaQuery<Instructor> cq = cb.createQuery(Instructor.class);
                    Root<Instructor> instructorRoot = cq.from(Instructor.class);
                    cq.where(cb.equal(instructorRoot.get("domainAccount"),
                            course.getCourseDirector().getDomainAccount()));
                    TypedQuery<Instructor> instructorQuery = entityManager.createQuery(cq);
                    try
                    {
                        dbCourseDir = instructorQuery.getSingleResult();
                    } catch (NoResultException ex)
                    {
                        dbCourseDir = null;
                    }
                }
                if (dbCourseDir != null)
                {
                    LOGGER.debug("- New Course Director exists in Database");
                    dbCourse.setCourseDirector(dbCourseDir);
                } else
                {
                    LOGGER.debug("- New Course Director doesn't exist in Database");
                    dbCourse.setCourseDirector(course.getCourseDirector());
                }
            } else
            {
                LOGGER.debug("- Course Director is null");
            }
            // Update the list of associated Instructors
            if (course.getInstructors().isEmpty())
            {
                LOGGER.debug("- There are no Instructors associated with the course");
                dbCourse.clearInstructors();
            } else
            {
                LOGGER.debug("- Need to update instructors to match.  Clearing old");
                dbCourse.clearInstructors();
                LOGGER.debug("- Database instructors cleared");
                LOGGER.debug("- # of Instructors in new Course: "
                        + course.getInstructors().size());
                for (int i = 0; i < course.getInstructors().size(); i++)
                {
                    LOGGER.debug("- Looping through Instructors.  Instance " + i);
                    Instructor instructor = null;
                    if (course.getInstructor(i).getId() != null)
                    {
                        LOGGER.debug("- Instructor has id = "
                                + course.getInstructor(i).getId());
                        instructor = entityManager.find(Instructor.class,
                                course.getInstructor(i).getId());
                    } else
                    {
                        LOGGER.debug("- Instructor doesn't have an Id.  Domain Acct: "
                                + course.getInstructor(i).getDomainAccount());
                        CriteriaQuery<Instructor> cq = cb.createQuery(Instructor.class);
                        Root<Instructor> instructorRoot = cq.from(Instructor.class);
                        cq.where(cb.equal(instructorRoot.get("domainAccount"),
                                course.getInstructor(i).getDomainAccount()));
                        TypedQuery<Instructor> instructorQuery =
                                entityManager.createQuery(cq);
                        try
                        {
                            instructor = instructorQuery.getSingleResult();
                        } catch (NoResultException ex)
                        {
                            instructor = null;
                        }
                    }
                    if (instructor != null)
                    {
                        LOGGER.debug("- Adding existing instructor: "
                                + instructor.getDomainAccount());
                        dbCourse.addInstructor(instructor);
                    } else
                    {
                        LOGGER.debug("- Adding non-existent instructor: "
                                + course.getInstructor(i).getDomainAccount());
                        dbCourse.addInstructor(course.getInstructor(i));
                    }
                }
            }
            // Update the list of Sections
            if (course.getSections().isEmpty())
            {
                LOGGER.debug("- There are no Sections associated with the course");
                dbCourse.clearSections();
            } else
            {
                LOGGER.debug("- Need to update sections to match.  Clearing old");
                dbCourse.clearSections();
                LOGGER.debug("- Database sections cleared");
                LOGGER.debug("- # of Sections in new Course: "
                        + course.getSections().size());
                for (int i = 0; i < course.getSections().size(); i++)
                {
                    LOGGER.debug("- Looping through Sections.  Instance " + i);
                    Section section = null;
                    if (course.getSection(i).getId() != null)
                    {
                        LOGGER.debug("- Section has id = "
                                + course.getSection(i).getId());
                        section = entityManager.find(Section.class,
                                course.getSection(i).getId());
                    }
                    /**
                     * Don't need to look up Section by name since matching names
                     * can happen.  The Id is critical.
                     */
                    if (section != null)
                    {
                        LOGGER.debug("- Adding existing section: "
                                + section);
                        section.setName(course.getSection(i).getName());
                        dbCourse.addSection(section);
                    } else
                    {
                        LOGGER.debug("- Adding non-existent section: "
                                + course.getSection(i));
                        dbCourse.addSection(course.getSection(i));
                    }
                }
            }

            // Save the course back to the Data Source with the changes
            tx.begin();
            dbCourse = entityManager.merge(dbCourse);

            tx.commit();
            return dbCourse;
        }

    }
    private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CourseDAO.class);
}
