/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.listmodel;

import com.darkhonor.rage.model.Section;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 * Extension of the {@link AbstractListModel} class to hold {@link Section}
 * objects.
 * 
 * @author Alexander Ackerman
 */
public class SectionListModel extends AbstractListModel<Section>
{

    public SectionListModel()
    {
        data = new ArrayList<Section>();
    }

    @Override
    public int getSize()
    {
        return data.size();
    }

    @Override
    public Section getElementAt(int index)
    {
        return data.get(index);
    }

    public void add(Section section)
    {
        data.add(section);
        this.fireContentsChanged(data, data.indexOf(section),
                data.lastIndexOf(section));
    }

    public void addAll(List<Section> sections)
    {
        data.addAll(sections);
        this.fireContentsChanged(data, 0, data.size());
    }

    public void removeAll()
    {
        data.clear();
        this.fireIntervalRemoved(data, 0, 0);
    }

    public Section remove(int index)
    {
        this.fireIntervalRemoved(data, index, index);
        return data.remove(index);
    }

    public void sort()
    {
        Collections.sort(data);
    }
    
    private final List<Section> data;
}
