/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.GradedEvent;
import com.darkhonor.rage.model.Question;
import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;

/**
 * Class to handle all database queries for {@link com.darkhonor.rage.model.GradedEvent}
 * objects.
 *
 * @author Alex Ackerman
 */
public class GradedEventDAO
{

    /**
     * Default constructor.
     *
     * @param em    EntityManager connection to the database.
     * @throws IllegalArgumentException Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager is
     *                                  <code>null</code>
     */
    public GradedEventDAO(EntityManager em) throws IllegalArgumentException,
            NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("EntityManager is closed.  Throwing IllegalArgumentException");
            throw new IllegalArgumentException("Invalid EntityManager state");
        } else
        {
            this.em = em;
        }
    }

    /**
     * Finds a Graded Event object in the database with the given Id value.  The database
     * connection will still be active at the end of this method call.
     *
     * @param id                        The Id of the Graded Event to find
     * @return                          The selected Graded Event if found or
     *                                  <code>null</code> otherwise
     * @throws IllegalArgumentException Thrown when the Id is an invalid value--
     *                                  negative or 0.
     * @throws NullPointerException     Thrown when the Id is <code>null</code>
     * @throws IllegalStateException    Thrown if the connection to data source is not
     *                                  open
     */
    public GradedEvent find(Long id) throws IllegalArgumentException, NullPointerException,
            IllegalStateException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (id == null)
        {
            LOGGER.error("The provided Graded Event Id is null");
            throw new NullPointerException("The provided Graded Event Id is null");
        } else if (id < 1)
        {
            LOGGER.error("The provided Graded Event Id is less than 1");
            throw new IllegalArgumentException("The provided Graded Event Id is less than 1");
        } else if (!em.isOpen())
        {
            LOGGER.error("The connection to the data source is closed");
            throw new IllegalStateException("The connection to the data source is closed");
        }
        return em.find(GradedEvent.class, id);
    }

    /**
     * Returns a GradedEvent object that matches the given Course, Assignment 
     * name, Term, and Version.  Returns <code>null</code> if no result is found.
     * 
     * @param course        The assignment Course
     * @param assignment    The name of the Graded Event
     * @param term          The term the event is valid
     * @param version       The version of the Event
     * @return              The selected GradedEvent object or <code>null</code> 
     *                      if not found.
     * @throws IllegalArgumentException Thrown if the Course is not valid or any
     *                                  of the Strings are blank
     * @throws NullPointerException     Thrown if any of the arguments are 
     *                                  <code>null</code>
     * @throws IllegalStateException    Thrown if the connection to the data source
     *                                  is not open
     */
    public GradedEvent find(Course course, String assignment, String term, 
            String version) throws IllegalArgumentException, NullPointerException,
            IllegalStateException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("Connection to data source is closed");
            throw new IllegalStateException("Connection to data source is closed");
        } else if (course == null || assignment == null || term == null ||
                version == null)
        {
            LOGGER.error("A provided argument is null");
            throw new NullPointerException("A provided argument is null");
        } else if (course.getName() == null)
        {
            LOGGER.error("Improperly formed Course.  Name is null");
            throw new IllegalArgumentException("Malformed Course.  Name cannot be null");
        } else if (assignment.isEmpty() || term.isEmpty() || version.isEmpty())
        {
            LOGGER.error("The specified assignment name, term, or version is invalid");
            throw new IllegalArgumentException("The specified assignment name, term, "
                    + "or version is invalid");
        } else
        {
            // Build the Query
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<GradedEvent> cq = cb.createQuery(GradedEvent.class);
            Root<GradedEvent> gradedEventRoot = cq.from(GradedEvent.class);
            cq.where(cb.and(cb.equal(gradedEventRoot.get("assignment"), assignment),
                    cb.equal(gradedEventRoot.get("term"), term),
                    cb.equal(gradedEventRoot.get("course"), course),
                    cb.equal(gradedEventRoot.get("version"), version)));

            TypedQuery<GradedEvent> gradedEventQuery = em.createQuery(cq);

            // Run the query and return the located Graded Event
            try
            {
                return gradedEventQuery.getSingleResult();
            }
            catch (NoResultException ex)
            {
                LOGGER.info("GradedEvent does not found");
                return null;
            }
        }
    }
    
    public List<GradedEvent> getGradedEventsForCourse(Course course) throws
            IllegalArgumentException, NullPointerException, IllegalStateException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("Connection to data source is closed");
            throw new IllegalStateException("Connection to data source is closed");
        } else if (course == null)
        {
            LOGGER.error("The specified course is null");
            throw new NullPointerException("The specified course is null");
        } else if (course.getName() == null)
        {
            LOGGER.error("Improperly formed Course.  Name is null");
            throw new IllegalArgumentException("Malformed Course.  Name cannot be null");
        } else
        {
            // Build the Query
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<GradedEvent> cq = cb.createQuery(GradedEvent.class);
            Root<GradedEvent> gradedEventRoot = cq.from(GradedEvent.class);
            cq.where(cb.equal(gradedEventRoot.get("course"), course));
            cq.orderBy(cb.asc(gradedEventRoot.get("assignment")),
                    cb.asc(gradedEventRoot.get("version")));
            TypedQuery<GradedEvent> gradedEventQuery = em.createQuery(cq);

            // Run the query and return the resulting list of Graded Events
            return gradedEventQuery.getResultList();
        }
    }

    /**
     * Queries the data source for a list of all {@link GradedEvent} objects that
     * are stored for the provided Course and Term.
     * 
     * @param course                The specified Course
     * @param term                  The term for the Graded Events
     * @return                      A list of GradedEvent objects for the Course and
     *                              term
     * @throws IllegalArgumentException Thrown when the term is blank
     * @throws NullPointerException     Thrown when the term or course are
     *                                  <code>null</code>
     * @throws IllegalStateException    Thrown when the connection to the data source
     *                                  is closed
     */
    public List<GradedEvent> getGradedEventsForCourseAndTerm(Course course, String term)
            throws IllegalArgumentException, NullPointerException, IllegalStateException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("Connection to data source is closed");
            throw new IllegalStateException("Connection to data source is closed");
        } else if (course == null)
        {
            LOGGER.error("The specified course is null");
            throw new NullPointerException("The specified course is null");
        } else if (course.getName() == null)
        {
            LOGGER.error("Improperly formed Course.  Name is null");
            throw new IllegalArgumentException("Malformed Course.  Name cannot be null");
        } else if (term == null)
        {
            LOGGER.error("The specified term is null");
            throw new NullPointerException("The specified term is null");
        } else if (term.isEmpty())
        {
            LOGGER.error("The specified term is blank");
            throw new IllegalArgumentException("The specified term is blank");
        } else
        {
            // Build the Query
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<GradedEvent> cq = cb.createQuery(GradedEvent.class);
            Root<GradedEvent> gradedEventRoot = cq.from(GradedEvent.class);
            cq.where(cb.and(cb.equal(gradedEventRoot.get("course"), course),
                    cb.equal(gradedEventRoot.get("term"), term)));
            cq.orderBy(cb.asc(gradedEventRoot.get("assignment")),
                    cb.asc(gradedEventRoot.get("version")));
            TypedQuery<GradedEvent> gradedEventQuery = em.createQuery(cq);

            // Run the query and return the resulting list of Graded Events
            return gradedEventQuery.getResultList();
        }
    }

    /**
     * Returns the number of Versions for a given GradedEvent.
     *
     * @param assignment        The Graded Event to check
     * @return                  The number of Versions that exist of a given
     *                          GradedEvent
     * @throws NullPointerException     Thrown when the assignment is <code>null</code>
     * @throws IllegalStateException    Thrown when the connection to the Data Source
     *                                  is closed
     */
    public int getNumVersionsOfGradedEvent(GradedEvent assignment)
            throws IllegalArgumentException, NullPointerException, IllegalStateException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("Connection to data source is closed");
            throw new IllegalStateException("Connection to data source is closed");
        } else if (assignment == null)
        {
            LOGGER.error("The specified assignment is null");
            throw new NullPointerException("The specified assignment is null");
        } else if (assignment.getCourse() == null || assignment.getAssignment() == null
                || assignment.getTerm() == null)
        {
            LOGGER.error("Malformed Graded Event");
            throw new IllegalArgumentException("Malformed Graded Event.  Course, "
                    + "Assignment, and Term cannot be null");
        } else if (assignment.getAssignment().isEmpty() || assignment.getTerm().isEmpty())
        {
            LOGGER.error("Malformed GradedEvent.  Assignment or Term is blank");
            throw new IllegalArgumentException("Malformed Graded Event.  Assignment and"
                    + " term cannot be blank");
        }

        /**
         * Build the Query.  JPQL Equivalent:
         *      SELECT COUNT(g)
         *      FROM GradedEvent g
         *      WHERE g.course = :course AND
         *            g.term = :term AND
         *            g.assignment = :assignment
         */
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<GradedEvent> gradedEventRoot = cq.from(GradedEvent.class);
        cq.select(cb.count(gradedEventRoot));
        cq.where(cb.and(cb.equal(gradedEventRoot.get("course"), assignment.getCourse()),
                cb.equal(gradedEventRoot.get("term"), assignment.getTerm()),
                cb.equal(gradedEventRoot.get("assignment"), assignment.getAssignment())));

        // Run the query and return the resulting list of Graded Events
        return em.createQuery(cq).getSingleResult().intValue();
    }

    /**
     * Returns the a List of all versions for a given GradedEvent.
     *
     * @param assignment        The Graded Event to check
     * @return                  A List containing every version of the Graded Event
     * @throws NullPointerException     Thrown when the assignment is <code>null</code>
     * @throws IllegalStateException    Thrown when the connection to the Data Source
     *                                  is closed
     */
    public List<GradedEvent> getAllVersionsOfGradedEvent(GradedEvent assignment)
            throws IllegalArgumentException, NullPointerException, IllegalStateException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("Connection to data source is closed");
            throw new IllegalStateException("Connection to data source is closed");
        } else if (assignment == null)
        {
            LOGGER.error("The specified assignment is null");
            throw new NullPointerException("The specified assignment is null");
        } else if (assignment.getCourse() == null || assignment.getAssignment() == null
                || assignment.getTerm() == null)
        {
            LOGGER.error("Malformed GradedEvent. Course, Assignment, Term, and Version "
                    + "cannot be null");
            throw new IllegalArgumentException("Malformed Graded Event");
        } else if (assignment.getAssignment().isEmpty() || assignment.getTerm().isEmpty()
                || assignment.getVersion().isEmpty())
        {
            LOGGER.error("Malformed GradedEvent.  Assignment, Term, and Version cannot "
                    + "be blank");
            throw new IllegalArgumentException("Malformed Graded Event");
        }

        // Build the Query
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<GradedEvent> cq = cb.createQuery(GradedEvent.class);
        Root<GradedEvent> gradedEventRoot = cq.from(GradedEvent.class);
        cq.where(cb.and(cb.equal(gradedEventRoot.get("course"), assignment.getCourse()),
                cb.equal(gradedEventRoot.get("term"), assignment.getTerm()),
                cb.equal(gradedEventRoot.get("assignment"), assignment.getAssignment())));
        TypedQuery<GradedEvent> gradedEventQuery = em.createQuery(cq);

        // Run the query and return the resulting list of Graded Events
        return gradedEventQuery.getResultList();
    }

    /**
     * Queries the data source for a full list of {@link GradedEvent} objects stored
     * in the data source ordered by {@link Course} and Assignment name.
     * 
     * @return                      The list of Graded Events in the data source
     * @throws IllegalStateException    Thrown when the connection to the data source
     *                                  is closed
     */
    public List<GradedEvent> getGradedEventsByCourseAndAssignment()
            throws IllegalStateException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("Connection to data source is closed");
            throw new IllegalStateException("Connection to data source is closed");
        }

        // Build the Query
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<GradedEvent> cq = cb.createQuery(GradedEvent.class);
        Root<GradedEvent> gradedEventRoot = cq.from(GradedEvent.class);
        cq.orderBy(cb.asc(gradedEventRoot.get("course").get("name")),
                cb.asc(gradedEventRoot.get("assignment")),
                cb.asc(gradedEventRoot.get("version")));
        TypedQuery<GradedEvent> gradedEventQuery = em.createQuery(cq);

        // Run the query and return the resulting list of Graded Events
        return gradedEventQuery.getResultList();

    }

    /**
     * Returns whether or not the connection to the data source is open.
     *
     * @return      <code>true</code> if the connection is open, <code>false</code>
     *              otherwise
     * @throws NullPointerException     Thrown when the EntityManager is
     *                                  <code>null</code>
     */
    public boolean isOpen() throws NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        }
        return em.isOpen();
    }

    /**
     * Closes the connection to the database.
     *
     * @throws IllegalStateException    Thrown if the database connection is
     *                                  already closed.
     * @throws NullPointerException     Thrown when the EntityManager is
     *                                  <code>null</code>
     */
    public void closeConnection() throws IllegalStateException, NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        }
        em.close();
    }

    /**
     * Saves the provided GradedEvent to the data source.  This method assumes the
     * Question, TestCase, and Course objects contained in this GradedEvent have
     * all previously been saved to the data source and contain valid Id values.
     *
     * @param gradedEvent      The GradedEvent to save to the data source
     * @throws IllegalStateException    Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager or GradedEvent is
     *                                  <code>null</code>
     * @throws IllegalArgumentException Thrown when the GradedEvent exists in the
     *                                  data source
     * @return                           The Id of the created GradedEvent object
     */
    public Long create(GradedEvent gradedEvent) throws IllegalStateException,
            NullPointerException, IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("Connection to data source is closed");
            throw new IllegalStateException("Connection to data source is closed");
        } else if (gradedEvent == null)
        {
            LOGGER.error("GradedEvent is null.  Throwing NullPointerException");
            throw new NullPointerException("GradedEvent is null");
        } else if (gradedEvent.getCourse() == null || gradedEvent.getAssignment() == null
                || gradedEvent.getTerm() == null || gradedEvent.getVersion() == null)
        {
            LOGGER.error("Badly formed Graded Event.  Throwing IlegalArgumentException");
            throw new IllegalArgumentException("Badly formed Graded Event.");
        } else
        {
            EntityTransaction tx = em.getTransaction();
            try
            {
                tx.begin();
                em.persist(gradedEvent);
                tx.commit();
                LOGGER.debug("(create) GradedEvent created.  Id: "
                        + gradedEvent.getId());
                return gradedEvent.getId();
            } catch (EntityExistsException ex)
            {
                LOGGER.warn("GradedEvent already exists in the data source");
                throw new IllegalArgumentException("GradedEvent already exists "
                        + "in the data source");
            } catch (RollbackException ex)
            {
                LOGGER.warn("GradedEvent already exists in the data source");
                throw new IllegalArgumentException("GradedEvent already exists "
                        + "in the data source");
            }
        }
    }

    public GradedEvent update(GradedEvent gradedEvent) throws IllegalStateException,
            NullPointerException, IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(update) EntityManager is null.  Throwing "
                    + "NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(update) Connection to data source is closed");
            throw new IllegalStateException("Connection to data source is closed");
        } else if (gradedEvent == null)
        {
            LOGGER.error("(update) GradedEvent is null.  Throwing NullPointerException");
            throw new NullPointerException("GradedEvent is null");
        } else if (gradedEvent.getCourse() == null || gradedEvent.getAssignment() == null
                || gradedEvent.getTerm() == null || gradedEvent.getVersion() == null
                || gradedEvent.getId() == null)
        {
            LOGGER.error("(update) Badly formed Graded Event.  Throwing "
                    + "IlegalArgumentException");
            throw new IllegalArgumentException("Badly formed Graded Event.");
        } else
        {
            GradedEvent event = em.find(GradedEvent.class, gradedEvent.getId());
            if (event == null)
            {
                LOGGER.error("(update) GradedEvent not found to update.");
                throw new IllegalArgumentException("Graded Event not found to update");
            }
            EntityTransaction tx = em.getTransaction();

            event.setAssignment(gradedEvent.getAssignment());
            event.setCourse(gradedEvent.getCourse());

            // Due Date is optional, but will throw an exception if you try to set
            // a null value
            if (gradedEvent.getDueDate() != null)
            {
                event.setDueDate(gradedEvent.getDueDate());
            }

            event.setPartialCredit(gradedEvent.getPartialCredit());
            event.setTerm(gradedEvent.getTerm());
            event.setVersion(gradedEvent.getVersion());

            LOGGER.debug("update: # of Questions in event (pre-update): "
                    + event.getQuestions().size());

            GradedEvent tempEvent = new GradedEvent();

            int numQuestions = gradedEvent.getQuestions().size();
            LOGGER.debug("update: # of Questions in input Graded event: "
                    + numQuestions);

            int count = 0;
            try
            {
                for (int i = 0; i < numQuestions; i++)
                {
                    LOGGER.debug("i = " + i);
                    tempEvent.addQuestion(gradedEvent.getQuestion(i));
                    LOGGER.debug("update: Question " + i + ": "
                            + gradedEvent.getQuestion(i).getName());
                    count++;
                }
            } catch (IndexOutOfBoundsException ex)
            {
                LOGGER.error("Out of Bounds Exception caught: "
                        + ex.getLocalizedMessage());
            }

            LOGGER.debug("update: Added " + count + " Questions");
            event.clearQuestions();

            count = 0;
            try
            {
                for (int i = 0; i < numQuestions; i++)
                {
                    LOGGER.debug("i = " + i);
                    event.addQuestion(tempEvent.getQuestion(i));
                    LOGGER.debug("update: Question " + i + ": "
                            + tempEvent.getQuestion(i).getName());
                    count++;
                }
            } catch (IndexOutOfBoundsException ex)
            {
                LOGGER.error("Out of Bounds Exception caught: "
                        + ex.getLocalizedMessage());
            }

            LOGGER.debug("update: Added " + count + " Questions");

            LOGGER.debug("update: # of Questions in event (post-update): "
                    + event.getQuestions().size());

            tx.begin();
            event = em.merge(event);
            tx.commit();

            return event;
        }
    }

    public void delete(GradedEvent gradedEvent) throws IllegalStateException,
            NullPointerException, IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("Connection to data source is closed");
            throw new IllegalStateException("Connection to data source is closed");
        } else if (gradedEvent == null)
        {
            LOGGER.error("GradedEvent is null.  Throwing NullPointerException");
            throw new NullPointerException("GradedEvent is null");
        } else if (gradedEvent.getCourse() == null || gradedEvent.getAssignment() == null
                || gradedEvent.getTerm() == null || gradedEvent.getVersion() == null)
        {
            LOGGER.error("Badly formed Graded Event.  Throwing IlegalArgumentException");
            throw new IllegalArgumentException("Badly formed Graded Event.");
        } else
        {

            EntityTransaction tx = em.getTransaction();
            try
            {
                // Build the Query
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery<GradedEvent> cq = cb.createQuery(GradedEvent.class);
                Root<GradedEvent> gradedEventRoot = cq.from(GradedEvent.class);
                cq.where(cb.and(cb.equal(gradedEventRoot.get("course"),
                        gradedEvent.getCourse()),
                        cb.equal(gradedEventRoot.get("term"),
                        gradedEvent.getTerm()),
                        cb.equal(gradedEventRoot.get("assignment"),
                        gradedEvent.getAssignment()),
                        cb.equal(gradedEventRoot.get("version"),
                        gradedEvent.getVersion())));
                TypedQuery<GradedEvent> gradedEventQuery = em.createQuery(cq);

                tx.begin();
                GradedEvent result = gradedEventQuery.getSingleResult();
                tx.commit();

                tx.begin();
                em.remove(result);
                tx.commit();
            } catch (NoResultException ex)
            {
                LOGGER.error("GradedEvent not found in Data Source");
                throw new IllegalArgumentException("GradedEvent not found in Data Source");
            }
        }
    }

    public void delete(Long id) throws IllegalStateException, NullPointerException,
            IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("Connection to data source is closed");
            throw new IllegalStateException("Connection to data source is closed");
        } else if (id == null)
        {
            LOGGER.error("GradedEvent Id is null.  Throwing NullPointerException");
            throw new NullPointerException("GradedEvent Id is null");
        } else if (id < 1)
        {
            LOGGER.error("GradedEvent Id is less than 1.");
            throw new IllegalArgumentException("GradedEvent Id is less than 1");
        } else
        {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            GradedEvent result = em.find(GradedEvent.class, id);
            tx.commit();

            if (result == null)
            {
                LOGGER.error("GradedEvent not found in the Data Source");
                throw new IllegalArgumentException("GradedEvent not found in the "
                        + "Data Source");
            }

            tx.begin();
            em.remove(result);
            tx.commit();
        }
    }

    /**
     * Removes the provided Question from the specified GradedEvent in the data
     * source.  This method does not remove the Question from the data source.  It
     * will only detach the Question.  This method assumes the Question still
     * exists in the data source.  An exception will be thrown otherwise.
     *
     * @param gradedEvent               The GradedEvent to modify
     * @param question                  The Question to remove from the GradedEvent
     * @throws NullPointerException     Thrown when the Question, GradedEvent, or
     *                                  EntityManager are <code>null</code>
     * @throws IllegalArgumentException Thrown when the Questin or GradedEvent
     *                                  does not exist in the data source
     * @throws IllegalStateException    Thrown when the connection to the data source
     *                                  is closed.
     */
    public void removeQuestionFromGradedEvent(GradedEvent gradedEvent, Question question)
            throws NullPointerException, IllegalArgumentException, IllegalStateException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("Connection to data source is closed");
            throw new IllegalStateException("Connection to data source is closed");
        } else if (gradedEvent == null)
        {
            LOGGER.error("GradedEvent is null.  Throwing NullPointerException");
            throw new NullPointerException("GradedEvent is null");
        } else if (gradedEvent.getCourse() == null || gradedEvent.getAssignment() == null
                || gradedEvent.getTerm() == null || gradedEvent.getVersion() == null)
        {
            LOGGER.error("Badly formed Graded Event.  Throwing IlegalArgumentException");
            throw new IllegalArgumentException("Badly formed Graded Event.");
        } else if (question == null)
        {
            LOGGER.error("(removeQuestionFromGradedEvent) Question is null.  Throwing "
                    + "NullPointerException");
            throw new NullPointerException("Question is null");
        } else if (question.getName() == null)
        {
            LOGGER.error("(removeQuestionFromGradedEvent) Question Name is null. "
                    + "Throwing IllegalArgumentException");
            throw new IllegalArgumentException("Question Name is null");
        } else
        {
            EntityTransaction tx = em.getTransaction();
            GradedEvent result = null;
            if (gradedEvent.getId() != null)
            {
                result = em.find(GradedEvent.class, gradedEvent.getId());
            } else
            {
                try
                {
                    // Build the Query
                    CriteriaBuilder cb = em.getCriteriaBuilder();
                    CriteriaQuery<GradedEvent> cq = cb.createQuery(GradedEvent.class);
                    Root<GradedEvent> gradedEventRoot = cq.from(GradedEvent.class);
                    cq.where(cb.and(cb.equal(gradedEventRoot.get("course"),
                            gradedEvent.getCourse()),
                            cb.equal(gradedEventRoot.get("term"),
                            gradedEvent.getTerm()),
                            cb.equal(gradedEventRoot.get("assignment"),
                            gradedEvent.getAssignment()),
                            cb.equal(gradedEventRoot.get("version"),
                            gradedEvent.getVersion())));
                    TypedQuery<GradedEvent> gradedEventQuery = em.createQuery(cq);

                    result = gradedEventQuery.getSingleResult();
                } catch (NoResultException ex)
                {
                    LOGGER.error("GradedEvent not found in Data Source");
                    throw new IllegalArgumentException("GradedEvent not found in "
                            + "Data Source");
                }

            }

            if (result == null)
            {
                LOGGER.error("GradedEvent not found in the Data Source");
                throw new IllegalArgumentException("GradedEvent not found in the "
                        + "Data Source");
            }

            try
            {
                // Build the Query
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery<Question> cq = cb.createQuery(Question.class);
                Root<Question> questionRoot = cq.from(Question.class);
                cq.where(cb.equal(questionRoot.get("name"), question.getName()));
                TypedQuery<Question> questionQuery = em.createQuery(cq);

                Question resultQ = questionQuery.getSingleResult();
            } catch (NoResultException ex)
            {
                LOGGER.error("Question not found in Data Source");
                throw new IllegalArgumentException("Question not found in "
                        + "Data Source");
            }

            if (!result.getQuestions().contains(question))
            {
                LOGGER.error("Question not assigned to GradedEvent.  Raising "
                        + "IllegalArgumentException");
                throw new IllegalArgumentException("Question not assigned to "
                        + "GradedEvent");
            }

            result.removeQuestion(question);
            tx.begin();
            em.merge(result);
            tx.commit();
        }
    }

    /**
     * Removes the provided Question from all GradedEvents in the data source that
     * may have that Question included.  This method does not remove the Question
     * from the data source.  It will only detach the Question.  This method
     * assumes the Question still exists in the data source.  An exception will
     * be thrown otherwise.
     * 
     * @param question                  The Question to remove from all GradedEvents
     *                                  in the Data Source
     * @return                          The Number of GradedEvents modified
     * @throws NullPointerException     Thrown when the Question or the EntityManager
     *                                  is <code>null</code>
     * @throws IllegalArgumentException Thrown when the Question is not found in
     *                                  the Data Source
     * @throws IllegalStateException    Thrown when the connection to the data source
     *                                  is closed.
     */
    public int removeQuestionFromAllGradedEvents(Question question) throws
            NullPointerException, IllegalArgumentException, IllegalStateException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("Connection to data source is closed");
            throw new IllegalStateException("Connection to data source is closed");
        } else if (question == null)
        {
            LOGGER.error("(removeQuestionFromAllGradedEvents) Question is null.  Throwing "
                    + "NullPointerException");
            throw new NullPointerException("Question is null");
        } else if (question.getName() == null)
        {
            LOGGER.error("(removeQuestionFromAllGradedEvents) Question Name is null. "
                    + "Throwing IllegalArgumentException");
            throw new IllegalArgumentException("Question Name is null");
        } else
        {
            try
            {
                // Build the Query
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery<Question> cq = cb.createQuery(Question.class);
                Root<Question> questionRoot = cq.from(Question.class);
                cq.where(cb.equal(questionRoot.get("name"), question.getName()));
                TypedQuery<Question> questionQuery = em.createQuery(cq);

                Question resultQ = questionQuery.getSingleResult();
            } catch (NoResultException ex)
            {
                LOGGER.error("Question not found in Data Source");
                throw new IllegalArgumentException("Question not found in "
                        + "Data Source");
            }
            
            List<GradedEvent> gradedEvents = this.getGradedEventsByCourseAndAssignment();
            LOGGER.debug("(removeQuestionFromAllGradedEvents) Number of Events: " +
                    gradedEvents.size());
            
            EntityTransaction tx = em.getTransaction();
            
            int count = 0;
            LOGGER.debug("(removeQuestionFromAllGradedEvents) Looping...");
            for (int i = 0; i < gradedEvents.size(); i++)
            {
                LOGGER.debug("i = " + i);
                GradedEvent event = em.find(GradedEvent.class, gradedEvents.get(i).getId());
                if (event.getQuestions().contains(question))
                {
                    LOGGER.debug("(removeQuestionFromAllGradedEvents) Question in " +
                            "event " + event.getId());
                    LOGGER.debug("Questions in Event " + event.getId() + " (pre): " + 
                            event.getQuestions().size());
                    tx.begin();
                    event.removeQuestion(question);
                    event = em.merge(event);
                    tx.commit();
                    count++;
                    LOGGER.debug("Questions in Event " + event.getId() + " (post): " + 
                            event.getQuestions().size());
                }
            }
            LOGGER.debug("Question removed from " + count + " Graded Events");

            return count;
        }
    }
    private Logger LOGGER = Logger.getLogger(GradedEventDAO.class);
    private EntityManager em;
}
