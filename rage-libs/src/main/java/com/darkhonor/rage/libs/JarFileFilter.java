/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs;

import java.io.*;

/**
 * The JarFileFilter class implements a {@link FileFilter} that looks for JAR
 * files
 * 
 * @author Alexander Ackerman
 * 
 * @version 1.0.0
 * 
 * @see java.io.FileFilter
 */
public class JarFileFilter implements FileFilter
{

    /**
     * Compares the given file to see if it is a JAR file
     * 
     * @param pathname The file being filtered
     * @return <code>true</code> if the File is a JAR file and can be read
     *         by the user, <code>false</code> otherwise.
     */
    @Override
    public boolean accept(File pathname)
    {
        // 1.1.0: Changed filename to lowercase then checked for .rap extension
        if (pathname.canRead() && !pathname.isDirectory()
                && pathname.getName().toLowerCase().endsWith(".jar"))
        {
            return true;
        }
        return false;
    }
}
