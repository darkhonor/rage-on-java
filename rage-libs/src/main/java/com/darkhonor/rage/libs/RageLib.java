/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs;

import com.darkhonor.rage.libs.dataaccess.CategoryDAO;
import com.darkhonor.rage.libs.dataaccess.VersionDAO;
import com.darkhonor.rage.model.Category;
import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.GradedEvent;
import com.darkhonor.rage.model.Instructor;
import com.darkhonor.rage.model.Question;
import com.darkhonor.rage.model.Response;
import com.darkhonor.rage.model.Section;
import com.darkhonor.rage.model.SectionReport;
import com.darkhonor.rage.model.Student;
import com.darkhonor.rage.model.StudentReport;
import com.darkhonor.rage.model.TestCase;
import com.darkhonor.rage.model.Version;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import java.util.zip.*;
import org.apache.log4j.Logger;

/**
 * The RageLib class contains methods used throughout the RAGE on Java suite of
 * tools.
 * 
 * The purpose of this class is to help reduce redundancy and provide a single resource
 * of methods that can be used in each of the RAGE applicaitons.
 * 
 * @author Alexander Ackerman
 * 
 * @version 1.2.5
 */
public class RageLib
{

    public RageLib()
    {
    }

    /**
     * Checks the version of the Database an application is compatible with the
     * value stored in the database.  The EntityManager will be closed at the
     * end of the method.
     *
     * @param em            The EntityManager connection to the Database
     * @param appVersion    The Version from the Application
     * @return              <code>true</code> if the application version equals
     *                      the stored version, <code>false</code> otherwise.
     *
     * @deprecated      Do not use this method.
     */
    public static boolean checkDBVersion(EntityManager em, Version appVersion)
            throws IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("Illegal Argument: EntityManager is null");
            throw new IllegalArgumentException("EntityManager is null");
        } else
        {
            VersionDAO versionDAO = new VersionDAO(em);
            try
            {
                boolean result = versionDAO.checkDbVersion(appVersion);
                versionDAO.closeConnection();
                return result;
            }
            catch (IllegalStateException ex)
            {
                LOGGER.error("Illegal Database State.  EntityManager is closed");
                throw new IllegalArgumentException("EntityManager is closed");
            }
        }
    }

    /**
     * Evaluates a {@link Response} to a given {@link TestCase} with the 
     * identified limitations.
     * 
     * @param tc        The <code>TestCase</code> for the given question
     * @param response  The student's <code>Response</code> to the
     *                  <code>TestCase</code>
     * @param verbatim  <code>true</code> if the answer must match exactly,
     *                  <code>false</code> otherwise
     * @param exclusions <code>true</code> if there are words or phrases that
     *                  must not be in the response
     * @return          <code>true</code> if the student's Response matches
     *                  what was expected by the <code>TestCase</code>
     * 
     * @since 1.2.0
     */
    public static boolean gradeTestCase(TestCase tc, Response response,
            boolean verbatim, boolean exclusions)
    {
        if (tc.getOutputs().size() != response.getAnswers().size())
        {
            LOGGER.debug("Response Size: " + response.getAnswers().size());
            LOGGER.debug("Expected Size: " + tc.getOutputs().size());
            for (int l = 0; l < response.getAnswers().size(); l++)
            {
                LOGGER.debug("Response " + l + ": " + response.getAnswers().get(l));
            }
            return false;
        } else if (verbatim) // If the answer is to be exact
        {
            for (int i = 0; i < tc.getOutputs().size(); i++)
            {
                if (!response.getAnswers().get(i).toLowerCase().equals(tc.getOutputs().get(i).toLowerCase()))
                {
                    return false;
                }
                // Don't need to check for exclusions since 
                // we are looking for exact answer
            }
        } else                // Grade if response contains expected response
        {
            for (int i = 0; i < tc.getOutputs().size(); i++)
            {
                if (!response.getAnswers().get(i).toLowerCase().contains(tc.getOutputs().get(i).toLowerCase()))
                {
                    LOGGER.debug("Expected: " + tc.getOutputs().get(i).toLowerCase());
                    LOGGER.debug("\tReceived: " + response.getAnswers().get(i).toLowerCase());
                    return false;
                }
                if (exclusions)
                {
                    for (String s : tc.getExcludes())
                    {
                        if (response.getAnswers().get(i).toLowerCase().contains(s.toLowerCase()))
                        {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * Returns a String containing the name of the {@link Question} based upon
     * the Filename.
     * 
     * @param fileName  The filename
     * @return          The String containing the {@link Question}'s name
     * 
     * @since 1.0.0
     */
    public static String getQuestionNameFromFilename(String fileName)
    {
        // Modification made in 1.2.2 to make filename extension case insenstive
        if (fileName.toLowerCase().endsWith(".rap")
                || fileName.toLowerCase().endsWith(".jar"))
        {
            String[] tempString = fileName.split("\\.");
            if (tempString.length != 2)
            {
                return "ERROR in Filename: " + fileName;
            }
            return tempString[0];
        }
        return null;
    }

    /**
     * Obtain a list of all RAPTOR files in a specified directory path.  The method
     * will recursively search all subdirectories starting at <code>path</code>.
     * 
     * @param path  The directory containing the RAPTOR files
     * @return      An array of RAPTOR {@link File} objects
     * 
     * @since 1.0.0
     */
    public static File[] getRapFiles(File path)
    {
        File[] output = null;
        try
        {
            File rootDir = path.getCanonicalFile();
            if (!rootDir.isDirectory())
            {
                System.err.println("ERROR: This is not a directory.\n\t" + path);
                output = null;
            } else
            {
                // List all Raptor files in the given Root Directory
                // --recurse if needed
                output = listRapFiles(rootDir, true);
            }
        } catch (IOException ex)
        {
            System.err.println("ERROR: " + ex.getMessage());
            return null;
        }

        return output;
    }

    /**
     * Obtain a list of all JAR files in a specified directory path.  The method
     * will recursively search all subdirectories starting at <code>path</code>.
     *
     * @param path  The directory containing the JAR files
     * @return      An array of RAPTOR {@link File} objects
     *
     * @since 1.0.0
     */
    public static File[] getJarFiles(File path)
    {
        File[] output = null;
        try
        {
            File rootDir = path.getCanonicalFile();
            if (!rootDir.isDirectory())
            {
                LOGGER.error("ERROR: This is not a directory.\n\t" + path);
                output = null;
            } else
            {
                // List all Raptor files in the given Root Directory
                // --recurse if needed
                output = listJarFiles(rootDir, true);
            }
        } catch (IOException ex)
        {
            LOGGER.error("ERROR: " + ex.getMessage());
            return null;
        }

        return output;
    }

    /**
     * Returns a list of sub-directories of the specified directory path.
     * This method does a deep search of all subdirectories.
     * 
     * @param path  The directory path to start from
     * @return      An array of {@link File} objects pointing to subdirectories
     * 
     * @since 1.0.0
     */
    public static File[] getSubDirs(String path)
    {
        File[] output;
        try
        {
            File rootDir = new File(path).getCanonicalFile();
            List<File> subDirs = new ArrayList<File>();

            if (!rootDir.isDirectory())
            {
                System.out.println("ERROR: This is not a directory.\n\t" + path);
                output = null;
            } else
            {
                //System.out.println("Root Directory: " + path);
                subDirs = digSubDirectories(subDirs, rootDir);
                //System.out.println("Size of SubDirectories: " + subDirs.size());
                output = subDirs.toArray(new File[subDirs.size()]);
                //System.out.println("Size of output array: " + output.length);
            }
            return output;
        } catch (IOException e)
        {
            return null;
        }
    }

    /**
     * Returns a list of sub-directories of the specified directory path.
     * This method does not do a deep search of all subdirectories. Only those
     * directories in the starting directory are returned.
     * 
     * @param path  The directory path to start from
     * @return      An array of {@link File} objects pointing to subdirectories
     * 
     * @since 1.0.0
     */
    public static File[] getDirectSubDirs(String path)
    {
        File[] output;
        try
        {
            File rootDir = new File(path).getCanonicalFile();
            List<File> subDirs = new ArrayList<File>();

            if (!rootDir.isDirectory())
            {
                System.out.println("ERROR: This is not a directory.\n\t" + path);
                output = null;
            } else
            {
                output = rootDir.listFiles(new DirFileFilter());
            }
        } catch (IOException e)
        {
            return null;
        }
        return output;
    }

    /**
     * Create a {@link String} containing the name of the first assignment in a 
     * given path.
     * 
     * @param path  The directory to search
     * @return      A {@link String} containing the Assignment. <code>null</code> 
     *      if directory doesn't have any subdirectories.
     * 
     * @since 1.0.0
     */
    public static String getSingleAssignmentDirFromPath(String path)
    {
        File[] directories = getDirectSubDirs(path);
        if ((directories != null) && (directories.length > 0))
        {
            return directories[0].getName();
        } else
        {
            return null;
        }
    }

    /**
     * Creates a <code>Set</code> of {@link Course} objects based upon a path.
     * 
     * @param path  The directory to search
     * @return      A <code>Set</code> of {@link Course} objects found in the path
     * 
     * @since 1.0.0
     */
    public static Set<Course> getCoursesFromPath(String path)
    {
        Set<Course> output = new HashSet<Course>();
        File[] directories = getDirectSubDirs(path);
        for (File f : directories)
        {
            if (!f.getName().contains("_") && !f.getName().contains(" ")
                    && !f.getName().contentEquals("webpost")
                    && !f.getName().contains("For_Testing_Only"))
            {
                output.add(new Course(f.getName().trim()));
            }
        }
        return output;
    }

    /**
     * Creates a <code>Set</code> of {@link Instructor} objects based upon a path.
     * 
     * @param path  The directory to search from
     * @return      A <code>List</code> of {@link Instructor} objects found in
     *              the path
     * 
     * @since 1.0.0
     */
    public static Set<Instructor> getInstructorsFromPath(String path)
    {
        Set<Instructor> output = new HashSet<Instructor>();
        File[] directories = getDirectSubDirs(path);

        if (directories.length != 0)
        {
            for (File f : directories)
            {
                Instructor newInstructor = new Instructor();
                String[] temp = f.getName().trim().split("\\.");
                newInstructor.setFirstName(temp[0]);
                newInstructor.setLastName(temp[1]);
                newInstructor.setWebID(f.getName().trim());
                newInstructor.setDomainAccount(f.getName().trim());
                output.add(newInstructor);
            }
        } else
        {
            output = null;
        }
        return output;
    }

    /**
     * Creates a <code>List</code> of {@link Section} objects based upon a path.
     * 
     * @param path  The directory to search from
     * @return      A <code>List</code> of {@link Section} objects found in the path
     * 
     * @since 1.0.0
     */
    public static List<Section> getSectionsFromPath(String path)
    {
        List<Section> output = new ArrayList<Section>();
        File[] directories = getDirectSubDirs(path);

        for (File f : directories)
        {
            // Ignore the Instructor section in each directory
            if (!f.getName().equalsIgnoreCase("INS"))
            {
                Section newSection = new Section();
                newSection.setName(f.getName());
                output.add(newSection);
            }
        }
        return output;
    }

    /**
     * Creates a <code>Set</code> of {@link Student} objects based upon a path.
     * 
     * @param path          The directory to search from
     * @param yearOffset    The year offset for the 2-digit year
     * @return              A <code>Set</code> of {@link Student} objects found 
     *                      in the path
     * 
     * @since 1.0.0
     */
    public static Set<Student> getStudentsFromPath(String path, Integer yearOffset)
    {
        Set<Student> output = new HashSet<Student>();
        File[] directories = getDirectSubDirs(path);

        for (File f : directories)
        {
            if (f.getName().matches("C\\d\\d(\\p{Alpha})+\\.(\\p{Alpha})+\\.*(\\p{Alpha})*"))
            {
                Student student = new Student();
                student.setWebID(f.getName().trim());
                student.setClassYear(Integer.parseInt(f.getName().trim().substring(1, 3)) + yearOffset);
                String[] temp = f.getName().trim().substring(3).split("\\.");
                student.setFirstName(temp[0]);
                student.setLastName(temp[1]);
                output.add(student);
            }
        }
        return output;
    }

    /**
     * Recursive method to build a {@link List} of subdirectories for a given directory.
     * 
     * @param dirList       The {@link List} of {@link File} objects (subdirectories)
     * @param directory     The starting root directory
     * @return              A {@link List} of {@link File} objects representing the 
     *                      subdirectories of a given directory.
     * 
     * @since 1.0.0
     */
    private static List<File> digSubDirectories(List<File> dirList, File directory)
    {
        if (directory.isDirectory())
        {
            if (!dirList.contains(directory))
            {
                dirList.add(directory);
            }
            for (File f : directory.listFiles(new DirFileFilter()))
            {
                // Go over each file/subdirectory.
                digSubDirectories(dirList, f);
            }
        }
        return dirList;
    }

    /**
     * List all Raptor (.rap) files in the given directory.  If recurse is true, 
     * do a recursive search of all subdirectories.  Code taken from example at
     * <a href="http://snippets.dzone.com/posts/show/1875">DZone Snippets</a>.
     * 
     * @param directory The starting directory
     * @param recurse   Whether you should recurse through subdirectories
     * @return          The array of files that meet the .rap criteria
     * 
     * @since 1.1.0
     */
    private static File[] listRapFiles(File directory, boolean recurse)
    {
        Collection<File> files = listFiles(directory, new RapFileFilter(), recurse);
        File[] arr = new File[files.size()];
        return files.toArray(arr);
    }

    /**
     * List all JAR (.jar) files in the given directory.  If recurse is true,
     * do a recursive search of all subdirectories.  Code taken from example at
     * <a href="http://snippets.dzone.com/posts/show/1875">DZone Snippets</a>.
     *
     * @param directory The starting directory
     * @param recurse   Whether you should recurse through subdirectories
     * @return          The array of files that meet the .jar criteria
     *
     * @since 1.1.0
     */
    private static File[] listJarFiles(File directory, boolean recurse)
    {
        Collection<File> files = listFiles(directory, new JarFileFilter(), recurse);
        File[] arr = new File[files.size()];
        return files.toArray(arr);
    }

    /**
     * A recursive method that lists all files in the given directory that
     * meet the optional filter criteria.  If recurse is true, list subdirectories.
     * Code taken from example at <a href="http://snippets.dzone.com/posts/show/1875">
     * DZone Snippets</a>.
     * 
     * @param directory The starting directory
     * @param filter    The optional FileFilter to use to filter files
     * @param recurse   Whether you should recurse through subdirectories
     * @return          A collection of files that meet the criteria
     * 
     * @since 1.1.0
     */
    private static Collection<File> listFiles(File directory, FileFilter filter,
            boolean recurse)
    {
        Vector<File> files = new Vector<File>();
        File[] entries = directory.listFiles();

        for (File entry : entries)
        {
            if (filter == null || filter.accept(entry))
            {
                files.add(entry);
            }

            if (recurse && entry.isDirectory())
            {
                files.addAll(listFiles(entry, filter, recurse));
            }
        }
        return files;
    }

    /**
     * Loads a {@link GradedEvent} into the specified database based upon the given 
     * XML {@link File} object.
     * 
     * TODO: Migrate loadGradedEventData to XMLFile Import Class
     * 
     * @param inputFile     The XML {@link File} to parse
     * @param em            The data source
     * @return              <code>true</code> if the {@link GradedEvent} is successfully
     *                      loaded in the database, <code>false</code> otherwise.
     * 
     * @since 1.0.0
     */
    public static boolean loadGradedEventData(File inputFile, EntityManager em)
    {
        EntityTransaction tx = em.getTransaction();
        GradedEvent gevent = new GradedEvent();
        Document doc;
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.parse(inputFile);
            XPathFactory xFactory = XPathFactory.newInstance();
            XPath xpath = xFactory.newXPath();
            gevent.setTerm((String) xpath.evaluate("/GradedEvent/Term/@name", doc,
                    XPathConstants.STRING));
            // Look to see if the course is already in the database.  If so, pull it
            Query query = em.createQuery("SELECT c FROM Course c WHERE c.name = :name");
            query.setParameter("name", (String) xpath.evaluate("/GradedEvent/Course/@name",
                    doc, XPathConstants.STRING));
            Course course;
            try
            {
                course = (Course) query.getSingleResult();
            } catch (NoResultException ex)
            {
                // Course is not already in the database, so we can add a new one
                // If this is a spelling error, it will come up when CourseGrader
                // is ran and the additional mis-spelled course is shown in the 
                // drop down box.  Otherwise, it should be caught in the XML on 
                // review by the department.
                course = new Course((String) xpath.evaluate("/GradedEvent/Course/@name", doc,
                        XPathConstants.STRING));
                // Save it to the database so the relationship will not be transient
                tx.begin();
                em.persist(course);
                tx.commit();
            }
            // Lookup and see if the GradedEvent is already in the database
            // if so, show an error box.
            query = em.createQuery("SELECT g FROM GradedEvent g WHERE "
                    + "g.assignment = :assign AND g.term = :term AND "
                    + "g.version = :ver AND g.course = :course");
            query.setParameter("assign", (String) xpath.evaluate("/GradedEvent/Assignment/@name", doc,
                    XPathConstants.STRING));
            query.setParameter("term", (String) xpath.evaluate("/GradedEvent/Term/@name", doc,
                    XPathConstants.STRING));
            query.setParameter("ver", (String) xpath.evaluate("/GradedEvent/Version/@id", doc,
                    XPathConstants.STRING));
            query.setParameter("course", course);
            try
            {
                GradedEvent g = (GradedEvent) query.getSingleResult();
                JOptionPane.showMessageDialog(null, "Graded Event already "
                        + "exists in the database",
                        "Error", JOptionPane.ERROR_MESSAGE);
            } catch (NoResultException ex)
            {
                // Doesn't exist, so go ahead and process
                gevent.setCourse(course);

                gevent.setAssignment((String) xpath.evaluate("/GradedEvent/Assignment/@name",
                        doc, XPathConstants.STRING));
                gevent.setVersion((String) xpath.evaluate("/GradedEvent/Version/@id",
                        doc, XPathConstants.STRING));
                gevent.setDueDate((String) xpath.evaluate("/GradedEvent/DueDate",
                        doc, XPathConstants.STRING));
                if (xpath.evaluate("/GradedEvent/PartialCredit/@value", doc,
                        XPathConstants.STRING).equals("true"))
                {
                    gevent.setPartialCredit(true);
                } else
                {
                    gevent.setPartialCredit(false);
                }

                // Loop through each of the questions and build the set of questions
                NodeList nodes = (NodeList) xpath.evaluate("/GradedEvent/Question",
                        doc, XPathConstants.NODESET);
                for (int i = 0; i < nodes.getLength(); i++)
                {
                    Question quest;
                    boolean qnew = true;
                    Node q = nodes.item(i);
                    String qname = xpath.evaluate("./Name/@name", q);
                    String qcategory = xpath.evaluate("./@category", q);
                    
                    // Swap for CategoryDAO interface
                    CategoryDAO catDAO = new CategoryDAO(em);
                    
                    //query = em.createQuery("SELECT c FROM Category c WHERE "
                    //        + "c.name = :cname");
                    //query.setParameter("cname", qcategory);
                    Category qcat;
                    try
                    {
                        //qcat = (Category) query.getSingleResult();
                        qcat = catDAO.find(qcategory);
                    } catch (NoResultException e)
                    {
                        qcat = new Category(qcategory);
                        Long newCatId = catDAO.create(qcat);
                        qcat.setId(newCatId);
                    }
                    //tx.begin();
                    //em.persist(qcat);
                    //tx.commit();
                    qcat = catDAO.update(qcat);
                    
                    query = em.createQuery("SELECT q FROM Question q WHERE "
                            + "q.name = :name AND q.category = :cat");
                    query.setParameter("name", qname);
                    query.setParameter("cat", qcat);
                    try
                    {
                        quest = (Question) query.getSingleResult();
                        qnew = false;
                    } catch (NoResultException e)
                    {
                        quest = new Question(qname, qcat);
                        qnew = true;
                    }

                    if (qnew)
                    {
                        // This is a new question, so add the other details
                        String qidStr = xpath.evaluate("./@id", q);
                        Long qid;
                        if (qidStr.equals(""))
                        {
                            qid = -1L;
                        } else
                        {
                            qid = Long.parseLong(xpath.evaluate("./@id", q));
                        }

                        String qdesc = xpath.evaluate("./Description/text()", q);
                        quest.setDescription(qdesc);
                        boolean verbatim;
                        if (xpath.evaluate("./Verbatim/@value", q).equals("true"))
                        {
                            verbatim = true;
                        } else
                        {
                            verbatim = false;
                        }
                        quest.setVerbatim(verbatim);

                        // Set whether the question requires ordered output
                        boolean orderedOutput;
                        if (xpath.evaluate("./OrderedOutput/@value", q).equals("true"))
                        {
                            orderedOutput = true;
                        } else
                        {
                            orderedOutput = false;
                        }

                        // Parse test cases for each question
                        NodeList testCaseNodes = (NodeList) xpath.evaluate("./TestCase", q, XPathConstants.NODESET);
                        for (int j = 0; j < testCaseNodes.getLength(); j++)
                        {
                            TestCase tc = new TestCase();
                            Node t = testCaseNodes.item(j);
                            String value = xpath.evaluate("./Value/@value", t);
                            tc.setValue(new BigDecimal(value));
                            NodeList tcInputs = (NodeList) xpath.evaluate("./input", t, XPathConstants.NODESET);
                            for (int k = 0; k < tcInputs.getLength(); k++)
                            {
                                Node inputNode = tcInputs.item(k);
                                String input = inputNode.getTextContent();
                                //System.out.println("Input: " + input);
                                tc.addInput(input);
                            }
                            NodeList tcOutputs = (NodeList) xpath.evaluate("./output", t, XPathConstants.NODESET);
                            for (int k = 0; k < tcOutputs.getLength(); k++)
                            {
                                Node outputNode = tcOutputs.item(k);
                                String output = outputNode.getTextContent();
                                //System.out.println("Expected Output: " + output);
                                tc.addOutput(output);
                            }
                            NodeList tcExcludes = (NodeList) xpath.evaluate("./exclude", t, XPathConstants.NODESET);
                            for (int k = 0; k < tcExcludes.getLength(); k++)
                            {
                                Node outputNode = tcExcludes.item(k);
                                String exclude = outputNode.getTextContent();
                                //System.out.println("Exclusion: " + exclude);
                                tc.addExclusion(exclude);
                            }
                            tx.begin();
                            em.persist(tc);
                            tx.commit();
                            quest.addTestCase(tc);
                        }
                    }
                    tx.begin();
                    em.persist(quest);
                    tx.commit();
                    // Finally, add the question to the set of questions
                    gevent.addQuestion(quest);
                }

                // Persist to the database, return true or false whether it was
                // successful
                tx.begin();
                em.persist(gevent);
                tx.commit();
            }
        } catch (ParserConfigurationException ex)
        {
            ex.printStackTrace();
            return false;
        } catch (SAXException ex)
        {
            ex.printStackTrace();
            return false;
        } catch (IOException ex)
        {
            ex.printStackTrace();
            return false;
        } catch (XPathException ex)
        {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Loads a {@link GradedEvent} into the specified database based upon the given
     * file name.  Calls {@link #loadGradedEventData(java.io.File, javax.persistence.EntityManager)} as
     * its implementation.
     * 
     * @param inputFileName {@link String} containing the source filename
     * @param em            The data source
     * @return              <code>true</code> if the {@link GradedEvent} is added to the 
     *                      database, <code>false</code> otherwise.
     * 
     * @since 1.0.0
     */
    public static boolean loadGradedEventData(String inputFileName, EntityManager em)
    {
        File f = new File(inputFileName);
        return loadGradedEventData(f, em);
    }

    /**
     * Loads a {@link Question} into the specified database based upon the given
     * XML {@link File} object.
     *
     * TODO: Migrate loadQuestionData to XMLImport class
     * 
     * @param inputFile     The XML {@link File} to parse
     * @param em            The data source
     * @return              <code>true</code> if the {@link Question} is successfully
     *                      loaded in the database, <code>false</code> otherwise.
     *
     * @since 1.2.4
     */
    public static boolean loadQuestionData(File inputFile, EntityManager em)
    {
        EntityTransaction tx = em.getTransaction();
        Question question = new Question();
        Query query;
        Document doc;
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.parse(inputFile);
            XPathFactory xFactory = XPathFactory.newInstance();
            XPath xpath = xFactory.newXPath();
            // Loop through each of the questions and build the set of questions
            NodeList nodes = (NodeList) xpath.evaluate("/GradedEvent/Question", doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++)
            {
                Question quest;
                boolean qnew = true;
                Node q = nodes.item(i);
                String qname = xpath.evaluate("./Name/@name", q);
                String qcategory = xpath.evaluate("./@category", q);
                
                // Switch to the CategoryDAO class
                CategoryDAO catDAO = new CategoryDAO(em);
                //query = em.createQuery("SELECT c FROM Category c WHERE c.name = :cname");
                //query.setParameter("cname", qcategory);
                Category qcat;
                try
                {
                    //qcat = (Category) query.getSingleResult();
                    qcat = catDAO.find(qcategory);
                } catch (NoResultException e)
                {
                    qcat = new Category(qcategory);
                    Long newCatId = catDAO.create(qcat);
                    qcat.setId(newCatId);
                }
                //tx.begin();
                //em.persist(qcat);
                //tx.commit();
                qcat = catDAO.update(qcat);
                query = em.createQuery("SELECT q FROM Question q WHERE "
                        + "q.name = :name AND q.category = :cat");
                query.setParameter("name", qname);
                query.setParameter("cat", qcat);
                try
                {
                    quest = (Question) query.getSingleResult();
                    qnew = false;
                } catch (NoResultException e)
                {
                    quest = new Question(qname, qcat);
                    qnew = true;
                }

                if (qnew)
                {
                    // This is a new question, so add the other details
                    String qidStr = xpath.evaluate("./@id", q);
                    Long qid;
                    if (qidStr.equals(""))
                    {
                        qid = -1L;
                    } else
                    {
                        qid = Long.parseLong(xpath.evaluate("./@id", q));
                    }

                    String qdesc = xpath.evaluate("./Description/text()", q);
                    quest.setDescription(qdesc);
                    boolean verbatim;
                    if (xpath.evaluate("./Verbatim/@value", q).equals("true"))
                    {
                        verbatim = true;
                    } else
                    {
                        verbatim = false;
                    }
                    quest.setVerbatim(verbatim);

                    // Set whether the question requires ordered output
                    boolean orderedOutput;
                    if (xpath.evaluate("./OrderedOutput/@value", q).equals("true"))
                    {
                        orderedOutput = true;
                    } else
                    {
                        orderedOutput = false;
                    }

                    // Parse test cases for each question
                    NodeList testCaseNodes = (NodeList) xpath.evaluate("./TestCase", q, XPathConstants.NODESET);
                    for (int j = 0; j < testCaseNodes.getLength(); j++)
                    {
                        TestCase tc = new TestCase();
                        Node t = testCaseNodes.item(j);
                        String value = xpath.evaluate("./Value/@value", t);
                        tc.setValue(new BigDecimal(value));
                        NodeList tcInputs = (NodeList) xpath.evaluate("./input", t, XPathConstants.NODESET);
                        for (int k = 0; k < tcInputs.getLength(); k++)
                        {
                            Node inputNode = tcInputs.item(k);
                            String input = inputNode.getTextContent();
                            //System.out.println("Input: " + input);
                            tc.addInput(input);
                        }
                        NodeList tcOutputs = (NodeList) xpath.evaluate("./output", t, XPathConstants.NODESET);
                        for (int k = 0; k < tcOutputs.getLength(); k++)
                        {
                            Node outputNode = tcOutputs.item(k);
                            String output = outputNode.getTextContent();
                            //System.out.println("Expected Output: " + output);
                            tc.addOutput(output);
                        }
                        NodeList tcExcludes = (NodeList) xpath.evaluate("./exclude", t, XPathConstants.NODESET);
                        for (int k = 0; k < tcExcludes.getLength(); k++)
                        {
                            Node outputNode = tcExcludes.item(k);
                            String exclude = outputNode.getTextContent();
                            //System.out.println("Exclusion: " + exclude);
                            tc.addExclusion(exclude);
                        }
                        tx.begin();
                        em.persist(tc);
                        tx.commit();
                        quest.addTestCase(tc);
                    }
                }
                tx.begin();
                em.persist(quest);
                tx.commit();
            }
        } catch (ParserConfigurationException ex)
        {
            ex.printStackTrace();
            return false;
        } catch (SAXException ex)
        {
            ex.printStackTrace();
            return false;
        } catch (IOException ex)
        {
            ex.printStackTrace();
            return false;
        } catch (XPathException ex)
        {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Loads a {@link Question} into the specified database based upon the given
     * file name.  Calls {@link #loadQuestionData(File, EntityManager)} as
     * its implementation.
     *
     * @param inputFileName {@link String} containing the source filename
     * @param em            The data source
     * @return              <code>true</code> if the {@link Question} is added to the
     *                      database, <code>false</code> otherwise.
     *
     * @since 1.2.4
     */
    public static boolean loadQuestionData(String inputFileName, EntityManager em)
    {
        File f = new File(inputFileName);
        return loadQuestionData(f, em);
    }

    /**
     * Selects a {@link GradedEvent} from a list of <code>GradedEvent</code> objects
     * that matches the selected {@link Section}.
     * 
     * @param glist     A {@link List} of {@link GradedEvent} objects to search
     * @param section   The {@link Section} to filter by
     * @return          The {@link GradedEvent} that matches the {@link Section}; 
     *                  <code>null</code> if no <code>GradedEvent</code> is found.
     * 
     * @since 1.0.0
     */
    public static GradedEvent getGradedEventFromSectionName(List<GradedEvent> glist,
            String section)
    {
        GradedEvent gevent = null;

        String ver = section.substring(0, 2);
        for (GradedEvent g : glist) // Set above
        {
            if (g.getVersion().equals(ver))
            {
                gevent = g;
            }
        }
        if (gevent == null)
        {
            // Do some error work...the version isn't loaded for this section
            LOGGER.error("ERROR: There is no Graded Event for this section "
                    + "loaded in the database.");
            LOGGER.error("\tSection: " + section);
        }
        return gevent;
    }

    /**
     * Validates the specified XML {@link File} against the provided GradedEvent DTD.
     * The location of the DTD must be specified in the XML document or else 
     * validation will fail.
     * 
     * @param inputFile The XML file to validate
     * @return          <code>true</code> if the XML is a valid {@link GradedEvent} XML 
     *                  file.
     * 
     * @since 1.0.0
     */
    public static boolean validateGradedEventXML(File inputFile)
    {
        try
        {
            LOGGER.info("Validating GradedEvent XML file");
            DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
            f.setValidating(true);
            DocumentBuilder b = f.newDocumentBuilder();
            ErrorHandler h = new XMLErrorHandler();
            b.setErrorHandler(h);
            Document d = b.parse(inputFile);
            // If it parses fine, go ahead and say it's ok
            LOGGER.info("GradedEvent file is Valid");
            return true;
        } catch (ParserConfigurationException e)
        {
            LOGGER.error("Parser Configuration Error: " + e.getLocalizedMessage());
            return false;
        } catch (SAXException e)
        {
            LOGGER.error("SAX Error: " + e.getLocalizedMessage());
            return false;
        } catch (IOException e)
        {
            LOGGER.error("IO Error: " + e.getLocalizedMessage());
            return false;
        }
    }

    /**
     * Unzips the Zip archive specified by the file name and located in the path.  
     * If <code>ignoreFoldersInZip</code> is <code>true</code>, the files will be extracted 
     * in the specified path directory.  If <code>ignoreFoldersInZip</code> is 
     * <code>false</code>, the files will be extracted in the folder structure 
     * specified by the archive.
     * 
     * @param path                  The root path to extract to
     * @param zipFileName           The archive file name
     * @param ignoreFoldersInZip    <code>true</code> if the folder structure in the 
     *                              archive should be ignored, <code>false</code>
     *                              otherwise.
     * 
     * @since 1.1.0
     */
    public static void unZip(String path, String zipFileName, boolean ignoreFoldersInZip)
    {
        Enumeration entries;
        ZipFile zipFile;
        String fullFileName;

        try
        {
            fullFileName = path + "/" + zipFileName;
            zipFile = new ZipFile(fullFileName);

            // Create any subdirectories
            entries = zipFile.entries();
            while (entries.hasMoreElements())
            {
                ZipEntry entry = (ZipEntry) entries.nextElement();
                String entryName = entry.getName();

                // Check to see if the entry is in a subfolder
                entryName = entryName.replace('\\', '/');
                if (entryName.indexOf("/") >= 0)
                {
                    // there is a folder path in front of the file name
                    String[] newDirectories = entryName.split("/");

                    // the last "newDirectory is the actual file name
                    if (newDirectories.length > 0)
                    {
                        if (ignoreFoldersInZip)
                        {
                            // ignore the subfolders and save the entry to the current folder
                            entryName = newDirectories[newDirectories.length - 1];
                        } else
                        {
                            // Create the subfolders
                            String newDirectoryName = "";
                            for (int j = 0; j < newDirectories.length - 1; j++)
                            {
                                newDirectoryName += "/" + newDirectories[j];

                                File newDirectory = new File(path + newDirectoryName);
                                if (!newDirectory.exists())
                                {
                                    newDirectory.mkdir();
                                }
                            }
                        }
                    }
                }

                if (entry.isDirectory())
                {
                    System.out.println("   Creating directory: " + entryName);
                    File newDirectory = new File(path + "/" + entryName);
                    if (!newDirectory.exists())
                    {
                        newDirectory.mkdir();
                    }
                } else
                {
                    entryName = path + "/" + entryName;
                    System.out.println("   Extracting file: " + entryName);
                    copyInputStream(zipFile.getInputStream(entry),
                            new BufferedOutputStream(new FileOutputStream(entryName)));
                }
            }
            zipFile.close();
        } catch (IOException error)
        {
            System.err.println("Error unzipping '" + zipFileName + "' " + error);
        }
    }

    /**
     * Utility method for the unZip method.
     * 
     * @param in    The InputStream to copy from
     * @param out   The OutputStream to copy to
     * @throws java.io.IOException  Thrown if the file cannot be read or written to.
     * 
     * @since 1.1.0
     */
    private static void copyInputStream(InputStream in, OutputStream out)
            throws IOException
    {
        byte[] buffer = new byte[1024];
        int len;

        while ((len = in.read(buffer)) >= 0)
        {
            out.write(buffer, 0, len);
        }

        in.close();
        out.close();
    }

    /**
     * Output the provided {@link SectionReport} to the identified File.
     * This method does not sort the report prior to printing.  Currently, this
     * method only prints a static HTML document and there are no links for viewing
     * individual student results.  The method prints full HTML headers and footers
     * in the document so no more than 1 section per report is possible at this
     * time.
     *
     * @param reportFile        The file to print the report to
     * @param sectionReport     The SectionReport for the assignment
     *
     * @since 1.2.3
     */
    public static void printSectionReport(File reportFile, SectionReport sectionReport)
    {
        try
        {
            FileOutputStream reportFileStream = new FileOutputStream(reportFile, false);
            PrintWriter reportWriter = new PrintWriter(reportFileStream, true);

            // Report header information
            reportWriter.println("<html>");
            reportWriter.println("\t<head>");
            reportWriter.println("\t\t<title>CourseGrader Report: "
                    + sectionReport.getAssignment() + "</title>");
            reportWriter.println("\t</head>");
            reportWriter.println("\t<body>");

            // Report Preamble
            reportWriter.println("\t\t<h1><center><u>CourseGrader Report</u>"
                    + "</center></h1>");
            reportWriter.println("\t\t<p><b>Course</b>: "
                    + sectionReport.getCourse() + "<br />");
            reportWriter.println("\t\t<b>Assignment</b>: "
                    + sectionReport.getAssignment() + "</p>");
            reportWriter.println("\t\t<b>Instructor</b>: "
                    + sectionReport.getInstructor().getLastName() + ", "
                    + sectionReport.getInstructor().getFirstName() + "<br />");
            reportWriter.println("\t\t<b>Section</b>: " + sectionReport.getSection()
                    + "</p>");

            // Student Results Table
            reportWriter.println("\t\t<table border=\"2\">");
            reportWriter.print("\t\t<tr><th>Student Name</th>");
            // Need to get question names...currently stored in
            // StudentResults.results.Question So, we're going to get the questions
            // from the first student's report
            for (int i = 0;
                    i < sectionReport.getStudentReports().get(0).getResults().size();
                    i++)
            {
                reportWriter.print("\t\t<th>"
                        + sectionReport.getStudentReports().get(0).getResults().get(i).getQuestion().getName() + "</th>");
            }
            reportWriter.println("\t\t<th>Score</th></tr>");

            LOGGER.debug("Ensure a 'responses' subfolder exists in the current "
                    + "directory");
            File responseDirectory = new File(reportFile.getParent()
                    + File.separator + "responses");
            boolean saveResponses = false;
            if (responseDirectory.exists() && responseDirectory.isDirectory()
                    && responseDirectory.canWrite())
            {
                LOGGER.debug("Able to write to the responses directory");
                saveResponses = true;
            } else if (!responseDirectory.exists())
            {
                LOGGER.debug("Responses directory doesn't exist.  Creating");
                if (responseDirectory.mkdir())
                {
                    LOGGER.debug("Response directory created.");
                    saveResponses = true;
                }
            } else if (!responseDirectory.isDirectory())
            {
                LOGGER.warn("File " + responseDirectory.getCanonicalPath()
                        + " needs to be a writable directory.");
            } else if (!responseDirectory.canWrite())
            {
                LOGGER.debug("Response directory not writable.  Attempting to "
                        + "fix permissions.");
                if (responseDirectory.setWritable(true))
                {
                    LOGGER.debug("Permissions fixed.");
                    saveResponses = true;
                }
            }


            /**
             * Loop through every student report saved in the Section Report.
             * For each student, create a new file in the "responses" subdirectory
             * containing the output from each student's response.
             */
            for (int i = 0; i < sectionReport.getStudentReports().size(); i++)
            {
                File studentResponseFile;
                FileOutputStream responseOutputStream = null;
                PrintWriter responseWriter = null;
                StudentReport sReport = sectionReport.getStudentReports().get(i);
                if (saveResponses)
                {
                    studentResponseFile = new File(responseDirectory,
                            sReport.getStudent().getWebID() + ".html");
                    try
                    {
                        LOGGER.debug("Creating student report file with headers");
                        studentResponseFile.createNewFile();
                        responseOutputStream = new FileOutputStream(studentResponseFile,
                                false);
                        responseWriter = new PrintWriter(responseOutputStream, true);
                        responseWriter.println("<html>\n<head>");
                        responseWriter.println("<title>CourseGrader Student Report"
                                + "</title>\n</head>\n<body>");
                        responseWriter.println("<h1><center><u>CourseGrader Student "
                                + "Report</u></center></h1>\n");
                        responseWriter.println("<p><a href=\"../"
                                + sReport.getSection().getName()
                                + ".html\">Back</a></p>");
                        responseWriter.println("<p><b>Student</b>: "
                                + sReport.getStudent().getLastName() + ", "
                                + sReport.getStudent().getFirstName() + "</p>");
                        LOGGER.debug("File created with headers.");
                    } catch (IOException ex)
                    {
                        LOGGER.warn("Problem creating response file for student: "
                                + sReport.getStudent().getLastName() + ", "
                                + sReport.getStudent().getFirstName());
                        LOGGER.warn(ex.getLocalizedMessage());
                        saveResponses = false;
                    }
                }
                BigDecimal overallScore = new BigDecimal("0");
                // Print student name
                reportWriter.print("\t\t<tr><td><a href=\"responses/"
                        + sReport.getStudent().getWebID() + ".html\">"
                        + sReport.getStudent().getLastName()
                        + ", " + sReport.getStudent().getFirstName() + "</a></td>");
                // Print results to each question
                for (int j = 0; j < sReport.getResults().size(); j++)
                {
                    int questionResult = sReport.getResults().get(j).getScore().compareTo(sReport.getResults().get(j).getValue());
                    boolean completeMiss = sReport.getResults().get(j).getScore().compareTo(BigDecimal.ZERO) == 0;
                    if (questionResult == 0)
                    {
                        reportWriter.print("<td>");
                    } else if (completeMiss)
                    {
                        reportWriter.print("<td bgcolor=\"Red\" style=\"font-weight:bold;\">");
                    } else
                    {
                        reportWriter.print("<td bgcolor=\"Yellow\">");
                    }
                    reportWriter.print(sReport.getResults().get(j).getScore()
                            + "</td>");
                    overallScore =
                            overallScore.add(sReport.getResults().get(j).getScore());
                    if (saveResponses)
                    {
                        responseWriter.println("<p><b>Question: "
                                + sReport.getResults().get(j).getQuestion().getName()
                                + "<br />\n<b>Result: "
                                + sReport.getResults().get(j).getScore().toPlainString()
                                + "</p>");
                        responseWriter.println("<table border='1'>\n<tr>\n<th>Test Case</th>");
                        responseWriter.println("<th>Inputs</th><th>Expected Output</th>");
                        responseWriter.println("<th>Actual Output</th>\n</tr>\n");
                        Question question = sReport.getResults().get(j).getQuestion();
                        // Loop through all the Test Cases
                        for (int k = 0; k < question.getTestCases().size(); k++)
                        {
                            int testCaseIndex = k + 1;
                            responseWriter.println("<tr>\n<td>" + testCaseIndex
                                    + "</td>");
                            responseWriter.println("<td><center>");
                            TestCase testCase = question.getTestCases().get(k);
                            for (int m = 0; m < testCase.getInputs().size(); m++)
                            {
                                responseWriter.println(testCase.getInputs().get(m)
                                        + "<br />");
                            }
                            responseWriter.println("</center></td>");
                            responseWriter.println("<td><center>");
                            for (int m = 0; m < testCase.getOutputs().size(); m++)
                            {
                                responseWriter.println(testCase.getOutputs().get(m)
                                        + "<br />");
                            }
                            responseWriter.println("</center></td>");
                            responseWriter.println("<td><center>");
                            Response response;
                            try
                            {
                                response =
                                        sReport.getResults().get(j).getResponses().get(k);
                            } catch (Exception ex1)
                            {
                                response = null;
                            }
                            if (response != null && response.getAnswers().size() > 0)
                            {
                                for (int m = 0; m < response.getAnswers().size(); m++)
                                {
                                    responseWriter.println(response.getAnswers().get(m)
                                            + "<br />");
                                }
                            } else
                            {
                                responseWriter.println(" ");
                            }
                            responseWriter.println("</center></td>\n</tr>\n");
                        }
                        responseWriter.println("</table>\n");
                    }
                }
                // Print overall score for student and close the row
                reportWriter.println("<td>" + overallScore.toPlainString() + "</td></tr>");
                // Close the Student Response Report if it's open
                if (saveResponses)
                {
                    responseWriter.println("<p><a href=\"../"
                            + sReport.getSection().getName()
                            + ".html\">Back</a></p>");
                    responseWriter.println("</body>\n</html>");
                    responseWriter.close();
                    try
                    {
                        responseOutputStream.close();
                    } catch (IOException ex)
                    {
                        LOGGER.warn("Unable to close Student Response Report.");
                    }
                }
            }
            reportWriter.println("\t\t</table>");
            reportWriter.println("<p style=\"font-weight:bold;\">Report Key</p>");
            reportWriter.println("<table border=\"1\">");
            reportWriter.println("<tr><td>Full Credit</td></tr>");
            reportWriter.println("<tr><td bgcolor=\"Yellow\">Partial Credit</td></tr>");
            reportWriter.println("<tr><td bgcolor=\"Red\" style=\"font-weight:bold;\">"
                    + "Incorrect</td></tr>");
            reportWriter.println("</table>");
            reportWriter.println("\t\t<br /><hr /><br />");

            // Add report creation timestamp
            Date now = new Date(System.currentTimeMillis());
            reportWriter.println("\t\t<p>Report created: " + now.toString());

            // Close the HTML document
            reportWriter.println("\t</body>");
            reportWriter.println("</html>");
            reportWriter.close();
            reportFileStream.close();
        } catch (FileNotFoundException ex)
        {
            LOGGER.error("Unable to find file: " + reportFile.getName());
            LOGGER.error("Report not written. " + ex.getLocalizedMessage());
        } catch (IOException ex)
        {
            LOGGER.error("Unable to close Report File Stream. "
                    + ex.getLocalizedMessage());
        }
    }
    private static Logger LOGGER = Logger.getLogger(RageLib.class);
}
