/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import com.darkhonor.rage.model.Instructor;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;

/**
 * Class to handle all database queries for {@link com.darkhonor.rage.model.Instructor}
 * objects.
 *
 * @author Alex Ackerman
 */
public class InstructorDAO
{
    /**
     * Default constructor.
     *
     * @param em    EntityManager connection to the database.
     * @throws IllegalArgumentException Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager is
     *                                  <code>null</code>
     */
    public InstructorDAO(EntityManager em) throws IllegalArgumentException,
            NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("EntityManager is closed.  Throwing IllegalArgumentException");
            throw new IllegalArgumentException("Invalid EntityManager state");
        } else
        {
            this.em = em;
        }
        this.em = em;
    }

    /**
     * Finds a Instructor object in the database with the given Id value.  The database
     * connection will still be active at the end of this method call.
     *
     * @param id                        The Id of the Instructor to find
     * @return                          The selected Instructor if found or
     *                                  <code>null</code> otherwise
     * @throws IllegalArgumentException Thrown when the Id is an invalid value--
     *                                  negative or 0.
     * @throws NullPointerException     Thrown when the Id is <code>null</code>
     * @throws IllegalStateException    Thrown if the connection to data source is not
     *                                  open
     */
    public Instructor find(Long id) throws IllegalArgumentException, NullPointerException,
            IllegalStateException
    {
        if (id == null)
        {
            LOGGER.error("The provided Instructor Id is null");
            throw new NullPointerException("The provided Instructor Id is null");
        }
        else if (id < 1)
        {
            LOGGER.error("The provided Instructor Id is less than 1");
            throw new IllegalArgumentException("The provided Instructor Id is less than 1");
        }
        else if (!em.isOpen())
        {
            LOGGER.error("The connection to the data source is closed");
            throw new IllegalStateException("The connection to the data source is closed");
        }
        return em.find(Instructor.class, id);
    }

    /**
     * Finds an Instructor object in the database with the given Web ID value.  The
     * database connection will still be active at the end of this method call.
     * 
     * @param webId                     The Web ID of the Instructor to find
     * @return                          The selected Instructor if found
     * @throws IllegalArgumentException Thrown when the Web ID is blank
     * @throws NullPointerException     Thrown when the Web ID is <code>null</code>
     * @throws IllegalStateException    Thrown when the connection to the data source
     *                                  is closed
     * @throws NoResultException        Thrown if no result is found in the data
     *                                  source
     */
    public Instructor findInstructorByWebId(String webId) throws IllegalArgumentException,
            NullPointerException, IllegalStateException, NoResultException
    {
        if (webId == null)
        {
            LOGGER.error("The provided Instructor Web Id is null");
            throw new NullPointerException("The provided Instructor Web Id is null");
        }
        else if (webId.length() == 0)
        {
            LOGGER.error("The provided Instructor Web Id is blank");
            throw new IllegalArgumentException("The provided Instructor Web Id is blank");
        }
        else if (!em.isOpen())
        {
            LOGGER.error("The connection to the data source is closed");
            throw new IllegalStateException("The connection to the data source is closed");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Instructor> cq = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cq.from(Instructor.class);
        cq.where(cb.equal(instructorRoot.get("webID"), webId));
        TypedQuery<Instructor> instructorQuery = em.createQuery(cq);
        return instructorQuery.getSingleResult();
    }

    /**
     * Finds an Instructor in the data source with the given Domain Account.  The
     * connection to the data source will still be active at the end of the method
     * call.
     *
     * @param domainAccount             The domain account of the Instructor to find
     * @return                          The selected Instructor if found
     * @throws IllegalArgumentException Thrown when the domain account is blank
     * @throws NullPointerException     Thrown when the domain account is <code>
     *                                  null</code>
     * @throws IllegalStateException    Thrown when the connection to the data
     *                                  source is closed.
     * @throws NoResultException        Thrown if the Instructor is not found
     */
    public Instructor findInstructorByDomainAccount(String domainAccount) throws
            IllegalArgumentException, NullPointerException, IllegalStateException,
            NoResultException
    {
        if (domainAccount == null)
        {
            LOGGER.error("(findInstructorByDomainAccount) The provided Instructor "
                    + "Domain Account is null");
            throw new NullPointerException("The provided Instructor Domain Account "
                    + "is null");
        }
        else if (domainAccount.length() == 0)
        {
            LOGGER.error("(findInstructorByDomainAccount) The provided Instructor "
                    + "Domain Account is blank");
            throw new IllegalArgumentException("The provided Instructor Domain "
                    + "Account is blank");
        }
        else if (!em.isOpen())
        {
            LOGGER.error("The connection to the data source is closed");
            throw new IllegalStateException("The connection to the data source is closed");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Instructor> cq = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cq.from(Instructor.class);
        cq.where(cb.equal(instructorRoot.get("domainAccount"), domainAccount));
        TypedQuery<Instructor> instructorQuery = em.createQuery(cq);
        return instructorQuery.getSingleResult();
    }
    
    /**
     * Closes the connection to the database.
     *
     * @throws IllegalStateException    Thrown if the database connection is
     *                                  already closed.
     */
    public void closeConnection() throws IllegalStateException
    {
        em.close();
    }

    private Logger LOGGER = Logger.getLogger(TestCaseDAO.class);
    private EntityManager em;
}
