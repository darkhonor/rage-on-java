/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.listmodel;

import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 * Extension of the {@link AbstractListModel} class to hold {@link String}
 * objects.
 * 
 * @author Alexander Ackerman
 */
public class StringListModel extends AbstractListModel<String>
{

    /**
     * StringListModel constructor.
     */
    public StringListModel()
    {
        data = new ArrayList<String>();
    }

    /**
     * Returns the number of elements in the List Model
     *
     * @return  The number of elements in the Model
     */
    @Override
    public int getSize()
    {
        return data.size();
    }

    /**
     * Returns the Object found at the specified index.
     *
     * @param index     The index of the Object to return
     * @return          The object found at the specified index
     */
    @Override
    public String getElementAt(int index)
    {
        return data.get(index);
    }

    /**
     * Populates the List Model with every element found in the specified List.
     * After this method is ran, the size of the model will be the size of the 
     * model prior to execution plus the size of the provided List.
     *
     * @param newData   The List of elements to add to the model
     */
    public void addAll(List<String> newData)
    {
        data.addAll(newData);
        this.fireContentsChanged(data, 0, data.size());
    }

    /**
     * Adds the specified String to the List Model.
     *
     * @param string    The String to add to the List Model
     */
    public void add(String string)
    {
        data.add(string);
        this.fireContentsChanged(data, data.indexOf(string),
                data.lastIndexOf(string));
    }

    /**
     * Removes the element at the specified index and returns it to the calling
     * method.  After this method is called, the object will no longer be in the
     * Model.
     * 
     * @param index     The index of the element to remove
     * @return          The String found at the specified index.
     */
    public String remove(int index)
    {
        String element = data.remove(index);
        this.fireIntervalRemoved(data, index, index);
        return element;
    }

    /**
     * Removes all elements from the Model.  After this method is ran, the model
     * will be empty.
     */
    public void removeAll()
    {
        data.clear();
        this.fireContentsChanged(data, 0, 0);
    }
    
    private final List<String> data;
}
