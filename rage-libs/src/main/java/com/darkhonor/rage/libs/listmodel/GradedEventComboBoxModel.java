/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.listmodel;

import com.darkhonor.rage.model.GradedEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 * Implementation of the {@link AbstractListModel} to store
 * {@link com.darkhonor.rage.model.GradedEvent} objects for a JComboBox element
 * 
 * @author Alexander Ackerman
 */
public class GradedEventComboBoxModel extends AbstractListModel<GradedEvent> 
    implements ComboBoxModel<GradedEvent>
{

    /**
     * Default constructor
     */
    public GradedEventComboBoxModel()
    {
        data = new ArrayList<GradedEvent>();
        selectedItem = null;
    }

    /**
     * Returns the number of elements in the ListModel
     *
     * @return  The number of elements in the ListModel
     */
    @Override
    public int getSize()
    {
        return data.size();
    }

    /**
     * Returns the {@link com.darkhonor.rage.model.GradedEvent} stored at the given
     * index in the ListModel.  The ListModel index is 0-based.
     *
     * @param index The index of the element to return
     * @return      The GradedEvent object stored in the ListModel
     */
    @Override
    public GradedEvent getElementAt(int index)
    {
        return data.get(index);
    }

    /**
     * Add a {@link com.darkhonor.rage.model.GradedEvent} to the ListModel
     *
     * @param gradedEvent   The GradedEvent to add to the ListModel
     */
    public void add(GradedEvent gradedEvent)
    {
        data.add(gradedEvent);
        this.fireContentsChanged(data, data.indexOf(gradedEvent),
                data.lastIndexOf(gradedEvent));
    }

    /**
     * Removes all elements from the ListModel.  After this is called, the
     * ListModel will be empty.
     */
    public void removeAll()
    {
        data.clear();
        this.fireIntervalRemoved(data, 0, 0);
    }

    /**
     * Remove the {@link com.darkhonor.rage.model.GradedEvent} stored in the
     * ListModel at the given index.  The ListModel index is 0-based.
     *
     * @param index     The index of the GradedEvent to remove
     * @return          The removed GradedEvent
     */
    public GradedEvent remove(int index)
    {
        this.fireIntervalRemoved(data, index, index);
        return data.remove(index);
    }

    /**
     * Updates the provided {@link com.darkhonor.rage.model.GradedEvent} if
     * it exists in the ListModel.  If it does not exist in the ListModel,
     * no action is taken.
     *
     * @param gradedEvent   The GradedEvent object to replace with the one stored
     *                      in the ListModel
     */
    public void update(GradedEvent gradedEvent)
    {
        if (data.contains(gradedEvent))
        {
            int orig = data.indexOf(gradedEvent);
            data.remove(gradedEvent);
            data.add(gradedEvent);
            this.fireContentsChanged(data, orig, data.indexOf(gradedEvent));
        }
    }
    
    private final List<GradedEvent> data;
    private GradedEvent selectedItem;

    /**
     * Sets the selected Graded Event in the ComboBox.
     *
     * @param anItem        The GradedEvent selected by the user
     */
    @Override
    public void setSelectedItem(Object anItem)
    {
        this.selectedItem = (GradedEvent)anItem;
    }

    /**
     * Returns the selected item in the Combo Box.
     *
     * @return      The selected GradedEvent
     */
    @Override
    public GradedEvent getSelectedItem()
    {
        return this.selectedItem;
    }
}
