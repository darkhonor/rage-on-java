/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.Instructor;
import com.darkhonor.rage.model.Section;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;

/**
 * Class to handle all database queries for {@link com.darkhonor.rage.model.Section}
 * objects.
 *
 * @author Alex Ackerman
 */
public class SectionDAO
{

    /**
     * Default constructor.
     *
     * @param em    EntityManager connection to the database.
     * @throws IllegalArgumentException Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager is
     *                                  <code>null</code>
     */
    public SectionDAO(EntityManager em) throws IllegalArgumentException,
            NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("(SectionDAO) EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(SectionDAO) EntityManager is closed.  Throwing IllegalArgumentException");
            throw new IllegalArgumentException("Invalid EntityManager state");
        } else
        {
            this.em = em;
        }
        this.em = em;
    }

    public Long create(Section section) throws NullPointerException, IllegalArgumentException,
            IllegalStateException
    {
        if (section == null)
        {
            LOGGER.error("(create) The provided Section is null");
            throw new NullPointerException("The provided Section is null");
        } else if (section.getName() == null)
        {
            LOGGER.error("(create) The provided Section name is null");
            throw new NullPointerException("The provided Section name is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(create) The connection to the data source is closed");
            throw new IllegalStateException("The connection to the data source is closed");
        } else if (section.getCourse() == null)
        {
            LOGGER.error("(create) The provided Section's Course is null");
            throw new NullPointerException("The provided Section's Course is null");
        } else if (section.getInstructor() == null)
        {
            LOGGER.error("(create) The provided Section's Instructor is null");
            throw new NullPointerException("The provided Section's Instructor is null");
        }
        EntityTransaction tx = em.getTransaction();
        try
        {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Section> cq = cb.createQuery(Section.class);
            Root<Section> sectionRoot = cq.from(Section.class);
            Predicate criteria = cb.conjunction();
            criteria = cb.and(criteria, cb.equal(sectionRoot.get("course").get("name"),
                    section.getCourse().getName()));
            criteria = cb.and(criteria, cb.equal(sectionRoot.get("name"), section.getName()));
            cq.where(criteria);
            TypedQuery<Section> sectionQuery = em.createQuery(cq);

            Section dbSection = sectionQuery.getSingleResult();
            LOGGER.error("(create) The provided Section already exists");
            throw new IllegalArgumentException("The provided Section already exists");
        } catch (NoResultException ex)
        {
            LOGGER.error("(getSectionsForCourse) Exception Caught: "
                    + ex.getLocalizedMessage());
        }
        try
        {
            LOGGER.debug("(create) Saving the Section to the data source");
            tx.begin();
            em.persist(section);
            tx.commit();
            LOGGER.debug("(create) Saved Section (id = " + section.getId() +
                    ") to the data source");
            return section.getId();
        } catch (Exception ex)
        {
            LOGGER.error("(create) Exception thrown saving to the data source: " +
                    ex.getLocalizedMessage());
            return new Long(-1L);
        }
    }

    /**
     * Finds a Section object in the database with the given Id value.  The database
     * connection will still be active at the end of this method call.
     *
     * @param id                        The Id of the Section to find
     * @return                          The selected Section if found or
     *                                  <code>null</code> otherwise
     * @throws IllegalArgumentException Thrown when the Id is an invalid value--
     *                                  negative or 0.
     * @throws NullPointerException     Thrown when the Id is <code>null</code>
     * @throws IllegalStateException    Thrown if the connection to data source is not
     *                                  open
     */
    public Section find(Long id) throws IllegalArgumentException, NullPointerException,
            IllegalStateException
    {
        if (id == null)
        {
            LOGGER.error("(find) The provided Section Id is null");
            throw new NullPointerException("The provided Section Id is null");
        } else if (id < 1)
        {
            LOGGER.error("(find) The provided Section Id is less than 1");
            throw new IllegalArgumentException("The provided Section Id is less than 1");
        } else if (!em.isOpen())
        {
            LOGGER.error("(find) The connection to the data source is closed");
            throw new IllegalStateException("The connection to the data source is closed");
        }
        return em.find(Section.class, id);
    }

    public Section update(Section section) throws IllegalArgumentException, NullPointerException,
            IllegalStateException
    {
        if (section == null)
        {
            LOGGER.error("(update) The provided Section is null");
            throw new NullPointerException("The provided Section is null");
        } else if (section.getName() == null)
        {
            LOGGER.error("(update) The provided Section name is null");
            throw new NullPointerException("The provided Section name is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(update) The connection to the data source is closed");
            throw new IllegalStateException("The connection to the data source is closed");
        } else if (section.getCourse() == null)
        {
            LOGGER.error("(update) The provided Section's Course is null");
            throw new NullPointerException("The provided Section's Course is null");
        } else if (section.getInstructor() == null)
        {
            LOGGER.error("(update) The provided Section's Instructor is null");
            throw new NullPointerException("The provided Section's Instructor is null");
        }
        Section dbSection = em.find(Section.class, section.getId());
        if (dbSection == null)
        {
            LOGGER.error("(delete) The provided Section is not persisted");
            throw new IllegalArgumentException("The provided Section is not persisted");
        }
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        section = em.merge(section);
        tx.commit();

        return section;
    }

    public void delete(Section section) throws IllegalArgumentException, NullPointerException,
            IllegalStateException
    {
        if (section == null)
        {
            LOGGER.error("(delete) The provided Section is null");
            throw new NullPointerException("The provided Section is null");
        } else if (section.getName() == null)
        {
            LOGGER.error("(delete) The provided Section name is null");
            throw new NullPointerException("The provided Section name is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(delete) The connection to the data source is closed");
            throw new IllegalStateException("The connection to the data source is closed");
        } else if (section.getCourse() == null)
        {
            LOGGER.error("(delete) The provided Section's Course is null");
            throw new NullPointerException("The provided Section's Course is null");
        }
        Section dbSection = em.find(Section.class, section.getId());
        if (dbSection == null)
        {
            LOGGER.error("(delete) The provided Section is not persisted");
            throw new IllegalArgumentException("The provided Section is not persisted");
        }
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(dbSection);
        tx.commit();
    }

    public void delete(Long id) throws IllegalArgumentException, NullPointerException,
            IllegalStateException
    {
        if (id == null)
        {
            LOGGER.error("(delete) The provided Section Id is null");
            throw new NullPointerException("The provided Section Id is null");
        } else if (id < 1)
        {
            LOGGER.error("(delete) The provided Section Id is less than 1");
            throw new IllegalArgumentException("The provided Section Id is less than 1");
        } else if (!em.isOpen())
        {
            LOGGER.error("(delete) The connection to the data source is closed");
            throw new IllegalStateException("The connection to the data source is closed");
        }

        Section section = em.find(Section.class, id);
        if (section == null)
        {
            LOGGER.error("(delete) The provided Section id cannot be found in the "
                    + "data source");
            throw new IllegalArgumentException("The provided Section id cannot be "
                    + "found in the data source");
        }
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(section);
        tx.commit();
    }

    public List<Section> getSectionsForCourse(Course course) throws NullPointerException,
            IllegalStateException
    {
        if (!em.isOpen())
        {
            LOGGER.error("(getSectionsForCourse) EntityManager is closed.  "
                    + "Throwing IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (course == null)
        {
            LOGGER.error("(getSectionsForCourse) Course is null");
            throw new NullPointerException("Course is null");
        }
        LOGGER.debug("(getSectionsForCourse) Course: " + course.getName());

        List<Section> sectionResults = null;

        // Build the Query
        try
        {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Section> cq = cb.createQuery(Section.class);
            Root<Section> sectionRoot = cq.from(Section.class);
            cq.where(cb.equal(sectionRoot.get("course").get("name"), course.getName()));
            cq.orderBy(cb.asc(sectionRoot.get("name")));
            TypedQuery<Section> sectionQuery = em.createQuery(cq);

            sectionResults = sectionQuery.getResultList();
            LOGGER.debug("(getSectionsForCourse) # Sections Found: "
                    + sectionResults.size());
            for (int i = 0; i < sectionResults.size(); i++)
            {
                LOGGER.debug("(getSectionsForCourse) - Section " + i + ": "
                        + sectionResults.get(i));
            }
        } catch (Exception ex)
        {
            LOGGER.error("(getSectionsForCourse) Exception Caught: "
                    + ex.getLocalizedMessage());
        }

        // Run the query and return the resulting list of Graded Events
        return sectionResults;
    }

    public List<Section> getSectionsForCourseAndInstructor(Course course,
            Instructor instructor) throws NullPointerException, IllegalStateException
    {
        if (!em.isOpen())
        {
            LOGGER.error("(getSectionsForCourseAndInstructor) EntityManager is closed.  "
                    + "Throwing IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (course == null)
        {
            LOGGER.error("(getSectionsForCourseAndInstructor) Course is null");
            throw new NullPointerException("Course is null");
        } else if (instructor == null)
        {
            LOGGER.error("(getSectionsForCourseAndInstructor) Instructor is null");
            throw new NullPointerException("Instructor is null");
        }

        LOGGER.debug("(getSectionsForCourseAndInstructor) Course: " + course.getName());
        LOGGER.debug("(getSectionsForCourseAndInstructor) Instructor: "
                + instructor.getDomainAccount());

        List<Section> sectionResults = null;

        // Build the Query
        try
        {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Section> cq = cb.createQuery(Section.class);
            Root<Section> sectionRoot = cq.from(Section.class);
            Predicate criteria = cb.conjunction();
            criteria = cb.and(criteria, cb.equal(sectionRoot.get("course").get("name"),
                    course.getName()));
            criteria = cb.and(criteria, cb.equal(sectionRoot.get("instructor").get("webID"),
                    instructor.getWebID()));
            criteria = cb.and(criteria, cb.equal(sectionRoot.get("instructor").get("firstName"),
                    instructor.getFirstName()));
            criteria = cb.and(criteria, cb.equal(sectionRoot.get("instructor").get("lastName"),
                    instructor.getLastName()));
            cq.where(criteria);
            cq.orderBy(cb.asc(sectionRoot.get("name")));
            TypedQuery<Section> sectionQuery = em.createQuery(cq);

            sectionResults = sectionQuery.getResultList();
            LOGGER.debug("(getSectionsForCourseAndInstructor) # Sections Found: "
                    + sectionResults.size());
            for (int i = 0; i < sectionResults.size(); i++)
            {
                LOGGER.debug("(getSectionsForCourseAndInstructor) - Section " + i + ": "
                        + sectionResults.get(i));
            }
        } catch (Exception ex)
        {
            LOGGER.error("(getSectionsForCourseAndInstructor) Exception Caught: "
                    + ex.getLocalizedMessage());
        }

        // Run the query and return the resulting list of Graded Events
        return sectionResults;
    }

    /**
     * Closes the connection to the database.
     *
     * @throws IllegalStateException    Thrown if the database connection is
     *                                  already closed.
     */
    public void closeConnection() throws IllegalStateException
    {
        em.close();
    }

    /**
     * Returns whether or not the connection to the data source is open.
     *
     * @return      <code>true</code> if the connection is open, <code>false</code>
     *              otherwise
     * @throws NullPointerException     Thrown when the EntityManager is
     *                                  <code>null</code>
     */
    public boolean isOpen() throws NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("(isOpen) EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        }
        return em.isOpen();
    }
    private Logger LOGGER = Logger.getLogger(SectionDAO.class);
    private EntityManager em;
}
