/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs;

/**
 * Constants used throughout the RAGE on Java suite of tools.
 * 
 * @author Alexander Ackerman
 * 
 * @version 0.6.0
 */
public class RAGEConst {

    // Default Program Locations
    /**
     * The default location of the RAPTOR program.
     */
    public static final String DEFAULT_RAPTOR_EXECUTABLE =
            "C:\\Program Files\\RAPTOR\\raptor.exe";

    /**
     * The default location of the runme helper application
     */
    public static final String DEFAULT_PROCESSING_RUNNER =
            "C:\\Program Files\\RAGE on Java\\runme.exe";

    /**
     * Default PUBLIC String for RAGE DTD
     */
    public static final String DEFAULT_PUBLIC_URI =
            "-//Darkhonor Development//RAGE 1.0 DTD//EN";

    /**
     * Default SYSTEM String for RAGE DTD
     */
    public static final String DEFAULT_SYSTEM_URI =
            "http://rage.darkhonor.com/GradedEvent-DTD.dtd";

    /**
     * Default SCHEMA Namespace for RAGE Course Schema
     */
    public static final String DEFAULT_COURSE_SCHEMA_NAMESPACE =
            "http://rage.darkhonor.com/course";

    /**
     * Default SCHEMA Location for RAGE Course Schema
     */
    public static final String DEFAULT_COURSE_SCHEMA_LOCATION =
            "http://rage.darkhonor.com/course " +
            "http://rage.darkhonor.com/course/course-2.0.xsd";

    /**
     * Default SCHEMA Namespace for RAGE GradedEvent Schema
     */
    public static final String DEFAULT_GRADEDEVENT_SCHEMA_NAMESPACE =
            "http://rage.darkhonor.com/gradedEvent";

    /**
     * Default SCHEMA Location for RAGE GradedEvent Schema
     */
    public static final String DEFAULT_GRADEDEVENT_SCHEMA_LOCATION =
            "http://rage.darkhonor.com/gradedEvent " +
            "http://rage.darkhonor.com/gradedEvent/gradedEvent-2.0.xsd";

    // Default Database Connection Strings
    /**
     * A dummy String to indicate the database host has not been set.
     */
    public static final String DEFAULT_DBHOST = "<<DBHOST>>";
    
    /**
     * A dummy String to indicate the database name has not been set.
     */
    public static final String DEFAULT_DBNAME = "<<DBNAME>>";
    
    /**
     * A dummy String to indicate the database user name has not been set.
     */
    public static final String DEFAULT_DBUSER = "<<DBUSER>>";
    
    /**
     * A dummy String to indicate the database password has not been set.
     */
    public static final String DEFAULT_DBPASS = "<<DBPASSWORD>>";
    
    /**
     * A dummy String to indicate the database connection URI has not been set.
     */
    public static final String DEFAULT_DBURI = "<<DBURI>>";
    
    /**
     * A dummy String to indicate the Hibernate database dialect has not been set.
     */
    public static final String DEFAULT_DBDIALECT = "<<DBDIALECT>>";
    
    /**
     * A dummy String to indicate the database driver has not been set.
     */
    public static final String DEFAULT_DBDRIVER = "<<DBDRIVER>>";

    /**
     * The default value for RAGE applications for the eclipselink.ddl-generation.  
     * This is a flag value that will cause the line not to be added to the
     * configuration.
     */
    public static final String DEFAULT_DDL_OPTION = "none";

    /**
     * The value for Embedded databases when setting eclipselink.ddl-generation.
     */
    public static final String EMDEDDED_DDL_OPTION = "drop-and-create-tables";

    /**
     * The default database type selection for the ComboBox
     */
    public static final String DEFAULT_DBTYPE = "Select Database...";
    
    /**
     * An empty String to indicate the database port has not been set.
     */
    public static final String DEFAULT_DBPORT = "";
    
    /**
     * Supported Database Drivers and Dialects
     */
    ////////////////////////////////////////////////////////////////////////////
    // Apache Derby
    ////////////////////////////////////////////////////////////////////////////
    /**
     * String specifying the driver for the Apache Derby Embedded database
     */
    public static final String DERBY_EMBED_DRIVER =
            "org.apache.derby.jdbc.EmbeddedDriver";
    
    /**
     * String specifying the driver for the Apache Derby database
     */
    public static final String DERBY_NETWORK_DRIVER =
            "org.apache.derby.jdbc.ClientDriver";
    
    /**
     * String specifying the Hibernate Dialect for the Apache Derby database
     */
    public static final String DERBY_DIALECT =
            "org.hibernate.dialect.DerbyDialect";
    
    /**
     * The default port for the Apache Derby database
     */
    public static final int DERBY_NETWORK_PORT = 1527;
    
    ////////////////////////////////////////////////////////////////////////////
    // HSQL Database
    ////////////////////////////////////////////////////////////////////////////
    /**
     * String specifying the driver for the HypersonicSQL database
     */
    public static final String HSQLDB_DRIVER = "org.hsqldb.jdbcDriver";
    
    /**
     * String specifying the Hibernate Dialect for the HypersonicSQL database
     */
    public static final String HSQLDB_DIALECT =
            "org.hibernate.dialect.HSQLDialect";

    /**
     * The default port for the HypersonicSQL database
     */
    public static final int HSQLDB_PORT = 9001;

    /**
     * The default TLS port for the HypersonicSQL database
     */
    public static final int HSQLDB_TLS_PORT = 554;

    ////////////////////////////////////////////////////////////////////////////
    // MySQL
    ////////////////////////////////////////////////////////////////////////////
    /**
     * String specifying the driver for the MySQL database
     */
    public static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";

    /**
     * String specifying the Hibernate Dialect for the MySQL database
     */
    public static final String MYSQL_DIALECT =
            "org.hibernate.dialect.MySQLDialect";

    /**
     * The default port for the MySQL database
     */
    public static final int MYSQL_PORT = 3306;

    ////////////////////////////////////////////////////////////////////////////
    // PostgreSQL
    ////////////////////////////////////////////////////////////////////////////
    /**
     * String specifying the driver for the PostgreSQL database
     */
    public static final String POSTGRESQL_DRIVER = "org.postgresql.Driver";

    /**
     * String specifying the Hibernate Dialect for the PostgreSQL database
     */
    public static final String POSTGRESQL_DIALECT =
            "org.hibernate.dialect.PostgreSQLDialect";

    /**
     * The default port for the PostgreSQL database
     */
    public static final int POSTGRESQL_PORT = 5432;

    ////////////////////////////////////////////////////////////////////////////
    // Microsoft SQL Server
    ////////////////////////////////////////////////////////////////////////////
    /**
     * String specifying the driver for the Microsoft SQL Server database
     */
    public static final String MSSQL_DRIVER =
            "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    /**
     * String specifying the Hibernate Dialect for the Microsoft SQL Server database
     */
    public static final String MSSQL_DIALECT =
            "org.hibernate.dialect.SQLServerDialect";

    /**
     * The default port for the Microsoft SQL Server database
     */
    public static final int MSSQL_PORT = 1433;

    ////////////////////////////////////////////////////////////////////////////
    // Oracle
    ////////////////////////////////////////////////////////////////////////////
    /**
     * String specifying the driver for the Oracle database
     */
    public static final String ORACLE_DRIVER = "oracle.jdbc.driver.OracleDriver";

    /**
     * String specifying the Hibernate Dialect for the Oracle database
     */
    public static final String ORACLE_DIALECT =
            "org.hibernate.dialect.OracleDialect";

    /**
     * String specifying the Hibernate Dialect for the Oracle 9 database
     */
    public static final String ORACLE_9_DIALECT =
            "org.hibernate.dialect.Oracle9Dialect";

    /**
     * The default port for the Oracle database
     */
    public static final int ORACLE_PORT = 1521;

    /**
     * The default port for the Oracle Thin-Client database
     */
    public static final int ORACLE_THIN_PORT = 1526;

    /**
     * Question Type: RAPTOR
     */
    public static final int RAPTOR_QUESTION = 0;

    /**
     * Question Type: Processing
     */
    public static final int PROCESSING_QUESTION = 1;
}
