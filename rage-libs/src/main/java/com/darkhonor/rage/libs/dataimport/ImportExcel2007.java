/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataimport;

import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.Instructor;
import com.darkhonor.rage.model.Section;
import com.darkhonor.rage.model.Student;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Methods to import data into the RAGE Database from an Excel 2007
 * spreadsheet.
 * 
 * @author Alex Ackerman
 */
public class ImportExcel2007 implements ImportData
{

    /**
     * No argument constructor.  
     */
    public ImportExcel2007()
    {
    }

//    @Override
//    public List<Instructor> importInstructors(String filename)
//    {
//        OPCPackage pkg;
//        List<Instructor> instructors = null;
//        try
//        {
//            pkg = OPCPackage.open(filename, PackageAccess.READ);
//            XSSFWorkbook workbook = new XSSFWorkbook(pkg);
//            LOGGER.debug("File opened.  Opening Instructor worksheet");
//            XSSFSheet instructorSheet = workbook.getSheet("instructors");
//            instructors = importInstructors(instructorSheet);
//            LOGGER.debug("Closing the Excel file");
//            pkg.close();
//        }
//        catch (InvalidFormatException ex)
//        {
//            LOGGER.error("Invalid File Format: " + ex.getLocalizedMessage());
//        }
//        catch (OpenXML4JException ex)
//        {
//            LOGGER.error("OpenXML Exception Caught: " + ex.getLocalizedMessage());
//        }
//        catch (IOException ex)
//        {
//            LOGGER.error("IO Exception Caught: " + ex.getLocalizedMessage());
//        }
//        LOGGER.info("Imported " + instructors.size() + " instructors from file");
//        return instructors;
//    }
//    @Override
//    public List<Student> importStudents(String filename)
//    {
//        OPCPackage pkg;
//        List<Student> students = null;
//        try
//        {
//            pkg = OPCPackage.open(filename, PackageAccess.READ);
//            XSSFWorkbook workbook = new XSSFWorkbook(pkg);
//            LOGGER.debug("File opened.  Opening Student worksheet");
//            XSSFSheet studentSheet = workbook.getSheet("students");
//            students = importStudents(studentSheet);
//            LOGGER.debug("Closing the Excel file");
//            pkg.close();
//        }
//        catch (InvalidFormatException ex)
//        {
//            LOGGER.error("Invalid File Format: " + ex.getLocalizedMessage());
//        }
//        catch (OpenXML4JException ex)
//        {
//            LOGGER.error("OpenXML Exception Caught: " + ex.getLocalizedMessage());
//        }
//        catch (IOException ex)
//        {
//            LOGGER.error("IO Exception Caught: " + ex.getLocalizedMessage());
//        }
//        LOGGER.info("Imported " + students.size() + " students from file");
//        return students;
//    }
//    @Override
//    public List<Section> importSections(String filename)
//    {
//        OPCPackage pkg;
//        ArrayList<Section> sections = new ArrayList<Section>();
//        try
//        {
//            pkg = OPCPackage.open(filename, PackageAccess.READ);
//            XSSFWorkbook workbook = new XSSFWorkbook(pkg);
//            LOGGER.debug("File opened.  Opening Section worksheet");
//            XSSFSheet sectionSheet = workbook.getSheet("sections");
//            if (sectionSheet == null)
//            {
//                 LOGGER.error("Worksheet sections does not exist in file.");
//            }
//            else
//            {
//                int totalRows = sectionSheet.getPhysicalNumberOfRows();
//                LOGGER.debug("Total Rows in Section Worksheet: " + totalRows);
//                // Verify the Header row exists and contains the right columns
//                XSSFRow headerRow = sectionSheet.getRow(0);
//                String headerCell1 = headerRow.getCell(0).getStringCellValue();
//
//                if (headerCell1.isEmpty() || !headerCell1.equals("Section"))
//                {
//                    LOGGER.error("Section sheet is missing a proper header row");
//                }
//                else
//                {
//                    LOGGER.debug("Header information correct");
//                    for (int i = 1; i < totalRows; i++)
//                    {
//                        LOGGER.debug("Parsing Section Row #" + i);
//                        XSSFRow sectionRow = sectionSheet.getRow(i);
//                        Section section = new Section();
//                        section.setName(sectionRow.getCell(0).getStringCellValue());
//                        LOGGER.debug("Section " + section.getName() + " loaded.");
//                        sections.add(section);
//                    }
//                }
//                LOGGER.debug("Done loading sections.");
//            }
//            LOGGER.debug("Closing the Excel file");
//            pkg.close();
//        }
//        catch (InvalidFormatException ex)
//        {
//            LOGGER.error("Invalid File Format: " + ex.getLocalizedMessage());
//        }
//        catch (OpenXML4JException ex)
//        {
//            LOGGER.error("OpenXML Exception Caught: " + ex.getLocalizedMessage());
//        }
//        catch (IOException ex)
//        {
//            LOGGER.error("IO Exception Caught: " + ex.getLocalizedMessage());
//        }
//        return sections;
//    }
    @Override
    public Course importCourse(String filename)
    {
        OPCPackage pkg;
        Course course;
        ArrayList<Section> sections;
        ArrayList<Instructor> instructors;
        ArrayList<Student> students;
        try
        {
            pkg = OPCPackage.open(filename, PackageAccess.READ);
            XSSFWorkbook workbook = new XSSFWorkbook(pkg);
            LOGGER.debug("File opened.  Opening Student and Instructor worksheets");
            XSSFSheet courseSheet = workbook.getSheet("course");
            XSSFSheet sectionSheet = workbook.getSheet("sections");
            XSSFSheet studentSheet = workbook.getSheet("students");
            XSSFSheet instructorSheet = workbook.getSheet("instructors");
            if (courseSheet == null || studentSheet == null
                    || instructorSheet == null || sectionSheet == null)
            {
                LOGGER.error("Spreadsheet missing Course, Section, Instructor "
                        + "and/or Student worksheets");
                course = null;
            } else
            {
                LOGGER.debug("course, sections, students, and instructors worksheets found");
                XSSFRow courseRow = courseSheet.getRow(0);
                course = new Course(courseRow.getCell(0).getStringCellValue());
                LOGGER.debug("Course Name: " + course.getName());
                sections = (ArrayList<Section>) importSections(sectionSheet, course);
                if (sections.size() > 0)
                {
                    int count = 0;
                    instructors = (ArrayList<Instructor>) importInstructors(instructorSheet,
                            sections, course);
                    for (int i = 0; i < instructors.size(); i++)
                    {
                        course.addInstructor(instructors.get(i));
                        count++;
                    }
                    LOGGER.info("Added " + count + " instructors to " + course);
                    students = (ArrayList<Student>) importStudents(studentSheet,
                            sections, course);
                    LOGGER.info("Added " + students.size() + " students to " + course);
                    count = 0;
                    for (int i = 0; i < sections.size(); i++)
                    {
                        course.addSection(sections.get(i));
                        count++;
                    }
                    LOGGER.info("Added " + count + " sections to " + course);
                }
            }
            LOGGER.debug("Closing the Excel file");
            pkg.close();
        } catch (InvalidFormatException ex)
        {
            LOGGER.error("Invalid File Format: " + ex.getLocalizedMessage());
            course = null;
        } catch (IOException ex)
        {
            LOGGER.error("IO Exception Caught: " + ex.getLocalizedMessage());
            course = null;
        }
        LOGGER.info("Done importing Course");
        return course;
    }

    protected List<Section> importSections(XSSFSheet sectionSheet, Course course)
    {
        List<Section> sections = new ArrayList<>();
        if (sectionSheet == null)
        {
            LOGGER.error("Worksheet sections does not exist in file");
        } else
        {
            try
            {
                int totalRows = sectionSheet.getPhysicalNumberOfRows();
                LOGGER.debug("Total Rows in Section Worksheet: " + totalRows);
                XSSFRow headerRow = sectionSheet.getRow(0);
                String headerCell1 = headerRow.getCell(0).getStringCellValue();
                if (headerCell1.isEmpty() || !headerCell1.equals("Section"))
                {
                    LOGGER.error("Section sheet is missing a proper header row");
                    sections = null;
                } else
                {
                    LOGGER.debug("Header information correct");
                    for (int i = 1; i < totalRows; i++)
                    {
                        LOGGER.debug("Parsing Section Row #" + i);
                        XSSFRow sectionRow = sectionSheet.getRow(i);
                        Section section = new Section(sectionRow.getCell(0).getStringCellValue());
                        section.setCourse(course);
                        if (!sections.contains(section))
                        {
                            LOGGER.debug("Adding Section " + section.getName()
                                    + " to Course " + course);
                            sections.add(section);
                        }
                    }
                }
            } catch (IllegalArgumentException ex)
            {
                LOGGER.error("Illegal Argument imported: " + ex.getLocalizedMessage());
                sections = null;
            }
        }
        LOGGER.debug("Done loading sections.");
        return sections;
    }

    protected List<Instructor> importInstructors(XSSFSheet instructorSheet,
            List<Section> sections, Course course)
    {
        List<Instructor> instructors = new ArrayList<>();

        if (instructorSheet == null)
        {
            LOGGER.error("Worksheet instructors does not exist in file.");
            instructors = null;
        } else
        {
            try
            {
                int totalRows = instructorSheet.getPhysicalNumberOfRows();
                LOGGER.debug("Total Rows in Instructor Worksheet: " + totalRows);
                // Verify the Header row exists and contains the right columns
                XSSFRow headerRow = instructorSheet.getRow(0);
                String headerCell1 = headerRow.getCell(0).getStringCellValue();
                String headerCell2 = headerRow.getCell(1).getStringCellValue();
                String headerCell3 = headerRow.getCell(2).getStringCellValue();
                String headerCell4 = headerRow.getCell(3).getStringCellValue();
                String headerCell5 = headerRow.getCell(4).getStringCellValue();

                if (headerCell1.isEmpty() || !headerCell1.equals("Login")
                        || headerCell2.isEmpty() || !headerCell2.equals("Password")
                        || headerCell3.isEmpty() || !headerCell3.equals("First Name")
                        || headerCell4.isEmpty() || !headerCell4.equals("Last Name")
                        || headerCell5.isEmpty() || !headerCell5.equals("Section"))
                {
                    LOGGER.error("Instructor sheet is missing a proper header row");
                    instructors = null;
                } else
                {
                    LOGGER.debug("Header information correct");
                    for (int i = 1; i < totalRows; i++)
                    {
                        LOGGER.debug("Parsing Instructor Row #" + i);
                        XSSFRow instructorRow = instructorSheet.getRow(i);
                        Instructor instructor = new Instructor();
                        instructor.setDomainAccount(instructorRow.getCell(0).getStringCellValue());
                        instructor.setWebID(instructorRow.getCell(0).getStringCellValue());
                        instructor.setPassword(instructorRow.getCell(1).getStringCellValue());
                        instructor.setFirstName(instructorRow.getCell(2).getStringCellValue());
                        instructor.setLastName(instructorRow.getCell(3).getStringCellValue());

                        /**
                         * The assumption is that all Course Sections will be
                         * identified prior to loading instructors.
                         */
                        Section importedSection = new Section();
                        importedSection.setName(instructorRow.getCell(4).getStringCellValue());
                        importedSection.setCourse(course);
                        int sectionIndex = sections.indexOf(importedSection);

                        int instructorIndex = instructors.indexOf(instructor);
                        if (instructorIndex > -1)
                        {
                            LOGGER.debug("Instructor is in the system");
                            instructors.get(instructorIndex).addSection(sections.get(sectionIndex));
                            sections.get(sectionIndex).setInstructor(instructors.get(instructorIndex));
                        } else
                        {
                            LOGGER.debug("Instructor is not in system");
                            instructor.addSection(sections.get(sectionIndex));
                            instructors.add(instructor);
                            sections.get(sectionIndex).setInstructor(instructor);
                        }
                        LOGGER.debug(instructor.getFirstName() + " "
                                + instructor.getLastName() + " teaches Section "
                                + sections.get(sectionIndex) + " with Login "
                                + instructor.getDomainAccount());
                    }
                }
            } catch (IllegalArgumentException ex)
            {
                LOGGER.error("Invalid Argument: " + ex.getLocalizedMessage());
                instructors = null;
            }
            LOGGER.debug("Done loading instructors.");
        }
        return instructors;
    }

    protected List<Student> importStudents(XSSFSheet studentSheet,
            List<Section> sections, Course course)
    {
        ArrayList<Student> students = new ArrayList<>();
        if (studentSheet == null)
        {
            LOGGER.error("Worksheet students does not exist in file.");
        } else
        {
            try
            {
                int totalRows = studentSheet.getPhysicalNumberOfRows();
                LOGGER.debug("Total Rows in Student Worksheet: " + totalRows);
                // Verify the Header row exists and contains the right columns
                XSSFRow headerRow = studentSheet.getRow(0);
                // Cell A1: Year
                String headerCell1 = headerRow.getCell(0).getStringCellValue();
                // We're ignoring Cell B1 since it contains email--not stored at
                // this time.  Cell B1 should contain "Possible Email"
                
                // Cell C1: Login Name
                String headerCell2 = headerRow.getCell(2).getStringCellValue();
                // Cell D1: First Name
                String headerCell3 = headerRow.getCell(3).getStringCellValue();
                // Cell E1: Last Name
                String headerCell4 = headerRow.getCell(4).getStringCellValue();
                // Cell F1: Section
                String headerCell5 = headerRow.getCell(5).getStringCellValue();

                if (headerCell1.isEmpty() || !headerCell1.equals("Year")
                        || headerCell2.isEmpty() || !headerCell2.equals("Login Name")
                        || headerCell3.isEmpty() || !headerCell3.equals("First Name")
                        || headerCell4.isEmpty() || !headerCell4.equals("Last Name")
                        || headerCell5.isEmpty() || !headerCell5.equals("Section"))
                {
                    LOGGER.error("Student sheet is missing a proper header row");
                } else
                {
                    LOGGER.debug("Header information correct");
                    for (int i = 1; i < totalRows; i++)
                    {
                        LOGGER.debug("Parsing Student Row #" + i);
                        XSSFRow studentRow = studentSheet.getRow(i);
                        Student student = new Student();
                        student.setWebID(studentRow.getCell(2).getStringCellValue());
                        LOGGER.debug("Student Web ID: " + student.getWebID());
                        student.setFirstName(studentRow.getCell(3).getStringCellValue());
                        LOGGER.debug("Student First Name: " + student.getFirstName());
                        student.setLastName(studentRow.getCell(4).getStringCellValue());
                        LOGGER.debug("Student Last Name: " + student.getLastName());
                        Double tempClassYear = new Double(studentRow.getCell(0).getNumericCellValue());
                        LOGGER.debug("Class Year (Temp): " + tempClassYear);

                        student.setClassYear(new Integer(tempClassYear.intValue()));
                        LOGGER.debug("Student Class Year: " + student.getClassYear());

                        /**
                         * The assumption is that all Course Sections will be
                         * identified prior to loading students.
                         */
                        Section importedSection = new Section();
                        importedSection.setName(studentRow.getCell(5).getStringCellValue());
                        importedSection.setCourse(course);

                        int sectionIndex = sections.indexOf(importedSection);

                        int studentIndex = students.indexOf(student);
                        if (studentIndex > -1)
                        {
                            LOGGER.debug("Student is in the system");
                            students.get(studentIndex).addSection(sections.get(sectionIndex));
                            sections.get(sectionIndex).addStudent(students.get(studentIndex));
                            LOGGER.debug("Added Student to section "
                                    + sections.get(sectionIndex));
                        } else
                        {
                            LOGGER.debug("Student is not in the system");
                            LOGGER.debug("Section (from Spreadsheet): " + importedSection);
                            LOGGER.debug("Section (from Database): "
                                    + sections.get(sectionIndex));
                            student.addSection(sections.get(sectionIndex));
                            sections.get(sectionIndex).addStudent(student);
                            LOGGER.debug("Added Student to section "
                                    + sections.get(sectionIndex));
                            students.add(student);
                            LOGGER.debug("Added Student to list");
                        }
                        LOGGER.debug(student.getFirstName() + " "
                                + student.getLastName() + " is in Section "
                                + sections.get(sectionIndex).getName() + " with Login "
                                + student.getWebID());
                    }
                }
            } catch (NumberFormatException ex)
            {
                LOGGER.error("Invalid Number Format: " + ex.getLocalizedMessage());
                students = null;
            } catch (IllegalArgumentException ex)
            {
                LOGGER.error("Invalid Argument: " + ex.getLocalizedMessage());
                students = null;
            } catch (NullPointerException ex)
            {
                LOGGER.error("Null Pointer Exception Caught: " + ex.getLocalizedMessage());
            }
            LOGGER.debug("Done loading students.");
        }
        return students;
    }
    
    private final Logger LOGGER = Logger.getLogger(ImportExcel2007.class);
}
