/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.listmodel;

import com.darkhonor.rage.model.Student;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 * An implementation of an {@link AbstractListModel} to hold {@link Student}
 * objects.
 * 
 * @author Alex Ackerman
 */
public class StudentComboBoxModel extends AbstractListModel<Student> 
    implements ComboBoxModel<Student>
{

    public StudentComboBoxModel()
    {
        data = new ArrayList<Student>();
        selectedItem = null;
    }

    /**
     * Returns the number of Student objects in the ListModel.
     * 
     * @return  The number of Students in the ListModel object
     */
    @Override
    public int getSize()
    {
        return data.size();
    }

    /**
     * Returns a generic Object from the ListModel.  Since the model contains
     * Student objects, it is safe to cast the result of this to a Student
     * object.
     * 
     * @param index     The item in the list to return
     * @return          The identified Object.  This is castable to Student.
     */
    @Override
    public Student getElementAt(int index)
    {
        return data.get(index);
    }

    /**
     * Adds the provided Student to the ListModel object.
     *
     * @param student       The Student object to add to the ListModel
     */
    public void add(Student student)
    {
        data.add(student);
        this.fireContentsChanged(data, data.indexOf(student),
                data.lastIndexOf(student));
    }

    /**
     * Adds all elements of the provided List of Students to the ListModel object.
     * Since the model is a typed Object, an IllegalArgumentException will be
     * thrown if students is not a List of student objects.
     *
     * @param students      The List of Student objects
     * @throws IllegalArgumentException     Thrown if the first object in the
     *                              List is not a Student object.
     */
    public void addAll(List<Student> students) throws IllegalArgumentException
    {
        if (students == null
                || students.get(0).getClass() != Student.class)
        {
            throw new IllegalArgumentException();
        }
        data.addAll(students);
        this.fireContentsChanged(data, 0, data.size());
    }

    /**
     * Removes all Student objects in the ListModel object.
     */
    public void removeAll()
    {
        data.clear();
        this.fireIntervalRemoved(data, 0, 0);
    }

    /**
     * Removes the Student object from the list at the index identified.
     *
     * @param index     The index of the Student to remove from the model.
     * @return          The removed Student object
     */
    public Student remove(int index)
    {
        this.fireIntervalRemoved(data, index, index);
        return data.remove(index);
    }

    /**
     * Sorts the list of Students in the List Model.
     */
    public void sort()
    {
        Collections.sort(data);
        this.fireContentsChanged(data, 0, data.size());
    }
    
    private final List<Student> data;
    private Student selectedItem;


    /**
     * Sets the Selected Item in the ComboBox to the specfied Item.
     * 
     * @param anItem        The selected item
     */
    @Override
    public void setSelectedItem(Object anItem)
    {
        this.selectedItem = (Student)anItem;
    }

    /**
     * Returns the selected item from the ComboBox
     *
     * @return              The selected Item
     */
    @Override
    public Object getSelectedItem()
    {
        return this.selectedItem;
    }
}
