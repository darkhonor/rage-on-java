/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.listmodel;

import com.darkhonor.rage.model.TestCase;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 * The TestCaseListModel implements a simple {@link AbstractListModel} for the
 * {@link TestCase} class.  This will be available for use by RAGE applications.
 *
 * @author Alexander Ackerman
 *
 * @version 1.0.1
 * 
 */
public class TestCaseListModel extends AbstractListModel<TestCase>
{

    /**
     * TestCaseListModel default constructor
     */
    public TestCaseListModel()
    {
        testCases = new ArrayList<TestCase>();
    }

    /**
     * Returns the number of elements in the List Model
     *
     * @return  The number of elements in the model.
     */
    @Override
    public int getSize()
    {
        return testCases.size();
    }

    /**
     * Returns the Object found at the specified index.
     *
     * @param index The index of the element to return
     * @return      The object at the index specified
     */
    @Override
    public TestCase getElementAt(int index)
    {
        return testCases.get(index);
    }

    /**
     * Adds a TestCase object to the List Model
     *
     * @param test  The TestCase to add to the model
     */
    public void addTestCase(TestCase test)
    {
        testCases.add(test);
        this.fireContentsChanged(testCases, testCases.indexOf(test),
                testCases.lastIndexOf(test));
    }

    /**
     * Removes all elements from the List Model.  After this method has ran,
     * the list will be empty.
     */
    public void removeAll()
    {
        testCases.clear();
        this.fireContentsChanged(testCases, 0, 0);
    }

    /**
     * Removes the TestCase at the specified index.  After this method is ran,
     * the TestCase object will no longer be in the List Model
     *
     * @param index     The index of the TestCase to remove
     * @return          The removed TestCase
     */
    public TestCase remove(int index)
    {
        this.fireIntervalRemoved(testCases, index, index);
        return testCases.remove(index);
    }

    /**
     * Return the list of Test Cases from the List Model
     *
     * @return List     The list of TestCase objects
     */
    public List<TestCase> getTestCases()
    {
        return testCases;
    }

    /**
     * Clone the List of Test Cases.
     *
     * @return List     The List of TestCase cloned.
     * @throws java.lang.CloneNotSupportedException Thrown if the clone command is not supported
     */
    public List<TestCase> cloneTestCases() throws CloneNotSupportedException
    {
        List<TestCase> outputList = new ArrayList<TestCase>();
        for (int i = 0; i < testCases.size(); i++)
        {
            // Clone the TestCase and add the clone to the new list
            outputList.add(testCases.get(i).clone());
        }
        return outputList;
    }
    
    private final List<TestCase> testCases;
}
