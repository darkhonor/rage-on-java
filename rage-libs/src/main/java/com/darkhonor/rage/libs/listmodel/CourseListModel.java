/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.listmodel;

import com.darkhonor.rage.model.Course;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 * Extension of the {@link AbstractListModel} class to display {@link Course}
 * objects.
 * 
 * @author Alexander Ackerman
 */
public class CourseListModel extends AbstractListModel<Course>
{

    public CourseListModel()
    {
        data = new ArrayList<Course>();
    }

    @Override
    public int getSize()
    {
        return data.size();
    }

    @Override
    public Course getElementAt(int index)
    {
        return data.get(index);
    }

    public void add(Course course)
    {
        data.add(course);
        this.fireContentsChanged(data, data.indexOf(course),
                data.lastIndexOf(course));
    }

    public void removeAll()
    {
        data.clear();
        this.fireIntervalRemoved(data, 0, 0);
    }

    public Course remove(int index)
    {
        this.fireIntervalRemoved(data, index, index);
        return data.remove(index);
    }
    
    private final List<Course> data;
}
