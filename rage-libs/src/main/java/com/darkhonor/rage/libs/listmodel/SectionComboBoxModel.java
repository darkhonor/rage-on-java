/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.listmodel;

import com.darkhonor.rage.model.Section;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import org.apache.log4j.Logger;

/**
 * Implementation of the {@link ComboBoxModel} for the Section selection ComboBox
 * in CourseGrader.  This implementation stores the actual {@link Section} in the
 * list.
 * 
 * @author Alex Ackerman
 */
public class SectionComboBoxModel extends AbstractListModel<Section> 
    implements ComboBoxModel<Section>
{

    /**
     * No argument constructor.
     */
    public SectionComboBoxModel()
    {
        data = new ArrayList<Section>();
    }

    /**
     * Returns the number of elements in the ComboBox list.
     *
     * @return  The number of elements in the list
     */
    @Override
    public int getSize()
    {
        return data.size();
    }

    /**
     * Returns the {@link Section} stored at the given index in the ComboBox
     * (starting at 0).
     *
     * @param index     The index of the item to retrieve
     * @return          The Section stored in the list
     * @throws IllegalArgumentException Thrown when an invalid index is specified
     */
    @Override
    public Section getElementAt(int index) throws IllegalArgumentException
    {
        if (index < 0 || index >= data.size())
        {
            throw new IllegalArgumentException("Invalid index specified.");
        } else
        {
            return data.get(index);
        }
    }

    /**
     * Sets the selected {@link Section} in the ComboBox.
     *
     * @param anItem    The Section that is selected
     */
    @Override
    public void setSelectedItem(Object anItem)
    {
        selectedItem = (Section) anItem;
    }

    /**
     * Returns the selected {@link Section} from the ComboBox.
     *
     * @return  The selected Section
     */
    @Override
    public Object getSelectedItem()
    {
        return selectedItem;
    }

    /**
     * Add a {@link Section} to the ComboBox.
     *
     * @param section    The Section to add
     * @throws IllegalArgumentException Thrown when section is {@code null}
     */
    public void add(Section section) throws IllegalArgumentException
    {
        if (section == null)
        {
            throw new IllegalArgumentException("Invalid Argument - Section "
                    + "cannot be null");
        } else
        {
            data.add(section);
            this.fireContentsChanged(data, data.indexOf(section),
                    data.lastIndexOf(section));
        }
    }

    /**
     * Adds a {@link Set} of sections to the ComboBox Model.  After the set is 
     * added, the List of {@link Section} objects will be sorted.
     * 
     * @param sections      The Set of Sections to add to the Model
     * @throws IllegalArgumentException Thrown when the set of sections is
     *          {@code null}
     */
    public void addSet(Set<Section> sections) throws IllegalArgumentException
    {
        if (sections == null)
        {
            throw new IllegalArgumentException("Invalid argument.  Sections "
                    + "cannot be a null set");
        } else
        {
            for (Section s : sections)
            {
                data.add(s);
            }
            Collections.sort(data);
        }
    }

    /**
     * Adds a {@link List} of sections to the ComboBox model.  After the list is
     * added, the List of {@link Section} objects will be sorted.
     *
     * @param sections   The List of Sections
     * @throws IllegalArgumentException Thrown when the list of sections is
     *      {@code null}
     */
    public void addList(List<Section> sections) throws IllegalArgumentException
    {
        if (sections == null)
        {
            LOGGER.error("Sections cannot be a null List");
            throw new IllegalArgumentException("Invalid argument.  Sections "
                    + "cannot be a null list");
        } else
        {
            for (int i = 0; i < sections.size(); i++)
            {
                data.add(sections.get(i));
            }
            LOGGER.debug("Added " + sections.size() + " sections to data");
            try
            {
                LOGGER.debug("Sorting Data");
                Collections.sort(data);
            } catch (ClassCastException ex)
            {
                LOGGER.error("Elemets are not mutually comparable");
            } catch (UnsupportedOperationException ex)
            {
                LOGGER.error("The Specified List does not support the set operation");
            } catch (Exception ex)
            {
                LOGGER.error("Unknown exception caught: " + ex.getLocalizedMessage());
            }
        }
    }

    /**
     * Clear the ComboBox of any {@link Section} objects.  After this method has
     * been run, the ComboBox list will be empty.
     */
    public void removeAll()
    {
        data.clear();
        this.fireIntervalRemoved(data, 0, 0);
    }

    /**
     * Remove the {@link Section} at the specified index in the list.  The list
     * is 0-based.  The Section that is removed is returned to the calling method.
     *
     * @param index     The index of the Section to remove
     * @return          The removed Section
     * @throws IllegalArgumentException Thrown when an invalid index is specified
     */
    public Section remove(int index) throws IllegalArgumentException
    {
        if (index < 0 || index >= data.size())
        {
            throw new IllegalArgumentException("Invalid index specified");
        } else
        {
            this.fireIntervalRemoved(data, index, index);
            return data.remove(index);
        }
    }

    /**
     * Sorts the {@link Section} objects stored in the list using the natural
     * sorting order.
     */
    public void sort()
    {
        Collections.sort(data);
    }
    
    private final ArrayList<Section> data;
    private Section selectedItem;
    private final Logger LOGGER = Logger.getLogger(SectionComboBoxModel.class);
}
