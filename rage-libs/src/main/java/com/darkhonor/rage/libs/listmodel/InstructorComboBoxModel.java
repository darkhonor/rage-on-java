/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.listmodel;

import com.darkhonor.rage.model.Instructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 * Implementation of the {@link ComboBoxModel} for the Instructor selection ComboBox
 * in CourseGrader.  This implementation stores the actual {@link Instructor}
 * in the list.
 * 
 * @author Alex Ackerman
 */
public class InstructorComboBoxModel extends AbstractListModel<Instructor> 
    implements ComboBoxModel<Instructor>
{

    /**
     * No argument constructor.
     */
    public InstructorComboBoxModel()
    {
        data = new ArrayList<Instructor>();
    }

    /**
     * Returns the number of elements in the ComboBox list.
     *
     * @return  The number of elements in the list
     */
    @Override
    public int getSize()
    {
        return data.size();
    }

    /**
     * Returns the {@link Instructor} stored at the given index in the ComboBox
     * (starting at 0).
     *
     * @param index     The index of the item to retrieve
     * @return          The Instructor stored in the list
     */
    @Override
    public Instructor getElementAt(int index) throws IllegalArgumentException
    {
        if (index < 0 || index >= data.size())
        {
            throw new IllegalArgumentException("Invalid index specified");
        } else
        {
            return data.get(index);
        }
    }

    /**
     * Sets the selected {@link Instructor} in the ComboBox.
     *
     * @param anItem    The Instructor that is selected
     */
    @Override
    public void setSelectedItem(Object anItem)
    {
        selectedItem = (Instructor) anItem;
    }

    /**
     * Returns the selected {@link Instructor} from the ComboBox.
     *
     * @return  The selected Instructor
     */
    @Override
    public Object getSelectedItem()
    {
        return selectedItem;
    }

    /**
     * Add a {@link Instructor} to the ComboBox.
     *
     * @param instructor    The Instructor to add
     * @throws IllegalArgumentException Thrown when the specified Instructor is
     *      {@code null}
     */
    public void add(Instructor instructor) throws IllegalArgumentException
    {
        if (instructor == null)
        {
            throw new IllegalArgumentException("Invalid Argument - Instructor "
                    + "cannot be null");
        } else
        {
            data.add(instructor);
            this.fireContentsChanged(data, data.indexOf(instructor),
                    data.lastIndexOf(instructor));
        }
    }

    /**
     * Adds a {@link Set} of instructors to the ComboBox Model.  After the set is 
     * added, the List of {@link Instructor} objects will be sorted.
     * 
     * @param instructors   The Set of Instructors
     * @throws IllegalArgumentException Thrown when the set of instructos is 
     *          {@code null}
     */
    public void addSet(Set<Instructor> instructors) throws IllegalArgumentException
    {
        if (instructors == null)
        {
            throw new IllegalArgumentException("Invalid argument.  Instructors "
                    + "cannot be a null set");
        } else
        {
            for (Instructor i : instructors)
            {
                data.add(i);
            }
            Collections.sort(data);
        }
    }

    /**
     * Adds a {@link List} of instructos to the ComboBox model.  After the list is
     * added, the List of {@link Instructor} objects will be sorted.
     * 
     * @param instructors   The List of Instructors
     * @throws IllegalArgumentException Thrown when the list of instructos is
     *      {@code null}
     */
    public void addList(List<Instructor> instructors) throws IllegalArgumentException
    {
        if (instructors == null)
        {
            throw new IllegalArgumentException("Invalid argument.  Instructors "
                    + "cannot be a null list");
        } else
        {
            for (int i = 0; i < instructors.size(); i++)
            {
                data.add(instructors.get(i));
            }
            Collections.sort(data);
        }
    }

    /**
     * Clear the ComboBox of any {@link Instructor} objects.  After this method
     * has been run, the ComboBox list will be empty.
     */
    public void removeAll()
    {
        data.clear();
        this.fireIntervalRemoved(data, 0, 0);
    }

    /**
     * Remove the {@link Instructor} at the specified index in the list.  The list
     * is 0-based.  The Instructor that is removed is returned to the calling method.
     *
     * @param index     The index of the Instructor to remove
     * @return          The removed Instructor
     * @throws IllegalArgumentException Thrown when an invalid index is specified
     */
    public Instructor remove(int index) throws IllegalArgumentException
    {
        if (index < 0 || index >= data.size())
        {
            throw new IllegalArgumentException("Invalid index specified");
        } else
        {
            this.fireIntervalRemoved(data, index, index);
            return data.remove(index);
        }
    }

    /**
     * Sorts the {@link Instructor} objects stored in the list using the natural
     * sorting order.
     */
    public void sort()
    {
        Collections.sort(data);
    }
    
    private final ArrayList<Instructor> data;
    private Instructor selectedItem;
}
