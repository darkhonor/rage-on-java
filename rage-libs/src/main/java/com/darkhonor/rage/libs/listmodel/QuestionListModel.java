/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.listmodel;

import com.darkhonor.rage.model.Question;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 * Implementation of the {@link AbstractListModel} to store
 * {@link com.darkhonor.rage.model.Question} objects.
 *
 * @author Alexander Ackerman
 */
public class QuestionListModel extends AbstractListModel<Question>
{

    /**
     * Default constructor
     */
    public QuestionListModel()
    {
        data = new ArrayList<Question>();
    }

    /**
     * Returns the number of elements in the ListModel
     *
     * @return  The number of elements in the ListModel
     */
    @Override
    public int getSize()
    {
        return data.size();
    }

    /**
     * Returns the {@link com.darkhonor.rage.model.Question} stored at the given
     * index in the ListModel.  The ListModel index is 0-based.
     *
     * @param index The index of the element to return
     * @return      The Question object stored in the ListModel
     */
    @Override
    public Question getElementAt(int index)
    {
        return data.get(index);
    }
    
    /**
     * Add a {@link com.darkhonor.rage.model.Question} to the ListModel
     *
     * @param question   The Question to add to the ListModel
     */
    public void add(Question question)
    {
        data.add(question);
        this.fireContentsChanged(data, data.indexOf(question),
                data.lastIndexOf(question));
    }

    /**
     * Removes all elements from the ListModel.  After this is called, the
     * ListModel will be empty.
     */
    public void removeAll()
    {
        data.clear();
        this.fireIntervalRemoved(data, 0, 0);
    }

    /**
     * Remove the {@link com.darkhonor.rage.model.Question} stored in the
     * ListModel at the given index.  The ListModel index is 0-based.
     *
     * @param index     The index of the Question to remove
     * @return          The removed Question
     */
    public Question remove(int index)
    {
        this.fireIntervalRemoved(data, index, index);
        return data.remove(index);
    }
    
    private final List<Question> data;
}
