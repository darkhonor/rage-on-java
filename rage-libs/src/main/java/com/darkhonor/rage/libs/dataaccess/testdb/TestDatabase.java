/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess.testdb;

import com.darkhonor.rage.libs.RAGEConst;
import com.darkhonor.rage.model.Category;
import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.GradedEvent;
import com.darkhonor.rage.model.Instructor;
import com.darkhonor.rage.model.Question;
import com.darkhonor.rage.model.Section;
import com.darkhonor.rage.model.Student;
import com.darkhonor.rage.model.TestCase;
import com.darkhonor.rage.model.Version;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;

/**
 * An in-memory Derby database used by the DAO class tests.  This class will be
 * constructed and initialized.  All elements required for all tests will be
 * stored in the database using this class.  This will help to provide some
 * consistency between DAO tests.  
 *
 * @author Alex Ackerman
 */
public class TestDatabase
{

    public TestDatabase()
    {
        Map<String,String> dbProperties = new HashMap<>();
        // Derby Memory Only
        dbProperties.put("javax.persistence.jdbc.driver", RAGEConst.DERBY_EMBED_DRIVER);
        dbProperties.put("javax.persistence.jdbc.url",
                "jdbc:derby:memory:testdb;create=true");
        dbProperties.put("javax.persistence.jdbc.user", "sa");
        dbProperties.put("javax.persistence.jdbc.password", "");

        emf = Persistence.createEntityManagerFactory("rage-libs", dbProperties);
        initialized = false;
    }

    /**
     * Method to create all database entries prior to use.  All methods should
     * check to ensure this method has successfully completed prior to providing
     * any access to calling methods.
     */
    public void initialize()
    {
        persistDatabaseVersion(2.1);
        Course course1 = createTestCourse();
        Course course2 = createSecondCourse();

        Category category1 = createPersistedCategory(1L, "Array");
        Category category2 = createPersistedCategory(2L, "Loop");
        Category category3 = createPersistedCategory(3L, "Iteration");
        // Create some specific TestCases to start with that can be used for testing
        // createPersistedQuestion does this as well, but with unpredictable
        // TestCase id values
        TestCase testCase1 = new TestCase(new BigDecimal("1.0"));
        testCase1.setId(5L);
        testCase1.addInput("1234");
        testCase1.addInput("5678");
        testCase1.addOutput("Result = 12345678");
        testCase1.addExclusion("2345");
        testCase1.addExclusion("9876");
        testCase1.addExclusion("Result = 12345678");

        TestCase testCase2 = new TestCase(new BigDecimal("2.5"));
        testCase2.setId(2L);
        testCase2.addInput("4");
        testCase2.addInput("5");
        testCase2.addOutput("Result = 999999999");
        
        TestCase testCase3 = new TestCase(new BigDecimal("1.0"));
        testCase3.setId(3L);
        testCase3.addInput("123");
        testCase3.addInput("1234");
        testCase3.addOutput("12345");

        TestCase testCase4 = new TestCase(new BigDecimal("1.0"));
        testCase4.setId(4L);
        testCase4.addInput("234");
        testCase4.addInput("2345");
        testCase4.addOutput("23456");
        
        TestCase testCase5 = new TestCase(new BigDecimal("3.5"));
        testCase5.setId(6L);
        testCase5.addOutput("Hello World");
        testCase5.addExclusion("Mini Me");

        persistTestCase(testCase1);
        persistTestCase(testCase2);
        persistTestCase(testCase3);
        persistTestCase(testCase4);
        persistTestCase(testCase5);
        
        List<TestCase> testCases = new ArrayList<>();
        testCases.add(testCase3);
        testCases.add(testCase4);
        
        Question question1 = createPersistedQuestion(1L, category1, "Sequencer",
                "Test Question 1", true, false, testCases);
        Question question2 = createPersistedQuestion(2L, category2, "Looper",
                "Test Question 2", false, false, testCases);
        Question question3 = createPersistedQuestion(3L, category2, "Jumper",
                "Test Question 3", true, true, testCases);

        GradedEvent gradedEvent1 = createPersistedGradedEvent(5L, "Summer 2010", course1,
                "PEX1", false, "ALL");
        gradedEvent1 = addQuestionToGradedEvent(gradedEvent1, question1);
        gradedEvent1 = addQuestionToGradedEvent(gradedEvent1, question2);

        GradedEvent gradedEvent2a = createPersistedGradedEvent(2L, "Summer 2010", course1,
                "PEX2", true, "M1");
        gradedEvent2a = addQuestionToGradedEvent(gradedEvent2a, question2);

        GradedEvent gradedEvent2b = createPersistedGradedEvent(3L, "Summer 2010", course1,
                "PEX2", true, "M2");
        gradedEvent2b = addQuestionToGradedEvent(gradedEvent2b, question3);

        GradedEvent gradedEvent3 = createPersistedGradedEvent(4L, "Summer 2010", course2,
                "Design 1", true, "FALSE");
        gradedEvent3 = addQuestionToGradedEvent(gradedEvent3, question3);

        initialized = true;
    }

    public void reset()
    {
        initialized = false;

        clearDatabase();
        initialize();
    }

    private void clearDatabase()
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        String queryString;
        Query query;

        queryString = "DELETE FROM GradedEvent g";
        query = em.createQuery(queryString);
        tx.begin();
        query.executeUpdate();
        tx.commit();

        queryString = "DELETE FROM Question q";
        query = em.createQuery(queryString);
        tx.begin();
        query.executeUpdate();
        tx.commit();

        queryString = "DELETE FROM Category c";
        query = em.createQuery(queryString);
        tx.begin();
        query.executeUpdate();
        tx.commit();

        queryString = "DELETE FROM Course c";
        query = em.createQuery(queryString);
        tx.begin();
        query.executeUpdate();
        tx.commit();

        queryString = "DELETE FROM Instructor i";
        query = em.createQuery(queryString);
        tx.begin();
        query.executeUpdate();
        tx.commit();

        queryString = "DELETE FROM Student s";
        query = em.createQuery(queryString);
        tx.begin();
        query.executeUpdate();
        tx.commit();

        queryString = "DELETE FROM Section s";
        query = em.createQuery(queryString);
        tx.begin();
        query.executeUpdate();
        tx.commit();

        queryString = "DELETE FROM Version v";
        query = em.createQuery(queryString);
        tx.begin();
        query.executeUpdate();
        tx.commit();
        
        queryString = "DELETE FROM TestCase t";
        query = em.createQuery(queryString);
        tx.begin();
        query.executeUpdate();
        tx.commit();

        em.close();
    }
    
    /**
     * Returns a connection to the in-memory database used for testing.  It is
     * the responsibility of the calling method to close the EntityManager when
     * complete.
     *
     * @return      A connection to the test database
     * @throws IllegalStateException        Thrown when the test database has not
     *                                      been initialized.
     */
    public EntityManager getConnection() throws IllegalStateException
    {
        if (!initialized)
        {
            throw new IllegalStateException("Test Database has not been initialized");
        }
        return emf.createEntityManager();
    }

    /**
     * Closes the connection to the test database.  This will in essence erase the
     * contents of the database.
     *
     * @return      <code>true</code> if the test database has been shut down.
     *              <code>false</code> otherwise.
     */
    public boolean shutdown()
    {
        emf.close();
        return !emf.isOpen();
    }

    /**
     * Utility method to create the same course for testing.
     * @return      The created Course;
     */
    private Course createTestCourse()
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        Course course = new Course("CS110");
        course.setId(1L);
        tx.begin();
        em.persist(course);
        tx.commit();
        // Create 2 Sections, 2 Students per Section, and 2 Instructors
        Student student1 = createPersistedStudent(1L, "John", "Doe", "C13John.Doe",
                new Integer(2013));
        Student student2 = createPersistedStudent(2L, "Mary", "Johnson", "C13Mary.Johnson",
                new Integer(2013));
        Student student3 = createPersistedStudent(3L, "Bob", "Reimer", "C13Robert.Reimer",
                new Integer(2013));
        Student student4 = createPersistedStudent(4L, "Andrew", "Callow", 
                "C13Andrew.Callow", new Integer(2013));

        Section section1 = new Section("M1A");
        section1.setId(1L);
        Section section2 = new Section("M2B");
        section2.setId(2L);
        section1.setCourse(course);
        section2.setCourse(course);
        
        Instructor instructor1 = createPersistedInstructor(5L, "David", "Roberts",
                "David.Roberts");
        Instructor instructor2 = createPersistedInstructor(6L, "Sarah", "O'Reilly",
                "Sarah.OReilly");

        // Set the Instructors for Sections 1 & 2
        section1.setInstructor(instructor1);
        section2.setInstructor(instructor2);

        // Add Instructors to Course
        course.addInstructor(instructor1);
        course.addInstructor(instructor2);

        // Add Students 1 & 2 to Section 1
        section1.addStudent(student1);
        student1.addSection(section1);
        section1.addStudent(student2);
        student2.addSection(section1);

        course.addSection(section1);
        course.addSection(section2);

        // Add Students 3 & 4 to Section 2
        section2.addStudent(student3);
        student3.addSection(section2);
        section2.addStudent(student4);
        student4.addSection(section2);

        tx.begin();
        em.persist(section1);
        em.persist(section2);
        em.merge(instructor1);
        em.merge(instructor2);
        em.merge(student1);
        em.merge(student2);
        em.merge(student3);
        em.merge(student4);
        em.merge(course);
        tx.commit();

        LOGGER.info("Saved Course (" + course + ") to database with " +
                course.getSections().size() + " sections and " +
                course.getInstructors().size() + " instructors");
        em.close();
        return course;
    }

    private Course createSecondCourse()
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        // Create Second Course
        Course course = new Course("CS364");
        course.setId(2L);
        tx.begin();
        em.persist(course);
        tx.commit();
        // Create 2 Sections, 2 Students per Section, and 2 Instructors
        Student student1 = createPersistedStudent(7L, "John", "Doe", "C11John.Doe",
                new Integer(2011));
        Student student2 = createPersistedStudent(8L, "Mary", "Johnson",
                "C11Mary.Johnson", new Integer(2011));
        Student student3 = createPersistedStudent(3L, "Bob", "Reimer",
                "C13Robert.Reimer", new Integer(2013));
        Student student4 = createPersistedStudent(4L, "Andrew", "Callow",
                "C13Andrew.Callow", new Integer(2013));
        Student student5 = createPersistedStudent(9L, "Kerry", "Beevers",
                "C11Kerry.Beevers", new Integer(2011));

        Instructor instructor1 = createPersistedInstructor(5L, "David", "Roberts",
                "David.Roberts");

        Section section1 = new Section("T5A");
        section1.setId(3L);
        Section section2 = new Section("T6A");
        section2.setId(4L);
        section1.setCourse(course);
        section2.setCourse(course);

        // Set the Instructors for Sections 1 & 2
        section1.setInstructor(instructor1);
        section2.setInstructor(instructor1);

        // Add Instructors to Course
        course.addInstructor(instructor1);

        // Add Students 1 & 2 to Section 1
        section1.addStudent(student1);
        student1.addSection(section1);
        section1.addStudent(student2);
        student2.addSection(section1);

        course.addSection(section1);
        course.addSection(section2);

        // Add Students 3 & 4 to Section 2
        section2.addStudent(student3);
        student3.addSection(section2);
        section2.addStudent(student4);
        student4.addSection(section2);
        section2.addStudent(student5);
        student5.addSection(section2);

        tx.begin();
        em.persist(section1);
        em.persist(section2);
        em.merge(instructor1);
        em.merge(student1);
        em.merge(student2);
        em.merge(student3);
        em.merge(student4);
        em.merge(student5);
        em.merge(course);
        tx.commit();

        LOGGER.info("Saved Course (" + course + ") to database with " +
                course.getSections().size() + " sections and " +
                course.getInstructors().size() + " instructors");
        em.close();
        return course;
    }

    private Section createPersistedSection(Long id, Course course, String name)
    {
        EntityManager em = emf.createEntityManager();
        Section section;

        try
        {
            section = findSectionById(id);
            LOGGER.warn("Section already in database: " + section);
        } catch (NoResultException ex)
        {
            section = new Section(name);
            section.setId(id);
            Course dbCourse = em.find(Course.class, course.getId());
            section.setCourse(dbCourse);
            dbCourse.addSection(section);
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(section);
            em.merge(dbCourse);
            tx.commit();
            LOGGER.info("Section saved to database: " + section);
        }
        em.close();
        return section;
    }

    private Section findSectionById(Long id) throws NoResultException
    {
        EntityManager em = emf.createEntityManager();
        Section section = em.find(Section.class, id);
        if (section == null)
            throw new NoResultException();
        em.close();
        return section;
    }

    private Student createPersistedStudent(Long id, String firstName, String lastName,
            String webId, Integer classYear) throws IllegalArgumentException
    {
        EntityManager em = emf.createEntityManager();
        Student student;

        try
        {
            student = findStudentByWebId(webId);
            LOGGER.warn("Student already exists: " + student.getWebID());
        } catch (NoResultException ex)
        {
            student = new Student (firstName, lastName, webId, classYear);
            student.setId(id);
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(student);
            tx.commit();
            LOGGER.debug("Student ID (post-persist): " + student.getId());
        }
        em.close();
        return student;
    }

    private Student findStudentByWebId(String webId) throws NoResultException
    {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Student> cq = cb.createQuery(Student.class);
        Root<Student> studentRoot = cq.from(Student.class);
        cq.where(cb.equal(studentRoot.get("webID"), webId));
        TypedQuery<Student> studentQuery = em.createQuery(cq);
        return studentQuery.getSingleResult();
    }

    private Instructor createPersistedInstructor(Long id, String firstName, 
            String lastName, String webId) throws IllegalArgumentException
    {
        EntityManager em = emf.createEntityManager();
        Instructor instructor;

        try
        {
            instructor = findInstructorByWebId(webId);
            LOGGER.warn("Instructor already exists: " + instructor.getWebID());
        } catch (NoResultException ex)
        {
            instructor = new Instructor(firstName, lastName, webId);
            instructor.setId(id);
            instructor.setDomainAccount(webId);
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(instructor);
            tx.commit();
            LOGGER.debug("Instructor Id (post-persist): " + instructor.getId());
        }
        em.close();
        return instructor;
    }

    private Instructor findInstructorByWebId(String webId) throws NoResultException
    {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Instructor> cq = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cq.from(Instructor.class);
        cq.where(cb.equal(instructorRoot.get("webID"), webId));
        TypedQuery<Instructor> instructorQuery = em.createQuery(cq);
        return instructorQuery.getSingleResult();
    }

    /**
     * Utility method to create a different course for testing.  This course
     * has 1 instructor, 2 sections, and 5 students.
     *
     * @param name      The name of the Course
     * @return          The created Course object
     */
    private Course createPersistedCourse(Long id, String name)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        Course course = new Course(name);
        course.setId(id);

        tx.begin();
        em.persist(course);
        tx.commit();

        LOGGER.info("Saved Course (" + course + ") to database with " +
                course.getSections().size() + " sections and " +
                course.getInstructors().size() + " instructors");
        em.close();
        return course;
    }

    private void persistSectionInstructor(Section section, Instructor instructor)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        Section dbSection = em.find(Section.class, section.getId());
        Course dbCourse = em.find(Course.class, section.getCourse().getId());
        Instructor dbInstructor = em.find(Instructor.class, instructor.getId());
        dbSection.setInstructor(dbInstructor);
        dbInstructor.addSection(dbSection);
        if (dbCourse.getInstructors().contains(dbInstructor))
        {
            LOGGER.warn("Instructor already in the course");
        } else
        {
            dbCourse.addInstructor(dbInstructor);
        }
        tx.begin();
        em.merge(dbSection);
        em.merge(dbInstructor);
        em.merge(dbCourse);
        tx.commit();
        em.close();
    }

    private void persistSectionStudent(Section section, Student student)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        Section dbSection = em.find(Section.class, section.getId());
        Student dbStudent = em.find(Student.class, student.getId());
        dbSection.addStudent(dbStudent);
        dbStudent.addSection(dbSection);
        tx.begin();
        em.merge(dbSection);
        em.merge(dbStudent);
        tx.commit();
        em.close();
    }

    private Category createPersistedCategory(Long id, String name)
    {
        EntityManager em = emf.createEntityManager();
        Category category;
        
        try
        {
            category = findCategoryByName(name);
            LOGGER.warn("Category already exists: " + category);
        } catch (NoResultException ex)
        {
            category = new Category(name);
            category.setId(id);
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(category);
            tx.commit();
            LOGGER.debug("Category Id (post-persist): " + category.getId());
        }
        em.close();
        return category;
    }

    private Category findCategoryByName(String name) throws NoResultException
    {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Category> cq = cb.createQuery(Category.class);
        Root<Category> categoryRoot = cq.from(Category.class);
        cq.where(cb.equal(categoryRoot.get("name"), name));
        TypedQuery<Category> categoryQuery = em.createQuery(cq);
        return categoryQuery.getSingleResult();
    }

    private Question createPersistedQuestion(Long id, Category category, String name,
            String description, boolean verbatim, boolean orderedOutput, 
            List<TestCase> testCases)
    {
        EntityManager em = emf.createEntityManager();
        Question question;

        try
        {
            question = findQuestionByName(name);
            LOGGER.warn("Question already exists: " + question);
        } catch (NoResultException ex)
        {
            question = new Question(name, category, description);
            question.setId(id);
            question.setVerbatim(verbatim);
            question.setOrderedOutput(orderedOutput);

            for (TestCase testCase1 : testCases)
            {
                TestCase testCase = em.find(TestCase.class, testCase1.getId());
                question.addTestCase(testCase);
            }
            
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(question);
            tx.commit();
            LOGGER.debug("Question Id (post-persist): " + question.getId());
        }
        em.close();
        return question;
    }

    private Question findQuestionByName(String name) throws NoResultException
    {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Question> cq = cb.createQuery(Question.class);
        Root<Question> questionRoot = cq.from(Question.class);
        cq.where(cb.equal(questionRoot.get("name"), name));
        TypedQuery<Question> questionQuery = em.createQuery(cq);
        return questionQuery.getSingleResult();
    }

    private GradedEvent createPersistedGradedEvent(Long id, String term, Course course,
            String assignment, boolean partialCredit, String version)
    {
        EntityManager em = emf.createEntityManager();
        GradedEvent gevent;
        
        try
        {
            gevent = findGradedEvent(term, course, assignment, version);
            LOGGER.warn("Graded Event exists: " + gevent);
        } catch (NoResultException ex)
        {
            gevent = new GradedEvent();
            gevent.setId(id);
            gevent.setCourse(course);
            gevent.setAssignment(assignment);
            gevent.setDueDate("29 Apr 2010");
            gevent.setPartialCredit(partialCredit);
            gevent.setTerm(term);
            gevent.setVersion(version);

            // Save to the database
            em.getTransaction().begin();
            em.persist(gevent);
            em.merge(course);
            em.getTransaction().commit();
            LOGGER.debug("GradedEvent Id (post-persist): " + gevent.getId());
        }
        em.close();
        return gevent;
    }

    private GradedEvent addQuestionToGradedEvent(GradedEvent gradedEvent, Question question)
    {
        EntityManager em = emf.createEntityManager();
        gradedEvent.addQuestion(question);
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        gradedEvent = em.merge(gradedEvent);
        tx.commit();
        em.close();
        return gradedEvent;
    }

    private GradedEvent findGradedEvent(String term, Course course, String assignment,
            String version) throws NoResultException
    {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<GradedEvent> cq = cb.createQuery(GradedEvent.class);
        Root<GradedEvent> gradedEventRoot = cq.from(GradedEvent.class);

        List<Predicate> criteria = new ArrayList<>();
        criteria.add(cb.equal(gradedEventRoot.get("course"), course));
        criteria.add(cb.equal(gradedEventRoot.get("term"), term));
        criteria.add(cb.equal(gradedEventRoot.get("assignment"), assignment));
        criteria.add(cb.equal(gradedEventRoot.get("version"), version));
        cq.where(cb.and(criteria.toArray(new Predicate[0])));

        TypedQuery<GradedEvent> gradedEventQuery = em.createQuery(cq);
        return gradedEventQuery.getSingleResult();
    }

    private void persistDatabaseVersion(Double versionNumber)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        Version version = new Version(versionNumber);
        tx.begin();
        em.persist(version);
        tx.commit();
        em.close();
    }
    
    private void persistTestCase(TestCase testCase)
    {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(testCase);
        tx.commit();
        em.close();
    }
    
    private boolean initialized;
    private static EntityManagerFactory emf;
    private final Logger LOGGER = Logger.getLogger(TestDatabase.class);
}
