/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import com.darkhonor.rage.model.Category;
import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;

/**
 * Class to handle all data source queries for {@link com.darkhonor.rage.model.Category}
 * objects.
 *
 * @author Alex Ackerman
 */
public class CategoryDAO
{

    /**
     * Default constructor.
     *
     * @param em    EntityManager connection to the database.
     * @throws IllegalArgumentException Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager is
     *                                  <code>null</code>
     */
    public CategoryDAO(EntityManager em) throws IllegalArgumentException,
            NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("EntityManager is closed.  Throwing IllegalArgumentException");
            throw new IllegalArgumentException("Invalid EntityManager state");
        } else
        {
            this.em = em;
        }
    }

    /**
     * Closes the connection to the database.
     *
     * @throws IllegalStateException    Thrown if the database connection is
     *                                  already closed.
     */
    public void closeConnection() throws IllegalStateException
    {
        em.close();
    }

    /**
     * Returns whether or not the connection to the data source is open.
     * 
     * @return  <code>true</code> if the connection is open, <code>false</code>
     *          otherwise.
     */
    public boolean isOpen()
    {
        return em.isOpen();
    }

    /**
     * Returns the Category identified by the provided Id or <code>null</code>
     * if it cannot be found in the data source.
     * 
     * @param id        The Id of the Category
     * @return          The Category from the data source or <code>null</code>
     * @throws IllegalArgumentException Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager is
     *                                  <code>null</code>
     */
    public Category find(Long id) throws IllegalStateException, NullPointerException,
            IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(find) EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(find) EntityManager is closed.  Throwing "
                    + "IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (id == null)
        {
            LOGGER.error("(find) Provided Category Id is null");
            throw new NullPointerException("Provided Category Id is null");
        } else if (id < 1L)
        {
            LOGGER.error("(find) Provided Category Id is less than 1");
            throw new IllegalArgumentException("Provided Category Id is less than 1");
        } else
        {
            EntityTransaction tx = em.getTransaction();
            Category category;
            tx.begin();
            category = em.find(Category.class, id);
            tx.commit();
            return category;
        }
    }

    /**
     * Returns the Category with the name provided or <code>null</code> if it
     * cannot be found in the data source.
     *
     * @param name      The name of the Category
     * @return          The Category from the data source or <code>null</code>
     * @throws IllegalStateException    Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager or name is
     *                                  <code>null</code>
     * @throws IllegalArgumentException Thrown when the category name is blank
     */
    public Category find(String name) throws IllegalStateException, NullPointerException,
                IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(find) EntityManager is null.  Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(find) EntityManager is closed.  Throwing "
                    + "IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (name == null )
        {
            LOGGER.error("(find) Category name is either null.");
            throw new NullPointerException("Category name is either null");
        } else if (name.isEmpty())
        {
            LOGGER.error("(find) Category name is blank");
            throw new IllegalArgumentException("Category name is blank");
        } else
        {
            Category category;
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Category> cq = cb.createQuery(Category.class);
            Root<Category> categoryRoot = cq.from(Category.class);
            cq.where(cb.equal(categoryRoot.get("name"), name));
            TypedQuery<Category> categoryQuery = em.createQuery(cq);
            EntityTransaction tx = null;
            try
            {
                tx = em.getTransaction();
                tx.begin();
                category = categoryQuery.getSingleResult();
                tx.commit();
            } catch (NoResultException ex)
            {
                LOGGER.info("(find) No category found with the name " + name);
                tx.rollback();
                category = null;
            }
            return category;
        }
    }

    /**
     * Saves the provided Category to the data source.
     *
     * @param category      The Category to save to the data source
     * @throws IllegalStateException    Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the EntityManager or Category is
     *                                  <code>null</code>
     * @throws IllegalArgumentException Thrown when the Category exists in the
     *                                  data source or the Category name is not
     *                                  set.
     * @return                          The Primary Key for the new Category
     */
    public Long create(Category category) throws IllegalStateException,
            NullPointerException, IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(create) EntityManager is null.  Throwing "
                    + "NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(create) EntityManager is closed.  Throwing "
                    + "IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (category == null)
        {
            LOGGER.error("(create) Category is null.  Throwing "
                    + "NullPointerException.");
            throw new NullPointerException("Category is null");
        } else if (category.getName() == null || category.getName().isEmpty())
        {
            LOGGER.error("(create) Category name is either null or blank.");
            throw new IllegalArgumentException("Category name is either null or blank");
        } else
        {
            EntityTransaction tx = null;
            // Don't need to put this into a Transaction since the transaction will
            // be created in the find method
            Category testCategory = this.find(category.getName());
            if (testCategory != null)
            {
                LOGGER.warn("(create) Category already exists in the data source");
                throw new IllegalArgumentException("Category already exists in the "
                        + "data source");
            }
            try
            {
                tx = em.getTransaction();
                tx.begin();
                em.persist(category);
                tx.commit();
                return category.getId();
            } catch (EntityExistsException ex)
            {
                LOGGER.warn("(create) Category already exists in the data source");
                tx.rollback();
                throw new IllegalArgumentException("Category already exists in "
                        + "the data source");
            }
        }
    }

    /**
     * Returns the full list of Categories stored in the data source ordered by
     * Category name in ascending order.
     * 
     * @return          The List of Categories stored in the data source
     * @throws IllegalStateException    Thrown when the EntityManager is closed
     * @throws NullPointerException     Thrown when the connection to the Data source
     *                                  is <code>null</code>
     */
    public List<Category> getAllCategoriesByName() throws IllegalStateException,
            NullPointerException
    {
        if (em == null)
        {
            LOGGER.error("(getAllCategoriesByName) EntityManager is null.  "
                    + "Throwing NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(getAllCategoriesByName) EntityManager is closed.  "
                    + "Throwing IllegalStateException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else
        {
            List<Category> categoryList;
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Category> cq = cb.createQuery(Category.class);
            Root<Category> categoryRoot = cq.from(Category.class);
            cq.orderBy(cb.asc(categoryRoot.get("name")));

            TypedQuery<Category> categoryQuery = em.createQuery(cq);
            categoryList = categoryQuery.getResultList();
            return categoryList;
        }
    }

    /**
     * Deletes the provided category from the Data Source.  If the Category does
     * not exist in the Data Source, and error is thrown.
     *
     * @param category      The Category to delete
     * @throws IllegalStateException    Thrown when the connection to the data source
     *                                  is not open.
     * @throws NullPointerException     Thrown when the provided Category is
     *                                  <code>null</code> 
     * @throws IllegalArgumentException Thrown when the name of the Category is
     *                                  <code>null</code> or blank or when the
     *                                  provided Category does not exist in the
     *                                  data source
     */
    public void delete(Category category) throws IllegalStateException,
            NullPointerException, IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(delete) EntityManager is null.  Throwing "
                    + "NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(delete) EntityManager is closed.  Throwing "
                    + "IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (category == null)
        {
            LOGGER.error("(delete) Category is null.  Throwing NullPointerException.");
            throw new NullPointerException("Category is null");
        } else if (category.getName() == null || category.getName().isEmpty())
        {
            LOGGER.error("(delete) Category name is either null or blank.");
            throw new IllegalArgumentException("Category name is either null or "
                    + "blank");
        } else
        {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Category deletedCategory = em.find(Category.class, category.getId());
            tx.commit();
            if (deletedCategory == null)
            {
                LOGGER.error("(delete) Provided Category does not exist");
                throw new IllegalArgumentException("Provided Category does not exist");
            }
            tx.begin();
            em.remove(deletedCategory);
            tx.commit();
        }
    }

    /**
     * Delete a specific Category element based on the given Id
     * @param id        The Id of the Category to delete
     * @throws IllegalStateException    Thrown if the connection is closed already
     * @throws NullPointerException     Thrown if the Id is <code>null</code>
     * @throws IllegalArgumentException Thrown if the Id is invalid
     */
    public void delete(Long id) throws IllegalStateException, NullPointerException,
            IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(delete) EntityManager is null.  Throwing "
                    + "NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(delete) EntityManager is closed.  Throwing "
                    + "IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (id == null)
        {
            LOGGER.error("(delete) Category is null.  Throwing NullPointerException.");
            throw new NullPointerException("Category is null");
        } else
        {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Category deletedCategory = em.find(Category.class, id);
            tx.commit();
            if (deletedCategory == null)
            {
                LOGGER.error("(delete) Provided Category ID does not exist");
                throw new IllegalArgumentException("Provided Category Id does not "
                        + "exist");
            }
            tx.begin();
            em.remove(deletedCategory);
            tx.commit();
        }
    }

    /**
     * Update a supplied Category in the data source with the one provided
     * 
     * @param category  The Category to replace
     * @return  Category    The updated Category from the data source
     * @throws IllegalStateException    Thrown if the connection is already closed
     * @throws NullPointerException     Thrown if the Category is <code>null</code>
     * @throws IllegalArgumentException Thrown if the Category is invalid
     */
    public Category update(Category category) throws IllegalStateException,
            NullPointerException,
            IllegalArgumentException
    {
        if (em == null)
        {
            LOGGER.error("(update) EntityManager is null.  Throwing "
                    + "NullPointerException.");
            throw new NullPointerException("EntityManager is null");
        } else if (!em.isOpen())
        {
            LOGGER.error("(update) EntityManager is closed.  Throwing "
                    + "IllegalArgumentException");
            throw new IllegalStateException("Invalid EntityManager state");
        } else if (category == null)
        {
            LOGGER.error("(update) Category is null.  Throwing NullPointerException.");
            throw new NullPointerException("Category is null");
        } else if (category.getName() == null || category.getName().isEmpty())
        {
            LOGGER.error("(update) Category name is either null or blank.");
            throw new IllegalArgumentException("Category name is either null or blank");
        } else
        {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Category updatedCategory = em.find(Category.class, category.getId());
            tx.commit();
            if (updatedCategory == null)
            {
                LOGGER.error("(update) Provided Category does not exist");
                throw new IllegalArgumentException("Provided Category does not exist");
            }
            updatedCategory.setName(category.getName());
            tx.begin();
            updatedCategory = em.merge(updatedCategory);
            tx.commit();
            return updatedCategory;
        }
    }
    
    private final Logger LOGGER = Logger.getLogger(TestCaseDAO.class);
    private EntityManager em;
}
