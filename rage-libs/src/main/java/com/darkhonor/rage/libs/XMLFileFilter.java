/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs;

import java.io.*;

/**
 * The XMLFileFilter class implements a FileFilter that looks for XML files
 * 
 * @author Alexander Ackerman
 * 
 * @version 1.1.0
 * 
 * @see java.io.FileFilter
 */
public class XMLFileFilter implements FileFilter
{

    /**
     * Compares the given file to see if it ends with .xml
     * 
     * @param pathname The file being filtered
     * @return <code>true</code> if the File is an XML file and can be read
     *         by the user, <code>false</code> otherwise.
     */
    @Override
    public boolean accept(File pathname)
    {
        // 1.1.0: Converted filename to lowercase then looked for .xml extension
        if (pathname.canRead() && !pathname.isDirectory()
                && pathname.getName().toLowerCase().endsWith(".xml"))
        {
            return true;
        }
        return false;
    }
}
