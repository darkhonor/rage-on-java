/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaBuilder;
import com.darkhonor.rage.libs.dataaccess.testdb.TestDatabase;
import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.GradedEvent;
import com.darkhonor.rage.model.Instructor;
import com.darkhonor.rage.model.Question;
import com.darkhonor.rage.model.Section;
import com.darkhonor.rage.model.Student;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit 4 tests for CourseDAO class.
 * 
 * @author Alex Ackerman
 */
public class CourseDAOTest
{

    public CourseDAOTest()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        testDb = new TestDatabase();
        testDb.initialize();
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
        testDb.shutdown();
    }

    @Before
    public void setUp()
    {
        instance = new CourseDAO(testDb.getConnection());
    }

    /**
     * Method executed after each test is complete.  This clears the database
     * of any potential extra data so each test can be considered independently.
     */
    @After
    public void tearDown()
    {
        if (instance != null && instance.isOpen())
        {
            instance.closeConnection();
        }
        testDb.reset();
    }

    /**
     * Test of create method, of class CourseDAO.
     */
    @Test
    public void testCreate()
    {
        System.out.println("create");
        Course testCourse = createTestCourse();
        Long newId = instance.create(testCourse);
        assertNotNull(newId);
        assertEquals(new Long(15L), newId);


        EntityManager em = testDb.getConnection();
        List<Course> courses = instance.getAllCoursesByName();
        System.out.println("Number of Courses (Expecting 1): " + courses.size());
        if (courses.size() > 1)
        {
            for (int i = 0; i < courses.size(); i++)
            {
                Course crs = (Course) courses.get(i);
                System.out.println("Course " + i + ": " + crs.getName()
                        + " (ID: " + crs.getId() + ")");
            }
        }
        assertEquals(3, courses.size());
        Course course = (Course) courses.get(0);
        System.out.println("Course (Expecting CS110): " + course.getName());
        assertEquals("CS110", course.getName());
        // Instructors are Eagerly loaded.  Sections are lazily loaded
        System.out.println("Number of Instructors in Course (Expecting 2): "
                + course.getInstructors().size());
        assertEquals(2, course.getInstructors().size());
        System.out.println("Number of Sections in Course (Expecting 2): "
                + course.getSections().size());
        assertEquals(2, course.getSections().size());

        Query query = em.createQuery("SELECT ins FROM Instructor ins");
        List instructors = query.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 4): "
                + instructors.size());
        assertEquals(4, instructors.size());

        query = em.createQuery("SELECT s FROM Section s");
        List sections = query.getResultList();
        System.out.println("Number of Sections in DB (Expecting 6): "
                + sections.size());
        assertEquals(6, sections.size());

        query = em.createQuery("SELECT s FROM Student s");
        List students = query.getResultList();
        System.out.println("Number of Students in DB (Expecting 11): "
                + students.size());
        assertEquals(11, students.size());

        em.close();

    }

    @Test
    public void testCreateNullId()
    {
        System.out.println("create - null Id");
        Course testCourse = createTestCourse();
        Long newId = instance.create(testCourse);
        assertNotNull(newId);

        EntityManager em = testDb.getConnection();
        List<Course> courses = instance.getAllCoursesByName();
        System.out.println("Number of Courses (Expecting 1): " + courses.size());
        if (courses.size() > 1)
        {
            for (int i = 0; i < courses.size(); i++)
            {
                Course crs = (Course) courses.get(i);
                System.out.println("Course " + i + ": " + crs.getName()
                        + " (ID: " + crs.getId() + ")");
            }
        }
        assertEquals(3, courses.size());
 
        Query query = em.createQuery("SELECT ins FROM Instructor ins");
        List instructors = query.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 4): "
                + instructors.size());
        assertEquals(4, instructors.size());

        query = em.createQuery("SELECT s FROM Section s");
        List sections = query.getResultList();
        System.out.println("Number of Sections in DB (Expecting 6): "
                + sections.size());
        assertEquals(6, sections.size());

        query = em.createQuery("SELECT s FROM Student s");
        List students = query.getResultList();
        System.out.println("Number of Students in DB (Expecting 11): "
                + students.size());
        assertEquals(11, students.size());

        em.close();
    }

    @Test(expected=NullPointerException.class)
    public void testCreateNullCourse()
    {
        System.out.println("create - null Course");
        assertTrue(instance.isOpen());
        Course course = null;
        Long newId = instance.create(course);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateNegativeId()
    {
        System.out.println("create - Negative Id");
        assertTrue(instance.isOpen());
        Course course = createTestCourse();
        course.setId(new Long(-5L));
        Long newId = instance.create(course);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateNullName()
    {
        System.out.println("create - Null Name");
        assertTrue(instance.isOpen());
        Course course = createTestCourse();
        course.setName(null);
        Long newId = instance.create(course);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateBlankName()
    {
        System.out.println("create - Blank Name");
        assertTrue(instance.isOpen());
        Course course = createTestCourse();
        course.setName("");
        Long newId = instance.create(course);
    }

    @Test(expected=IllegalStateException.class)
    public void testCreateClosedConnection()
    {
        System.out.println("create - Closed Connection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Course course = createTestCourse();
        Long newId = instance.create(course);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateDuplicateCourse()
    {
        System.out.println("create - Duplicate Course");
        assertTrue(instance.isOpen());
        Course course = createExistingCourse();
        Long newId = instance.create(course);
    }

    /**
     * Test of delete method, of class CourseDAO.
     */
    @Test
    public void testDelete()
    {
        System.out.println("delete");
        EntityManager em = testDb.getConnection();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        // Get the Course from the database for the GradedEvent creation
        List<Course> courses = instance.getAllCoursesByName();
        System.out.println("Number of Courses (Expecting 2): " + courses.size());
        assertEquals(2, courses.size());
        Course course1 = createExistingCourse();

        // Validate Original Data was saved to database
        CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
        Root<Section> sectionRoot = cqSection.from(Section.class);
        TypedQuery<Section> sectionQuery = em.createQuery(cqSection);
        List<Section> sections = sectionQuery.getResultList();
        System.out.println("Number of Sections in DB (Expecting 4): "
                + sections.size());

        System.out.println("Sections in Course: " + course1.getSections().size());

        // Validate Original Data was saved to database
        CriteriaQuery<Question> cqQuestion = cb.createQuery(Question.class);
        Root<Question> questionRoot = cqQuestion.from(Question.class);
        TypedQuery<Question> questionQuery = em.createQuery(cqQuestion);
        List<Question> questions = questionQuery.getResultList();
        System.out.println("Number of Questions in DB (Expecting 3): "
                + questions.size());
        assertEquals(3, questions.size());

        CriteriaQuery<GradedEvent> cqGradedEvent = cb.createQuery(GradedEvent.class);
        Root<GradedEvent> gradedEventRoot = cqGradedEvent.from(GradedEvent.class);
        TypedQuery<GradedEvent> gradedEventQuery = em.createQuery(cqGradedEvent);
        List<GradedEvent> events = gradedEventQuery.getResultList();
        System.out.println("Number of GradedEvents in DB (Expecting 4): "
                + events.size());
        assertEquals(4, events.size());

        CriteriaQuery<Instructor> cqInstructor = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cqInstructor.from(Instructor.class);
        TypedQuery<Instructor> instructorQuery = em.createQuery(cqInstructor);
        List<Instructor> instructors = instructorQuery.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());

        CriteriaQuery<Student> cqStudent = cb.createQuery(Student.class);
        Root<Student> studentRoot = cqStudent.from(Student.class);
        TypedQuery<Student> studentQuery = em.createQuery(cqStudent);
        List<Student> students = studentQuery.getResultList();
        System.out.println("Number of Students in DB (Expecting 7): "
                + students.size());
        assertEquals(7, students.size());

        System.out.println("Removing Course 1");
        instance.delete(course1);

        // Validate GradedEvent for second course has not been deleted, but first
        // has
        courses = instance.getAllCoursesByName();
        System.out.println("(Post) Number of Courses (Expecting 1): " + courses.size());
        assertEquals(1, courses.size());

        questions = questionQuery.getResultList();
        System.out.println("(Post) Number of Questions in DB (Expecting 3): "
                + questions.size());
        assertEquals(3, questions.size());

        events = gradedEventQuery.getResultList();
        System.out.println("(Post) Number of GradedEvents in DB (Expecting 1): "
                + events.size());
        assertEquals(1, events.size());

        instructors = instructorQuery.getResultList();
        System.out.println("(Post) Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());

        Query query = em.createNativeQuery("SELECT * FROM Course_Instructor");
        List courseInsResult = query.getResultList();
        System.out.println("(Post) Number of Linked Instructor/Courses (Expecting 1): "
                + courseInsResult.size());
        assertEquals(1, courseInsResult.size());

        sections = sectionQuery.getResultList();
        System.out.println("(Post) Number of Sections in DB (Expecting 2): "
                + sections.size());
        assertEquals(2, sections.size());

        students = studentQuery.getResultList();
        System.out.println("(Post) Number of Students in DB (Expecting 7): "
                + students.size());
        assertEquals(7, students.size());

        em.close();
    }

    @Test
    public void testDeleteNullIdExisting()
    {
        System.out.println("delete - Null Id, but Existing Course");
        assertTrue(instance.isOpen());
        EntityManager em = testDb.getConnection();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        // Get the Course from the database for the GradedEvent creation
        List<Course> courses = instance.getAllCoursesByName();
        System.out.println("Number of Courses (Expecting 2): " + courses.size());
        assertEquals(2, courses.size());
        Course course = createExistingCourse();

        // Validate Original Data was saved to database
        CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
        Root<Section> sectionRoot = cqSection.from(Section.class);
        TypedQuery<Section> sectionQuery = em.createQuery(cqSection);
        List<Section> sections = sectionQuery.getResultList();
        System.out.println("Number of Sections in DB (Expecting 4): "
                + sections.size());

        System.out.println("Sections in Course: " + course.getSections().size());

        // Validate Original Data was saved to database
        CriteriaQuery<Question> cqQuestion = cb.createQuery(Question.class);
        Root<Question> questionRoot = cqQuestion.from(Question.class);
        TypedQuery<Question> questionQuery = em.createQuery(cqQuestion);
        List<Question> questions = questionQuery.getResultList();
        System.out.println("Number of Questions in DB (Expecting 3): "
                + questions.size());
        assertEquals(3, questions.size());

        CriteriaQuery<GradedEvent> cqGradedEvent = cb.createQuery(GradedEvent.class);
        Root<GradedEvent> gradedEventRoot = cqGradedEvent.from(GradedEvent.class);
        TypedQuery<GradedEvent> gradedEventQuery = em.createQuery(cqGradedEvent);
        List<GradedEvent> events = gradedEventQuery.getResultList();
        System.out.println("Number of GradedEvents in DB (Expecting 4): "
                + events.size());
        assertEquals(4, events.size());

        CriteriaQuery<Instructor> cqInstructor = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cqInstructor.from(Instructor.class);
        TypedQuery<Instructor> instructorQuery = em.createQuery(cqInstructor);
        List<Instructor> instructors = instructorQuery.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());

        CriteriaQuery<Student> cqStudent = cb.createQuery(Student.class);
        Root<Student> studentRoot = cqStudent.from(Student.class);
        TypedQuery<Student> studentQuery = em.createQuery(cqStudent);
        List<Student> students = studentQuery.getResultList();
        System.out.println("Number of Students in DB (Expecting 7): "
                + students.size());
        assertEquals(7, students.size());

        System.out.println("Removing Course 1");
        course.setId(null);
        instance.delete(course);

        // Validate GradedEvent for second course has not been deleted, but first
        // has
        courses = instance.getAllCoursesByName();
        System.out.println("(Post) Number of Courses (Expecting 1): " + courses.size());
        assertEquals(1, courses.size());

        questions = questionQuery.getResultList();
        System.out.println("(Post) Number of Questions in DB (Expecting 3): "
                + questions.size());
        assertEquals(3, questions.size());

        events = gradedEventQuery.getResultList();
        System.out.println("(Post) Number of GradedEvents in DB (Expecting 1): "
                + events.size());
        assertEquals(1, events.size());

        instructors = instructorQuery.getResultList();
        System.out.println("(Post) Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());

        Query query = em.createNativeQuery("SELECT * FROM Course_Instructor");
        List courseInsResult = query.getResultList();
        System.out.println("(Post) Number of Linked Instructor/Courses (Expecting 1): "
                + courseInsResult.size());
        assertEquals(1, courseInsResult.size());

        sections = sectionQuery.getResultList();
        System.out.println("(Post) Number of Sections in DB (Expecting 2): "
                + sections.size());
        assertEquals(2, sections.size());

        students = studentQuery.getResultList();
        System.out.println("(Post) Number of Students in DB (Expecting 7): "
                + students.size());
        assertEquals(7, students.size());

        em.close();
    }

    @Test(expected = IllegalStateException.class)
    public void testDeleteClosedConnection()
    {
        System.out.println("delete - Closed Connection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        Course course = createExistingCourse();
        instance.delete(course);
    }

    @Test(expected=NullPointerException.class)
    public void testDeleteNullCourse()
    {
        System.out.println("delete - Null Course");
        assertTrue(instance.isOpen());
        Course course = null;
        instance.delete(course);
    }

    @Test(expected=NullPointerException.class)
    public void testDeleteNullCourseName()
    {
        System.out.println("delete - Null Course Name");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(1L));
        instance.delete(course);
    }

    @Test
    public void testDeleteId()
    {
        System.out.println("delete - By Id");
        assertTrue(instance.isOpen());
        EntityManager em = testDb.getConnection();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        // Get the Course from the database for the GradedEvent creation
        List<Course> courses = instance.getAllCoursesByName();
        System.out.println("Number of Courses (Expecting 2): " + courses.size());
        assertEquals(2, courses.size());
        Course course1 = createExistingCourse();

        // Validate Original Data was saved to database
        CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
        Root<Section> sectionRoot = cqSection.from(Section.class);
        TypedQuery<Section> sectionQuery = em.createQuery(cqSection);
        List<Section> sections = sectionQuery.getResultList();
        System.out.println("Number of Sections in DB (Expecting 4): "
                + sections.size());

        System.out.println("Sections in Course: " + course1.getSections().size());

        // Validate Original Data was saved to database
        CriteriaQuery<Question> cqQuestion = cb.createQuery(Question.class);
        Root<Question> questionRoot = cqQuestion.from(Question.class);
        TypedQuery<Question> questionQuery = em.createQuery(cqQuestion);
        List<Question> questions = questionQuery.getResultList();
        System.out.println("Number of Questions in DB (Expecting 3): "
                + questions.size());
        assertEquals(3, questions.size());

        CriteriaQuery<GradedEvent> cqGradedEvent = cb.createQuery(GradedEvent.class);
        Root<GradedEvent> gradedEventRoot = cqGradedEvent.from(GradedEvent.class);
        TypedQuery<GradedEvent> gradedEventQuery = em.createQuery(cqGradedEvent);
        List<GradedEvent> events = gradedEventQuery.getResultList();
        System.out.println("Number of GradedEvents in DB (Expecting 4): "
                + events.size());
        assertEquals(4, events.size());

        CriteriaQuery<Instructor> cqInstructor = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cqInstructor.from(Instructor.class);
        TypedQuery<Instructor> instructorQuery = em.createQuery(cqInstructor);
        List<Instructor> instructors = instructorQuery.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());

        CriteriaQuery<Student> cqStudent = cb.createQuery(Student.class);
        Root<Student> studentRoot = cqStudent.from(Student.class);
        TypedQuery<Student> studentQuery = em.createQuery(cqStudent);
        List<Student> students = studentQuery.getResultList();
        System.out.println("Number of Students in DB (Expecting 7): "
                + students.size());
        assertEquals(7, students.size());

        System.out.println("Removing Course 2");
        Long id = new Long(2L);
        instance.delete(id);

        // Validate GradedEvent for second course has not been deleted, but first
        // has
        courses = instance.getAllCoursesByName();
        System.out.println("(Post) Number of Courses (Expecting 1): " + courses.size());
        assertEquals(1, courses.size());
        System.out.println("(Post) Expecting remaining course (CS110): " +
                courses.get(0).getName());
        assertEquals("CS110", courses.get(0).getName());

        questions = questionQuery.getResultList();
        System.out.println("(Post) Number of Questions in DB (Expecting 3): "
                + questions.size());
        assertEquals(3, questions.size());

        events = gradedEventQuery.getResultList();
        System.out.println("(Post) Number of GradedEvents in DB (Expecting 3): "
                + events.size());
        assertEquals(3, events.size());

        instructors = instructorQuery.getResultList();
        System.out.println("(Post) Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());

        Query query = em.createNativeQuery("SELECT * FROM Course_Instructor");
        List courseInsResult = query.getResultList();
        System.out.println("(Post) Number of Linked Instructor/Courses (Expecting 2): "
                + courseInsResult.size());
        assertEquals(2, courseInsResult.size());

        sections = sectionQuery.getResultList();
        System.out.println("(Post) Number of Sections in DB (Expecting 2): "
                + sections.size());
        assertEquals(2, sections.size());

        students = studentQuery.getResultList();
        System.out.println("(Post) Number of Students in DB (Expecting 7): "
                + students.size());
        assertEquals(7, students.size());

        em.close();
        Course deletedCourse = instance.find(id);
        assertNull(deletedCourse);
    }

    @Test(expected=NullPointerException.class)
    public void testDeleteIdNullId()
    {
        System.out.println("delete - By Id, null Id");
        assertTrue(instance.isOpen());
        Long id = null;
        instance.delete(id);
    }

    @Test(expected=IllegalStateException.class)
    public void testDeleteIdClosedConnection()
    {
        System.out.println("delete - By Id, Closed Connection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Long id = new Long(2L);
        instance.delete(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteIdZeroId()
    {
        System.out.println("delete - By Id, Id = 0");
        assertTrue(instance.isOpen());
        Long id = new Long(0L);
        instance.delete(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteIdNegativeId()
    {
        System.out.println("delete - By Id, Id < 0");
        assertTrue(instance.isOpen());
        Long id = new Long(-5L);
        instance.delete(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteIdNonExistentCourse()
    {
        System.out.println("delete - By Id, non-existent Course");
        assertTrue(instance.isOpen());
        Long id = new Long(21L);
        instance.delete(id);
    }

    /**
     * Test of closeConnection method, of class CourseDAO.
     */
    @Test
    public void testCloseConnection()
    {
        System.out.println("closeConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }

    /**
     * Test of closeConnection method.  The EntityManager is closed already when
     * called.
     */
    @Test(expected = IllegalStateException.class)
    public void testCloseConnectionClosedEM()
    {
        System.out.println("closeConnection - Closed EntityManager");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        System.out.println("-- Closed connection opened at test start.");
        instance.closeConnection();
    }

    /**
     * Test of openConnection method, of class CourseDAO.
     */
    @Test
    public void testOpenConnection()
    {
        System.out.println("openConnection");
        EntityManager em = testDb.getConnection();
        instance.closeConnection();
        assertFalse(instance.isOpen());
        assertTrue(instance.openConnection(em));
        assertTrue(instance.isOpen());
    }

    /**
     * Test of openConnection method.  The EntityManager is already open.
     */
    @Test
    public void testOpenConnectionOpenEM()
    {
        System.out.println("openConnection - Open EntityManager");
        EntityManager em = testDb.getConnection();
        assertTrue(instance.isOpen());
        assertTrue(instance.openConnection(em));
        assertTrue(instance.isOpen());
    }

    /**
     * Test of openConnection method.  The EntityManager is already closed.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testOpenConnectionClosedEM()
    {
        System.out.println("openConnection - Closed EntityManager");
        EntityManager closedEM = testDb.getConnection();
        closedEM.close();
        instance.openConnection(closedEM);
    }

    /**
     * Test of openConnection method.  The EntityManager is null.
     */
    @Test(expected = NullPointerException.class)
    public void testOpenConnectionNullEM()
    {
        System.out.println("openConnection - Null EntityManager");
        EntityManager nullEM = null;
        instance.openConnection(nullEM);
    }

    /**
     * Test of isOpen method.  EntityManager is open.
     */
    @Test
    public void testIsOpen()
    {
        System.out.println("isOpen - Open EntityManager");
        assertTrue(instance.isOpen());
    }

    /**
     * Test of isOpen method.  EntityManager is closed.
     */
    @Test
    public void testIsOpenClosedConnection()
    {
        System.out.println("isOpen - Closed EntityManager");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }

    @Test
    public void testClearCourse()
    {
        System.out.println("clearCourse");
        EntityManager em = testDb.getConnection();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        // Get the Course from the database for the GradedEvent creation
        List<Course> courses = instance.getAllCoursesByName();
        System.out.println("Number of Courses in DB (Expecting 2): " +
                courses.size());
        assertEquals(2, courses.size());

        Course course1 = courses.get(0);
        System.out.println("Course 1 (Expecting CS110): " + course1.getName());
        assertEquals("CS110", course1.getName());

        CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
        Root<Section> sectionRoot = cqSection.from(Section.class);
        TypedQuery<Section> sectionQuery = em.createQuery(cqSection);
        List<Section> sections = sectionQuery.getResultList();
        System.out.println("Number of Sections in DB (Expecting 4): "
                + sections.size());

        System.out.println("Sections in Course: " + course1.getSections().size());
        
        // Validate Original Data was saved to database
        CriteriaQuery<Question> cqQuestion = cb.createQuery(Question.class);
        Root<Question> questionRoot = cqQuestion.from(Question.class);
        TypedQuery<Question> questionQuery = em.createQuery(cqQuestion);
        List<Question> questions = questionQuery.getResultList();
        System.out.println("Number of Questions in DB (Expecting 3): "
                + questions.size());
        assertEquals(3, questions.size());

        CriteriaQuery<GradedEvent> cqGradedEvent = cb.createQuery(GradedEvent.class);
        Root<GradedEvent> gradedEventRoot = cqGradedEvent.from(GradedEvent.class);
        TypedQuery<GradedEvent> gradedEventQuery = em.createQuery(cqGradedEvent);
        List<GradedEvent> events = gradedEventQuery.getResultList();
        System.out.println("Number of GradedEvents in DB (Expecting 4): "
                + events.size());
        assertEquals(4, events.size());

        CriteriaQuery<Instructor> cqInstructor = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cqInstructor.from(Instructor.class);
        TypedQuery<Instructor> instructorQuery = em.createQuery(cqInstructor);
        List<Instructor> instructors = instructorQuery.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());

        Query query = em.createNativeQuery("SELECT * FROM Course_Instructor WHERE "
                + "course_id = " + course1.getId());
        List courseInsResult = query.getResultList();
        System.out.println("Number of Instructors for " + course1.getName() +
                " (Expecting 2 / 2): "
                + courseInsResult.size() + " / " + course1.getInstructors().size());
        assertEquals(2, courseInsResult.size());
        assertEquals(2, course1.getInstructors().size());


        query = em.createNativeQuery("SELECT * FROM Section_Course WHERE " +
                "course_id = " + course1.getId());
        List sectionCrsResult = query.getResultList();
        System.out.println("Number of Sections in " + course1.getName() +
                " (Expecting 2 / 2): " + sectionCrsResult.size() + " / " +
                course1.getSections().size());
        assertEquals(2, sectionCrsResult.size());
        assertEquals(2, course1.getSections().size());

        CriteriaQuery<Student> cqStudent = cb.createQuery(Student.class);
        Root<Student> studentRoot = cqStudent.from(Student.class);
        TypedQuery<Student> studentQuery = em.createQuery(cqStudent);
        List<Student> students = studentQuery.getResultList();
        System.out.println("Number of Students in DB (Expecting 7): "
                + students.size());
        assertEquals(7, students.size());

        // Clear the first Course: CS110
        System.out.println("Clearing Course 1: " + course1.getName() + " (" +
                course1.getId() + ")");
        instance.clearCourse(course1);

        // GradedEvent data remains, but enrollment and Instructors have been
        // cleared
        courses = instance.getAllCoursesByName();
        System.out.println("(Post) Number of Courses (Expecting 2): " + courses.size());
        assertEquals(2, courses.size());

        questions = questionQuery.getResultList();
        System.out.println("(Post) Number of Questions in DB (Expecting 3): "
                + questions.size());
        assertEquals(3, questions.size());

        events = gradedEventQuery.getResultList();
        System.out.println("(Post) Number of GradedEvents in DB (Expecting 4): "
                + events.size());
        assertEquals(4, events.size());
        
        instructors = instructorQuery.getResultList();
        System.out.println("(Post) Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());

        query = em.createNativeQuery("SELECT * FROM Course_Instructor WHERE "
                + "course_id = " + course1.getId());
        courseInsResult = query.getResultList();
        System.out.println("(Post) Number of Instructors for " + course1.getName() +
                " (Expecting 0): "
                + courseInsResult.size());
        assertEquals(0, courseInsResult.size());

        students = studentQuery.getResultList();
        System.out.println("(Post) Number of Students in DB (Expecting 7): "
                + students.size());
        assertEquals(7, students.size());

        sections = sectionQuery.getResultList();
        System.out.println("(Post) Number of Sections in DB (Expecting 2): "
                + sections.size());
        assertEquals(2, sections.size());

        query = em.createNativeQuery("SELECT * FROM Section_Course WHERE " +
                "course_id = " + course1.getId());
        sectionCrsResult = query.getResultList();
        System.out.println("(Post) Number of Sections in " + course1.getName() +
                " (Expecting 0): " + sectionCrsResult.size());
        assertEquals(0, sectionCrsResult.size());

        em.close();
    }

    @Test(expected = IllegalStateException.class)
    public void testClearCourseClosedConnection()
    {
        System.out.println("clearCourse - Closed Connection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        Course course = new Course("CS110");
        course.setId(new Long(1L));
        assertTrue(instance.clearCourse(course));
    }

    @Test(expected = NullPointerException.class)
    public void testClearCourseNullCourse()
    {
        System.out.println("clearCourse  - null Course");
        assertTrue(instance.isOpen());
        Course course = null;
        assertFalse(instance.clearCourse(course));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testClearCourseNullCourseId()
    {
        System.out.println("clearCourse - null Course Id");
        assertTrue(instance.isOpen());
        Course course = new Course();
        assertFalse(instance.clearCourse(course));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testClearCourseZeroCourseId()
    {
        System.out.println("clearCourse - Course Id = 0");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(0L));
        assertFalse(instance.clearCourse(course));
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testClearCourseNegativeCourseId()
    {
        System.out.println("clearCourse - Course Id < 0");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(-5L));
        assertFalse(instance.clearCourse(course));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testClearCourseNonExistentCourse()
    {
        System.out.println("clearCourse - Non-existent Course");
        assertTrue(instance.isOpen());
        Course course = new Course("CS192");
        course.setId(new Long(15L));
        assertFalse(instance.clearCourse(course));
    }

    @Test
    public void testFindId()
    {
        System.out.println("find - by Id");
        assertTrue(instance.isOpen());
        Long id = new Long(2L);
        Course dbCourse = instance.find(id);
        assertNotNull(dbCourse);
        assertEquals("CS364", dbCourse.getName());
        assertEquals(id, dbCourse.getId());
        assertEquals(1, dbCourse.getInstructors().size());
        assertEquals(2, dbCourse.getSections().size());
    }

    @Test
    public void testFindIdNoResult()
    {
        System.out.println("findIdNoResult");
        assertTrue(instance.isOpen());
        Long resultId = new Long(12L);
        Course result = instance.find(resultId);
        assertNull(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindIdLessThanZero()
    {
        System.out.println("findIdLessThanZero");
        assertTrue(instance.isOpen());
        Long resultId = new Long(-3L);
        Course result = instance.find(resultId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindIdZero()
    {
        System.out.println("findIdZero");
        assertTrue(instance.isOpen());
        Long resultId = new Long(0L);
        Course result = instance.find(resultId);
    }

    @Test(expected = NullPointerException.class)
    public void testFindIdNull()
    {
        System.out.println("findIdNull");
        assertTrue(instance.isOpen());
        Long nullId = null;
        Course result = instance.find(nullId);
    }

    @Test(expected = IllegalStateException.class)
    public void testFindIdClosedConnection()
    {
        System.out.println("findIdClosedConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Long resultId = new Long(2L);
        Course result = instance.find(resultId);
    }

    @Test
    public void testFindName()
    {
        System.out.println("find - by Name");
        assertTrue(instance.isOpen());
        String courseName = "CS364";
        Course dbCourse = instance.find(courseName);
        assertNotNull(dbCourse);
        assertEquals("CS364", dbCourse.getName());
        Long expectedId = new Long(2L);
        assertEquals(expectedId, dbCourse.getId());
        assertEquals(1, dbCourse.getInstructors().size());
        assertEquals(2, dbCourse.getSections().size());
    }

    @Test
    public void testFindNameNoResult()
    {
        System.out.println("findNameNoResult");
        assertTrue(instance.isOpen());
        String courseName = "CS194P";
        Course result = instance.find(courseName);
        assertNull(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindNameBlank()
    {
        System.out.println("findNameBlank");
        assertTrue(instance.isOpen());
        String courseName = "";
        Course result = instance.find(courseName);
    }

    @Test(expected = NullPointerException.class)
    public void testFindNameNull()
    {
        System.out.println("findNameNull");
        assertTrue(instance.isOpen());
        String courseName = null;
        Course result = instance.find(courseName);
    }

    @Test(expected = IllegalStateException.class)
    public void testFindNameClosedConnection()
    {
        System.out.println("findNameClosedConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        String courseName = "CS364";
        Course result = instance.find(courseName);
    }

    @Test
    public void testGetAllCoursesByName()
    {
        System.out.println("getAllCoursesByName");
        assertTrue(instance.isOpen());
        List<Course> courses = instance.getAllCoursesByName();
        assertNotNull(courses);
        assertEquals(2, courses.size());
        Course course1 = courses.get(0);
        assertEquals("CS110", course1.getName());
        Course course2 = courses.get(1);
        assertEquals("CS364", course2.getName());
    }

    @Test(expected=IllegalStateException.class)
    public void testGetAllCoursesByNameClosedConnection()
    {
        System.out.println("getAllCoursesByName - Closed Connection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        List courses = instance.getAllCoursesByName();
    }

    @Test
    public void testUpdate()
    {
        System.out.println("update - No change to Instructors or Sections");
        assertTrue(instance.isOpen());
        // Verify the original course
        System.out.println("Original: ");
        Course original = createExistingCourse();

        // Update the Name and Course Director fields
        System.out.println("Updated Course: ");
        original.setName("CS192P");
        System.out.println("- Setting Course Director");
        Instructor courseDirector = new Instructor("David", "Roberts", "David.Roberts");
        courseDirector.setDomainAccount(courseDirector.getWebID());
        courseDirector.setId(new Long(5L));
        System.out.println("  -- CD: " + courseDirector.getDomainAccount());
        original.setCourseDirector(courseDirector);

        Course result = instance.update(original);

        // Verify the result
        assertEquals(new Long(1L), result.getId());
        assertEquals("CS192P", result.getName());
        assertNotNull(result.getCourseDirector());
        assertEquals(courseDirector, result.getCourseDirector());
        assertNotNull(result.getInstructors());
        assertEquals(2, result.getInstructors().size());
        Instructor ins1 = result.getInstructor(0);
        assertEquals("David.Roberts", ins1.getDomainAccount());
        Instructor ins2 = result.getInstructor(1);
        assertEquals("Sarah.OReilly", ins2.getDomainAccount());
        assertNotNull(result.getSections());
        assertEquals(2, result.getSections().size());
        for (int i = 0; i < result.getSections().size(); i++)
        {
            Section section = result.getSection(i);
            System.out.println("- Section: " + section + ", Id: " + section.getId());
        }
        assertTrue(result.getSection(0).getName().equals("M1A") ||
                result.getSection(0).getName().equals("M2B"));
        assertTrue(result.getSection(1).getName().equals("M1A") ||
                result.getSection(1).getName().equals("M2B"));

        // Verify the data in the database
        EntityManager em = testDb.getConnection();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
        Root<Section> sectionRoot = cqSection.from(Section.class);
        TypedQuery<Section> sectionQuery = em.createQuery(cqSection);
        List<Section> sections = sectionQuery.getResultList();
        System.out.println("Number of Sections in DB (Expecting 4): "
                + sections.size());
        assertEquals(4, sections.size());

        CriteriaQuery<Instructor> cqInstructor = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cqInstructor.from(Instructor.class);
        TypedQuery<Instructor> instructorQuery = em.createQuery(cqInstructor);
        List<Instructor> instructors = instructorQuery.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());
        em.close();
    }

    @Test
    public void testUpdateNoChangeCD()
    {
        System.out.println("update - No change to Course Director, Instructors, " +
                "or Sections");
        Course course = createExistingCourse();
        course.setName("CS192P");
        assertTrue(instance.isOpen());
        Course result = instance.update(course);
        assertEquals(new Long(1L), result.getId());
        assertEquals("CS192P", result.getName());
        assertNull(result.getCourseDirector());
        assertNotNull(result.getInstructors());
        assertEquals(2, result.getInstructors().size());
        Instructor ins1 = result.getInstructor(0);
        assertEquals("David.Roberts", ins1.getDomainAccount());
        Instructor ins2 = result.getInstructor(1);
        assertEquals("Sarah.OReilly", ins2.getDomainAccount());
        assertNotNull(result.getSections());
        assertEquals(2, result.getSections().size());
        for (int i = 0; i < result.getSections().size(); i++)
        {
            Section section = result.getSection(i);
            System.out.println("- Section: " + section + ", Id: " + section.getId());
        }
        assertTrue(result.getSection(0).getName().equals("M1A") ||
                result.getSection(0).getName().equals("M2B"));
        assertTrue(result.getSection(1).getName().equals("M1A") ||
                result.getSection(1).getName().equals("M2B"));

        // Verify the data in the database
        EntityManager em = testDb.getConnection();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
        Root<Section> sectionRoot = cqSection.from(Section.class);
        TypedQuery<Section> sectionQuery = em.createQuery(cqSection);
        List<Section> sections = sectionQuery.getResultList();
        System.out.println("Number of Sections in DB (Expecting 4): "
                + sections.size());
        assertEquals(4, sections.size());

        CriteriaQuery<Instructor> cqInstructor = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cqInstructor.from(Instructor.class);
        TypedQuery<Instructor> instructorQuery = em.createQuery(cqInstructor);
        List<Instructor> instructors = instructorQuery.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());
        em.close();
    }

    @Test
    public void testUpdateNewCourseDirector()
    {
        System.out.println("update - New Instructor as Course Director");
        Course course = createExistingCourse();
        course.setName("CS192P");
        Instructor courseDirector = new Instructor("James", "Kirk", "James.Kirk");
        courseDirector.setId(50L);
        courseDirector.setDomainAccount(courseDirector.getWebID());
        course.setCourseDirector(courseDirector);

        Course result = instance.update(course);
        // Verify the result
        assertEquals(new Long(1L), result.getId());
        assertEquals("CS192P", result.getName());
        assertNotNull(result.getCourseDirector());
        assertEquals(courseDirector, result.getCourseDirector());
        assertNotNull(result.getInstructors());
        assertEquals(2, result.getInstructors().size());
        Instructor ins1 = result.getInstructor(0);
        assertEquals("David.Roberts", ins1.getDomainAccount());
        Instructor ins2 = result.getInstructor(1);
        assertEquals("Sarah.OReilly", ins2.getDomainAccount());
        assertNotNull(result.getSections());
        assertEquals(2, result.getSections().size());
        for (int i = 0; i < result.getSections().size(); i++)
        {
            Section section = result.getSection(i);
            System.out.println("- Section: " + section + ", Id: " + section.getId());
        }
        assertTrue(result.getSection(0).getName().equals("M1A") ||
                result.getSection(0).getName().equals("M2B"));
        assertTrue(result.getSection(1).getName().equals("M1A") ||
                result.getSection(1).getName().equals("M2B"));

        // Verify the data in the database
        EntityManager em = testDb.getConnection();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
        Root<Section> sectionRoot = cqSection.from(Section.class);
        TypedQuery<Section> sectionQuery = em.createQuery(cqSection);
        List<Section> sections = sectionQuery.getResultList();
        System.out.println("Number of Sections in DB (Expecting 4): "
                + sections.size());
        assertEquals(4, sections.size());

        CriteriaQuery<Instructor> cqInstructor = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cqInstructor.from(Instructor.class);
        TypedQuery<Instructor> instructorQuery = em.createQuery(cqInstructor);
        List<Instructor> instructors = instructorQuery.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 3): "
                + instructors.size());
        assertEquals(3, instructors.size());
        em.close();
    }

    @Test
    public void testUpdateRemoveInstructor()
    {
        System.out.println("update - Instructor Removed");
        Course course = createExistingCourse();
        assertEquals(2, course.getInstructors().size());
        Instructor oldIns = new Instructor("Sarah", "O'Reilly", "Sarah.OReilly");
        oldIns.setId(new Long(6L));
        oldIns.setDomainAccount(oldIns.getWebID());
        course.removeInstructor(oldIns);
        
        assertTrue(instance.isOpen());
        Course result = instance.update(course);
        assertEquals(new Long(1L), result.getId());
        assertEquals("CS110", result.getName());
        assertNull(result.getCourseDirector());
        assertNotNull(result.getInstructors());
        assertEquals(1, result.getInstructors().size());
        Instructor ins1 = result.getInstructor(0);
        assertEquals("David.Roberts", ins1.getDomainAccount());
        assertNotNull(result.getSections());
        assertEquals(2, result.getSections().size());
        for (int i = 0; i < result.getSections().size(); i++)
        {
            Section section = result.getSection(i);
            System.out.println("- Section: " + section + ", Id: " + section.getId());
        }
        assertTrue(result.getSection(0).getName().equals("M1A") ||
                result.getSection(0).getName().equals("M2B"));
        assertTrue(result.getSection(1).getName().equals("M1A") ||
                result.getSection(1).getName().equals("M2B"));

        // Verify the data in the database
        EntityManager em = testDb.getConnection();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
        Root<Section> sectionRoot = cqSection.from(Section.class);
        TypedQuery<Section> sectionQuery = em.createQuery(cqSection);
        List<Section> sections = sectionQuery.getResultList();
        System.out.println("Number of Sections in DB (Expecting 4): "
                + sections.size());
        assertEquals(4, sections.size());

        CriteriaQuery<Instructor> cqInstructor = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cqInstructor.from(Instructor.class);
        TypedQuery<Instructor> instructorQuery = em.createQuery(cqInstructor);
        List<Instructor> instructors = instructorQuery.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());
        em.close();
    }
    
    @Test
    public void testUpdateInstructorsNewInstructor()
    {
        System.out.println("update - New Instructor added");
        Course course = createExistingCourse();
        assertEquals(2, course.getInstructors().size());
        
        Instructor newIns = new Instructor("John", "Smith", "John.Smith");
        newIns.setId(new Long(42L));
        newIns.setDomainAccount(newIns.getWebID());
        course.addInstructor(newIns);

        assertTrue(instance.isOpen());
        Course result = instance.update(course);
        assertEquals(new Long(1L), result.getId());
        assertEquals("CS110", result.getName());
        assertNull(result.getCourseDirector());
        assertNotNull(result.getInstructors());
        assertEquals(3, result.getInstructors().size());
        Instructor ins1 = result.getInstructor(0);
        assertEquals("David.Roberts", ins1.getDomainAccount());
        Instructor ins2 = result.getInstructor(1);
        assertEquals("Sarah.OReilly", ins2.getDomainAccount());
        Instructor ins3 = result.getInstructor(2);
        assertEquals("John.Smith", ins3.getDomainAccount());
        assertNotNull(result.getSections());
        assertEquals(2, result.getSections().size());
        for (int i = 0; i < result.getSections().size(); i++)
        {
            Section section = result.getSection(i);
            System.out.println("- Section: " + section + ", Id: " + section.getId());
        }
        assertTrue(result.getSection(0).getName().equals("M1A") ||
                result.getSection(0).getName().equals("M2B"));
        assertTrue(result.getSection(1).getName().equals("M1A") ||
                result.getSection(1).getName().equals("M2B"));


        // Verify the data in the database
        EntityManager em = testDb.getConnection();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
        Root<Section> sectionRoot = cqSection.from(Section.class);
        TypedQuery<Section> sectionQuery = em.createQuery(cqSection);
        List<Section> sections = sectionQuery.getResultList();
        System.out.println("Number of Sections in DB (Expecting 4): "
                + sections.size());
        assertEquals(4, sections.size());

        CriteriaQuery<Instructor> cqInstructor = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cqInstructor.from(Instructor.class);
        TypedQuery<Instructor> instructorQuery = em.createQuery(cqInstructor);
        List<Instructor> instructors = instructorQuery.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 3): "
                + instructors.size());
        assertEquals(3, instructors.size());
        em.close();
    }

    @Test
    public void testUpdateClearInstructors()
    {
        System.out.println("update - Clear Instructors");
        Course course = createExistingCourse();
        course.clearInstructors();
        
        assertTrue(instance.isOpen());
        Course result = instance.update(course);
        assertEquals(new Long(1L), result.getId());
        assertEquals("CS110", result.getName());
        assertNull(result.getCourseDirector());
        assertNotNull(result.getInstructors());
        assertEquals(0, result.getInstructors().size());
        assertNotNull(result.getSections());
        assertEquals(2, result.getSections().size());
        for (int i = 0; i < result.getSections().size(); i++)
        {
            Section section = result.getSection(i);
            System.out.println("- Section: " + section + ", Id: " + section.getId());
        }
        assertTrue(result.getSection(0).getName().equals("M1A") ||
                result.getSection(0).getName().equals("M2B"));
        assertTrue(result.getSection(1).getName().equals("M1A") ||
                result.getSection(1).getName().equals("M2B"));

        // Verify the data in the database
        EntityManager em = testDb.getConnection();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
        Root<Section> sectionRoot = cqSection.from(Section.class);
        TypedQuery<Section> sectionQuery = em.createQuery(cqSection);
        List<Section> sections = sectionQuery.getResultList();
        System.out.println("Number of Sections in DB (Expecting 4): "
                + sections.size());
        assertEquals(4, sections.size());

        CriteriaQuery<Instructor> cqInstructor = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cqInstructor.from(Instructor.class);
        TypedQuery<Instructor> instructorQuery = em.createQuery(cqInstructor);
        List<Instructor> instructors = instructorQuery.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());
        em.close();
    }

    @Test
    public void testUpdateSections()
    {
        System.out.println("update - Sections Updated");
        Course course = createExistingCourse();

        course.getSection(0).setName("M3A");

        assertTrue(instance.isOpen());
        Course result = instance.update(course);
        assertEquals(new Long(1L), result.getId());
        assertEquals("CS110", result.getName());
        assertNull(result.getCourseDirector());
        assertNotNull(result.getInstructors());
        assertEquals(2, result.getInstructors().size());
        Instructor ins1 = result.getInstructor(0);
        assertEquals("David.Roberts", ins1.getDomainAccount());
        Instructor ins2 = result.getInstructor(1);
        assertEquals("Sarah.OReilly", ins2.getDomainAccount());
        assertNotNull(result.getSections());
        assertEquals(2, result.getSections().size());
        for (int i = 0; i < result.getSections().size(); i++)
        {
            Section section = result.getSection(i);
            System.out.println("- Section: " + section + ", Id: " + section.getId());
        }
        assertTrue(result.getSection(0).getName().equals("M3A") ||
                result.getSection(0).getName().equals("M2B"));
        assertTrue(result.getSection(1).getName().equals("M3A") ||
                result.getSection(1).getName().equals("M2B"));

        // Verify the data in the database
        EntityManager em = testDb.getConnection();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
        Root<Section> sectionRoot = cqSection.from(Section.class);
        TypedQuery<Section> sectionQuery = em.createQuery(cqSection);
        List<Section> sections = sectionQuery.getResultList();
        System.out.println("Number of Sections in DB (Expecting 4): "
                + sections.size());
        assertEquals(4, sections.size());

        CriteriaQuery<Instructor> cqInstructor = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cqInstructor.from(Instructor.class);
        TypedQuery<Instructor> instructorQuery = em.createQuery(cqInstructor);
        List<Instructor> instructors = instructorQuery.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());
        em.close();
    }

    @Test
    public void testUpdateSectionRemoved()
    {
        System.out.println("update - Section Removed");
        Course course = createExistingCourse();

        Section removedSect = new Section("M2B");
        removedSect.setId(new Long(2L));
        removedSect.setCourse(course);
        course.removeSection(removedSect);

        assertTrue(instance.isOpen());
        Course result = instance.update(course);
        assertEquals(new Long(1L), result.getId());
        assertEquals("CS110", result.getName());
        assertNull(result.getCourseDirector());
        assertNotNull(result.getInstructors());
        assertEquals(2, result.getInstructors().size());
        Instructor ins1 = result.getInstructor(0);
        assertEquals("David.Roberts", ins1.getDomainAccount());
        Instructor ins2 = result.getInstructor(1);
        assertEquals("Sarah.OReilly", ins2.getDomainAccount());
        assertNotNull(result.getSections());
        assertEquals(1, result.getSections().size());
        for (int i = 0; i < result.getSections().size(); i++)
        {
            Section section = result.getSection(i);
            System.out.println("- Section: " + section + ", Id: " + section.getId());
        }
        Section sec1 = result.getSection(0);
        assertEquals("M1A", sec1.getName());

        // Verify the data in the database
        EntityManager em = testDb.getConnection();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
        Root<Section> sectionRoot = cqSection.from(Section.class);
        TypedQuery<Section> sectionQuery = em.createQuery(cqSection);
        List<Section> sections = sectionQuery.getResultList();
        System.out.println("Number of Sections in DB (Expecting 4): "
                + sections.size());
        assertEquals(4, sections.size());

        CriteriaQuery<Instructor> cqInstructor = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cqInstructor.from(Instructor.class);
        TypedQuery<Instructor> instructorQuery = em.createQuery(cqInstructor);
        List<Instructor> instructors = instructorQuery.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());
        em.close();
    }

    @Test
    public void testUpdateClearSections()
    {
        System.out.println("update - Clear Sections");
        Course course = createExistingCourse();

        course.clearSections();
        
        assertTrue(instance.isOpen());
        Course result = instance.update(course);
        assertEquals(new Long(1L), result.getId());
        assertEquals("CS110", result.getName());
        assertNull(result.getCourseDirector());
        assertNotNull(result.getInstructors());
        assertEquals(2, result.getInstructors().size());
        Instructor ins1 = result.getInstructor(0);
        assertEquals("David.Roberts", ins1.getDomainAccount());
        Instructor ins2 = result.getInstructor(1);
        assertEquals("Sarah.OReilly", ins2.getDomainAccount());
        assertNotNull(result.getSections());
        assertEquals(0, result.getSections().size());

        // Verify the data in the database
        EntityManager em = testDb.getConnection();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Section> cqSection = cb.createQuery(Section.class);
        Root<Section> sectionRoot = cqSection.from(Section.class);
        TypedQuery<Section> sectionQuery = em.createQuery(cqSection);
        List<Section> sections = sectionQuery.getResultList();
        System.out.println("Number of Sections in DB (Expecting 4): "
                + sections.size());
        assertEquals(4, sections.size());

        CriteriaQuery<Instructor> cqInstructor = cb.createQuery(Instructor.class);
        Root<Instructor> instructorRoot = cqInstructor.from(Instructor.class);
        TypedQuery<Instructor> instructorQuery = em.createQuery(cqInstructor);
        List<Instructor> instructors = instructorQuery.getResultList();
        System.out.println("Number of Instructors in DB (Expecting 2): "
                + instructors.size());
        assertEquals(2, instructors.size());
        em.close();
    }

    @Test(expected=NullPointerException.class)
    public void testUpdateNullCourse()
    {
        System.out.println("update - null Course");
        assertTrue(instance.isOpen());
        Course course = null;
        Course updatedCourse = instance.update(course);
    }

    @Test(expected=IllegalStateException.class)
    public void testUpdateClosedConnection()
    {
        System.out.println("update - Closed Connection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Course course = createExistingCourse();
        course.setName("CS110H");
        Course updatedCourse = instance.update(course);
    }

    @Test(expected=NullPointerException.class)
    public void testUpdateNullId()
    {
        System.out.println("update - Null Id");
        assertTrue(instance.isOpen());
        Course course = createExistingCourse();
        course.setId(null);
        Course updatedCourse = instance.update(course);
    }

    @Test(expected=NullPointerException.class)
    public void testUpdateNullName()
    {
        System.out.println("update - Null Name");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(1L));
        Course updatedCourse = instance.update(course);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testUpdateBlankName()
    {
        System.out.println("update - Blank Name");
        assertTrue(instance.isOpen());
        Course course = new Course("");
        course.setId(new Long(1L));
        Course updatedCourse = instance.update(course);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testUpdateZeroId()
    {
        System.out.println("update - Id = 0");
        assertTrue(instance.isOpen());
        Course course = createExistingCourse();
        course.setId(new Long(0L));
        Course updatedCourse = instance.update(course);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testUpdateNegativeId()
    {
        System.out.println("update - Id < 0");
        assertTrue(instance.isOpen());
        Course course = createExistingCourse();
        course.setId(new Long(-5L));
        Course updatedCourse = instance.update(course);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testUpdateNonExistentCourse()
    {
        System.out.println("update - Non-existent Course");
        assertTrue(instance.isOpen());
        Course course = createTestCourse();
        Course updatedCourse = instance.update(course);
    }
    
    /**
     * Utility method to create the same course for testing.
     * @return      The created Course;
     */
    private Course createTestCourse()
    {
        Course course = new Course("CS256");
        course.setId(new Long(15L));
        
        // Create 2 Sections, 2 Students per Section, and 2 Instructors
        Student student1 = new Student("Jerry", "Baker", "C14Jerry.Baker",
                new Integer(2013));
        student1.setId(new Long(10L));
        Student student2 = new Student("Lisa", "Thompson", "C14Lisa.Thompson",
                new Integer(2013));
        student2.setId(new Long(11L));
        Student student3 = new Student("Kelly", "Rogers", "C14Kelly.Rogers",
                new Integer(2013));
        student3.setId(new Long(12L));
        Student student4 = new Student("Jason", "Borough", "C14Jason.Borough",
                new Integer(2013));
        student4.setId(new Long(13L));
        Section section1 = new Section("M3A");
        section1.setId(new Long(10L));
        Section section2 = new Section("T2B");
        section2.setId(new Long(11L));
        Instructor instructor1 = new Instructor("Sam", "Pierce", "Sam.Pierce");
        instructor1.setDomainAccount(instructor1.getWebID());
        instructor1.setId(new Long(14L));
        Instructor instructor2 = new Instructor("Jenny", "Conners", "Jenny.Conners");
        instructor2.setDomainAccount(instructor2.getWebID());
        instructor2.setId(new Long(15L));

        // Set the Instructors for Sections 1 & 2
        section1.setInstructor(instructor1);
        section2.setInstructor(instructor2);
        section1.setCourse(course);
        section2.setCourse(course);

        // Add Instructors to Course
        course.addInstructor(instructor1);
        course.addInstructor(instructor2);

        // Add Students 1 & 2 to Section 1
        section1.addStudent(student1);
        student1.addSection(section1);
        section1.addStudent(student2);
        student2.addSection(section1);

        // Add Students 3 & 4 to Section 2
        section2.addStudent(student3);
        student3.addSection(section2);
        section2.addStudent(student4);
        student4.addSection(section2);

        // Add Sections 1 & 2 to Course
        course.addSection(section1);
        course.addSection(section2);

        return course;
    }

    /**
     * Utility method to create the same course for testing.
     * @return      The created Course;
     */
    private Course createTestCourseNullId()
    {
        Course course = new Course("CS256");
        
        // Create 2 Sections, 2 Students per Section, and 2 Instructors
        Student student1 = new Student("Jerry", "Baker", "C14Jerry.Baker",
                new Integer(2013));
        student1.setId(new Long(10L));
        Student student2 = new Student("Lisa", "Thompson", "C14Lisa.Thompson",
                new Integer(2013));
        student2.setId(new Long(11L));
        Student student3 = new Student("Kelly", "Rogers", "C14Kelly.Rogers",
                new Integer(2013));
        student3.setId(new Long(12L));
        Student student4 = new Student("Jason", "Borough", "C14Jason.Borough",
                new Integer(2013));
        student4.setId(new Long(13L));
        Section section1 = new Section("M3A");
        section1.setId(new Long(10L));
        Section section2 = new Section("T2B");
        section2.setId(new Long(11L));
        Instructor instructor1 = new Instructor("Sam", "Pierce", "Sam.Pierce");
        instructor1.setDomainAccount(instructor1.getWebID());
        instructor1.setId(new Long(14L));
        Instructor instructor2 = new Instructor("Jenny", "Conners", "Jenny.Conners");
        instructor2.setDomainAccount(instructor2.getWebID());
        instructor2.setId(new Long(15L));

        // Set the Instructors for Sections 1 & 2
        section1.setInstructor(instructor1);
        section2.setInstructor(instructor2);
        section1.setCourse(course);
        section2.setCourse(course);

        // Add Instructors to Course
        course.addInstructor(instructor1);
        course.addInstructor(instructor2);

        // Add Students 1 & 2 to Section 1
        section1.addStudent(student1);
        student1.addSection(section1);
        section1.addStudent(student2);
        student2.addSection(section1);

        // Add Students 3 & 4 to Section 2
        section2.addStudent(student3);
        student3.addSection(section2);
        section2.addStudent(student4);
        student4.addSection(section2);

        // Add Sections 1 & 2 to Course
        course.addSection(section1);
        course.addSection(section2);

        return course;
    }

    private Course createExistingCourse()
    {
        Course course = new Course("CS110");
        course.setId(1L);

        // Create 2 Sections, 2 Students per Section, and 2 Instructors
        Student student1 = new Student("John", "Doe", "C13John.Doe",
                new Integer(2013));
        student1.setId(new Long(1L));
        Student student2 = new Student("Mary", "Johnson", "C13Mary.Johnson",
                new Integer(2013));
        student2.setId(new Long(2L));
        Student student3 = new Student("Bob", "Reimer", "C13Robert.Reimer",
                new Integer(2013));
        student3.setId(new Long(3L));
        Student student4 = new Student("Andrew", "Callow", "C13Andrew.Callow",
                new Integer(2013));
        student4.setId(new Long(4L));

        Section section1 = new Section("M1A");
        section1.setId(new Long(1L));
        Section section2 = new Section("M2B");
        section2.setId(new Long(2L));
        section1.setCourse(course);
        section2.setCourse(course);

        Instructor instructor1 = new Instructor("David", "Roberts", "David.Roberts");
        instructor1.setId(new Long(5L));
        Instructor instructor2 = new Instructor("Sarah", "O'Reilly", "Sarah.OReilly");
        instructor2.setId(new Long(6L));

        // Set the Instructors for Sections 1 & 2
        section1.setInstructor(instructor1);
        section2.setInstructor(instructor2);

        // Add Instructors to Course
        course.addInstructor(instructor1);
        course.addInstructor(instructor2);

        // Add Students 1 & 2 to Section 1
        section1.addStudent(student1);
        student1.addSection(section1);
        section1.addStudent(student2);
        student2.addSection(section1);

        course.addSection(section1);
        course.addSection(section2);

        // Add Students 3 & 4 to Section 2
        section2.addStudent(student3);
        student3.addSection(section2);
        section2.addStudent(student4);
        student4.addSection(section2);

        return course;
    }

    private static TestDatabase testDb;
    private CourseDAO instance;
}
