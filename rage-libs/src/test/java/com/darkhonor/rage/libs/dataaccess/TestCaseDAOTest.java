/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import java.util.List;
import java.util.ArrayList;
import java.math.BigDecimal;
import com.darkhonor.rage.libs.dataaccess.testdb.TestDatabase;
import com.darkhonor.rage.model.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alex Ackerman
 */
public class TestCaseDAOTest
{

    public TestCaseDAOTest()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        testDb = new TestDatabase();
        testDb.initialize();
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
        testDb.shutdown();
    }

    @Before
    public void setUp()
    {
        instance = new TestCaseDAO(testDb.getConnection());
    }

    @After
    public void tearDown()
    {
        if (instance != null && instance.isOpen())
        {
            instance.closeConnection();
        }
        testDb.reset();
    }

    /**
     * Test of closeConnection method, of class TestCaseDAO.
     */
    @Test
    public void testCloseConnection()
    {
        System.out.println("closeConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }

    @Test(expected=IllegalStateException.class)
    public void testCloseConnectionAlreadyClosed()
    {
        System.out.println("closeConnectionAlreadyClosed");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.closeConnection();
    }

    /**
     * Test of isOpen method, of class TestCaseDAO.
     */
    @Test
    public void testIsOpen()
    {
        System.out.println("isOpen");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }

    /**
     * Test of find method, of class TestCaseDAO.
     */
    @Test
    public void testFind()
    {
        System.out.println("find");
        Long id = new Long(2L);
        BigDecimal expValue = new BigDecimal("2.5");
        TestCase expResult = createSampleTestCase2();

        assertTrue(instance.isOpen());
        TestCase result = instance.find(id);
        assertNotNull(result);
        assertEquals(id, result.getId());
        assertEquals(expValue, result.getValue());
        assertNotNull(result.getInputs());
        assertEquals(2, result.getInputs().size());
        assertNotNull(result.getOutputs());
        assertEquals(1, result.getOutputs().size());
        assertNotNull(result.getExcludes());
        assertEquals(0, result.getExcludes().size());
        assertEquals(expResult, result);
        assertTrue(instance.isOpen());
    }

    @Test
    public void testFindNoResult()
    {
        System.out.println("find - No result");
        Long id = new Long(53L);

        assertTrue(instance.isOpen());
        TestCase result = instance.find(id);
        assertNull(result);
        assertTrue(instance.isOpen());
    }
    
    @Test(expected = IllegalStateException.class)
    public void testFindIdClosedConnection()
    {
        System.out.println("find - Closed Connection");
        Long id = new Long(2L);

        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        TestCase result = instance.find(id);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFindIdEqualsZero()
    {
        System.out.println("find - id = 0");
        Long id = new Long(0L);
        assertTrue(instance.isOpen());
        TestCase result = instance.find(id);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFindIdLessThanZero()
    {
        System.out.println("find - id < 0");
        Long id = new Long(-2L);
        assertTrue(instance.isOpen());
        TestCase result = instance.find(id);
    }
    
    @Test(expected = NullPointerException.class)
    public void testFindIdEqualsNull()
    {
        System.out.println("find - id = null");
        Long id = null;
        assertTrue(instance.isOpen());
        TestCase result = instance.find(id);
    }
    
    /**
     * Test of create method, of class TestCaseDAO.
     */
    @Test
    public void testCreate()
    {
        System.out.println("create");
        TestCase testCase = createNewTestCase();
        Long expResult = new Long(43L);
        assertTrue(instance.isOpen());
        Long result = instance.create(testCase);
        assertTrue(instance.isOpen());
        assertEquals(expResult, result);
        
        TestCase searchTestCase = instance.find(new Long(43L));
        assertNotNull(searchTestCase);
        assertEquals(new BigDecimal("10.35"), searchTestCase.getValue());
        assertEquals(1, searchTestCase.getInputs().size());
        assertEquals("42", searchTestCase.getInputs().get(0));
        assertEquals(1, searchTestCase.getOutputs().size());
        assertEquals("The Ultimate Answer", searchTestCase.getOutputs().get(0));
        assertEquals(1, searchTestCase.getExcludes().size());
        assertEquals("Question", searchTestCase.getExcludes().get(0));
    }
    
    @Test
    public void testCreateNoExcludes()
    {
        TestCase testCase = createNewTestCaseNoExcludes();
        assertTrue(instance.isOpen());
        Long result = instance.create(testCase);
        assertTrue(instance.isOpen());
        assertEquals(new Long(42L), result);
        TestCase searchTestCase = instance.find(new Long(42L));
        assertNotNull(searchTestCase);
        assertEquals(new BigDecimal("10.24"), searchTestCase.getValue());
        assertEquals(2, searchTestCase.getInputs().size());
        assertEquals("12", searchTestCase.getInputs().get(0));
        assertEquals("34", searchTestCase.getInputs().get(1));
        assertEquals(1, searchTestCase.getOutputs().size());
        assertEquals("56", searchTestCase.getOutputs().get(0));
        assertEquals(0, searchTestCase.getExcludes().size());   
    }
    
    @Test
    public void testCreateNoInputs()
    {
        TestCase testCase = createNewTestCaseNoInputs();
        assertTrue(instance.isOpen());
        Long result = instance.create(testCase);
        assertTrue(instance.isOpen());
        assertEquals(new Long(45L), result);
        TestCase searchTestCase = instance.find(new Long(45L));
        assertNotNull(searchTestCase);
        assertEquals(new BigDecimal("10.25"), searchTestCase.getValue());
        assertEquals(0, searchTestCase.getInputs().size());
        assertEquals(1, searchTestCase.getOutputs().size());
        assertEquals("Hello World", searchTestCase.getOutputs().get(0));
        assertEquals(0, searchTestCase.getExcludes().size());
    }
    
    @Test
    public void testCreateNoId()
    {
        System.out.println("create - No Id");
        TestCase testCase = createNewTestCaseNoId();
        assertTrue(instance.isOpen());
        Long result = instance.create(testCase);
        assertTrue(instance.isOpen());
        TestCase searchTestCase = instance.find(result);
        assertNotNull(searchTestCase);
        assertEquals(new BigDecimal("10.18"), searchTestCase.getValue());
        assertEquals(2, searchTestCase.getInputs().size());
        assertEquals("12", searchTestCase.getInputs().get(0));
        assertEquals("34", searchTestCase.getInputs().get(1));
        assertEquals(1, searchTestCase.getOutputs().size());
        assertEquals("56", searchTestCase.getOutputs().get(0));
        assertEquals(0, searchTestCase.getExcludes().size());
    }
    
    @Test(expected = IllegalStateException.class)
    public void testCreateClosedConnection()
    {
        System.out.println("create - Closed Connection");
        TestCase testCase = createNewTestCase();
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Long result = instance.create(testCase);
    }
    
    @Test(expected = NullPointerException.class)
    public void testCreateNullTestCase()
    {
        System.out.println("create - null TestCase");
        TestCase testCase = null;
        assertTrue(instance.isOpen());
        Long result = instance.create(testCase);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testCreateNullTestCaseValue()
    {
        System.out.println("create - null TestCase Value");
        TestCase testCase = new TestCase();
        testCase.setId(new Long(42L));
        testCase.addInput("12");
        testCase.addInput("34");
        testCase.addOutput("56");
        assertTrue(instance.isOpen());
        Long result = instance.create(testCase);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testCreateDuplicate()
    {
        System.out.println("create - Duplicate TestCase");
        TestCase testCase = createSampleTestCase1();
        assertTrue(instance.isOpen());
        Long result = instance.create(testCase);
    }

    /**
     * Test of update method, of class TestCaseDAO.
     */
    @Test
    public void testUpdate()
    {
        System.out.println("update");
        TestCase testCase = createSampleTestCase1();
        assertTrue(instance.isOpen());
        
        testCase.setValue(new BigDecimal("1.6"));
        testCase.addInput("9876");
        testCase.setOutputs(new ArrayList<String>());
        testCase.addOutput("Name - Bigfellar");
        testCase.addOutput("Race - Tauren");
        testCase.addExclusion("Orc");
        testCase.addExclusion("Human");
               
        TestCase result = instance.update(testCase);
        assertTrue(instance.isOpen());

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(new Long(5L), result.getId());
        assertNotNull(result.getValue());
        assertEquals(new BigDecimal("1.6"), result.getValue());
        assertNotNull(result.getInputs());
        assertEquals(3, result.getInputs().size());
        assertEquals("1234", result.getInputs().get(0));
        assertEquals("5678", result.getInputs().get(1));
        assertEquals("9876", result.getInputs().get(2));
        assertNotNull(result.getOutputs());
        assertEquals(2, result.getOutputs().size());
        assertEquals("Name - Bigfellar", result.getOutputs().get(0));
        assertEquals("Race - Tauren", result.getOutputs().get(1));
        assertNotNull(result.getExcludes());
        assertEquals(5, result.getExcludes().size());
        assertEquals("2345", result.getExcludes().get(0));
        assertEquals("9876", result.getExcludes().get(1));
        assertEquals("Result = 12345678", result.getExcludes().get(2));
        assertEquals("Orc", result.getExcludes().get(3));
        assertEquals("Human", result.getExcludes().get(4));
    }

    @Test
    public void testUpdateNoInputs()
    {
        System.out.println("update - To no Inputs");
        TestCase testCase = createSampleTestCase1();
        assertTrue(instance.isOpen());
        
        testCase.setValue(new BigDecimal("1.6"));
        testCase.setInputs(new ArrayList<String>());
        testCase.setOutputs(new ArrayList<String>());
        testCase.addOutput("Name - Bigfellar");
        testCase.addOutput("Race - Tauren");
        testCase.addExclusion("Orc");
        testCase.addExclusion("Human");
               
        TestCase result = instance.update(testCase);
        assertTrue(instance.isOpen());

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(new Long(5L), result.getId());
        assertNotNull(result.getValue());
        assertEquals(new BigDecimal("1.6"), result.getValue());
        assertNotNull(result.getInputs());
        assertEquals(0, result.getInputs().size());
        assertNotNull(result.getOutputs());
        assertEquals(2, result.getOutputs().size());
        assertEquals("Name - Bigfellar", result.getOutputs().get(0));
        assertEquals("Race - Tauren", result.getOutputs().get(1));
        assertNotNull(result.getExcludes());
        assertEquals(5, result.getExcludes().size());
        assertEquals("2345", result.getExcludes().get(0));
        assertEquals("9876", result.getExcludes().get(1));
        assertEquals("Result = 12345678", result.getExcludes().get(2));
    }
    
    @Test
    public void testUpdateNoExemptions()
    {
        System.out.println("update - To no Exemptions");
        System.out.println("update");
        TestCase testCase = createSampleTestCase1();
        assertTrue(instance.isOpen());
        
        testCase.setValue(new BigDecimal("1.6"));
        testCase.addInput("9876");
        testCase.setOutputs(new ArrayList<String>());
        testCase.addOutput("Name - Bigfellar");
        testCase.addOutput("Race - Tauren");
        testCase.setExcludes(new ArrayList<String>());
               
        TestCase result = instance.update(testCase);
        assertTrue(instance.isOpen());

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(new Long(5L), result.getId());
        assertNotNull(result.getValue());
        assertEquals(new BigDecimal("1.6"), result.getValue());
        assertNotNull(result.getInputs());
        assertEquals(3, result.getInputs().size());
        assertEquals("1234", result.getInputs().get(0));
        assertEquals("5678", result.getInputs().get(1));
        assertEquals("9876", result.getInputs().get(2));
        assertNotNull(result.getOutputs());
        assertEquals(2, result.getOutputs().size());
        assertEquals("Name - Bigfellar", result.getOutputs().get(0));
        assertEquals("Race - Tauren", result.getOutputs().get(1));
        assertNotNull(result.getExcludes());
        assertEquals(0, result.getExcludes().size());
    }
    
    @Test
    public void testUpdateFromNoInputs()
    {
        System.out.println("update - From no inputs");
        TestCase testCase = createSampleTestCase3();
        assertTrue(instance.isOpen());
        
        testCase.setValue(new BigDecimal("1.6"));
        testCase.addInput("9876");
               
        TestCase result = instance.update(testCase);
        assertTrue(instance.isOpen());

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(new Long(6L), result.getId());
        assertNotNull(result.getValue());
        assertEquals(new BigDecimal("1.6"), result.getValue());
        assertNotNull(result.getInputs());
        assertEquals(1, result.getInputs().size());
        assertEquals("9876", result.getInputs().get(0));
        assertNotNull(result.getOutputs());
        assertEquals(1, result.getOutputs().size());
        assertEquals("Hello World", result.getOutputs().get(0));
        assertNotNull(result.getExcludes());
        assertEquals(1, result.getExcludes().size());
        assertEquals("Mini Me", result.getExcludes().get(0));
    }
    
    @Test
    public void testUpdateFromNoExemptions()
    {
        System.out.print("update - From no exemptions");
        TestCase testCase = createSampleTestCase2();
        assertTrue(instance.isOpen());
        
        testCase.addExclusion("Orc");
        testCase.addExclusion("Human");
               
        TestCase result = instance.update(testCase);
        assertTrue(instance.isOpen());

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(new Long(2L), result.getId());
        assertNotNull(result.getValue());
        assertEquals(new BigDecimal("2.5"), result.getValue());
        assertNotNull(result.getInputs());
        assertEquals(2, result.getInputs().size());
        assertEquals("4", result.getInputs().get(0));
        assertEquals("5", result.getInputs().get(1));
        assertNotNull(result.getOutputs());
        assertEquals(1, result.getOutputs().size());
        assertEquals("Result = 999999999", result.getOutputs().get(0));
        assertNotNull(result.getExcludes());
        assertEquals(2, result.getExcludes().size());
        assertEquals("Orc", result.getExcludes().get(0));
        assertEquals("Human", result.getExcludes().get(1));
    }
    
    @Test(expected = IllegalStateException.class)
    public void testUpdateClosedConnection()
    {
        System.out.println("update - Closed Connection");
        TestCase testCase = createSampleTestCase1();
        assertTrue(instance.isOpen());
        
        testCase.setValue(new BigDecimal("1.6"));
        testCase.addInput("9876");
        testCase.setOutputs(new ArrayList<String>());
        testCase.addOutput("Name - Bigfellar");
        testCase.addOutput("Race - Tauren");
        testCase.addExclusion("Orc");
        testCase.addExclusion("Human");
        
        instance.closeConnection();
        assertFalse(instance.isOpen());
        TestCase result = instance.update(testCase);
    }
    
    @Test(expected = NullPointerException.class)
    public void testUpdateNullTestCase()
    {
        System.out.println("update - Null TestCase");
        TestCase testCase = null;
        assertTrue(instance.isOpen());
        
        TestCase result = instance.update(testCase);
    }
    
    @Test(expected = NullPointerException.class)
    public void testUpdateNullId()
    {
        System.out.println("update - Null id");
        TestCase testCase = createSampleTestCase1();
        assertTrue(instance.isOpen());
        
        testCase.setId(null);
        testCase.setValue(new BigDecimal("1.6"));
        testCase.addInput("9876");
        testCase.setOutputs(new ArrayList<String>());
        testCase.addOutput("Name - Bigfellar");
        testCase.addOutput("Race - Tauren");
        testCase.addExclusion("Orc");
        testCase.addExclusion("Human");
               
        TestCase result = instance.update(testCase);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testUpdateNotFound()
    {
        System.out.println("update - TestCase not found");
        TestCase testCase = createSampleTestCase1();
        assertTrue(instance.isOpen());
        
        testCase.setId(new Long(42L));
        testCase.setValue(new BigDecimal("1.6"));
        testCase.addInput("9876");
        testCase.setOutputs(new ArrayList<String>());
        testCase.addOutput("Name - Bigfellar");
        testCase.addOutput("Race - Tauren");
        testCase.addExclusion("Orc");
        testCase.addExclusion("Human");
               
        TestCase result = instance.update(testCase);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testUpdateZeroId()
    {
        System.out.println("update - id = 0");
        TestCase testCase = createSampleTestCase1();
        assertTrue(instance.isOpen());
        
        testCase.setId(new Long(0L));
        testCase.setValue(new BigDecimal("1.6"));
        testCase.addInput("9876");
        testCase.setOutputs(new ArrayList<String>());
        testCase.addOutput("Name - Bigfellar");
        testCase.addOutput("Race - Tauren");
        testCase.addExclusion("Orc");
        testCase.addExclusion("Human");
               
        TestCase result = instance.update(testCase);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testUpdateNegativeId()
    {
        System.out.println("update - id < 0");
        TestCase testCase = createSampleTestCase1();
        assertTrue(instance.isOpen());
        
        testCase.setId(new Long(-2L));
        testCase.setValue(new BigDecimal("1.6"));
        testCase.addInput("9876");
        testCase.setOutputs(new ArrayList<String>());
        testCase.addOutput("Name - Bigfellar");
        testCase.addOutput("Race - Tauren");
        testCase.addExclusion("Orc");
        testCase.addExclusion("Human");
               
        TestCase result = instance.update(testCase);
    }
    
    /**
     * Test of delete method, of class TestCaseDAO.
     */
    @Test
    public void testDelete_TestCase()
    {
        System.out.println("delete");
        TestCase testCase = createSampleTestCase1();
        Long id = testCase.getId();

        TestCase result = instance.find(id);
        assertNotNull(result);

        assertTrue(instance.isOpen());
        instance.delete(testCase);
        assertTrue(instance.isOpen());
        
        result = instance.find(id);
        assertNull(result);
    }

    @Test
    public void testDeleteTestCaseNoInputs()
    {
        System.out.println("delete - No Inputs");
        TestCase testCase = createSampleTestCase3();
        Long id = testCase.getId();

        TestCase result = instance.find(id);
        assertNotNull(result);

        assertTrue(instance.isOpen());
        instance.delete(testCase);
        assertTrue(instance.isOpen());
        
        result = instance.find(id);
        assertNull(result);
    }
    
    @Test
    public void testDeleteTestCaseNoExemptions()
    {
        System.out.println("delete - No Exemptions");
        TestCase testCase = createSampleTestCase2();
        Long id = testCase.getId();

        TestCase result = instance.find(id);
        assertNotNull(result);

        assertTrue(instance.isOpen());
        instance.delete(testCase);
        assertTrue(instance.isOpen());
        
        result = instance.find(id);
        assertNull(result);
    }
    
    @Test(expected = IllegalStateException.class)
    public void testDeleteTestCaseClosedConnection()
    {
        System.out.println("delete - Closed Connection");
        TestCase testCase = createSampleTestCase1();
        Long id = testCase.getId();

        TestCase result = instance.find(id);
        assertNotNull(result);

        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.delete(testCase);
    }
    
    @Test(expected = NullPointerException.class)
    public void testDeleteTestCaseNullTestCase()
    {
        System.out.println("delete - Null TestCase");
        TestCase testCase = null;

        assertTrue(instance.isOpen());
        instance.delete(testCase);
    }
    
    @Test(expected = NullPointerException.class)
    public void testDeleteTestCaseNullId()
    {
        System.out.println("delete - Null id");
        TestCase testCase = createSampleTestCase1();
        
        testCase.setId(null);
        assertTrue(instance.isOpen());
        instance.delete(testCase);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDeleteTestCaseNotFound()
    {
        System.out.println("delete - TestCase not found");
        System.out.println("delete - Null id");
        TestCase testCase = createNewTestCase();
        Long id = testCase.getId();

        TestCase result = instance.find(id);
        assertNull(result);
        
        assertTrue(instance.isOpen());
        instance.delete(testCase);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDeleteTestCaseZeroId()
    {
        System.out.println("delete - id = 0");
        TestCase testCase = createSampleTestCase1();
        
        testCase.setId(new Long(0L));
        assertTrue(instance.isOpen());
        instance.delete(testCase);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDeleteTestCaseNegativeId()
    {
        System.out.println("delete - id < 0");
        TestCase testCase = createSampleTestCase1();
        
        testCase.setId(new Long(-3L));
        assertTrue(instance.isOpen());
        instance.delete(testCase);        
    }
    
    /**
     * Test of delete method, of class TestCaseDAO.
     */
    @Test
    public void testDelete_Long()
    {
        System.out.println("delete (id)");
        Long id = new Long(5L);

        TestCase result = instance.find(id);
        assertNotNull(result);

        assertTrue(instance.isOpen());
        instance.delete(id);
        assertTrue(instance.isOpen());
        
        result = instance.find(id);
        assertNull(result);
    }

    @Test
    public void testDeleteIdNoInputs()
    {
        System.out.println("delete (id) - No Inputs");
        Long id = new Long(6L);

        TestCase result = instance.find(id);
        assertNotNull(result);

        assertTrue(instance.isOpen());
        instance.delete(id);
        assertTrue(instance.isOpen());
        
        result = instance.find(id);
        assertNull(result);
    }
    
    @Test
    public void testDeleteIdNoExemptions()
    {
        System.out.println("delete (id) - No Exemptions");
        Long id = new Long(2L);

        TestCase result = instance.find(id);
        assertNotNull(result);

        assertTrue(instance.isOpen());
        instance.delete(id);
        assertTrue(instance.isOpen());
        
        result = instance.find(id);
        assertNull(result);
    }
    
    @Test(expected = IllegalStateException.class)
    public void testDeleteIdClosedConnection()
    {
        System.out.println("delete (id) - Closed Connection");
        Long id = new Long(5L);

        TestCase result = instance.find(id);
        assertNotNull(result);

        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.delete(id);
    }
    
    @Test(expected = NullPointerException.class)
    public void testDeleteIdNullId()
    {
        System.out.println("delete (id) - Null id");
        Long id = null;

        assertTrue(instance.isOpen());
        instance.delete(id);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDeleteIdNotFound()
    {
        System.out.println("delete (id) - TestCase not found");
        Long id = new Long(42L);

        TestCase result = instance.find(id);
        assertNull(result);

        assertTrue(instance.isOpen());
        instance.delete(id);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDeleteIdZeroId()
    {
        System.out.println("delete (id) - id = 0");
        Long id = new Long(0L);

        assertTrue(instance.isOpen());
        instance.delete(id);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDeleteIdNegativeId()
    {
        System.out.println("delete (id) - id < 0");
        Long id = new Long(-5L);

        assertTrue(instance.isOpen());
        instance.delete(id);
    }
    
    @Test
    public void testFindAllTestCases()
    {
        System.out.println("findAllTestCases");
        
        assertTrue(instance.isOpen());
        List<TestCase> testCases = instance.findAllTestCases();
        assertTrue(instance.isOpen());
        assertEquals(5, testCases.size());
    }
    
    @Test(expected = IllegalStateException.class)
    public void testFindAllTestCasesClosedConnection()
    {
        System.out.println("findAllTestCases - Closed Connection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        List<TestCase> testCases = instance.findAllTestCases();
    }
    
    /**
     * Helper Method
     * 
     * @return      A predictable TestCase
     */
    private TestCase createSampleTestCase1()
    {
        TestCase testCase = new TestCase(new BigDecimal("1.0"));
        testCase.setId(new Long(5L));
        testCase.addInput("1234");
        testCase.addInput("5678");
        testCase.addOutput("Result = 12345678");
        testCase.addExclusion("2345");
        testCase.addExclusion("9876");
        testCase.addExclusion("Result = 12345678");
        return testCase;
    }
    
    /**
     * Helper Method
     * 
     * @return      A predictable TestCase
     */
    private TestCase createSampleTestCase2()
    {
        TestCase testCase = new TestCase(new BigDecimal("2.5"));
        testCase.setId(new Long(2L));
        testCase.addInput("4");
        testCase.addInput("5");
        testCase.addOutput("Result = 999999999");
        return testCase;
    }
    
    /**
     * Helper method
     * 
     * @return      A predictable TestCase
     */
    private TestCase createSampleTestCase3()
    {
        TestCase testCase = new TestCase(new BigDecimal("3.5"));
        testCase.setId(new Long(6L));
        testCase.addOutput("Hello World");
        testCase.addExclusion("Mini Me");
        return testCase;
    }
    
    /**
     * Helper Method
     * 
     * @return      A predictable TestCase
     */
    private TestCase createNewTestCaseNoExcludes()
    {
        TestCase testCase = new TestCase(new BigDecimal("10.24"));
        testCase.setId(new Long(42L));
        testCase.addInput("12");
        testCase.addInput("34");
        testCase.addOutput("56");
        return testCase;
    }
    
    /**
     * Helper Method
     * 
     * @return      A predictable TestCase
     */
    private TestCase createNewTestCase()
    {
        TestCase testCase = new TestCase(new BigDecimal("10.35"));
        testCase.setId(new Long(43L));
        testCase.addInput("42");
        testCase.addOutput("The Ultimate Answer");
        testCase.addExclusion("Question");
        return testCase;
    }
    
    /**
     * Helper Method
     * 
     * @return      A predictable TestCase
     */
    private TestCase createNewTestCaseNoInputs()
    {
        TestCase testCase = new TestCase(new BigDecimal("10.25"));
        testCase.setId(new Long(45L));
        testCase.addOutput("Hello World");
        return testCase;
    }
    
    /**
     * Helper Method
     * 
     * @return      A predictable TestCase
     */
    private TestCase createNewTestCaseNoId()
    {
        TestCase testCase = new TestCase(new BigDecimal("10.18"));
        testCase.addInput("12");
        testCase.addInput("34");
        testCase.addOutput("56");
        return testCase;
    }
    
    private static TestDatabase testDb;
    private TestCaseDAO instance;
}