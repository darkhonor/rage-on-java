/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import com.darkhonor.rage.libs.dataaccess.testdb.TestDatabase;
import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.Instructor;
import com.darkhonor.rage.model.Section;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit 4 tests of the SectionDAO class.
 *
 * @author Alex Ackerman
 */
public class SectionDAOTest
{

    public SectionDAOTest()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        testDb = new TestDatabase();
        testDb.initialize();
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
        testDb.shutdown();
    }

    @Before
    public void setUp()
    {
        instance = new SectionDAO(testDb.getConnection());
    }

    @After
    public void tearDown()
    {
        if (instance != null && instance.isOpen())
        {
            instance.closeConnection();
        }
        testDb.reset();
    }

    /**
     * Test of find method, of class SectionDAO.
     */
    @Test
    public void testFind()
    {
        System.out.println("find");
        Long id = 3L;
        assertTrue(instance.isOpen());
        Section result = instance.find(id);
        assertNotNull(result);
        assertEquals("T5A", result.getName());
        assertEquals("CS364", result.getCourse().getName());
    }

    @Test
    public void testFindNoResult()
    {
        System.out.println("findNoResult");
        Long id = 42L;
        assertTrue(instance.isOpen());
        Section result = instance.find(id);
        assertNull(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindIdLessThanZero()
    {
        System.out.println("findIdLessThanZero");
        Long id = -5L;
        assertTrue(instance.isOpen());
        Section result = instance.find(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testFindIdZero()
    {
        System.out.println("findIdZero");
        Long id = 0L;
        assertTrue(instance.isOpen());
        Section result = instance.find(id);
    }
    
    @Test(expected=NullPointerException.class)
    public void testFindIdNull()
    {
        System.out.println("findIdNull");
        Long id = null;
        assertTrue(instance.isOpen());
        Section result = instance.find(id);
    }

    @Test(expected=IllegalStateException.class)
    public void testFindClosedConnection()
    {
        System.out.println("findClosedConnection");
        Long id = 3L;
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Section result = instance.find(id);
    }
    /**
     * Test of getSectionsForCourseAndInstructor method, of class SectionDAO.
     */
    @Test
    public void testGetSectionsForCourseAndInstructor()
    {
        System.out.println("getSectionsForCourseAndInstructor");
        Course course = new Course("CS110");
        course.setId(1L);
        
        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");
        
        ArrayList<Section> expectedResult = new ArrayList<Section>();
        Section section1 = new Section("M2B");
        section1.setId(2L);
        section1.setInstructor(instructor);
        section1.setCourse(course);
        expectedResult.add(section1);
        
        assertTrue(instance.isOpen());
        List results = instance.getSectionsForCourseAndInstructor(course, instructor);
        System.out.println("- Results Size (expecting 1): " + results.size());
        assertEquals(1, results.size());

        assertEquals(expectedResult.get(0), results.get(0));
    }

    @Test
    public void testGetSectionsForCourseAndInstructorMultSections()
    {
        System.out.println("getSectionsForCourseAndInstructorMultSections");
        Course course = new Course("CS364");
        course.setId(2L);
        
        Instructor instructor = new Instructor("David", "Roberts",
                "David.Roberts");
        instructor.setId(5L);
        instructor.setDomainAccount("David.Roberts");
        
        ArrayList<Section> expectedResults = new ArrayList<Section>();
        Section section1 = new Section("T5A");
        section1.setId(3L);
        section1.setInstructor(instructor);
        section1.setCourse(course);
        expectedResults.add(section1);

        Section section2 = new Section("T6A");
        section2.setId(4L);
        section2.setInstructor(instructor);
        section2.setCourse(course);
        expectedResults.add(section2);

        assertTrue(instance.isOpen());
        List results = instance.getSectionsForCourseAndInstructor(course, instructor);
        System.out.println("- Results Size (expecting 2): " + results.size());
        assertEquals(2, results.size());

        for (int i = 0; i < results.size(); i++)
        {
            assertEquals(expectedResults.get(i), results.get(i));
        }
    }

    @Test
    public void testGetSectionsForCourseAndInstructorNoResult()
    {
        System.out.println("getSectionsForCourseAndInstructorNoResult");
        Course course = new Course("CS364");
        course.setId(2L);

        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");

        assertTrue(instance.isOpen());
        List results = instance.getSectionsForCourseAndInstructor(course, instructor);
        System.out.println("- Results Size (expecting 0): " + results.size());
        assertEquals(0, results.size());
    }

    @Test(expected=NullPointerException.class)
    public void testGetSectionsForCourseAndInstructorNullCourse()
    {
        System.out.println("getSectionsForCourseAndInstructorNullCourse");
        Course course = null;

        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");
        assertTrue(instance.isOpen());
        List results = instance.getSectionsForCourseAndInstructor(course, instructor);
    }

    @Test(expected=NullPointerException.class)
    public void testGetSectionsForCourseAndInstructorNullInstructor()
    {
        System.out.println("getSectionsForCourseAndInstructorNullInstructor");
        Course course = new Course("CS110");
        course.setId(1L);

        Instructor instructor = null;
        assertTrue(instance.isOpen());
        List results = instance.getSectionsForCourseAndInstructor(course, instructor);
    }

    @Test(expected=IllegalStateException.class)
    public void testGetSectionsForCourseAndInstructorClosedConnection()
    {
        System.out.println("getSectionsForCourseAndInstructorClosedConnection");
        Course course = new Course("CS110");
        course.setId(1L);

        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");

        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        List results = instance.getSectionsForCourseAndInstructor(course, instructor);
    }

    @Test
    public void testGetSectionsForCourse()
    {
        System.out.println("getSectionsForCourse");
        Course course = new Course("CS364");
        course.setId(2L);

        ArrayList<Section> expectedResults = new ArrayList<Section>();
        Section section1 = new Section("T5A");
        section1.setId(3L);
        section1.setCourse(course);
        expectedResults.add(section1);

        Section section2 = new Section("T6A");
        section2.setId(4L);
        section2.setCourse(course);
        expectedResults.add(section2);

        assertTrue(instance.isOpen());
        List<Section> results = instance.getSectionsForCourse(course);
        System.out.println("- Results Size (expecting 2): " + results.size());
        assertEquals(2, results.size());

        for (int i = 0; i < results.size(); i++)
        {
            assertEquals(expectedResults.get(i), results.get(i));
        }
    }

    @Test
    public void testGetSectionsForCourseNotFound()
    {
        System.out.println("getSectionsForCourseNotFound");
        Course course = new Course("CS483");
        course.setId(3L);
        
        assertTrue(instance.isOpen());
        List<Section> results = instance.getSectionsForCourse(course);
        System.out.println("- Results Size (expecting 0): " + results.size());
        assertEquals(0, results.size());
    }

    @Test(expected=NullPointerException.class)
    public void testGetSectionsForCourseNullCourse()
    {
        System.out.println("getSectionsForCourseNullCourse");
        Course course = null;
        assertTrue(instance.isOpen());
        List<Section> results = instance.getSectionsForCourse(course);
    }

    @Test(expected=IllegalStateException.class)
    public void testGetSectionsForCourseClosedConnection()
    {
        System.out.println("getSectionsForCourseClosedConnection");
        Course course = new Course("CS364");
        course.setId(2L);
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        List<Section> results = instance.getSectionsForCourse(course);
    }
    
    /**
     * Test of closeConnection method, of class SectionDAO.
     */
    @Test
    public void testCloseConnection()
    {
        System.out.println("closeConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }

    @Test(expected=IllegalStateException.class)
    public void testCloseConnectionAlreadyClosed()
    {
        System.out.println("closeConnectionAlreadyClosed");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.closeConnection();
    }
    
    @Test
    public void testCreate()
    {
        System.out.println("create");
        Course course = new Course("CS364");
        course.setId(2L);

        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");

        Section section = new Section("T1A");
        section.setCourse(course);
        section.setId(5L);
        section.setInstructor(instructor);

        assertTrue(instance.isOpen());
        Long newId = instance.create(section);

        Long expectedId = new Long(5L);
        assertEquals(expectedId, newId);

        section = instance.find(5L);
        assertNotNull(section);
        assertEquals("T1A", section.getName());
        assertNotNull(section.getCourse());
        assertEquals("CS364", section.getCourse().getName());
    }

    @Test(expected=NullPointerException.class)
    public void testCreateNullSection()
    {
        System.out.println("createNullSection");
        Section section = null;
        
        assertTrue(instance.isOpen());
        Long newId = instance.create(section);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateExistingSection()
    {
        System.out.println("createExistingSection");
        Course course = new Course("CS364");
        course.setId(2L);

        Instructor instructor = new Instructor("David", "Roberts",
                "David.Roberts");
        instructor.setId(5L);
        instructor.setDomainAccount("David.Roberts");

        Section section = new Section("T5A");
        section.setCourse(course);
        section.setId(3L);
        section.setInstructor(instructor);

        assertTrue(instance.isOpen());
        Long newId = instance.create(section);
    }

    @Test(expected=NullPointerException.class)
    public void testCreateNullCourse()
    {
        System.out.println("createNullCourse");
        Course course = null;
        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");

        Section section = new Section("T1A");
        //System.out.println("- Setting Course");
        section.setId(5L);
        section.setInstructor(instructor);

        System.out.println("- Saving to Database");
        assertTrue(instance.isOpen());
        Long newId = instance.create(section);
    }

    @Test(expected=NullPointerException.class)
    public void testCreateNullInstructor()
    {
        System.out.println("createNullInstructor");
        Course course = new Course("CS364");
        course.setId(2L);

        Instructor instructor = null;

        Section section = new Section("T1A");
        section.setCourse(course);
        section.setId(5L);
        //System.out.println("- Setting Instructor");
        //section.setInstructor(instructor);

        System.out.println("- Saving to Database");
        assertTrue(instance.isOpen());
        Long newId = instance.create(section);
    }

    @Test(expected=NullPointerException.class)
    public void testCreateNullName()
    {
        System.out.println("createNullName");
        Course course = new Course("CS364");
        course.setId(2L);

        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");

        Section section = new Section();
        section.setCourse(course);
        section.setId(5L);
        section.setInstructor(instructor);

        assertTrue(instance.isOpen());
        Long newId = instance.create(section);
    }

    @Test(expected=IllegalStateException.class)
    public void testCreateClosedConnection()
    {
        System.out.println("createClosedConnection");
        Course course = new Course("CS364");
        course.setId(2L);

        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");

        Section section = new Section("T1A");
        section.setCourse(course);
        section.setId(5L);
        section.setInstructor(instructor);

        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Long newId = instance.create(section);
    }

    @Test
    public void testUpdate()
    {
        System.out.println("update");
        assertTrue(instance.isOpen());
        Section section = instance.find(4L);
        assertNotNull(section);
        assertEquals("T6A", section.getName());
        assertEquals(3, section.getStudents().size());
        section.setName("T6H");
        section = instance.update(section);
        assertNotNull(section);
        assertTrue(instance.isOpen());
        Section expectedResult = instance.find(4L);
        assertNotNull(expectedResult);
        assertEquals("T6H", expectedResult.getName());
        assertEquals(expectedResult.getName(), section.getName());
        assertEquals(3, expectedResult.getStudents().size());
        assertEquals(expectedResult.getStudents().size(), section.getStudents().size());
    }

    @Test(expected=NullPointerException.class)
    public void testUpdateNullSection()
    {
        System.out.println("updateNullSection");
        Section section = null;
        assertTrue(instance.isOpen());
        section = instance.update(section);
    }

    @Test(expected=NullPointerException.class)
    public void testUpdateNullCourse()
    {
        System.out.println("updateNullCourse");

        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");

        Section section = new Section("T6A");
        section.setId(4L);
        section.setInstructor(instructor);

        assertTrue(instance.isOpen());
        section = instance.update(section);
    }

    @Test(expected=NullPointerException.class)
    public void testUpdateNullInstructor()
    {
        System.out.println("updateNullInstructor");
        Course course = new Course("CS364");
        course.setId(2L);

        Section section = new Section("T6A");
        section.setId(4L);
        section.setCourse(course);

        assertTrue(instance.isOpen());
        section = instance.update(section);
    }

    @Test(expected=NullPointerException.class)
    public void testUpdateNullName()
    {
        System.out.println("updateNullName");
        Course course = new Course("CS364");
        course.setId(2L);

        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");

        Section section = new Section();
        section.setId(4L);
        section.setCourse(course);
        section.setInstructor(instructor);

        assertTrue(instance.isOpen());
        section = instance.update(section);
    }

    @Test(expected=IllegalStateException.class)
    public void testUpdateClosedConnection()
    {
        System.out.println("updateClosedConnection");
        assertTrue(instance.isOpen());
        Section section = instance.find(4L);
        assertNotNull(section);
        assertEquals("T6A", section.getName());
        assertEquals(3, section.getStudents().size());
        section.setName("T6H");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        section = instance.update(section);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testUpdateEntityNotExist()
    {
        System.out.println("updateEntityNotExist");
        Course course = new Course("CS364");
        course.setId(2L);

        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");

        Section section = new Section("T1A");
        section.setCourse(course);
        section.setId(5L);
        section.setInstructor(instructor);

        assertTrue(instance.isOpen());
        section = instance.update(section);
    }

    @Test
    public void testDelete()
    {
        System.out.println("delete");
        assertTrue(instance.isOpen());
        Section section = instance.find(4L);
        assertNotNull(section);
        assertEquals("T6A", section.getName());
        assertEquals(3, section.getStudents().size());
        instance.delete(section);
        assertTrue(instance.isOpen());
        section = instance.find(4L);
        assertNull(section);
    }

    @Test
    public void testDeleteId()
    {
        System.out.println("deleteId");
        assertTrue(instance.isOpen());
        Section section = instance.find(4L);
        assertNotNull(section);
        assertEquals("T6A", section.getName());
        assertEquals(3, section.getStudents().size());
        instance.delete(4L);
        section = instance.find(4L);
        assertNull(section);
    }

    @Test(expected=NullPointerException.class)
    public void testDeleteNullSection()
    {
        System.out.println("deleteNullSection");
        Section section = null;
        assertTrue(instance.isOpen());
        instance.delete(section);
    }

    @Test(expected=NullPointerException.class)
    public void testDeleteNullId()
    {
        System.out.println("deleteNullId");
        Long id = null;
        assertTrue(instance.isOpen());
        instance.delete(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteZeroId()
    {
        System.out.println("deleteZeroId");
        Long id = 0L;
        assertTrue(instance.isOpen());
        instance.delete(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteNegativeId()
    {
        System.out.println("deleteNegativeId");
        Long id = -4L;
        assertTrue(instance.isOpen());
        instance.delete(id);
    }

    @Test(expected=NullPointerException.class)
    public void testDeleteNullCourse()
    {
        System.out.println("deleteNullCourse");

        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");

        Section section = new Section("T6A");
        section.setId(4L);
        section.setInstructor(instructor);

        assertTrue(instance.isOpen());
        instance.delete(section);
    }

    @Test(expected=NullPointerException.class)
    public void testDeleteNullName()
    {
        System.out.println("deleteNullName");
        Course course = new Course("CS364");
        course.setId(2L);

        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");

        Section section = new Section();
        section.setId(4L);
        section.setCourse(course);
        section.setInstructor(instructor);

        assertTrue(instance.isOpen());
        instance.delete(section);
    }

    @Test(expected=IllegalStateException.class)
    public void testDeleteClosedConnection()
    {
        System.out.println("deleteClosedConnection");
        assertTrue(instance.isOpen());
        Section section = instance.find(4L);
        assertNotNull(section);
        assertEquals("T6A", section.getName());
        assertEquals(3, section.getStudents().size());
        System.out.println("- Section: " + section);
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.delete(section);
    }

    @Test(expected=IllegalStateException.class)
    public void testDeleteIdClosedConnection()
    {
        System.out.println("deleteIdClosedConnection");
        assertTrue(instance.isOpen());
        Section section = instance.find(4L);
        assertNotNull(section);
        assertEquals("T6A", section.getName());
        assertEquals(3, section.getStudents().size());
        System.out.println("- Section: " + section);
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.delete(4L);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteEntityNotExist()
    {
        System.out.println("deleteEntityNotExist");
        Course course = new Course("CS364");
        course.setId(2L);

        Instructor instructor = new Instructor("Sarah", "O'Reilly",
                "Sarah.OReilly");
        instructor.setId(6L);
        instructor.setDomainAccount("Sarah.OReilly");

        Section section = new Section("T1A");
        section.setCourse(course);
        section.setId(5L);
        section.setInstructor(instructor);
        assertTrue(instance.isOpen());
        instance.delete(section);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteIdEntityNotExist()
    {
        System.out.println("deleteIdEntityNotExist");
        assertTrue(instance.isOpen());
        instance.delete(5L);
    }

    @Test
    public void testIsOpen()
    {
        System.out.println("isOpen");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }
    
    private static TestDatabase testDb;
    private SectionDAO instance;
}
