/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import com.darkhonor.rage.libs.dataaccess.testdb.TestDatabase;
import com.darkhonor.rage.model.Category;
import com.darkhonor.rage.model.Question;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 * JUnit Test Cases for the QuestionDAO class.
 * 
 * @author Alex Ackerman
 */
public class QuestionDAOTest
{
    
    public QuestionDAOTest()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        testDb = new TestDatabase();
        testDb.initialize();
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
        testDb.shutdown();
    }
    
    @Before
    public void setUp()
    {
        instance = new QuestionDAO(testDb.getConnection());
    }
    
    @After
    public void tearDown()
    {
        if (instance != null && instance.isOpen())
        {
            instance.closeConnection();
        }
        testDb.reset();
    }

    /**
     * Test of closeConnection method, of class QuestionDAO.
     */
    @Test
    public void testCloseConnection()
    {
        System.out.println("closeConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }

    @Test(expected=IllegalStateException.class)
    public void testCloseConnectionAlreadyClosed()
    {
        System.out.println("closeConnectionAlreadyClosed");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.closeConnection();
    }

    /**
     * Test of isOpen method, of class QuestionDAO.
     */
    @Test
    public void testIsOpen()
    {
        System.out.println("isOpen");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }

    /**
     * Test of create method, of class QuestionDAO.
     */
    @Test
    @Ignore
    public void testCreate()
    {
        System.out.println("create");
        Question question = null;
        Long expResult = null;
        Long result = instance.create(question);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of find method, of class QuestionDAO.
     */
    @Test
    @Ignore
    public void testFind_Long()
    {
        System.out.println("find by Id");
        Long id = 2L;
        Question result = instance.find(id);
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testFindNoResult()
    {
        System.out.println("find - No result");
        Long id = new Long(2033L);

        assertTrue(instance.isOpen());
        Question result = instance.find(id);
        assertNull(result);
        assertTrue(instance.isOpen());
    }
    
    @Test(expected = IllegalStateException.class)
    public void testFindIdClosedConnection()
    {
        System.out.println("find - Closed Connection");
        Long id = new Long(2L);

        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Question result = instance.find(id);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFindIdEqualsZero()
    {
        System.out.println("find - id = 0");
        Long id = new Long(0L);
        assertTrue(instance.isOpen());
        Question result = instance.find(id);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFindIdLessThanZero()
    {
        System.out.println("find - id < 0");
        Long id = new Long(-2L);
        assertTrue(instance.isOpen());
        Question result = instance.find(id);
    }
    
    @Test(expected = NullPointerException.class)
    public void testFindIdEqualsNull()
    {
        System.out.println("find - id = null");
        Long id = null;
        assertTrue(instance.isOpen());
        Question result = instance.find(id);
    }
    
    /**
     * Test of find method, of class QuestionDAO.
     */
    @Test
    @Ignore
    public void testFind_String()
    {
        System.out.println("find");
        String name = "";
        Question expResult = null;
        Question result = instance.find(name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of update method, of class QuestionDAO.
     */
    @Test
    @Ignore
    public void testUpdate()
    {
        System.out.println("update");
        Question question = null;
        Question expResult = null;
        Question result = instance.update(question);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of delete method, of class QuestionDAO.
     */
    @Test
    @Ignore
    public void testDelete_Long()
    {
        System.out.println("delete");
        Long id = null;
        instance.delete(id);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of delete method, of class QuestionDAO.
     */
    @Test
    @Ignore
    public void testDelete_Question()
    {
        System.out.println("delete");
        Question question = null;
        instance.delete(question);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAllQuestionsByName method, of class QuestionDAO.
     */
    @Test
    @Ignore
    public void testGetAllQuestionsByName()
    {
        System.out.println("getAllQuestionsByName");
        List expResult = null;
        List result = instance.getAllQuestionsByName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAllQuestionsNotInIdList method, of class QuestionDAO.
     */
    @Test
    @Ignore
    public void testGetAllQuestionsNotInIdList()
    {
        System.out.println("getAllQuestionsNotInIdList");
        List<Long> idList = null;
        List expResult = null;
        List result = instance.getAllQuestionsNotInIdList(idList);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAllQuestionsInCategory method, of class QuestionDAO.
     */
    @Test
    @Ignore
    public void testGetAllQuestionsInCategory()
    {
        System.out.println("getAllQuestionsInCategory");
        Category category = null;
        List expResult = null;
        List result = instance.getAllQuestionsInCategory(category);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAllVerbatimQuestions method, of class QuestionDAO.
     */
    @Test
    @Ignore
    public void testGetAllVerbatimQuestions()
    {
        System.out.println("getAllVerbatimQuestions");
        boolean verbatim = false;
        List expResult = null;
        List result = instance.getAllVerbatimQuestions(verbatim);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNumberOfQuestions method, of class QuestionDAO.
     */
    @Test
    @Ignore
    public void testGetNumberOfQuestions()
    {
        System.out.println("getNumberOfQuestions");
        int expResult = 0;
        int result = instance.getNumberOfQuestions();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    private static TestDatabase testDb;
    private QuestionDAO instance;
    
}
