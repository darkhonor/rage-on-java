/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit 4 test harness for the DirFileFilter class.  This test harness requires
 * 3 files in the test/testFiles directory to run.
 *
 * @author Alexander Ackerman <alex@darkhonor.com>
 */
public class DirFileFilterTest {

    /**
     * Constructor
     */
    public DirFileFilterTest()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        /**
         * Verify the required test files are present before moving on.  If not,
         * fail the test.
         */
        File testDirectory = new File("testFiles");
        if (testDirectory.exists())
        {
            String fs[] = testDirectory.list();
            ArrayList<String> files = new ArrayList<String>(Arrays.asList(fs));
            if (!files.contains("rocky.aes"))
            {
                throw new Exception("Required test files not found.");
            }
        }
        else
        {
            throw new Exception("ERROR: Directory of test files is missing");
        }
    }

    @Before
    public void setUp()
    {
        instance = new DirFileFilter();
    }

    /**
     * Test of accept method, of class AesFileFilter.
     */
    @Test
    public void testAccept()
    {
        System.out.println("accept");
        File pathname = new File("testFiles");
        assertTrue(instance.accept(pathname));

    }

    /**
     * Test of accept method: wrong extension
     */
    @Test
    public void testAcceptFile()
    {
        System.out.println("acceptFile");
        File pathname = new File("testFiles" + fsep + "rocky.aes");
        assertFalse(instance.accept(pathname));
    }

    /**
     * Test of accept method: no extension
     */
    @Test
    public void testAcceptNoExtension()
    {
        System.out.println("acceptNoExtension");
        File pathname = new File("testFiles" + fsep + "rockyzip");
        assertFalse(instance.accept(pathname));
    }

    private DirFileFilter instance;
    private static final String fsep = System.getProperty("file.separator");
}