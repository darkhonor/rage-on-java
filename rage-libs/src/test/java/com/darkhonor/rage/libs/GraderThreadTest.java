/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs;

import com.darkhonor.rage.model.StudentReport;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alexander.Ackerman
 */
public class GraderThreadTest {

    public GraderThreadTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class GraderThread.
     */
    @Ignore("Not ready yet")
    @Test
    public void testRun() {
        System.out.println("run");
        GraderThread instance = null;
        instance.run();
        // TODO Develop JUnit test for GraderThread.run() method
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStudentReport method, of class GraderThread.
     */
    @Ignore("Not ready yet")
    @Test
    public void testGetStudentReport() {
        System.out.println("getStudentReport");
        //GraderThread instance = null;
        //StudentReport expResult = null;
        //StudentReport result = instance.getStudentReport();
        //assertEquals(expResult, result);
        // TODO Develop JUnit test for GraderThread.getStudentReport() method
        fail("The test case is a prototype.");
    }

}