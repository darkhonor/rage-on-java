/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import com.darkhonor.rage.libs.dataaccess.testdb.TestDatabase;
import com.darkhonor.rage.model.Version;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alex Ackerman
 */
public class VersionDAOTest
{

    public VersionDAOTest()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        testDb = new TestDatabase();
        testDb.initialize();
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
        testDb.shutdown();
    }

    @Before
    public void setUp()
    {
        instance = new VersionDAO(testDb.getConnection());
    }

    @After
    public void tearDown()
    {
        if (instance != null && instance.isOpen())
        {
            instance.closeConnection();
        }
        testDb.reset();
    }

    /**
     * Test of checkDbVersion method, of class VersionDAO.
     */
    @Test
    public void testCheckDbVersion()
    {
        System.out.println("checkDbVersion");
        assertTrue(instance.isOpen());
        Version appVersion = new Version(new Double(2.1));
        boolean expResult = true;
        boolean result = instance.checkDbVersion(appVersion);
        assertEquals(expResult, result);
        assertTrue(instance.isOpen());
    }

    @Test
    public void testCheckDbVersionNoMatch()
    {
        System.out.println("checkDbVersionNoMatch");
        Version appVersion = new Version(new Double(1.2));
        boolean expResult = false;
        boolean result = instance.checkDbVersion(appVersion);
        assertEquals(expResult, result);
        assertTrue(instance.isOpen());
    }

    @Test(expected=NullPointerException.class)
    public void testCheckDbVersionNullVersion()
    {
        System.out.println("checkDbVersionNullVersion");
        assertTrue(instance.isOpen());
        Version appVersion = null;
        boolean result = instance.checkDbVersion(appVersion);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCheckDbVersionVersionIdNull()
    {
        System.out.println("checkDbVersionVersionIdNull");
        assertTrue(instance.isOpen());
        Version appVersion = new Version();
        boolean result = instance.checkDbVersion(appVersion);
    }

    @Test(expected=IllegalStateException.class)
    public void testCheckDbVersionClosedConnection()
    {
        System.out.println("checkDbVersionClosedConnection");
        assertTrue(instance.isOpen());
        Version appVersion = new Version(new Double(2.1));
        instance.closeConnection();
        assertFalse(instance.isOpen());
        boolean result = instance.checkDbVersion(appVersion);
    }

    /**
     * Test of closeConnection method, of class VersionDAO.
     */
    @Test
    public void testCloseConnection()
    {
        System.out.println("closeConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }

    @Test(expected=IllegalStateException.class)
    public void testCloseConnectionAlreadyClosed()
    {
        System.out.println("closeConnectionAlreadyClosed");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.closeConnection();
    }

    /**
     * Test of isOpen method, of class VersionDAO.
     */
    @Test
    public void testIsOpen()
    {
        System.out.println("isOpen");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }

    /**
     * Test of create method, of class VersionDAO.
     */
    @Test
    public void testCreate()
    {
        System.out.println("create");
        assertTrue(instance.isOpen());
        Double versionNumber = new Double(10.4);
        Version version = new Version(versionNumber);
        Double newId = instance.create(version);
        Double expectedId = new Double(10.4);
        assertEquals(expectedId, newId);
        Version result = instance.find(versionNumber);
        assertNotNull(result);
        assertEquals(version, result);
        assertTrue(instance.isOpen());
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateExistingVersion()
    {
        System.out.println("createExistingVersion");
        assertTrue(instance.isOpen());
        Version version = new Version(new Double(2.1));
        Double newId = instance.create(version);
    }

    @Test(expected=NullPointerException.class)
    public void testCreateNullVersion()
    {
        System.out.println("createNullVersion");
        assertTrue(instance.isOpen());
        Version version = null;
        Double newId = instance.create(version);
    }

    @Test(expected=IllegalStateException.class)
    public void testCreateClosedConnection()
    {
        System.out.println("createClosedConnection");
        assertTrue(instance.isOpen());
        Version version = new Version(new Double(10.4));
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Double newId = instance.create(version);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateNullVersionId()
    {
        System.out.println("createNullVersionId");
        assertTrue(instance.isOpen());
        Version version = new Version();
        Double newId = instance.create(version);
    }
    
    /**
     * Test of find method, of class VersionDAO.
     */
    @Test
    public void testFind()
    {
        System.out.println("find");
        assertTrue(instance.isOpen());
        Double versionNumber = new Double(2.1);
        Version expResult = new Version(versionNumber);
        Version result = instance.find(versionNumber);
        assertNotNull(result);
        assertEquals(expResult, result);
        assertTrue(instance.isOpen());
    }

    @Test
    public void testFindNoResult()
    {
        System.out.println("findNoResult");
        assertTrue(instance.isOpen());
        Double versionNumber = new Double(1.2);
        Version expResult = null;
        Version result = instance.find(versionNumber);
        assertNull(result);
        assertEquals(expResult, result);
        assertTrue(instance.isOpen());
    }

    @Test
    public void testFindIdNoResult()
    {
        System.out.println("findIdNoResult");
        assertTrue(instance.isOpen());
        Double id = new Double(1.2);
        Version expResult = null;
        Version result = instance.find(id);
        assertNull(result);
        assertEquals(expResult, result);
        assertTrue(instance.isOpen());
    }

   @Test(expected=IllegalArgumentException.class)
    public void testFindIdNegativeId()
    {
        System.out.println("findIdNegativeId");
        assertTrue(instance.isOpen());
        Double id = new Double(-1.2);
        Version result = instance.find(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testFindIdZeroId()
    {
        System.out.println("findIdZeroId");
        assertTrue(instance.isOpen());
        Double id = new Double(0.0);
        Version result = instance.find(id);
    }
    
    @Test(expected=NullPointerException.class)
    public void testFindNullVersion()
    {
        System.out.println("findNullVersion");
        assertTrue(instance.isOpen());
        Double versionNumber = null;
        Version result = instance.find(versionNumber);
    }

    @Test(expected=IllegalStateException.class)
    public void testFindClosedConnection()
    {
        System.out.println("findClosedConnection");
        assertTrue(instance.isOpen());
        Double versionNumber = new Double(1.2);
        Version version = new Version(versionNumber);
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Version result = instance.find(versionNumber);
    }

    /**
     * Test of delete method, of class VersionDAO.
     */
    @Test
    public void testDelete()
    {
        System.out.println("delete");
        assertTrue(instance.isOpen());
        Version version = instance.find(new Double(2.1));
        assertNotNull(version);
        instance.delete(version);
        assertTrue(instance.isOpen());
        Version result = instance.find(new Double(2.1));
        assertNull(result);
    }

    @Test(expected=IllegalStateException.class)
    public void testDeleteClosedConnection()
    {
        System.out.println("deleteClosedConnection");
        assertTrue(instance.isOpen());
        Version version = instance.find(new Double(2.1));
        assertNotNull(version);
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.delete(version);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteNullVersionId()
    {
        System.out.println("deleteNullVersionId");
        assertTrue(instance.isOpen());
        Version version = new Version();
        instance.delete(version);
    }

    @Test(expected=NullPointerException.class)
    public void testDeleteNullVersion()
    {
        System.out.println("deleteNullVersion");
        assertTrue(instance.isOpen());
        Version version = null;
        instance.delete(version);
    }

    @Test
    public void testDeleteVersionNumber()
    {
        System.out.println("deleteVersionNumber");
        assertTrue(instance.isOpen());
        Double versionNumber = new Double(2.1);
        assertNotNull(versionNumber);
        instance.delete(versionNumber);
        assertTrue(instance.isOpen());
        Version result = instance.find(new Double(2.1));
        assertNull(result);
    }

    @Test(expected=IllegalStateException.class)
    public void testDeleteVersionNumberClosedConnection()
    {
        System.out.println("deleteClosedConnection");
        assertTrue(instance.isOpen());
        Double versionNumber = new Double(2.1);
        assertNotNull(versionNumber);
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.delete(versionNumber);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testNegativeVersionNumber()
    {
        System.out.println("deleteNullVersionId");
        assertTrue(instance.isOpen());
        Double versionNumber = new Double(-1.1);
        instance.delete(versionNumber);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testZeroVersionNumber()
    {
        System.out.println("deleteZeroVersionNumber");
        assertTrue(instance.isOpen());
        Double versionNumber = new Double(0.0);
        instance.delete(versionNumber);
    }
    
    @Test(expected=NullPointerException.class)
    public void testDeleteVersionNumberNullVersion()
    {
        System.out.println("deleteNullVersionNumber");
        assertTrue(instance.isOpen());
        Double versionNumber = null;
        instance.delete(versionNumber);
    }

    private static TestDatabase testDb;
    private VersionDAO instance;
}
