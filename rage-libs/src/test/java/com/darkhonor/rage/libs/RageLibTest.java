/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs;

import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.GradedEvent;
import com.darkhonor.rage.model.Instructor;
import com.darkhonor.rage.model.Response;
import com.darkhonor.rage.model.Section;
import com.darkhonor.rage.model.Student;
import com.darkhonor.rage.model.TestCase;
import java.io.File;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alexander.Ackerman
 */
public class RageLibTest {

    public RageLibTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of gradeTestCase method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testGradeTestCase() {
        System.out.println("gradeTestCase");
        TestCase tc = null;
        Response response = null;
        boolean verbatim = false;
        boolean exclusions = false;
        boolean expResult = false;
        boolean result = RageLib.gradeTestCase(tc, response, verbatim, exclusions);
        assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.gradeTestCase() method
        fail("The test case is a prototype.");
    }

    /**
     * Test of getQuestionNameFromFilename method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testGetQuestionNameFromFilename() {
        System.out.println("getQuestionNameFromFilename");
        String fileName = "";
        String expResult = "";
        String result = RageLib.getQuestionNameFromFilename(fileName);
        assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.getQuestionNameFromFilename() method
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRapFiles method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testGetRapFiles() {
        System.out.println("getRapFiles");
        //File path = null;
        //File[] expResult = null;
        //File[] result = RageLib.getRapFiles(path);
        //assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.getRapFiles() method
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSubDirs method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testGetSubDirs() {
        System.out.println("getSubDirs");
        String path = "";
        //File[] expResult = null;
        //File[] result = RageLib.getSubDirs(path);
        //assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.getSubDirs() method
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDirectSubDirs method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testGetDirectSubDirs() {
        System.out.println("getDirectSubDirs");
        String path = "";
        //File[] expResult = null;
        //File[] result = RageLib.getDirectSubDirs(path);
        //assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.getDirectSubDirs() method
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSingleAssignmentDirFromPath method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testGetSingleAssignmentDirFromPath() {
        System.out.println("getSingleAssignmentDirFromPath");
        String path = "";
        String expResult = "";
        String result = RageLib.getSingleAssignmentDirFromPath(path);
        assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.getSingleAssignmentDirFromPath() method
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCoursesFromPath method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testGetCoursesFromPath() {
        System.out.println("getCoursesFromPath");
        String path = "";
        Set<Course> expResult = null;
        Set<Course> result = RageLib.getCoursesFromPath(path);
        assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.getCoursesFromPath() method
        fail("The test case is a prototype.");
    }

    /**
     * Test of getInstructorsFromPath method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testGetInstructorsFromPath() {
        System.out.println("getInstructorsFromPath");
        String path = "";
        Set<Instructor> expResult = null;
        Set<Instructor> result = RageLib.getInstructorsFromPath(path);
        assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.getInstructorsFromPath() method
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSectionsFromPath method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testGetSectionsFromPath() {
        System.out.println("getSectionsFromPath");
        String path = "";
        List<Section> expResult = null;
        List<Section> result = RageLib.getSectionsFromPath(path);
        assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.getSectionsFromPath() method
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStudentsFromPath method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testGetStudentsFromPath() {
        System.out.println("getStudentsFromPath");
        String path = "";
        Integer yearOffset = null;
        Set<Student> expResult = null;
        Set<Student> result = RageLib.getStudentsFromPath(path, yearOffset);
        assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.getStudentsFromPath() method
        fail("The test case is a prototype.");
    }

    /**
     * Test of loadGradedEventData method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testLoadGradedEventData_File_EntityManager() {
        System.out.println("loadGradedEventData");
        File inputFile = null;
        EntityManager em = null;
        boolean expResult = false;
        boolean result = RageLib.loadGradedEventData(inputFile, em);
        assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.loadGradedEventData(File) method
        fail("The test case is a prototype.");
    }

    /**
     * Test of loadGradedEventData method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testLoadGradedEventData_String_EntityManager() {
        System.out.println("loadGradedEventData");
        String inputFileName = "";
        EntityManager em = null;
        boolean expResult = false;
        boolean result = RageLib.loadGradedEventData(inputFileName, em);
        assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.loadGradedEventData(String) method
        fail("The test case is a prototype.");
    }

    /**
     * Test of getGradedEventFromSectionName method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testGetGradedEventFromSectionName() {
        System.out.println("getGradedEventFromSectionName");
        List<GradedEvent> glist = null;
        String section = "";
        GradedEvent expResult = null;
        GradedEvent result = RageLib.getGradedEventFromSectionName(glist, section);
        assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.getGradedEventFromSectionName() method
        fail("The test case is a prototype.");
    }

    /**
     * Test of validateGradedEventXML method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testValidateGradedEventXML() {
        System.out.println("validateGradedEventXML");
        File inputFile = null;
        boolean expResult = false;
        boolean result = RageLib.validateGradedEventXML(inputFile);
        assertEquals(expResult, result);
        // TODO Develop JUnit test for RageLib.validateGradedEventXML() method
        fail("The test case is a prototype.");
    }

    /**
     * Test of unZip method, of class RageLib.
     */
    @Ignore("Not ready yet")
    @Test
    public void testUnZip() {
        System.out.println("unZip");
        String path = "";
        String zipFileName = "";
        boolean ignoreFoldersInZip = false;
        RageLib.unZip(path, zipFileName, ignoreFoldersInZip);
        // TODO Develop JUnit test for RageLib.testUnZip() method
        fail("The test case is a prototype.");
    }

}