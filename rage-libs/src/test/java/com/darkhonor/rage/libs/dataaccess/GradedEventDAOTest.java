/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import javax.persistence.EntityManager;
import com.darkhonor.rage.libs.dataaccess.testdb.TestDatabase;
import com.darkhonor.rage.model.Category;
import com.darkhonor.rage.model.Question;
import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.GradedEvent;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit 4 tests of the GradedEventDAO class.
 * 
 * @author Alex Ackerman
 */
public class GradedEventDAOTest
{

    public GradedEventDAOTest()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        testDb = new TestDatabase();
        testDb.initialize();
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
        testDb.shutdown();
    }

    @Before
    public void setUp()
    {
        instance = new GradedEventDAO(testDb.getConnection());
    }

    @After
    public void tearDown()
    {
        /**
         * Close the EntityManager associated with the CourseDAO instance.
         */
        if (instance.isOpen())
        {
            instance.closeConnection();
        }
        testDb.reset();
    }

    /**
     * Test of find method, of class GradedEventDAO.
     */
    @Test
    public void testFind()
    {
        System.out.println("find");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);
        Long id = new Long(3L);
        GradedEvent expResult = new GradedEvent(course, "PEX2", "M2");
        expResult.setTerm("Summer 2010");
        expResult.setId(new Long(3L));
        GradedEvent result = instance.find(id);
        assertNotNull(result);
        assertEquals("Summer 2010", result.getTerm());
        assertEquals("PEX2", result.getAssignment());
        assertEquals("M2", result.getVersion());
        assertEquals(course, result.getCourse());
        assertEquals(expResult, result);
        courseDAO.closeConnection();
    }

    @Test
    public void testFindNoResult()
    {
        System.out.println("findIdNoResult");
        assertTrue(instance.isOpen());
        Long resultId = new Long(12L);
        GradedEvent result = instance.find(resultId);
        assertNull(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindIdLessThanZero()
    {
        System.out.println("findIdLessThanZero");
        assertTrue(instance.isOpen());
        Long resultId = new Long(-3L);
        GradedEvent result = instance.find(resultId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindZeroId()
    {
        System.out.println("findZeroId");
        assertTrue(instance.isOpen());
        Long resultId = new Long(0L);
        GradedEvent result = instance.find(resultId);
    }

    @Test(expected = NullPointerException.class)
    public void testFindNullId()
    {
        System.out.println("findNullId");
        assertTrue(instance.isOpen());
        Long nullId = null;
        GradedEvent result = instance.find(nullId);
    }

    @Test(expected = IllegalStateException.class)
    public void testFindClosedConnection()
    {
        System.out.println("findClosedConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Long resultId = new Long(2L);
        GradedEvent result = instance.find(resultId);
    }
    
    @Test
    public void testFindExpanded()
    {
        System.out.println("findExpanded");
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");

        assertTrue(instance.isOpen());
        GradedEvent result = instance.find(course, "PEX2", "Summer 2010", "M1");
        assertNotNull(result);
        Long expectedId = new Long(2L);
        assertEquals(expectedId, result.getId());
        assertEquals(course, result.getCourse());
        assertEquals("PEX2", result.getAssignment());
        assertEquals("Summer 2010", result.getTerm());
        assertEquals("M1", result.getVersion());
        assertTrue(result.getPartialCredit());
        assertEquals(1, result.getQuestions().size());
    }

    @Test
    public void testFindExpandedNoResult()
    {
        System.out.println("findExpanded");
        Course course = new Course();
        course.setId(new Long(100L));
        course.setName("CS192");

        assertTrue(instance.isOpen());
        GradedEvent result = instance.find(course, "TowersOfHanoi", 
                "Summer 2012", "ALL");
        assertNull(result);
    }
    
    @Test(expected = NullPointerException.class)
    public void testFindExpandedNullCourse()
    {
        System.out.println("findExpanded - Null Course");
        Course course = null;
        
        assertTrue(instance.isOpen());
        GradedEvent result = instance.find(course, "PEX2", "Summer 2010", "M1");
    }
    
    @Test(expected = NullPointerException.class)
    public void testFindExpandedNullAssignment()
    {
        System.out.println("findExpanded - Null Assignment Name");
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");

        assertTrue(instance.isOpen());
        GradedEvent result = instance.find(course, null, "Summer 2010", "M1");   
    }
    
    @Test(expected = NullPointerException.class)
    public void testFindExpandedNullTerm()
    {
        System.out.println("findExpanded - Null Term");
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");

        assertTrue(instance.isOpen());
        GradedEvent result = instance.find(course, "PEX2", null, "M1");   
    }
    
    @Test(expected = NullPointerException.class)
    public void testFindExpandedNullVersion()
    {
        System.out.println("findExpanded - Null Version");
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");

        assertTrue(instance.isOpen());
        GradedEvent result = instance.find(course, "PEX2", "Summer 2010", null);   
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFindExpandedInvalidCourseNullCourseName()
    {
        System.out.println("findExpanded - Null Course Name");
        Course course = new Course();
        course.setId(new Long(1L));

        assertTrue(instance.isOpen());
        GradedEvent result = instance.find(course, "PEX2", "Summer 2010", "M1");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFindExpandedInvalidName()
    {
        System.out.println("findExpanded - Blank Assignment Name");
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");

        assertTrue(instance.isOpen());
        GradedEvent result = instance.find(course, "", "Summer 2010", "M1");   
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFindExpandedInvalidTerm()
    {
        System.out.println("findExpanded - Blank Term");
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");

        assertTrue(instance.isOpen());
        GradedEvent result = instance.find(course, "PEX2", "", "M1");   
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFindExpandedInvalidVersion()
    {
        System.out.println("findExpanded - Blank Version");
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");

        assertTrue(instance.isOpen());
        GradedEvent result = instance.find(course, "PEX2", "Summer 2010", "");   
    }
    
    @Test(expected = IllegalStateException.class)
    public void testFindExpandedClosedConnection()
    {
        System.out.println("findExpanded - Closed Connection");
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");

        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        GradedEvent result = instance.find(course, "PEX2", "Summer 2010", "M1");
    }
    
    @Test
    public void testGetGradedEventsForCourse()
    {
        System.out.println("getGradedEventsForCourse");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");
        List<GradedEvent> result = instance.getGradedEventsForCourse(course);
        assertNotNull(result);
        assertEquals(3, result.size());
        assertEquals("PEX1", result.get(0).getAssignment());
        assertEquals("PEX2", result.get(1).getAssignment());
        assertEquals("PEX2", result.get(2).getAssignment());
        assertEquals("M1", result.get(1).getVersion());
        assertEquals("M2", result.get(2).getVersion());
        assertTrue(instance.isOpen());
    }

    @Test
    public void testGetGradedEventsForCourseNotFound()
    {
        System.out.println("getGradedEventsForCourse - Course Not Found");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(51L));
        course.setName("CS192P");
        List<GradedEvent> result = instance.getGradedEventsForCourse(course);
        assertNotNull(result);
        assertEquals(0, result.size());
        assertTrue(instance.isOpen());
    }

    @Test(expected=IllegalStateException.class)
    public void testGetGradedEventsForCourseClosedConnection()
    {
        System.out.println("getGradedEventsForCourseClosedConnection");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");

        instance.closeConnection();
        assertFalse(instance.isOpen());

        List<GradedEvent> resultList =
                instance.getGradedEventsForCourse(course);
    }

    @Test(expected=NullPointerException.class)
    public void testGetGradedEventsForCourseNullCourse()
    {
        System.out.println("getGradedEventsForCourseNullCourse");
        assertTrue(instance.isOpen());
        Course course = null;
        List<GradedEvent> result = instance.getGradedEventsForCourse(course);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testGetGradedEventsForCourseNullCourseName()
    {
        System.out.println("getGradedEventsForCourseNullCourseName");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(111L));
        List<GradedEvent> result = instance.getGradedEventsForCourse(course);
    }

    @Test
    public void testGetGradedEventsForCourseEmptyCourse()
    {
        System.out.println("getGradedEventsForCourseEmptyCourse");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(240L));
        course.setName("CS987");
        List<GradedEvent> result = instance.getGradedEventsForCourse(course);
        assertNotNull(result);
        assertEquals(0, result.size());
        assertTrue(instance.isOpen());
    }

    /**
     * Test of getGradedEventsForCourseAndTerm method, of class GradedEventDAO.
     */
    @Test
    public void testGetGradedEventsForCourseAndTerm()
    {
        System.out.println("getGradedEventsForCourseAndTerm");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");
        String term = "Summer 2010";
        List<GradedEvent> result = instance.getGradedEventsForCourseAndTerm(course, term);
        assertEquals(3, result.size());
        assertEquals("PEX1", result.get(0).getAssignment());
        assertEquals("PEX2", result.get(1).getAssignment());
        assertEquals("PEX2", result.get(2).getAssignment());
        assertEquals("M1", result.get(1).getVersion());
        assertEquals("M2", result.get(2).getVersion());

        term = "Spring 2010";
        result = instance.getGradedEventsForCourseAndTerm(course, term);
        assertEquals(0, result.size());

        assertTrue(instance.isOpen());

        assertTrue(instance.isOpen());
    }

    @Test
    public void testGetGradedEventsForCourseAndTermNotFound()
    {
        System.out.println("getGradedEventsForCourseAndTermNotFound");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(222L));
        course.setName("CS365");
        String term = "Summer 2010";
        List<GradedEvent> result = instance.getGradedEventsForCourseAndTerm(course, term);
        assertEquals(0, result.size());

        assertTrue(instance.isOpen());
    }
    
    @Test(expected=IllegalStateException.class)
    public void testGetGradedEventsForCourseAndTermClosedConnection()
    {
        System.out.println("getGradedEventsForCourseAndTermClosedConnection");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");

        instance.closeConnection();
        assertFalse(instance.isOpen());

        List<GradedEvent> resultList =
                instance.getGradedEventsForCourseAndTerm(course, "Summer 2010");
    }

    @Test(expected=NullPointerException.class)
    public void testGetGradedEventsForCourseAndTermNullCourse()
    {
        System.out.println("getGradedEventsForCourseAndTermNullCourse");
        assertTrue(instance.isOpen());
        Course course = null;
        String term = "Summer 2010";
        List<GradedEvent> result = instance.getGradedEventsForCourseAndTerm(course, term);
    }

    @Test(expected=NullPointerException.class)
    public void testGetGradedEventsForCourseAndTermNullTerm()
    {
        System.out.println("getGradedEventsForCourseAndTermNullTerm");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");
        String term = null;
        List<GradedEvent> result = instance.getGradedEventsForCourseAndTerm(course, term);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testGetGradedEventsForCourseAndTermBlankTerm()
    {
        System.out.println("getGradedEventsForCourseAndTermBlankTerm");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");
        String term = "";
        List<GradedEvent> result = instance.getGradedEventsForCourseAndTerm(course, term);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testGetGradedEventsForCourseAndTermNullCourseName()
    {
        System.out.println("getGradedEventsForCourseAndTermNullCourseName");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(1L));
        String term = "Summer 2010";
        List<GradedEvent> result = instance.getGradedEventsForCourseAndTerm(course, term);
    }

    @Test
    public void testGetGradedEventsForCourseAndTermOffTerm()
    {
        System.out.println("getGradedEventsForCourseAndTermOffTerm");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(1L));
        course.setName("CS110");
        String term = "Spring 2011";
        List<GradedEvent> result = instance.getGradedEventsForCourseAndTerm(course, term);
        assertNotNull(result);
        assertEquals(0, result.size());
        assertTrue(instance.isOpen());
    }

    @Test
    public void testGetGradedEventsForCourseAndTermEmptyCourse()
    {
        System.out.println("getGradedEventsForCourseAndTermEmptyCourse");
        assertTrue(instance.isOpen());
        Course course = new Course();
        course.setId(new Long(222L));
        course.setName("CS654");
        String term = "Summer 2010";
        List<GradedEvent> result = instance.getGradedEventsForCourseAndTerm(course, term);
        assertNotNull(result);
        assertEquals(0, result.size());
        assertTrue(instance.isOpen());
    }

    @Test
    public void testGetGradedEventsForCourseAndTermOffCourse()
    {
        System.out.println("getGradedEventsForCourseAndTermOffCourse");
        assertTrue(instance.isOpen());
        Course course = new Course("CS193P");
        course.setId(new Long(42L));
        course.setName("CS192P");
        String term = "Summer 2010";
        List<GradedEvent> result = instance.getGradedEventsForCourseAndTerm(course, term);
        assertNotNull(result);
        assertEquals(0, result.size());
        assertTrue(instance.isOpen());
    }

    /**
     * Test of getNumVersionsOfGradedEvent method, of class GradedEventDAO.
     */
    @Test
    public void testGetNumVersionsOfGradedEvent()
    {
        System.out.println("getNumVersionsOfGradedEvent");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        GradedEvent assignment = new GradedEvent();
        assignment.setCourse(course);
        assignment.setTerm("Summer 2010");
        assignment.setAssignment("PEX2");
        assignment.setVersion("ALL");
        int expResult = 2;
        int result = instance.getNumVersionsOfGradedEvent(assignment);
        assertEquals(expResult, result);

        assignment.setAssignment("PEX1");
        result = instance.getNumVersionsOfGradedEvent(assignment);
        expResult = 1;
        assertEquals(expResult, result);

        assertTrue(instance.isOpen());
        courseDAO.closeConnection();
    }

    @Test(expected=NullPointerException.class)
    public void testGetNumVersionsOfGradedEventNullGradedEvent()
    {
        System.out.println("getNumVersionsOfGradedEventNullGradedEvent");
        assertTrue(instance.isOpen());
        GradedEvent gradedEvent = null;
        int result = instance.getNumVersionsOfGradedEvent(gradedEvent);
    }

    @Test(expected=IllegalStateException.class)
    public void testGetNumVersionsOfGradedEventClosedConnection()
    {
        System.out.println("getNumVersionsOfGradedEventClosedConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);
        GradedEvent gradedEvent = new GradedEvent(course, "", "");
        gradedEvent.setTerm("Summer 2010");
        int result = instance.getNumVersionsOfGradedEvent(gradedEvent);
        courseDAO.closeConnection();
    }

    @Test
    public void testGetNumVersionsOfGradedEventNotFound()
    {
        System.out.println("getNumVersionsOfGradedEventNotFound");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        GradedEvent assignment = new GradedEvent(course, "PEX3", "ALL");
        assignment.setTerm("Summer 2010");
        assignment.setVersion("ALL");
        int expResult = 0;
        int result = instance.getNumVersionsOfGradedEvent(assignment);
        assertEquals(expResult, result);
        assertTrue(instance.isOpen());
        courseDAO.closeConnection();
    }

    @Test(expected=IllegalArgumentException.class)
    public void testGetNumVersionsOfGradedEventNullCourse()
    {
        System.out.println("getNumVersionsOfGradedEventNullCourse");
        assertTrue(instance.isOpen());
        GradedEvent assignment = new GradedEvent();
        assignment.setTerm("Summer 2010");
        assignment.setAssignment("PEX2");
        assignment.setVersion("ALL");
        int result = instance.getNumVersionsOfGradedEvent(assignment);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testGetNumVersionsOfGradedEventNullTerm()
    {
        System.out.println("getNumVersionsOfGradedEventNullTerm");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        GradedEvent assignment = new GradedEvent();
        assignment.setCourse(course);
        assignment.setAssignment("PEX2");
        assignment.setVersion("ALL");
        courseDAO.closeConnection();
        int result = instance.getNumVersionsOfGradedEvent(assignment);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testGetNumVersionsOfGradedEventNullAssignment()
    {
        System.out.println("getNumVersionsOfGradedEventNullAssignment");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        GradedEvent assignment = new GradedEvent();
        assignment.setCourse(course);
        assignment.setTerm("Summer 2010");
        assignment.setVersion("ALL");
        courseDAO.closeConnection();
        int result = instance.getNumVersionsOfGradedEvent(assignment);
    }

    @Test
    public void testGetNumVersionsOfGradedEventNullVersion()
    {
        System.out.println("getNumVersionsOfGradedEventNullVersion");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        GradedEvent assignment = new GradedEvent();
        assignment.setCourse(course);
        assignment.setTerm("Summer 2010");
        assignment.setAssignment("PEX2");
        int expResult = 2;
        int result = instance.getNumVersionsOfGradedEvent(assignment);
        assertEquals(expResult, result);

        assertTrue(instance.isOpen());
        courseDAO.closeConnection();
    }

    /**
     * Test of getAllVersionsOfGradedEvent method, of class GradedEventDAO.
     */
    @Test
    public void testGetAllVersionsOfGradedEvent()
    {
        System.out.println("getAllVersionsOfGradedEvent");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        GradedEvent assignment = new GradedEvent();
        assignment.setCourse(course);
        assignment.setTerm("Summer 2010");
        assignment.setAssignment("PEX2");
        assignment.setVersion("ALL");
        
        List<GradedEvent> result = instance.getAllVersionsOfGradedEvent(assignment);
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals(course, result.get(0).getCourse());
        assertEquals("Summer 2010", result.get(0).getTerm());
        assertEquals("PEX2", result.get(0).getAssignment());
        assertEquals("M1", result.get(0).getVersion());
        assertEquals(course, result.get(1).getCourse());
        assertEquals("Summer 2010", result.get(1).getTerm());
        assertEquals("PEX2", result.get(1).getAssignment());
        assertEquals("M2", result.get(1).getVersion());
        assertTrue(instance.isOpen());

        assignment.setAssignment("PEX1");
        result = instance.getAllVersionsOfGradedEvent(assignment);
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(course, result.get(0).getCourse());
        assertEquals("Summer 2010", result.get(0).getTerm());
        assertEquals("PEX1", result.get(0).getAssignment());
        assertEquals("ALL", result.get(0).getVersion());
        courseDAO.closeConnection();
    }

    @Test
    public void testGetAllVersionsOfGradedEventNoResult()
    {
        System.out.println("getAllVersionsOfGradedEventNoResult");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        GradedEvent assignment = new GradedEvent();
        assignment.setCourse(course);
        assignment.setTerm("Spring 2010");
        assignment.setAssignment("PEX2");
        assignment.setVersion("ALL");

        List<GradedEvent> result = instance.getAllVersionsOfGradedEvent(assignment);
        assertNotNull(result);
        assertEquals(0, result.size());
        assertTrue(instance.isOpen());
        courseDAO.closeConnection();
    }

    @Test(expected=NullPointerException.class)
    public void testGetAllVersionsOfGradedEventNullGradedEvent()
    {
        System.out.println("getAllVersionsOfGradedEventNullGradedEvent");
        assertTrue(instance.isOpen());
        GradedEvent assignment = null;
        List<GradedEvent> result = instance.getAllVersionsOfGradedEvent(assignment);
    }

    @Test(expected=IllegalStateException.class)
    public void testGetAllVersionsOfGradedEventClosedConnection()
    {
        System.out.println("getAllVersionsOfGradedEventClosedConnection");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        GradedEvent assignment = new GradedEvent();
        assignment.setCourse(course);
        assignment.setTerm("Summer 2010");
        assignment.setAssignment("PEX2");
        assignment.setVersion("ALL");

        instance.closeConnection();
        assertFalse(instance.isOpen());
        courseDAO.closeConnection();
        List<GradedEvent> result = instance.getAllVersionsOfGradedEvent(assignment);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testGetAllVersionsOfGradedEventNullCourse()
    {
        System.out.println("getAllVersionsOfGradedEventNullCourse");
        assertTrue(instance.isOpen());

        GradedEvent assignment = new GradedEvent();
        assignment.setTerm("Spring 2010");
        assignment.setAssignment("PEX2");
        assignment.setVersion("ALL");

        List<GradedEvent> result = instance.getAllVersionsOfGradedEvent(assignment);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testGetAllVersionsOfGradedEventNullTerm()
    {
        System.out.println("getAllVersionsOfGradedEventNullTerm");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        GradedEvent assignment = new GradedEvent();
        assignment.setCourse(course);
        assignment.setAssignment("PEX2");
        assignment.setVersion("ALL");

        courseDAO.closeConnection();
        List<GradedEvent> result = instance.getAllVersionsOfGradedEvent(assignment);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testGetAllVersionsOfGradedEventNullAssignment()
    {
        System.out.println("getAllVersionsOfGradedEventNullTerm");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        GradedEvent assignment = new GradedEvent();
        assignment.setCourse(course);
        assignment.setTerm("Summer 2010");
        assignment.setVersion("ALL");

        courseDAO.closeConnection();
        List<GradedEvent> result = instance.getAllVersionsOfGradedEvent(assignment);
    }

    @Test(expected=NullPointerException.class)
    public void testGetAllVersionsOfGradedEventNullVersion()
    {
        System.out.println("getAllVersionsOfGradedEventNullVersion");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        GradedEvent assignment = new GradedEvent();
        assignment.setCourse(course);
        assignment.setTerm("Summer 2010");
        assignment.setAssignment("PEX1");

        courseDAO.closeConnection();
        List<GradedEvent> result = instance.getAllVersionsOfGradedEvent(assignment);
    }
    
    /**
     * Test of getGradedEventsByCourseAndAssignment method, of class GradedEventDAO.
     */
    @Test
    public void testGetGradedEventsByCourseAndAssignment()
    {
        System.out.println("getGradedEventsByCourseAndAssignment");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        List<GradedEvent> result = instance.getGradedEventsByCourseAndAssignment();
        assertNotNull(result);
        assertEquals(4, result.size());
        assertEquals(course, result.get(0).getCourse());
        assertEquals("Summer 2010", result.get(0).getTerm());
        assertEquals("PEX1", result.get(0).getAssignment());
        assertEquals("ALL", result.get(0).getVersion());
        assertEquals(course, result.get(1).getCourse());
        assertEquals("Summer 2010", result.get(1).getTerm());
        assertEquals("PEX2", result.get(1).getAssignment());
        assertEquals("M1", result.get(1).getVersion());
        assertEquals(course, result.get(2).getCourse());
        assertEquals("Summer 2010", result.get(2).getTerm());
        assertEquals("PEX2", result.get(2).getAssignment());
        assertEquals("M2", result.get(2).getVersion());
        assertTrue(instance.isOpen());
    }

    @Test(expected=IllegalStateException.class)
    public void testGetGradedEventsByCourseAndAssignmentClosedConnection()
    {
        System.out.println("getGradedEventsByCourseAndAssignmentClosedConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        List<GradedEvent> result = instance.getGradedEventsByCourseAndAssignment();
    }

    /**
     * Test of closeConnection method, of class GradedEventDAO.
     */
    @Test
    public void testCloseConnection()
    {
        System.out.println("closeConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }

    @Test(expected=IllegalStateException.class)
    public void testCloseConnectionAlreadyClosed()
    {
        System.out.println("closeConnectionAlreadyClosed");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.closeConnection();
    }

    @Test
    public void testIsOpen()
    {
        System.out.println("isOpen");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }

    @Test
    public void testCreate()
    {
        System.out.println("create");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(2L);
        GradedEvent testGradedEvent = new GradedEvent();
        testGradedEvent.setId(new Long(42L));
        testGradedEvent.setCourse(course);
        testGradedEvent.setAssignment("Final");
        testGradedEvent.setTerm("Spring 2010");
        testGradedEvent.setVersion("M1");
        Long newId = instance.create(testGradedEvent);
        assertNotNull(newId);
        assertEquals(newId, new Long(42L));
        assertEquals(5, instance.getGradedEventsByCourseAndAssignment().size());
        GradedEvent result = instance.find(new Long(42L));
        assertNotNull(result);
        assertEquals("Final", result.getAssignment());
        assertEquals("Spring 2010", result.getTerm());
        assertEquals("M1", result.getVersion());
        assertEquals(course, result.getCourse());
        assertEquals(testGradedEvent, result);
        courseDAO.closeConnection();
    }

    @Test
    public void testCreateNullId()
    {
        System.out.println("createNullId");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(2L);
        GradedEvent testGradedEvent = new GradedEvent();
        testGradedEvent.setCourse(course);
        testGradedEvent.setAssignment("Final");
        testGradedEvent.setTerm("Spring 2011");
        testGradedEvent.setVersion("M1");
        Long newId = instance.create(testGradedEvent);
        assertNotNull(newId);
        assertTrue(newId != 0);
        assertTrue(newId > 0);
        System.out.println("New Id: " + newId);
        assertEquals(5, instance.getGradedEventsByCourseAndAssignment().size());
        courseDAO.closeConnection();
   }
    
    @Test(expected = NullPointerException.class)
    public void testCreateNullGradedEvent()
    {
        System.out.println("createNullGradedEvent");
        assertTrue(instance.isOpen());
        GradedEvent testGradedEvent = null;
        instance.create(testGradedEvent);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateNullCourseTermAssignmentVersion()
    {
        System.out.println("createNullCourseTermAssignmentVersion");
        assertTrue(instance.isOpen());
        GradedEvent testGradedEvent = new GradedEvent();
        instance.create(testGradedEvent);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateNullCourse()
    {
        System.out.println("createNullCourse");
        assertTrue(instance.isOpen());
        GradedEvent testGradedEvent = new GradedEvent();
        testGradedEvent.setId(new Long(42L));
        testGradedEvent.setAssignment("Final");
        testGradedEvent.setTerm("Spring 2010");
        testGradedEvent.setVersion("ALL");
        instance.create(testGradedEvent);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateNullAssignment()
    {
        System.out.println("createNullAssignment");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(2L);
        GradedEvent testGradedEvent = new GradedEvent();
        testGradedEvent.setId(new Long(42L));
        testGradedEvent.setCourse(course);
        testGradedEvent.setTerm("Spring 2010");
        testGradedEvent.setVersion("ALL");
        courseDAO.closeConnection();
        instance.create(testGradedEvent);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateNullTerm()
    {
        System.out.println("createNullTerm");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);
        GradedEvent testGradedEvent = new GradedEvent();
        testGradedEvent.setId(new Long(42L));
        testGradedEvent.setCourse(course);
        testGradedEvent.setAssignment("Final");
        testGradedEvent.setVersion("ALL");
        courseDAO.closeConnection();
        instance.create(testGradedEvent);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateNullTermFromConstructor()
    {
        System.out.println("createNullTermFromConstructor");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);
        GradedEvent testGradedEvent = new GradedEvent(course, "Final", "ALL");
        testGradedEvent.setId(new Long(42L));
        courseDAO.closeConnection();
        instance.create(testGradedEvent);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateNullVersion()
    {
        System.out.println("createNullVersion");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);
        GradedEvent testGradedEvent = new GradedEvent();
        testGradedEvent.setId(new Long(42L));
        testGradedEvent.setCourse(course);
        testGradedEvent.setAssignment("Final");
        testGradedEvent.setTerm("Spring 2010");
        courseDAO.closeConnection();
        instance.create(testGradedEvent);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateExistingGradedEvent()
    {
        System.out.println("createExistingGradedEvent");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);
        GradedEvent testGradedEvent = new GradedEvent();
        testGradedEvent.setCourse(course);
        testGradedEvent.setAssignment("PEX2");
        testGradedEvent.setTerm("Summer 2010");
        testGradedEvent.setVersion("M1");
        testGradedEvent.setId(new Long(2L));
        courseDAO.closeConnection();
        instance.create(testGradedEvent);
    }

    @Test(expected=IllegalStateException.class)
    public void testCreateClosedConnection()
    {
        System.out.println("createClosedConnection");
        assertTrue(instance.isOpen());
        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);
        GradedEvent testGradedEvent = new GradedEvent();
        testGradedEvent.setId(new Long(42L));
        testGradedEvent.setCourse(course);
        testGradedEvent.setAssignment("Final");
        testGradedEvent.setTerm("Spring 2010");
        testGradedEvent.setVersion("M1");
        courseDAO.closeConnection();
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.create(testGradedEvent);
    }

    @Test
    public void testDelete()
    {
        System.out.println("delete");
        assertTrue(instance.isOpen());
        GradedEvent result = instance.find(new Long(3L));
        assertNotNull(result);
        assertEquals("Summer 2010", result.getTerm());
        assertEquals("M2", result.getVersion());
        assertEquals("PEX2", result.getAssignment());

        instance.delete(result);

        assertTrue(instance.isOpen());
        result = instance.find(new Long(3L));
        assertNull(result);

        List<GradedEvent> resultList = instance.getGradedEventsByCourseAndAssignment();
        assertEquals(3, resultList.size());
    }

    @Test(expected=NullPointerException.class)
    public void testDeleteNullGradedEvent()
    {
        System.out.println("deleteNullGradedEvent");
        assertTrue(instance.isOpen());
        GradedEvent gradedEvent = null;
        instance.delete(gradedEvent);
    }

    @Test(expected=IllegalStateException.class)
    public void testDeleteClosedConnection()
    {
        System.out.println("deleteClosedConnection");
        assertTrue(instance.isOpen());
        GradedEvent result = instance.find(new Long(3L));
        assertNotNull(result);
        assertEquals("Summer 2010", result.getTerm());
        assertEquals("M2", result.getVersion());
        assertEquals("PEX2", result.getAssignment());

        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        
        instance.delete(result);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteNullCourse()
    {
        System.out.println("deleteNullCourse");
        assertTrue(instance.isOpen());
        GradedEvent gradedEvent = new GradedEvent();

        gradedEvent.setAssignment("PEX2");
        gradedEvent.setTerm("Summer 2010");
        gradedEvent.setVersion("M2");
        gradedEvent.setId(new Long(3L));
        
        instance.delete(gradedEvent);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteNullAssignment()
    {
        System.out.println("deleteNullAssignment");
        assertTrue(instance.isOpen());
        GradedEvent gradedEvent = new GradedEvent();

        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        gradedEvent.setCourse(course);
        gradedEvent.setTerm("Summer 2010");
        gradedEvent.setVersion("M2");
        gradedEvent.setId(new Long(3L));

        courseDAO.closeConnection();
        instance.delete(gradedEvent);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testDeleteNullTerm()
    {
        System.out.println("deleteNullTerm");
        assertTrue(instance.isOpen());
        GradedEvent gradedEvent = new GradedEvent();

        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        gradedEvent.setCourse(course);
        gradedEvent.setAssignment("PEX2");
        gradedEvent.setVersion("M2");
        gradedEvent.setId(new Long(3L));

        courseDAO.closeConnection();
        instance.delete(gradedEvent);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testDeleteNullVersion()
    {
        System.out.println("deleteNullVersion");
        assertTrue(instance.isOpen());
        GradedEvent gradedEvent = new GradedEvent();

        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        gradedEvent.setCourse(course);
        gradedEvent.setTerm("Summer 2010");
        gradedEvent.setAssignment("PEX2");
        gradedEvent.setId(new Long(3L));

        courseDAO.closeConnection();
        instance.delete(gradedEvent);
    }

    @Test
    public void testDeleteNullId()
    {
        System.out.println("deleteNullId");
        assertTrue(instance.isOpen());
        GradedEvent gradedEvent = new GradedEvent();

        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        gradedEvent.setCourse(course);
        gradedEvent.setTerm("Summer 2010");
        gradedEvent.setAssignment("PEX2");
        gradedEvent.setVersion("M2");

        courseDAO.closeConnection();

        instance.delete(gradedEvent);

        assertTrue(instance.isOpen());
        GradedEvent result = instance.find(new Long(3L));
        assertNull(result);

        List<GradedEvent> resultList = instance.getGradedEventsByCourseAndAssignment();
        assertEquals(3, resultList.size());
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteNotFound()
    {
        System.out.println("deleteNotFound");
        assertTrue(instance.isOpen());
        GradedEvent gradedEvent = new GradedEvent();

        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(1L);

        gradedEvent.setCourse(course);
        gradedEvent.setTerm("Summer 2011");
        gradedEvent.setAssignment("PEX4");
        gradedEvent.setVersion("ALL");
        gradedEvent.setId(new Long(42L));

        courseDAO.closeConnection();

        instance.delete(gradedEvent);
    }

    @Test
    public void testDeleteId()
    {
        System.out.println("deleteId");
        assertTrue(instance.isOpen());
        Long id = new Long(3L);
        GradedEvent result = instance.find(id);
        assertNotNull(result);
        assertEquals("Summer 2010", result.getTerm());
        assertEquals("M2", result.getVersion());
        assertEquals("PEX2", result.getAssignment());

        instance.delete(id);
        assertTrue(instance.isOpen());

        result = instance.find(id);
        assertNull(result);

        List<GradedEvent> resultList = instance.getGradedEventsByCourseAndAssignment();
        assertEquals(3, resultList.size());
    }

    @Test(expected=NullPointerException.class)
    public void testDeleteIdNullId()
    {
        System.out.println("deleteIdNullId");
        assertTrue(instance.isOpen());
        Long id = null;
        instance.delete(id);
    }

    @Test(expected=IllegalStateException.class)
    public void testDeleteIdClosedConnection()
    {
        System.out.println("deleteIdClosedConnection");
        assertTrue(instance.isOpen());
        Long id = new Long(3L);
        GradedEvent result = instance.find(id);
        assertNotNull(result);
        assertEquals("Summer 2010", result.getTerm());
        assertEquals("M2", result.getVersion());
        assertEquals("PEX2", result.getAssignment());

        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        
        instance.delete(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteIdNegativeId()
    {
        System.out.println("deleteIdNegativeId");
        assertTrue(instance.isOpen());
        Long id = new Long(-42L);

        instance.delete(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteIdZeroId()
    {
        System.out.println("deleteIdZeroId");
        assertTrue(instance.isOpen());

        Long id = new Long(0L);

        instance.delete(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDeleteIdNotFound()
    {
        System.out.println("deleteIdNotFound");
        assertTrue(instance.isOpen());
        Long id = new Long(42L);

        instance.delete(id);
    }

    
    @Test
    public void testUpdateAllFields()
    {
        System.out.println("updateAllFields");
        assertTrue(instance.isOpen());

        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(2L);

        CategoryDAO categoryDAO = new CategoryDAO(testDb.getConnection());
        Category category = categoryDAO.find(2L);

        GradedEvent gradedEvent = instance.find(new Long(3L));
        assertEquals(1, gradedEvent.getQuestions().size());
        gradedEvent.setCourse(course);
        gradedEvent.setAssignment("PEX3");
        gradedEvent.setDueDate("19 May 2010");
        gradedEvent.setPartialCredit(false);
        gradedEvent.setTerm("Fall 2010");
        gradedEvent.setVersion("ALL");

        Question newQuestion = new Question(42L, "TestQ", category,
                "This is a new test question");

        assertEquals(1, gradedEvent.getQuestions().size());
        gradedEvent.addQuestion(newQuestion);

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(gradedEvent);
        assertNotNull(result);

        // Grab the updated GradedEvent from the Data source
        GradedEvent requestQuery = instance.find(new Long(3L));

        // Verify the data is maintained across the 3 versions (local, result from
        // update, and query of the data source

        // Id
        assertEquals(new Long(3L), requestQuery.getId());
        assertEquals(result.getId(), requestQuery.getId());

        // Course
        assertEquals(course, requestQuery.getCourse());
        assertEquals(result.getCourse(), requestQuery.getCourse());

        // Assignment
        assertEquals("PEX3", requestQuery.getAssignment());
        assertEquals(result.getAssignment(), requestQuery.getAssignment());

        // Term
        assertEquals("Fall 2010", requestQuery.getTerm());
        assertEquals(result.getTerm(), requestQuery.getTerm());

        // Version
        assertEquals("ALL", requestQuery.getVersion());
        assertEquals(result.getVersion(), requestQuery.getVersion());

        // Due Date
        assertEquals("19 May 2010", requestQuery.getDueDate());
        assertEquals(result.getDueDate(), requestQuery.getDueDate());

        // Partial Credit
        assertEquals(false, requestQuery.getPartialCredit());
        assertEquals(result.getPartialCredit(), requestQuery.getPartialCredit());

        // Number of Questions
        assertEquals(2, requestQuery.getQuestions().size());
        assertEquals(result.getQuestions().size(), requestQuery.getQuestions().size());

        assertTrue(instance.isOpen());
        courseDAO.closeConnection();
        categoryDAO.closeConnection();
    }

    @Test
    public void testUpdateCourse()
    {
        System.out.println("updateCourse");
        assertTrue(instance.isOpen());

        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(2L);

        GradedEvent gradedEvent = instance.find(new Long(3L));
        gradedEvent.setCourse(course);

        assertEquals(1, gradedEvent.getQuestions().size());

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(gradedEvent);
        assertNotNull(result);

        // Grab the updated GradedEvent from the Data source
        GradedEvent requestQuery = instance.find(new Long(3L));

        // Verify the data is maintained across the 3 versions (local, result from
        // update, and query of the data source

        // Id
        assertEquals(new Long(3L), requestQuery.getId());
        assertEquals(result.getId(), requestQuery.getId());

        // Course
        assertEquals(course, requestQuery.getCourse());
        assertEquals(result.getCourse(), requestQuery.getCourse());

        // Assignment
        assertEquals("PEX2", requestQuery.getAssignment());
        assertEquals(result.getAssignment(), requestQuery.getAssignment());

        // Term
        assertEquals("Summer 2010", requestQuery.getTerm());
        assertEquals(result.getTerm(), requestQuery.getTerm());

        // Version
        assertEquals("M2", requestQuery.getVersion());
        assertEquals(result.getVersion(), requestQuery.getVersion());

        // Due Date
        assertEquals("29 Apr 2010", requestQuery.getDueDate());
        assertEquals(result.getDueDate(), requestQuery.getDueDate());

        // Partial Credit
        assertEquals(true, requestQuery.getPartialCredit());
        assertEquals(result.getPartialCredit(), requestQuery.getPartialCredit());

        // Number of Questions
        assertEquals(1, requestQuery.getQuestions().size());
        assertEquals(result.getQuestions().size(), requestQuery.getQuestions().size());

        assertTrue(instance.isOpen());
        courseDAO.closeConnection();
    }

    @Test
    public void testUpdateAssignment()
    {
        System.out.println("updateAssignment");
        assertTrue(instance.isOpen());

        GradedEvent gradedEvent = instance.find(new Long(3L));
        gradedEvent.setAssignment("PEX3");

        assertEquals(1, gradedEvent.getQuestions().size());

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(gradedEvent);
        assertNotNull(result);

        assertEquals(1, result.getQuestions().size());

        // Grab the updated GradedEvent from the Data source
        GradedEvent requestQuery = instance.find(new Long(3L));

        // Verify the data is maintained across the 3 versions (local, result from
        // update, and query of the data source

        // Id
        assertEquals(new Long(3L), requestQuery.getId());
        assertEquals(result.getId(), requestQuery.getId());

        // Course
        assertEquals("CS110", requestQuery.getCourse().getName());
        assertEquals(result.getCourse(), requestQuery.getCourse());

        // Assignment
        assertEquals("PEX3", requestQuery.getAssignment());
        assertEquals(result.getAssignment(), requestQuery.getAssignment());

        // Term
        assertEquals("Summer 2010", requestQuery.getTerm());
        assertEquals(result.getTerm(), requestQuery.getTerm());

        // Version
        assertEquals("M2", requestQuery.getVersion());
        assertEquals(result.getVersion(), requestQuery.getVersion());

        // Due Date
        assertEquals("29 Apr 2010", requestQuery.getDueDate());
        assertEquals(result.getDueDate(), requestQuery.getDueDate());

        // Partial Credit
        assertEquals(true, requestQuery.getPartialCredit());
        assertEquals(result.getPartialCredit(), requestQuery.getPartialCredit());

        // Number of Questions
        assertEquals(1, requestQuery.getQuestions().size());
        assertEquals(result.getQuestions().size(), requestQuery.getQuestions().size());

        assertTrue(instance.isOpen());
    }

    @Test
    public void testUpdateTerm()
    {
        System.out.println("updateTerm");
        assertTrue(instance.isOpen());

        GradedEvent gradedEvent = instance.find(new Long(3L));
        gradedEvent.setTerm("Fall 2010");

        assertEquals(1, gradedEvent.getQuestions().size());

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(gradedEvent);
        assertNotNull(result);

        // Grab the updated GradedEvent from the Data source
        GradedEvent requestQuery = instance.find(new Long(3L));

        // Verify the data is maintained across the 3 versions (local, result from
        // update, and query of the data source

        // Id
        assertEquals(new Long(3L), requestQuery.getId());
        assertEquals(result.getId(), requestQuery.getId());

        // Course
        assertEquals("CS110", requestQuery.getCourse().getName());
        assertEquals(result.getCourse(), requestQuery.getCourse());

        // Assignment
        assertEquals("PEX2", requestQuery.getAssignment());
        assertEquals(result.getAssignment(), requestQuery.getAssignment());

        // Term
        assertEquals("Fall 2010", requestQuery.getTerm());
        assertEquals(result.getTerm(), requestQuery.getTerm());

        // Version
        assertEquals("M2", requestQuery.getVersion());
        assertEquals(result.getVersion(), requestQuery.getVersion());

        // Due Date
        assertEquals("29 Apr 2010", requestQuery.getDueDate());
        assertEquals(result.getDueDate(), requestQuery.getDueDate());

        // Partial Credit
        assertEquals(true, requestQuery.getPartialCredit());
        assertEquals(result.getPartialCredit(), requestQuery.getPartialCredit());

        // Number of Questions
        assertEquals(1, requestQuery.getQuestions().size());
        assertEquals(result.getQuestions().size(), requestQuery.getQuestions().size());

        assertTrue(instance.isOpen());
    }

    @Test
    public void testUpdateVersion()
    {
        System.out.println("updateVersion");
        assertTrue(instance.isOpen());

        GradedEvent gradedEvent = instance.find(new Long(3L));
        gradedEvent.setVersion("ALL");

        assertEquals(1, gradedEvent.getQuestions().size());

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(gradedEvent);
        assertNotNull(result);

        // Grab the updated GradedEvent from the Data source
        GradedEvent requestQuery = instance.find(new Long(3L));

        // Verify the data is maintained across the 3 versions (local, result from
        // update, and query of the data source

        // Id
        assertEquals(new Long(3L), requestQuery.getId());
        assertEquals(result.getId(), requestQuery.getId());

        // Course
        assertEquals("CS110", requestQuery.getCourse().getName());
        assertEquals(result.getCourse(), requestQuery.getCourse());

        // Assignment
        assertEquals("PEX2", requestQuery.getAssignment());
        assertEquals(result.getAssignment(), requestQuery.getAssignment());

        // Term
        assertEquals("Summer 2010", requestQuery.getTerm());
        assertEquals(result.getTerm(), requestQuery.getTerm());

        // Version
        assertEquals("ALL", requestQuery.getVersion());
        assertEquals(result.getVersion(), requestQuery.getVersion());

        // Due Date
        assertEquals("29 Apr 2010", requestQuery.getDueDate());
        assertEquals(result.getDueDate(), requestQuery.getDueDate());

        // Partial Credit
        assertEquals(true, requestQuery.getPartialCredit());
        assertEquals(result.getPartialCredit(), requestQuery.getPartialCredit());

        // Number of Questions
        assertEquals(1, requestQuery.getQuestions().size());
        assertEquals(result.getQuestions().size(), requestQuery.getQuestions().size());

        assertTrue(instance.isOpen());
    }

    @Test
    public void testUpdateDueDate()
    {
        System.out.println("updateDueDate");
        assertTrue(instance.isOpen());

        GradedEvent gradedEvent = instance.find(new Long(3L));
        gradedEvent.setDueDate("23 May 2010");

        assertEquals(1, gradedEvent.getQuestions().size());

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(gradedEvent);
        assertNotNull(result);

        // Grab the updated GradedEvent from the Data source
        GradedEvent requestQuery = instance.find(new Long(3L));

        // Verify the data is maintained across the 3 versions (local, result from
        // update, and query of the data source

        // Id
        assertEquals(new Long(3L), requestQuery.getId());
        assertEquals(result.getId(), requestQuery.getId());

        // Course
        assertEquals("CS110", requestQuery.getCourse().getName());
        assertEquals(result.getCourse(), requestQuery.getCourse());

        // Assignment
        assertEquals("PEX2", requestQuery.getAssignment());
        assertEquals(result.getAssignment(), requestQuery.getAssignment());

        // Term
        assertEquals("Summer 2010", requestQuery.getTerm());
        assertEquals(result.getTerm(), requestQuery.getTerm());

        // Version
        assertEquals("M2", requestQuery.getVersion());
        assertEquals(result.getVersion(), requestQuery.getVersion());

        // Due Date
        assertEquals("23 May 2010", requestQuery.getDueDate());
        assertEquals(result.getDueDate(), requestQuery.getDueDate());

        // Partial Credit
        assertEquals(true, requestQuery.getPartialCredit());
        assertEquals(result.getPartialCredit(), requestQuery.getPartialCredit());

        // Number of Questions
        assertEquals(1, requestQuery.getQuestions().size());
        assertEquals(result.getQuestions().size(), requestQuery.getQuestions().size());

        assertTrue(instance.isOpen());
    }

    @Test
    public void testUpdatePartialCredit()
    {
        System.out.println("updatePartialCredit");
        assertTrue(instance.isOpen());

        GradedEvent gradedEvent = instance.find(new Long(3L));
        gradedEvent.setPartialCredit(false);

        assertEquals(1, gradedEvent.getQuestions().size());

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(gradedEvent);
        assertNotNull(result);

        // Grab the updated GradedEvent from the Data source
        GradedEvent requestQuery = instance.find(new Long(3L));

        // Verify the data is maintained across the 3 versions (local, result from
        // update, and query of the data source

        // Id
        assertEquals(new Long(3L), requestQuery.getId());
        assertEquals(result.getId(), requestQuery.getId());

        // Course
        assertEquals("CS110", requestQuery.getCourse().getName());
        assertEquals(result.getCourse(), requestQuery.getCourse());

        // Assignment
        assertEquals("PEX2", requestQuery.getAssignment());
        assertEquals(result.getAssignment(), requestQuery.getAssignment());

        // Term
        assertEquals("Summer 2010", requestQuery.getTerm());
        assertEquals(result.getTerm(), requestQuery.getTerm());

        // Version
        assertEquals("M2", requestQuery.getVersion());
        assertEquals(result.getVersion(), requestQuery.getVersion());

        // Due Date
        assertEquals("29 Apr 2010", requestQuery.getDueDate());
        assertEquals(result.getDueDate(), requestQuery.getDueDate());

        // Partial Credit
        assertEquals(false, requestQuery.getPartialCredit());
        assertEquals(result.getPartialCredit(), requestQuery.getPartialCredit());

        // Number of Questions
        assertEquals(1, requestQuery.getQuestions().size());
        assertEquals(result.getQuestions().size(), requestQuery.getQuestions().size());

        assertTrue(instance.isOpen());
    }

    @Test
    public void testUpdateRemoveAllQuestions()
    {
        System.out.println("updateRemoveAllQuestions");
        assertTrue(instance.isOpen());

        GradedEvent gradedEvent = instance.find(new Long(3L));
        assertEquals(1, gradedEvent.getQuestions().size());
        gradedEvent.clearQuestions();

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(gradedEvent);
        assertNotNull(result);

        // Grab the updated GradedEvent from the Data source
        GradedEvent requestQuery = instance.find(new Long(3L));

        // Verify the data is maintained across the 3 versions (local, result from
        // update, and query of the data source

        // Id
        assertEquals(new Long(3L), requestQuery.getId());
        assertEquals(result.getId(), requestQuery.getId());

        // Course
        assertEquals("CS110", requestQuery.getCourse().getName());
        assertEquals(result.getCourse(), requestQuery.getCourse());

        // Assignment
        assertEquals("PEX2", requestQuery.getAssignment());
        assertEquals(result.getAssignment(), requestQuery.getAssignment());

        // Term
        assertEquals("Summer 2010", requestQuery.getTerm());
        assertEquals(result.getTerm(), requestQuery.getTerm());

        // Version
        assertEquals("M2", requestQuery.getVersion());
        assertEquals(result.getVersion(), requestQuery.getVersion());

        // Due Date
        assertEquals("29 Apr 2010", requestQuery.getDueDate());
        assertEquals(result.getDueDate(), requestQuery.getDueDate());

        // Partial Credit
        assertEquals(true, requestQuery.getPartialCredit());
        assertEquals(result.getPartialCredit(), requestQuery.getPartialCredit());

        // Number of Questions
        assertEquals(0, requestQuery.getQuestions().size());
        assertEquals(result.getQuestions().size(), requestQuery.getQuestions().size());

        assertTrue(instance.isOpen());
    }

    @Test
    public void testUpdateAddExistingQuestion()
    {
        System.out.println("updateAddExistingQuestion");
        assertTrue(instance.isOpen());

        QuestionDAO questionDAO = new QuestionDAO(testDb.getConnection());
        Question question = questionDAO.find(2L);


        GradedEvent gradedEvent = instance.find(new Long(3L));

        assertEquals(1, gradedEvent.getQuestions().size());
        gradedEvent.addQuestion(question);

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(gradedEvent);
        assertNotNull(result);

        // Grab the updated GradedEvent from the Data source
        GradedEvent requestQuery = instance.find(new Long(3L));

        // Verify the data is maintained across the 3 versions (local, result from
        // update, and query of the data source

        // Id
        assertEquals(new Long(3L), requestQuery.getId());
        assertEquals(result.getId(), requestQuery.getId());

        // Course
        assertEquals("CS110", requestQuery.getCourse().getName());
        assertEquals(result.getCourse(), requestQuery.getCourse());

        // Assignment
        assertEquals("PEX2", requestQuery.getAssignment());
        assertEquals(result.getAssignment(), requestQuery.getAssignment());

        // Term
        assertEquals("Summer 2010", requestQuery.getTerm());
        assertEquals(result.getTerm(), requestQuery.getTerm());

        // Version
        assertEquals("M2", requestQuery.getVersion());
        assertEquals(result.getVersion(), requestQuery.getVersion());

        // Due Date
        assertEquals("29 Apr 2010", requestQuery.getDueDate());
        assertEquals(result.getDueDate(), requestQuery.getDueDate());

        // Partial Credit
        assertEquals(true, requestQuery.getPartialCredit());
        assertEquals(result.getPartialCredit(), requestQuery.getPartialCredit());

        // Number of Questions
        assertEquals(2, requestQuery.getQuestions().size());
        assertEquals(result.getQuestions().size(), requestQuery.getQuestions().size());

        assertTrue(instance.isOpen());
        questionDAO.closeConnection();
    }

    @Test
    public void testUpdateAddNewQuestion()
    {
        System.out.println("updateAddNewQuestion");
        assertTrue(instance.isOpen());

        CategoryDAO categoryDAO = new CategoryDAO(testDb.getConnection());
        Category category = categoryDAO.find(2L);

        GradedEvent gradedEvent = instance.find(new Long(3L));

        Question newQuestion = new Question(42L, "TestQ", category,
                "This is a new test question");

        assertEquals(1, gradedEvent.getQuestions().size());
        gradedEvent.addQuestion(newQuestion);

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(gradedEvent);
        assertNotNull(result);

        // Grab the updated GradedEvent from the Data source
        GradedEvent requestQuery = instance.find(new Long(3L));

        // Verify the data is maintained across the 3 versions (local, result from
        // update, and query of the data source

        // Id
        assertEquals(new Long(3L), requestQuery.getId());
        assertEquals(result.getId(), requestQuery.getId());

        // Course
        assertEquals("CS110", requestQuery.getCourse().getName());
        assertEquals(result.getCourse(), requestQuery.getCourse());

        // Assignment
        assertEquals("PEX2", requestQuery.getAssignment());
        assertEquals(result.getAssignment(), requestQuery.getAssignment());

        // Term
        assertEquals("Summer 2010", requestQuery.getTerm());
        assertEquals(result.getTerm(), requestQuery.getTerm());

        // Version
        assertEquals("M2", requestQuery.getVersion());
        assertEquals(result.getVersion(), requestQuery.getVersion());

        // Due Date
        assertEquals("29 Apr 2010", requestQuery.getDueDate());
        assertEquals(result.getDueDate(), requestQuery.getDueDate());

        // Partial Credit
        assertEquals(true, requestQuery.getPartialCredit());
        assertEquals(result.getPartialCredit(), requestQuery.getPartialCredit());

        // Number of Questions
        assertEquals(2, requestQuery.getQuestions().size());
        assertEquals(result.getQuestions().size(), requestQuery.getQuestions().size());

        assertTrue(instance.isOpen());
        categoryDAO.closeConnection();
    }

    @Test
    public void testUpdateRemoveQuestion()
    {
        System.out.println("updateDueDate");
        assertTrue(instance.isOpen());

        GradedEvent gradedEvent = instance.find(new Long(3L));
        assertEquals(1, gradedEvent.getQuestions().size());
        assertNotNull(gradedEvent.getQuestions());
        Question question = gradedEvent.getQuestions().get(0);
        gradedEvent.removeQuestion(question);

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(gradedEvent);
        assertNotNull(result);

        // Grab the updated GradedEvent from the Data source
        GradedEvent requestQuery = instance.find(new Long(3L));

        // Verify the data is maintained across the 3 versions (local, result from
        // update, and query of the data source

        // Id
        assertEquals(new Long(3L), requestQuery.getId());
        assertEquals(result.getId(), requestQuery.getId());

        // Course
        assertEquals("CS110", requestQuery.getCourse().getName());
        assertEquals(result.getCourse(), requestQuery.getCourse());

        // Assignment
        assertEquals("PEX2", requestQuery.getAssignment());
        assertEquals(result.getAssignment(), requestQuery.getAssignment());

        // Term
        assertEquals("Summer 2010", requestQuery.getTerm());
        assertEquals(result.getTerm(), requestQuery.getTerm());

        // Version
        assertEquals("M2", requestQuery.getVersion());
        assertEquals(result.getVersion(), requestQuery.getVersion());

        // Due Date
        assertEquals("29 Apr 2010", requestQuery.getDueDate());
        assertEquals(result.getDueDate(), requestQuery.getDueDate());

        // Partial Credit
        assertEquals(true, requestQuery.getPartialCredit());
        assertEquals(result.getPartialCredit(), requestQuery.getPartialCredit());

        // Number of Questions
        assertEquals(0, requestQuery.getQuestions().size());
        assertEquals(result.getQuestions().size(), requestQuery.getQuestions().size());

        assertTrue(instance.isOpen());
    }

    @Test(expected=IllegalStateException.class)
    public void testUpdateClosedConnection()
    {
        System.out.println("updateClosedConnection");
        assertTrue(instance.isOpen());

        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(2L);

        CategoryDAO categoryDAO = new CategoryDAO(testDb.getConnection());
        Category category = categoryDAO.find(2L);

        GradedEvent gradedEvent = instance.find(new Long(3L));
        gradedEvent.setCourse(course);
        gradedEvent.setAssignment("PEX3");
        gradedEvent.setDueDate("19 May 2010");
        gradedEvent.setPartialCredit(false);
        gradedEvent.setTerm("Fall 2010");
        gradedEvent.setVersion("ALL");

        Question newQuestion = new Question(42L, "TestQ", category,
                "This is a new test question");

        gradedEvent.addQuestion(newQuestion);

        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(gradedEvent);
    }

    @Test(expected=NullPointerException.class)
    public void testUpdateNullGradedEvent()
    {
        System.out.println("updateNullGradedEvent");
        assertTrue(instance.isOpen());

        GradedEvent gradedEvent = null;

        instance.update(gradedEvent);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testUpdateNullCourse()
    {
        System.out.println("updateNullCourse");
        assertTrue(instance.isOpen());

        CategoryDAO categoryDAO = new CategoryDAO(testDb.getConnection());
        Category category = categoryDAO.find(2L);

        GradedEvent gradedEvent = instance.find(new Long(3L));
        GradedEvent event = new GradedEvent();
        event.setId(gradedEvent.getId());
        event.setAssignment("PEX3");
        event.setDueDate("19 May 2010");
        event.setPartialCredit(false);
        event.setTerm("Fall 2010");
        event.setVersion("ALL");

        Question newQuestion = new Question(42L, "TestQ", category,
                "This is a new test question");

        for (int i = 0; i < gradedEvent.getQuestions().size(); i++)
        {
            event.addQuestion(gradedEvent.getQuestions().get(i));
        }
        
        event.addQuestion(newQuestion);

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(event);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testUpdateNullAssignment()
    {
        System.out.println("updateNullAssignment");
        assertTrue(instance.isOpen());

        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(2L);

        CategoryDAO categoryDAO = new CategoryDAO(testDb.getConnection());
        Category category = categoryDAO.find(2L);

        GradedEvent gradedEvent = instance.find(new Long(3L));
        GradedEvent event = new GradedEvent();
        event.setId(gradedEvent.getId());
        event.setCourse(course);
        event.setDueDate("19 May 2010");
        event.setPartialCredit(false);
        event.setTerm("Fall 2010");
        event.setVersion("ALL");

        Question newQuestion = new Question(42L, "TestQ", category,
                "This is a new test question");

        for (int i = 0; i < gradedEvent.getQuestions().size(); i++)
        {
            event.addQuestion(gradedEvent.getQuestions().get(i));
        }

        event.addQuestion(newQuestion);

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(event);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testUpdateNullTerm()
    {
        System.out.println("updateNullTerm");
        assertTrue(instance.isOpen());

        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(2L);

        CategoryDAO categoryDAO = new CategoryDAO(testDb.getConnection());
        Category category = categoryDAO.find(2L);

        GradedEvent gradedEvent = instance.find(new Long(3L));
        GradedEvent event = new GradedEvent();
        event.setId(gradedEvent.getId());
        event.setCourse(course);
        event.setAssignment("PEX3");
        event.setDueDate("19 May 2010");
        event.setPartialCredit(false);
        event.setVersion("ALL");

        Question newQuestion = new Question(42L, "TestQ", category,
                "This is a new test question");

        for (int i = 0; i < gradedEvent.getQuestions().size(); i++)
        {
            event.addQuestion(gradedEvent.getQuestions().get(i));
        }

        event.addQuestion(newQuestion);

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(event);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testUpdateNullVersion()
    {
        System.out.println("updateNullVersion");
        assertTrue(instance.isOpen());

        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(2L);

        CategoryDAO categoryDAO = new CategoryDAO(testDb.getConnection());
        Category category = categoryDAO.find(2L);

        GradedEvent gradedEvent = instance.find(new Long(3L));
        GradedEvent event = new GradedEvent();
        event.setId(gradedEvent.getId());
        event.setCourse(course);
        event.setAssignment("PEX3");
        event.setDueDate("19 May 2010");
        event.setPartialCredit(false);
        event.setTerm("Fall 2010");

        Question newQuestion = new Question(42L, "TestQ", category,
                "This is a new test question");

        for (int i = 0; i < gradedEvent.getQuestions().size(); i++)
        {
            event.addQuestion(gradedEvent.getQuestions().get(i));
        }

        event.addQuestion(newQuestion);

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(event);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testUpdateNotFound()
    {
        System.out.println("updateNotFound");
        assertTrue(instance.isOpen());

        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(2L);

        CategoryDAO categoryDAO = new CategoryDAO(testDb.getConnection());
        Category category = categoryDAO.find(2L);

        GradedEvent gradedEvent = instance.find(new Long(3L));
        GradedEvent event = new GradedEvent();
        event.setId(new Long(42L));
        event.setCourse(course);
        event.setAssignment("PEX3");
        event.setDueDate("19 May 2010");
        event.setPartialCredit(false);
        event.setTerm("Fall 2010");
        event.setVersion("ALL");

        Question newQuestion = new Question(42L, "TestQ", category,
                "This is a new test question");

        for (int i = 0; i < gradedEvent.getQuestions().size(); i++)
        {
            event.addQuestion(gradedEvent.getQuestions().get(i));
        }

        event.addQuestion(newQuestion);

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(event);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testUpdateNullId()
    {
        System.out.println("updateNullId");
        assertTrue(instance.isOpen());

        CourseDAO courseDAO = new CourseDAO(testDb.getConnection());
        Course course = courseDAO.find(2L);

        CategoryDAO categoryDAO = new CategoryDAO(testDb.getConnection());
        Category category = categoryDAO.find(2L);

        GradedEvent gradedEvent = instance.find(new Long(3L));
        GradedEvent event = new GradedEvent();
        event.setCourse(course);
        event.setAssignment("PEX3");
        event.setDueDate("19 May 2010");
        event.setPartialCredit(false);
        event.setTerm("Fall 2010");
        event.setVersion("ALL");

        Question newQuestion = new Question(42L, "TestQ", category,
                "This is a new test question");

        for (int i = 0; i < gradedEvent.getQuestions().size(); i++)
        {
            event.addQuestion(gradedEvent.getQuestions().get(i));
        }

        event.addQuestion(newQuestion);

        // Make sure the updated GradedEvent is returned from the method
        GradedEvent result = instance.update(event);
    }

    @Test
    public void testRemoveQuestionFromGradedEvent()
    {
        System.out.println("removeQuestionFromGradedEvent");
        assertTrue(instance.isOpen());
        
        GradedEvent gradedEvent = instance.find(new Long(5L));
        assertEquals(2, gradedEvent.getQuestions().size());
        
        Question question = new Question();
        question.setId(new Long(1L));
        question.setName("Sequencer");
        
        instance.removeQuestionFromGradedEvent(gradedEvent, question);
        
        gradedEvent = instance.find(new Long(5L));
        assertEquals(1, gradedEvent.getQuestions().size());
        
        EntityManager em = testDb.getConnection();
        
        Question testQuestion = em.find(Question.class, new Long(1L));
        assertNotNull(testQuestion);
        
        assertTrue(instance.isOpen());
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveQuestionFromGradedEventNotFound()
    {
        System.out.println("removeQuestionFromGradedEvent - Question Not in DB");
        assertTrue(instance.isOpen());
        
        GradedEvent gradedEvent = instance.find(new Long(5L));
        assertEquals(2, gradedEvent.getQuestions().size());
        
        Question question = new Question();
        question.setId(new Long(123L));
        question.setName("Sequencer 2");
        
        instance.removeQuestionFromGradedEvent(gradedEvent, question);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveQuestionFromGradedEventNotInQuestion()
    {
        System.out.println("removeQuestionFromGradedEvent - Question Not in GE");
        assertTrue(instance.isOpen());
        
        GradedEvent gradedEvent = instance.find(new Long(5L));
        assertEquals(2, gradedEvent.getQuestions().size());
        
        Question question = new Question();
        question.setId(new Long(3L));
        question.setName("Jumper");
        
        instance.removeQuestionFromGradedEvent(gradedEvent, question);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveQuestionFromGradedEventNotIn()
    {
        System.out.println("removeQuestionFromGradedEvent - GradedEvent not in DB");
        assertTrue(instance.isOpen());
        
        GradedEvent gradedEvent = new GradedEvent();
        gradedEvent.setId(new Long(333L));
        gradedEvent.setAssignment("PEX Something");
        gradedEvent.setTerm("Summer 2011");
        gradedEvent.setVersion("ALL");
        
        Course course = new Course("CS110");
        course.setId(new Long(1L));
        gradedEvent.setCourse(course);
        
        Question question = new Question();
        question.setId(new Long(1L));
        question.setName("Sequencer");
        
        instance.removeQuestionFromGradedEvent(gradedEvent, question);
        
    }
    
    @Test(expected=NullPointerException.class)
    public void testRemoveQuestionFromGradedEventNullQuestion()
    {
        System.out.println("removeQuestionFromGradedEvent - Null Question");
        assertTrue(instance.isOpen());
        
        GradedEvent gradedEvent = instance.find(new Long(5L));
        assertEquals(2, gradedEvent.getQuestions().size());
        
        instance.removeQuestionFromGradedEvent(gradedEvent, null);
    }
    
    @Test(expected=NullPointerException.class)
    public void testRemoveQuestionFromGradedEventNullGradedEvent()
    {
        System.out.println("removeQuestionFromGradedEvent - Null GradedEvent");
        assertTrue(instance.isOpen());
        
        Question question = new Question();
        question.setId(new Long(1L));
        question.setName("Sequencer");
        
        instance.removeQuestionFromGradedEvent(null, question);
    }
    
    @Test
    public void testRemoveQuestionFromGradedEventNullQuestionId()
    {
        System.out.println("removeQuestionFromGradedEvent - Null Question Id");
        assertTrue(instance.isOpen());
        
        GradedEvent gradedEvent = instance.find(new Long(5L));
        assertEquals(2, gradedEvent.getQuestions().size());
        
        Question question = new Question();
        question.setName("Sequencer");
        
        instance.removeQuestionFromGradedEvent(gradedEvent, question);
        
        gradedEvent = instance.find(new Long(5L));
        assertEquals(1, gradedEvent.getQuestions().size());
        
        EntityManager em = testDb.getConnection();
        
        Question testQuestion = em.find(Question.class, new Long(1L));
        assertNotNull(testQuestion);
        
        assertTrue(instance.isOpen());
    }
    
    @Test
    public void testRemoveQuestionFromGradedEventNullGradedEventId()
    {
        System.out.println("removeQuestionFromGradedEvent - Null GradedEvent Id");
        assertTrue(instance.isOpen());
        
        GradedEvent gradedEvent = new GradedEvent();
        gradedEvent.setAssignment("PEX1");
        gradedEvent.setTerm("Summer 2010");
        gradedEvent.setVersion("ALL");
        
        Course course = new Course("CS110");
        course.setId(new Long(1L));
        gradedEvent.setCourse(course);
        
        Question question = new Question();
        question.setId(new Long(1L));
        question.setName("Sequencer");
        
        instance.removeQuestionFromGradedEvent(gradedEvent, question);
        
        gradedEvent = instance.find(new Long(5L));
        assertEquals(1, gradedEvent.getQuestions().size());
        
        EntityManager em = testDb.getConnection();
        
        Question testQuestion = em.find(Question.class, new Long(1L));
        assertNotNull(testQuestion);
        
        assertTrue(instance.isOpen());
    }

    @Test(expected=IllegalArgumentException.class)
    public void testRemoveQuestionFromGradedEventNullGradedEventAssignment()
    {
        System.out.println("removeQuestionFromGradedEvent - Null GradedEvent Assignment");
        assertTrue(instance.isOpen());
        
        GradedEvent gradedEvent = new GradedEvent();
        gradedEvent.setId(new Long(5L));
        gradedEvent.setTerm("Summer 2010");
        gradedEvent.setVersion("ALL");
        
        Course course = new Course("CS110");
        course.setId(new Long(1L));
        gradedEvent.setCourse(course);
        
        Question question = new Question();
        question.setId(new Long(1L));
        question.setName("Sequencer");
        
        instance.removeQuestionFromGradedEvent(gradedEvent, question);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testRemoveQuestionFromGradedEventNullGradedEventTerm()
    {
        System.out.println("removeQuestionFromGradedEvent - Null GradedEvent Term");
        assertTrue(instance.isOpen());
        
        GradedEvent gradedEvent = new GradedEvent();
        gradedEvent.setId(new Long(5L));
        gradedEvent.setAssignment("PEX1");
        gradedEvent.setVersion("ALL");
        
        Course course = new Course("CS110");
        course.setId(new Long(1L));
        gradedEvent.setCourse(course);
        
        Question question = new Question();
        question.setId(new Long(1L));
        question.setName("Sequencer");
        
        instance.removeQuestionFromGradedEvent(gradedEvent, question);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveQuestionFromGradedEventNullGradedEventVersion()
    {
        System.out.println("removeQuestionFromGradedEvent - Null GradedEvent Version");
        assertTrue(instance.isOpen());
        
        GradedEvent gradedEvent = new GradedEvent();
        gradedEvent.setId(new Long(5L));
        gradedEvent.setAssignment("PEX1");
        gradedEvent.setTerm("Summer 2010");
        
        Course course = new Course("CS110");
        course.setId(new Long(1L));
        gradedEvent.setCourse(course);
        
        Question question = new Question();
        question.setId(new Long(1L));
        question.setName("Sequencer");
        
        instance.removeQuestionFromGradedEvent(gradedEvent, question);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveQuestionFromGradedEventNullGradedEventCourse()
    {
        System.out.println("removeQuestionFromGradedEvent - Null GradedEvent Course");
        assertTrue(instance.isOpen());
        
        GradedEvent gradedEvent = new GradedEvent();
        gradedEvent.setId(new Long(5L));
        gradedEvent.setAssignment("PEX1");
        gradedEvent.setTerm("Summer 2010");
        gradedEvent.setVersion("ALL");
        
        Question question = new Question();
        question.setId(new Long(1L));
        question.setName("Sequencer");
        
        instance.removeQuestionFromGradedEvent(gradedEvent, question);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveQuestionFromGradedEventNullQuestionName()
    {
        System.out.println("removeQuestionFromGradedEvent - Null Question Name");
        assertTrue(instance.isOpen());
        
        GradedEvent gradedEvent = instance.find(new Long(5L));
        assertEquals(2, gradedEvent.getQuestions().size());
        
        Question question = new Question();
        question.setId(new Long(1L));
        
        instance.removeQuestionFromGradedEvent(gradedEvent, question);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveQuestionFromGradedEventBlankQuestionName()
    {
        System.out.println("removeQuestionFromGradedEvent - Blank Question Name");
        assertTrue(instance.isOpen());
        
        GradedEvent gradedEvent = instance.find(new Long(5L));
        assertEquals(2, gradedEvent.getQuestions().size());
        
        Question question = new Question();
        question.setId(new Long(1L));
        question.setName("");
        
        instance.removeQuestionFromGradedEvent(gradedEvent, question);
    }
    
    @Test(expected=IllegalStateException.class)
    public void testRemoveQuestionFromGradedEventClosedConnection()
    {
        System.out.println("removeQuestionFromGradedEvent - Closed Connection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        
        GradedEvent gradedEvent = instance.find(new Long(5L));
        assertEquals(2, gradedEvent.getQuestions().size());
        
        Question question = new Question();
        question.setId(new Long(1L));
        question.setName("Sequencer");
        
        instance.removeQuestionFromGradedEvent(gradedEvent, question);
    }
    
    @Test
    public void testRemoveQuestionFromAllGradedEvents()
    {
        System.out.println("removeQuestionFromAllGradedEvents");
        assertTrue(instance.isOpen());
        
        Question question1 = new Question();
        question1.setId(new Long(2L));
        question1.setName("Looper");
        
        Question question2 = new Question();
        question2.setId(new Long(1L));
        question2.setName("Sequencer");
        
        int result1 = instance.removeQuestionFromAllGradedEvents(question1);
        assertEquals(2, result1);
        
        GradedEvent gradedEvent = instance.find(new Long(2L));
        assertEquals(0, gradedEvent.getQuestions().size());
        
        int result2 = instance.removeQuestionFromAllGradedEvents(question2);
        assertEquals(1, result2);
        
        gradedEvent = instance.find(new Long(5L));
        assertEquals(0, gradedEvent.getQuestions().size());
        
        EntityManager em = testDb.getConnection();
        
        Question testQuestion1 = em.find(Question.class, new Long(2L));
        assertNotNull(testQuestion1);
        Question testQuestion2 = em.find(Question.class, new Long(1L));
        assertNotNull(testQuestion2);
        
        assertTrue(instance.isOpen());
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveQuestionFromAllGradedEventsNotFound()
    {
        System.out.println("removeQuestionFromAllGradedEvents - Not in DB");
        assertTrue(instance.isOpen());
        
        Question question1 = new Question();
        question1.setId(new Long(234L));
        question1.setName("Looper 2");
        
        int result1 = instance.removeQuestionFromAllGradedEvents(question1);
    }
    
    @Test(expected=NullPointerException.class)
    public void testRemoveQuestionFromAllGradedEventsNullQuestion()
    {
        System.out.println("removeQuestionFromAllGradedEvents - Null Question");
        assertTrue(instance.isOpen());
        
        int result1 = instance.removeQuestionFromAllGradedEvents(null);
    }
    
    @Test
    public void testRemoveQuestionFromAllGradedEventsNullQuestionId()
    {
        System.out.println("removeQuestionFromAllGradedEvents - Null Question Id");
        assertTrue(instance.isOpen());
        
        Question question1 = new Question();
        question1.setName("Looper");
        
        Question question2 = new Question();
        question2.setName("Sequencer");
        
        int result1 = instance.removeQuestionFromAllGradedEvents(question1);
        assertEquals(2, result1);
        
        GradedEvent gradedEvent = instance.find(new Long(2L));
        assertEquals(0, gradedEvent.getQuestions().size());
        
        int result2 = instance.removeQuestionFromAllGradedEvents(question2);
        assertEquals(1, result2);
        
        gradedEvent = instance.find(new Long(5L));
        assertEquals(0, gradedEvent.getQuestions().size());
        
        EntityManager em = testDb.getConnection();
        
        Question testQuestion1 = em.find(Question.class, new Long(2L));
        assertNotNull(testQuestion1);
        Question testQuestion2 = em.find(Question.class, new Long(1L));
        assertNotNull(testQuestion2);
        
        assertTrue(instance.isOpen());
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveQuestionFromAllGradedEventsNullQuestionName()
    {
        System.out.println("removeQuestionFromAllGradedEvents - Null Question Name");
        assertTrue(instance.isOpen());
        
        Question question1 = new Question();
        question1.setId(new Long(2L));
        
        int result1 = instance.removeQuestionFromAllGradedEvents(question1);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveQuestionFromAllGradedEventsBlankQuestionName()
    {
        System.out.println("removeQuestionFromAllGradedEvents - Blank Question Name");
        assertTrue(instance.isOpen());
        
        Question question1 = new Question();
        question1.setId(new Long(2L));
        question1.setName("");
        
        int result1 = instance.removeQuestionFromAllGradedEvents(question1);
    }
    
    @Test(expected=IllegalStateException.class)
    public void testRemoveQuestionFromAllGradedEventsClosedConnection()
    {
        System.out.println("removeQuestionFromAllGradedEvents - Closed Connection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        
        Question question1 = new Question();
        question1.setId(new Long(2L));
        question1.setName("Looper");
        
        int result1 = instance.removeQuestionFromAllGradedEvents(question1);
    }
    
    private GradedEventDAO instance;
    private static TestDatabase testDb;
}
