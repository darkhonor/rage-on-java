/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataimport;

import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.Instructor;
import com.darkhonor.rage.model.Section;
import com.darkhonor.rage.model.Student;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit tests for the ImportExcel2007 class
 * 
 * @author Alex Ackerman
 */
public class ImportExcel2007Test
{

    public ImportExcel2007Test()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    /**
     * Test of importCourse method, of class ImportExcel2007.
     */
    @Test
    public void testImportCourse()
    {
        System.out.println("importCourse");
        Course checkCourse = createCheckCourse();
        //String filename = "";
        //ImportExcel2007 instance = new ImportExcel2007();
        //Course expResult = null;
        //Course result = instance.importCourse(filename);
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of importSections method, of class ImportExcel2007.
     */
    @Ignore
    @Test
    public void testImportSections()
    {
        System.out.println("importSections");
        XSSFSheet sectionSheet = null;
        Course course = null;
        ImportExcel2007 instance = new ImportExcel2007();
        List expResult = null;
        List result = instance.importSections(sectionSheet, course);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of importInstructors method, of class ImportExcel2007.
     */
    @Ignore
    @Test
    public void testImportInstructors()
    {
        System.out.println("importInstructors");
        XSSFSheet instructorSheet = null;
        List<Section> sections = null;
        Course course = null;
        ImportExcel2007 instance = new ImportExcel2007();
        List expResult = null;
        List result = instance.importInstructors(instructorSheet, sections, course);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of importStudents method, of class ImportExcel2007.
     */
    @Ignore
    @Test
    public void testImportStudents()
    {
        System.out.println("importStudents");
        XSSFSheet studentSheet = null;
        List<Section> sections = null;
        Course course = null;
        ImportExcel2007 instance = new ImportExcel2007();
        List expResult = null;
        List result = instance.importStudents(studentSheet, sections, course);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    private Course createCheckCourse()
    {
        Course course = new Course();
        course.setName("CS110");

        ArrayList<Section> sections =
                (ArrayList<Section>)createCheckSectionList(course);
        System.out.println("Course has " + course.getSections().size() + " sections");

        ArrayList<Instructor> instructors = 
                (ArrayList<Instructor>)createCheckInstructorList(course);
        System.out.println("Course has " + course.getInstructors().size() + " instructors");
        return course;
    }

    private List<Section> createCheckSectionList(Course course)
    {
        List<Section> sections = new ArrayList<Section>();
        
        Section section1 = new Section("M1A");
        section1.setCourse(course);
        sections.add(section1);
        course.addSection(section1);

        Section section2 = new Section("M1B");
        section2.setCourse(course);
        sections.add(section2);
        course.addSection(section2);

        Section section3 = new Section("M2A");
        section3.setCourse(course);
        sections.add(section3);
        course.addSection(section3);

        Section section4 = new Section("M3A");
        section4.setCourse(course);
        sections.add(section4);
        course.addSection(section4);

        Section section5 = new Section("T1A");
        section5.setCourse(course);
        sections.add(section5);
        course.addSection(section5);

        Section section6 = new Section("T6A");
        section6.setCourse(course);
        sections.add(section6);
        course.addSection(section6);
        
        Section section7 = new Section("T6B");
        section7.setCourse(course);
        sections.add(section7);
        course.addSection(section7);

        return sections;
    }

    private List<Instructor> createCheckInstructorList(Course course)
    {
        List<Instructor> instructors = new ArrayList<Instructor>();
        Instructor instructor1 = new Instructor("Alexander", "Ackerman", "Alexander.Ackerman");
        instructor1.setDomainAccount(instructor1.getWebID());
        Section section1;

        return instructors;
    }

    private List<Student> createCheckStudentList(Course course, List<Section> sections)
    {
        List<Student> students = new ArrayList<Student>();

        return students;
    }

    private static final String FILENAME = "testFiles" +
            System.getProperty("file.separator") + "TestCourse.xlsx";
    Course instance;
}