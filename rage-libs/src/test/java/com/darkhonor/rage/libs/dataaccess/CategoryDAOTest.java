/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs.dataaccess;

import com.darkhonor.rage.libs.dataaccess.testdb.TestDatabase;
import com.darkhonor.rage.model.Category;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit tests of the CategoryDAO class.
 * 
 * @author Alex Ackerman
 */
public class CategoryDAOTest
{

    public CategoryDAOTest()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        testDb = new TestDatabase();
        testDb.initialize();
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
        testDb.shutdown();
    }

    @Before
    public void setUp()
    {
        instance = new CategoryDAO(testDb.getConnection());
    }

    @After
    public void tearDown()
    {
        if (instance != null && instance.isOpen())
        {
            instance.closeConnection();
        }
        testDb.reset();
    }

    /**
     * Test of closeConnection method, of class CategoryDAO.
     */
    @Test
    public void testCloseConnection()
    {
        System.out.println("closeConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }

    @Test(expected = IllegalStateException.class)
    public void testCloseConnectionAlreadyClosed()
    {
        System.out.println("closeConnectionAlreadyClosed");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.closeConnection();
    }

    /**
     * Test of isOpen method, of class CategoryDAO.
     */
    @Test
    public void testIsOpen()
    {
        System.out.println("isOpen");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
    }

    /**
     * Test of find method, of class CategoryDAO.
     */
    @Test
    public void testFindId()
    {
        System.out.println("findId");
        assertTrue(instance.isOpen());
        Long resultId = new Long(2L);
        Category result = instance.find(resultId);
        assertEquals("Loop", result.getName());
        Long expectedId = new Long(resultId);
        assertEquals(expectedId, result.getId());
    }

    @Test
    public void testFindIdNoResult()
    {
        System.out.println("findIdNoResult");
        assertTrue(instance.isOpen());
        Long resultId = new Long(12L);
        Category result = instance.find(resultId);
        assertNull(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindIdLessThanZero()
    {
        System.out.println("findIdLessThanZero");
        assertTrue(instance.isOpen());
        Long resultId = new Long(-3L);
        Category result = instance.find(resultId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindIdZero()
    {
        System.out.println("findIdZero");
        assertTrue(instance.isOpen());
        Long resultId = new Long(0L);
        Category result = instance.find(resultId);
    }

    @Test(expected = NullPointerException.class)
    public void testFindIdNull()
    {
        System.out.println("findIdNull");
        assertTrue(instance.isOpen());
        Long nullId = null;
        Category result = instance.find(nullId);
    }

    @Test(expected = IllegalStateException.class)
    public void testFindIdClosedConnection()
    {
        System.out.println("findIdClosedConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Long resultId = new Long(2L);
        Category result = instance.find(resultId);
    }

    /**
     * Test of find method, of class CategoryDAO.
     */
    @Test
    public void testFindName()
    {
        System.out.println("findName");
        assertTrue(instance.isOpen());
        String name = "Loop";
        Category result = instance.find(name);
        assertEquals("Loop", result.getName());
        Long expectedId = new Long(2L);
        assertEquals(expectedId, result.getId());
    }

    @Test(expected = NullPointerException.class)
    public void testFindNameNull()
    {
        System.out.println("findNameNull");
        assertTrue(instance.isOpen());
        String name = null;
        Category result = instance.find(name);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindNameBlank()
    {
        System.out.println("findNameBlank");
        assertTrue(instance.isOpen());
        String name = "";
        Category result = instance.find(name);
    }

    @Test
    public void testFindNameNoResult()
    {
        System.out.println("findNameNoResult");
        assertTrue(instance.isOpen());
        String name = "Bob Rocks";
        Category result = instance.find(name);
        assertNull(result);
    }

    @Test(expected = IllegalStateException.class)
    public void testFindNameClosedConnection()
    {
        System.out.println("findNameClosedConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        String name = "Iteration";
        Category result = instance.find(name);
    }

    /**
     * Test of create method, of class CategoryDAO.
     */
    @Test
    public void testCreate()
    {
        System.out.println("create");
        assertTrue(instance.isOpen());
        List<Category> resultList = instance.getAllCategoriesByName();
        assertNotNull(resultList);
        assertEquals(3, resultList.size());
        Category newCategory = new Category("Bob");
        newCategory.setId(new Long(4L));
        Long newId = instance.create(newCategory);
        Long expectedId = new Long(4L);
        assertEquals(expectedId, newId);
        resultList = instance.getAllCategoriesByName();
        assertNotNull(resultList);
        assertEquals(4, resultList.size());
        Category testResult = instance.find(4L);
        assertNotNull(testResult);
        Long resultId = new Long(4L);
        assertEquals(resultId, testResult.getId());
        assertEquals("Bob", testResult.getName());
    }

    @Test(expected = NullPointerException.class)
    public void testCreateNullCategory()
    {
        System.out.println("createNullCategory");
        assertTrue(instance.isOpen());
        Category testCat = null;
        Long newId = instance.create(testCat);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateNullCategoryName()
    {
        System.out.println("createNullCategoryName");
        assertTrue(instance.isOpen());
        Category testCat = new Category();
        Long newId = instance.create(testCat);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateExistingCategory()
    {
        System.out.println("createExistingCategory");
        assertTrue(instance.isOpen());
        Category newCat = new Category("Array");
        newCat.setId(new Long(1L));
        Long newId = instance.create(newCat);
    }

    @Test(expected = IllegalStateException.class)
    public void testCreateClosedConnection()
    {
        System.out.println("createClosedConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Category testCat = new Category("Bob is Cool");
        testCat.setId(new Long(5L));
        Long newId = instance.create(testCat);
    }

    /**
     * Test of getAllCategoriesByName method, of class CategoryDAO.
     */
    @Test
    public void testGetAllCategoriesByName()
    {
        System.out.println("getAllCategoriesByName");
        assertTrue(instance.isOpen());
        Category newCat = new Category("Bob");
        newCat.setId(5L);
        instance.create(newCat);
        List<Category> resultList = instance.getAllCategoriesByName();
        assertNotNull(resultList);
        assertEquals(4, resultList.size());
        assertEquals("Array", resultList.get(0).getName());
        assertEquals("Bob", resultList.get(1).getName());
        assertEquals("Iteration", resultList.get(2).getName());
        assertEquals("Loop", resultList.get(3).getName());
    }

    @Test(expected=IllegalStateException.class)
    public void testGetAllCategoriesByNameClosedConnection()
    {
        System.out.println("getAllCategoriesByNameClosedConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        List<Category> resultList = instance.getAllCategoriesByName();
    }

    /**
     * Test of delete method, of class CategoryDAO.
     */
    @Test
    public void testDelete()
    {
        System.out.println("delete");
        assertTrue(instance.isOpen());
        Category category = new Category("Iteration");
        category.setId(new Long(3L));
        instance.delete(category);
        Category result = instance.find(3L);
        assertNull(result);
        result = instance.find("Iteration");
        assertNull(result);
        List<Category> resultList = instance.getAllCategoriesByName();
        assertNotNull(resultList);
        assertEquals(2, resultList.size());
    }

    @Test(expected = IllegalStateException.class)
    public void testDeleteClosedConnection()
    {
        System.out.println("deleteClosedConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        Category category = new Category("Iteration");
        category.setId(new Long(3L));
        instance.delete(category);
    }

    @Test(expected = NullPointerException.class)
    public void testDeleteNullCategory()
    {
        System.out.println("deleteNullCategory");
        assertTrue(instance.isOpen());
        Category category = null;
        instance.delete(category);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteNullCategoryName()
    {
        System.out.println("deleteNullCategoryName");
        assertTrue(instance.isOpen());
        Category category = new Category();
        category.setId(new Long(3L));
        instance.delete(category);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteEntityNotExist()
    {
        System.out.println("deleteEntityNotExist");
        assertTrue(instance.isOpen());
        Category category = new Category("Bob");
        category.setId(new Long(5L));
        instance.delete(category);
    }

    /**
     * Test of delete method, of class CategoryDAO.
     */
    @Test
    public void testDeleteId()
    {
        System.out.println("deleteId");
        assertTrue(instance.isOpen());
        Long id = new Long(3L);
        instance.delete(id);
        Category result = instance.find(id);
        assertNull(result);
        result = instance.find("Iteration");
        assertNull(result);
        List<Category> resultList = instance.getAllCategoriesByName();
        assertNotNull(resultList);
        assertEquals(2, resultList.size());
    }

    @Test(expected = IllegalStateException.class)
    public void testDeleteIdClosedConnection()
    {
        System.out.println("deleteIdClosedConnection");
        assertTrue(instance.isOpen());
        instance.closeConnection();
        assertFalse(instance.isOpen());
        instance.delete(new Long(3L));
    }

    @Test(expected = NullPointerException.class)
    public void testDeleteNullId()
    {
        System.out.println("deleteNullId");
        assertTrue(instance.isOpen());
        Long id = null;
        instance.delete(id);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteZeroId()
    {
        System.out.println("deleteZeroId");
        assertTrue(instance.isOpen());
        Long id = new Long(0L);
        instance.delete(id);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteNegativeId()
    {
        System.out.println("deleteNegativeId");
        assertTrue(instance.isOpen());
        Long id = new Long(-5L);
        instance.delete(id);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteIdEntityNotExist()
    {
        System.out.println("deleteIdEntityNotExist");
        assertTrue(instance.isOpen());
        Long id = new Long(42L);
        instance.delete(id);
    }

    /**
     * Test of update method, of class CategoryDAO.
     */
    @Test
    public void testUpdate()
    {
        System.out.println("update");
        assertTrue(instance.isOpen());
        Long resultId = new Long(2L);
        Category category = instance.find(resultId);
        assertEquals("Loop", category.getName());
        category.setName("Loop and Array");
        Category result = instance.update(category);
        assertNotNull(result);
        assertEquals(category, result);
        assertEquals(resultId, result.getId());
        assertEquals("Loop and Array", result.getName());
    }

    @Test(expected=NullPointerException.class)
    public void testUpdateNullCategory()
    {
        System.out.println("updateNullCategory");
        assertTrue(instance.isOpen());
        Category result = null;
        result = instance.update(result);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testUpdateNullCategoryName()
    {
        System.out.println("updateNullCategoryName");
        assertTrue(instance.isOpen());
        Category category = instance.find(new Long(2L));
        category = new Category();
        category.setId(new Long(2L));
        category = instance.update(category);
    }

    @Test(expected=IllegalStateException.class)
    public void testUpdateClosedConnection()
    {
        System.out.println("updateClosedConnection");
        assertTrue(instance.isOpen());
        Category category = instance.find(new Long(1L));
        category.setName("Infinite Loop");
        instance.closeConnection();
        assertFalse(instance.isOpen());
        category = instance.update(category);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testUpdateEntityNotExist()
    {
        System.out.println("updateEntityNotExist");
        assertTrue(instance.isOpen());
        Category category = new Category("Loop and Array");
        category.setId(new Long(33L));
        instance.update(category);
    }
    
    private static TestDatabase testDb;
    private CategoryDAO instance;
}
