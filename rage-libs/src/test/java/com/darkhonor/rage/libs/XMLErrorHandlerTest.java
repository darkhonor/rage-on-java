/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs;

import org.junit.*;
import static org.junit.Assert.*;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author Alexander Ackerman <alex@darkhonor.com>
 */
public class XMLErrorHandlerTest {

    /**
     * Constructor
     */
    public XMLErrorHandlerTest()
    {
    }

    @Before
    public void setUp() {
        handler = new XMLErrorHandler();
        error = new SAXParseException("Invalid XML", "RAGE", "SYSTEM", 17, 33);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of warning method, of class XMLErrorHandler.
     *
     * @throws org.xml.sax.SAXException
     */
    @Test(expected=SAXException.class)
    public void testWarning() throws SAXException
    {
        System.out.println("warning");
        handler.warning(error);
        fail("SAXException didn't get thrown.");
    }

    /**
     * Test of error method, of class XMLErrorHandler.
     *
     * @throws org.xml.sax.SAXException
     */
    @Test(expected=SAXException.class)
    public void testError() throws SAXException
    {
        System.out.println("error");
        handler.error(error);
        fail("SAXException didn't get thrown.");
    }

    /**
     * Test of fatalError method, of class XMLErrorHandler.
     *
     * @throws org.xml.sax.SAXException
     */
    @Test(expected=SAXException.class)
    public void testFatalError() throws SAXException
    {
        System.out.println("fatalError");
        handler.fatalError(error);
        fail("SAXException didn't get thrown.");
    }

    private ErrorHandler handler;
    private SAXParseException error;
}