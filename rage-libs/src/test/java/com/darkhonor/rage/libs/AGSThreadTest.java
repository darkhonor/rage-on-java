/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.libs;

import com.darkhonor.rage.model.Category;
import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.GradedEvent;
import com.darkhonor.rage.model.Instructor;
import com.darkhonor.rage.model.Question;
import com.darkhonor.rage.model.Section;
import com.darkhonor.rage.model.Student;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit 4 test of the AGSThread class.  This test depends on the existence of
 * the Apache Derby database with the test cases loaded as well as the
 * <tt>META-INF/persistence.xml</tt> file.
 * 
 * @author Alexander Ackerman <alex@darkhonor.com>
 */
public class AGSThreadTest
{

    /**
     * Constructor
     */
    public AGSThreadTest()
    {
    }

    /**
     * Setup method that will run when the test harness is first run.  Establishes
     * an EntityManagerFactory based upon the test information stored in
     * META-INF/persistence.xml.
     *
     * @throws java.lang.Exception  When anything happens with the setup
     */
    @Ignore
    @BeforeClass
    public static void setUpClass() throws Exception 
    {
        Map<String,String> dbProperties = new HashMap<String,String>();
        // Derby Memory Only
        dbProperties.put("javax.persistence.jdbc.driver", RAGEConst.DERBY_EMBED_DRIVER);
        dbProperties.put("javax.persistence.jdbc.url", "jdbc:derby:memory:testdb;create=true");
        dbProperties.put("javax.persistence.jdbc.user", "sa");
        dbProperties.put("javax.persistence.jdbc.password", "");

        emf = Persistence.createEntityManagerFactory("rage-libs", dbProperties);
    }

    /**
     * Tear down method that will run when the test harness is complete.  If
     * the EntityManagerFactory is still open, it will close the Factory.
     *
     * @throws java.lang.Exception  Whenever an error occurs.
     */
    @Ignore
    @AfterClass
    public static void tearDownClass() throws Exception
    {
        if (emf != null && emf.isOpen())
            emf.close();
    }

    /**
     * Setup method for each test.  Establishes a baseline for each test case to
     * work with.  Creates several InputStream and OutputStream objects to enable
     * communications between the test and the AGSServer threads.  The input stream
     * of the thread is connected to the output stream of the test.  The output
     * stream of the thread is connected to the input stream of the test.  Once
     * the streams are established and connected the server thread is started.
     */
    @Ignore
    @Before
    public void setUp()
    {
        Course course = createTestCourse();
        in = new PipedInputStream();
        out = new PipedOutputStream();
        readStream = new PipedInputStream();
        writeStream = new PipedOutputStream();

        try
        {
            in.connect(writeStream);
            readStream.connect(out);
        }
        catch (IOException ex)
        {
            System.err.println("ERROR: IOException caught");
        }

        instance = new AGSThread(in, out, emf.createEntityManager());
        client = new Thread(instance);
        client.start();
    }

    /**
     * Tear down method to be run after every test.  If the AGSServer thread is
     * still running, go ahead and interrupt.  Would prefer to actually kill it,
     * but there is no method to kill it at this time.
     */
    @Ignore
    @After
    public void tearDown()
    {
        try
        {
            in.close();
            out.close();
            readStream.close();
            writeStream.close();
        }
        catch (IOException ex)
        {
            System.err.println("ERROR: Streams not closed properly");
        }
        if (client.isAlive())
            client.interrupt();
    }

    /**
     * Test of RAPTOR Protocol: DIRECTORY
     *
     * Request the list of available Raptor questions.  Should receive two
     * responses (Bits and Odds) followed by the string "EOF".
     */
    @Ignore
    @Test
    public void runDirectory()
    {
        char buf[] = new char[20];

        System.out.println("runDirectory");
        PrintWriter pw = new PrintWriter(writeStream);
        InputStreamReader isr = new InputStreamReader(readStream);
        //Scanner scanner = new Scanner(readStream);
        // Specify the delimeter to use with the stream: This HAS to be used for
        // platform compatibility between the RAPTOR client and the server.
        // TODO: This needs to be tested so it doesn't get broken accidentally
        //       However, will probably have to turn to a char array rather than
        //       a Scanner to get the bytes.
        //scanner.useDelimiter("\r\n");
        
        // Write to the input stream: "DIRECTORY" followed by "\r\n"
        pw.print("DIRECTORY");
        pw.print("\r\n");
        pw.flush();


        // Read the contents of the output stream.  Ensure you receive 3 strings:
        // "Bits"
        // "Odds"
        // "EOF"
        // All should be followed by "\r\n".
        try
        {
            Thread.sleep(100);
            isr.read(buf);
            StringBuilder sb = new StringBuilder();
            sb.append(buf);
            assertTrue("BITS not received: " + sb.substring(0, 4),
                    "BITS".equals(sb.substring(0, 4)));
            assertTrue("CR LF 1 not received", "\r\n".equals(sb.substring(4, 6)));
            assertTrue("ODDS not received: " + sb.substring(6, 10),
                    "ODDS".equals(sb.substring(6, 10)));
            assertTrue("CR LF 2 not received", "\r\n".equals(sb.substring(10, 12)));
            assertTrue("EOF not received: " + sb.substring(12, 15),
                    "EOF".equals(sb.substring(12, 15)));
            assertTrue("CR LF 3 not received", "\r\n".equals(sb.substring(15, 17)));
            isr.close();
        }
        catch (IOException ex)
        {
            fail("Unexpected IOException caught.");
        }
        catch (InterruptedException ex2)
        {
            fail("Thread communication took longer than 100 miliseconds");
        }
        pw.close();
    }

    @Ignore
    @Test
    public void runBadDirectoryRequest()
    {
        System.out.println("runBadDirectoryRequest");
        PrintWriter pw = new PrintWriter(writeStream);
        Scanner scanner = new Scanner(readStream);
        scanner.useDelimiter("\r\n");

        // Write to the input stream: "DIRCTORY" followed by "\r\n"
        pw.print("DIRCTORY");
        pw.print("\r\n");
        pw.flush();
        
        // Read the contents of the output stream.  Ensure you receive 1 string:
        // "INVALID COMMAND OR ASSIGNMENT"
        // followed by "\r\n".  If not, fail.
        assertTrue(scanner.hasNext());
        assertEquals("INVALID COMMAND OR ASSIGNMENT", scanner.next());
        assertFalse(scanner.hasNext());
        scanner.close();
        pw.close();
    }

    @Ignore
    @Test
    public void runCorrectSingleInputTest()
    {
        System.out.println("runCorrectSingleInputTest");
        PrintWriter pw = new PrintWriter(writeStream);
        Scanner scanner = new Scanner(readStream);
        scanner.useDelimiter("\r\n");

        // Write to the input stream: "BITS"
        pw.print("BITS");
        pw.print("\r\n");
        pw.flush();

        // Verify the server sends the number of test cases and then the inputs
        // for the first test case:
        // Expected Number of test Cases: 2
        // Input: 500000 or 65465
        assertTrue(scanner.hasNext());
        assertEquals("2", scanner.next());

        ArrayList<String> inputs = new ArrayList<String>();
        inputs.add("500000");
        inputs.add("65465");

        // There are 2 test cases, run the test for both
        for (int i = 0; i < 2; i++)
        {
            assertTrue(scanner.hasNext());
            String input = scanner.next();
            assertTrue(inputs.contains(input));
            assertTrue(scanner.hasNext());
            assertEquals("EOF", scanner.next());
            //System.out.println("After line check: " + scanner.hasNext());
            //assertFalse(scanner.hasNext());

            if (input.equals("500000"))
            {
                pw.print("61.0352 KB");
                pw.print("\r\n");
                pw.print("EOF\r\n");
                pw.flush();
            }
            else if (input.equals("65465"))  // input is "65465"
            {
                pw.print("7.9913 KB");
                pw.print("\r\n");
                pw.print("EOF\r\n");
                pw.flush();
            }
            else
            {
                fail("ERROR: Incorrect input received from the server");
            }

            assertTrue(scanner.hasNext());
            assertEquals("CORRECT", scanner.next());
        }
        scanner.close();
    }

    @Ignore
    @Test
    public void runCorrectMultipleInputTest()
    {
        System.out.println("runCorrectMultipleInputTest");
        PrintWriter pw = new PrintWriter(writeStream);
        Scanner scanner = new Scanner(readStream);
        scanner.useDelimiter("\r\n");

        // Write to the input stream: "ODDS"
        pw.print("ODDS");
        pw.print("\r\n");
        pw.flush();

        assertTrue(scanner.hasNext());
        assertEquals("3", scanner.next());

        ArrayList<String> inputs = new ArrayList<String>();
        // Test Case 1
        inputs.add("5");
        inputs.add("4");

        // Test Case 2
        inputs.add("1");
        inputs.add("3");
        inputs.add("2");

        // Test Case 3
        inputs.add("6");

        for (int i = 0; i < 3; i++)
        {
            assertTrue(scanner.hasNext());
            String input = scanner.next();
            assertTrue(inputs.contains(input));
            if (input.equals("5"))
            {
                assertTrue(scanner.hasNext());
                assertEquals("4", scanner.next());
                assertEquals("EOF", scanner.next());

                pw.print("1\r\n");
                pw.print("EOF\r\n");
                pw.flush();
            }
            else if (input.equals("1"))
            {
                assertTrue(scanner.hasNext());
                assertEquals("3", scanner.next());
                assertEquals("2", scanner.next());
                assertEquals("EOF", scanner.next());

                pw.print("2\r\n");
                pw.print("EOF\r\n");
                pw.flush();
            }
            else if (input.equals("6"))
            {
                assertTrue(scanner.hasNext());
                assertEquals("EOF", scanner.next());

                pw.print("0\r\n");
                pw.print("EOF\r\n");
                pw.flush();
            }
            else
            {
                fail("ERROR: Unexpected first input from the server.");
            }

            assertTrue(scanner.hasNext());
            assertEquals("CORRECT", scanner.next());
        }

        scanner.close();
        pw.close();
    }

    @Ignore
    @Test
    public void runIncorrectSingleTest()
    {
        System.out.println("runIncorrectSingleTest");
        PrintWriter pw = new PrintWriter(writeStream);
        Scanner scanner = new Scanner(readStream);
        scanner.useDelimiter("\r\n");

        // Write to the input stream: "BITS"
        pw.print("BITS");
        pw.print("\r\n");
        pw.flush();

        // Verify the server sends the number of test cases and then the inputs
        // for the first test case:
        // Expected Number of test Cases: 2
        // Input: 500000 or 65465
        assertTrue(scanner.hasNext());
        assertEquals("2", scanner.next());

        ArrayList<String> inputs = new ArrayList<String>();
        inputs.add("500000");
        inputs.add("65465");

        // There are 2 test cases, run the test for both
        for (int i = 0; i < 2; i++)
        {
            assertTrue(scanner.hasNext());
            String input = scanner.next();
            assertTrue(inputs.contains(input));
            assertTrue(scanner.hasNext());
            assertEquals("EOF", scanner.next());
            //System.out.println("After line check: " + scanner.hasNext());
            //assertFalse(scanner.hasNext());

            if (input.equals("500000"))
            {
                pw.print("61.0351KB");
                pw.print("\r\n");
                pw.print("EOF\r\n");
                pw.flush();
            }
            else if (input.equals("65465")) // input is "65465"
            {
                pw.print("7.9914KB");
                pw.print("\r\n");
                pw.print("EOF\r\n");
                pw.flush();
            }
            else
            {
                fail("ERROR: Incorrect input received from the server");
            }

            assertTrue(scanner.hasNext());
            assertEquals("INCORRECT", scanner.next());
        }
        scanner.close();
        pw.close();
    }

    @Ignore
    @Test
    public void runCorrectIncorrectSingleTest()
    {
        System.out.println("runCorrectIncorrectSingleTest");
        PrintWriter pw = new PrintWriter(writeStream);
        Scanner scanner = new Scanner(readStream);
        scanner.useDelimiter("\r\n");

        // Write to the input stream: "BITS"
        pw.print("BITS");
        pw.print("\r\n");
        pw.flush();

        // Verify the server sends the number of test cases and then the inputs
        // for the first test case:
        // Expected Number of test Cases: 2
        // Input: 500000 or 65465
        assertTrue(scanner.hasNext());
        assertEquals("2", scanner.next());

        ArrayList<String> inputs = new ArrayList<String>();
        inputs.add("500000");
        inputs.add("65465");

        // There are 2 test cases, run the test for both
        for (int i = 0; i < 2; i++)
        {
            assertTrue(scanner.hasNext());
            String input = scanner.next();
            assertTrue(inputs.contains(input));
            assertTrue(scanner.hasNext());
            assertEquals("EOF", scanner.next());
            //System.out.println("After line check: " + scanner.hasNext());
            //assertFalse(scanner.hasNext());

            if (input.equals("500000"))
            {
                pw.print("61.0352 KB");
                pw.print("\r\n");
                pw.print("EOF\r\n");
                pw.flush();
                assertTrue(scanner.hasNext());
                assertEquals("CORRECT", scanner.next());
            }
            else if (input.equals("65465")) // input is "65465"
            {
                pw.print("7.9914KB");
                pw.print("\r\n");
                pw.print("EOF\r\n");
                pw.flush();
                assertTrue(scanner.hasNext());
                assertEquals("INCORRECT", scanner.next());
            }
            else
            {
                fail("ERROR: Incorrect input received from the server");
            }
        }
        scanner.close();
        pw.close();
    }

    /**
     * Utility method to create the same course for testing.
     * @return      The created Course;
     */
    protected Course createTestCourse()
    {
        Course course = new Course("CS110");
        // Create 2 Sections, 2 Students per Section, and 2 Instructors
        Student student1 = new Student("John", "Doe", "C13John.Doe",
                new Integer(2013));
        Student student2 = new Student("Mary", "Johnson", "C13Mary.Johnson",
                new Integer(2013));
        Student student3 = new Student("Bob", "Reimer", "C13Robert.Reimer",
                new Integer(2013));
        Student student4 = new Student("Andrew", "Callow", "C13Andrew.Callow",
                new Integer(2013));
        Section section1 = new Section("M1A");
        Section section2 = new Section("M2B");
        Instructor instructor1 = new Instructor("David", "Roberts", "David.Roberts");
        instructor1.setDomainAccount(instructor1.getWebID());
        Instructor instructor2 = new Instructor("Sarah", "O'Reilly", "Sarah.OReilly");
        instructor2.setDomainAccount(instructor2.getWebID());

        // Set the Instructors for Sections 1 & 2
        section1.setInstructor(instructor1);
        section2.setInstructor(instructor2);

        // Add Instructors to Course
        course.addInstructor(instructor1);
        course.addInstructor(instructor2);

        // Add Students 1 & 2 to Section 1
        section1.addStudent(student1);
        student1.addSection(section1);
        section1.addStudent(student2);
        student2.addSection(section1);

        // Add Students 3 & 4 to Section 2
        section2.addStudent(student3);
        student3.addSection(section2);
        section2.addStudent(student4);
        student4.addSection(section2);

        // Add Sections 1 & 2 to Course
        section1.setCourse(course);
        section2.setCourse(course);
        course.addSection(section1);
        course.addSection(section2);

        return course;
    }

    /**
     * Utility method to create a different course for testing.  This course
     * has 1 instructor, 2 sections, and 5 students.
     *
     * @param name      The name of the Course
     * @return          The created Course object
     */
    protected Course createDiffTestCourse(String name)
    {
        Course course = new Course(name);

        // Create 2 Sections, 2 Students in 1 section, 3 in the other,
        // and 2 Instructors
        Student student1 = new Student("John", "Doe", "C11John.Doe",
                new Integer(2011));
        Student student2 = new Student("Mary", "Johnson", "C11Mary.Johnson",
                new Integer(2011));
        Student student3 = new Student("Bob", "Reimer", "C13Robert.Reimer",
                new Integer(2013));
        Student student4 = new Student("Andrew", "Callow", "C13Andrew.Callow",
                new Integer(2013));
        Student student5 = new Student("Kerry", "Beevers", "C11Kerry.Beevers",
                new Integer(2011));
        Section section1 = new Section("M2A");
        Section section2 = new Section("T1A");
        Instructor instructor1 = new Instructor("David", "Roberts", "David.Roberts");
        instructor1.setDomainAccount(instructor1.getWebID());

        // Set the Instructors for Sections 1 & 2
        section1.setInstructor(instructor1);
        section2.setInstructor(instructor1);

        // Add Instructor to Course
        course.addInstructor(instructor1);

        // Add Students 1 & 2 to Section 1
        section1.addStudent(student1);
        student1.addSection(section1);
        section1.addStudent(student2);
        student2.addSection(section1);

        // Add Students 3, 4, & 5 to Section 2
        section2.addStudent(student3);
        student3.addSection(section2);
        section2.addStudent(student4);
        student4.addSection(section2);
        section2.addStudent(student5);
        student5.addSection(section2);

        // Add Sections 1 & 2 to Course
        section1.setCourse(course);
        section2.setCourse(course);
        course.addSection(section1);
        course.addSection(section2);

        return course;
    }

    protected void createPersistedGradedEvent(Course course, String name)
    {
        GradedEvent gevent = new GradedEvent();
        EntityManager em = emf.createEntityManager();
        gevent.setCourse(course);
        gevent.setAssignment(name);
        gevent.setDueDate("29 Apr 2010");
        gevent.setPartialCredit(false);
        gevent.setTerm("Spring 2010");
        gevent.setVersion("ALL");

        Query query = em.createQuery("SELECT c FROM Category c");
        Category dbCat;
        try
        {
            dbCat = (Category) query.getSingleResult();
        } catch (NoResultException ex)
        {
            dbCat = new Category("Array");
            em.getTransaction().begin();
            em.persist(dbCat);
            em.getTransaction().commit();
        }

        query = em.createQuery("SELECT q FROM Question q");
        Question question;
        try
        {
            question = (Question) query.getSingleResult();
        } catch (NoResultException ex)
        {
            question = new Question("Runaway", dbCat, "Test Question");
            em.getTransaction().begin();
            em.persist(question);
            em.getTransaction().commit();
        }
        gevent.addQuestion(question);


        // Save to the database
        em.getTransaction().begin();
        em.persist(gevent);
        em.merge(course);
        em.getTransaction().commit();
    }

    private static EntityManagerFactory emf;
    private PipedInputStream in;
    private PipedInputStream readStream;
    private PipedOutputStream out;
    private PipedOutputStream writeStream;
    private Runnable instance;
    private Thread client;
}