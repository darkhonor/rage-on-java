/**
 * Copyright 2011-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.ags;

import com.darkhonor.rage.libs.AGSThread;
import com.darkhonor.rage.libs.RAGEConst;
import com.darkhonor.rage.libs.dataaccess.VersionDAO;
import com.darkhonor.rage.model.Version;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.apache.log4j.Logger;

/**
 * AutoGrader Server main class.  After parsing any available input parameters,
 * the program spawns several threaded AGSThread objects to handle incoming
 * requests.
 * 
 * @author Alexander Ackerman
 */
public class Server {

    public static final int TEMP_AGS_PORT = 4255; // Used for testing side-by-side
    public static final int AGS_PORT = 4242;
    
    public static int port = AGS_PORT;

    /**
     * Displays a usage message for the program and exits the program with an
     * error message.
     *
     * @param args  The arguments sent to the application
     */
    private static void usage(String[] args)
    {
        System.err.print("Arguments: ");
        for (int i = 0; i < args.length; i++)
        {
            System.err.print(args[i] + " ");
        }
        System.err.println("\n");
        System.err.println("Usage:");
        System.err.println("\tAutoGraderServer.exe -c configFile [-p|--port port]");
        System.err.println("\tAutoGraderServer.exe [-p|--port port] -t|--type type ");
        System.err.println("\t\t-h|--host hostname -dbp dbport -db|--database database");
        System.err.println("\t\t-u|--user username -P|--password password");
        System.err.println();
        System.err.println("\tYou may also start the server using the Java command:");
        System.err.println("\tjava -jar rage-ags.jar [options]");
        System.err.println();
        System.err.println("Arguments:");
        System.err.println("\t-c configFile\t     The configuration file in Java " +
                "Properties");
        System.err.println("\t\t\t\t  format.  See the doc folder for an example");
        System.err.println("\t\t\t\t  configuration file.");
        System.err.println("\t-p|--port port\t    The port for the AutoGrader Server. " +
                "If");
        System.err.println("\t\t\t\t  this is not specified, the default value of ");
        System.err.println("\t\t\t\t  4242 will be used.");
        System.err.println("\t-h|--host hostname\tThe database server hostname");
        System.err.println("\t-t|--type type\t    The database type.  Valid values " +
                "include:");
        System.err.println("\t\t\t\t\tmysql\tMySQL Server");
        System.err.println("\t\t\t\t\tmssql\tMicrosoft SQL Server");
        //System.err.println("\t\t\t\t\toracle       Oracle SQL Server");
        //System.err.println("\t\t\t\t\toracle9      Oracle 9 SQL Server");
        System.err.println("\t\t\t\t\tpgsql\tPostgreSQL Server");
        //System.err.println("\t\t\t\t\tderby\tApache Derby (Embedded)");
        System.err.println("\t\t\t\t\tderby-net    Apache Derby (Network)");
        System.err.println("\t\t\t\t\thsqldb       Hypersonic SQL Database");
        System.err.println("\t-dbp dbport\t       The database port");
        System.err.println("\t-db|--database database   The database name");
        System.err.println("\t-u|--user username\tThe username to connect to the database");
        System.err.println("\t-P|--password password    The password to connect " +
                "to the database");

        // Exit the program with an error condition
        LOGGER.error("Usage message displayed.  Exiting.");
        System.exit(1);
    }

    /**
     * Builds a connection string based upon the type of the database specified.
     * The method is designed to fill the dbProperties map variable with the 
     * appropriate values for hibernate.connection.driver_class,
     * hibernate.connection.url, and hibernate.dialect properties.
     */
    private static void buildConnectionString(Properties props)
    {
        String dbType = props.getProperty("DBType");
        if (dbType.equals("mysql"))
        {
            dbProperties.put("javax.persistence.jdbc.driver",
                    RAGEConst.MYSQL_DRIVER);
            dbProperties.put("javax.persistence.jdbc.url",
                    "jdbc:mysql://" + props.getProperty("DBHost") + ":" +
                    props.getProperty("DBPort", Integer.toString(RAGEConst.MYSQL_PORT)) + 
                    "/" + props.getProperty("DBName"));
        }
        else if (dbType.equals("mssql"))
        {
            dbProperties.put("javax.persistence.jdbc.driver",
                    RAGEConst.MSSQL_DRIVER);
            dbProperties.put("javax.persistence.jdbc.url",
                    "jdbc:sqlserver://" + props.getProperty("DBHost") + ":" +
                    props.getProperty("DBPort", Integer.toString(RAGEConst.MSSQL_PORT)) +
                    ";databaseName=" + props.getProperty("DBName"));
        }
        else if (dbType.equals("derby"))
        {
            dbProperties.put("javax.persistence.jdbc.driver",
                    RAGEConst.DERBY_EMBED_DRIVER);
        }
        else if (dbType.equals("derby-net"))
        {
            dbProperties.put("javax.persistence.jdbc.driver",
                    RAGEConst.DERBY_NETWORK_DRIVER);
            dbProperties.put("javax.persistence.jdbc.url",
                    "jdbc:derby://" + props.getProperty("DBHost") + ":" +
                    props.getProperty("DBPort",
                    Integer.toString(RAGEConst.DERBY_NETWORK_PORT)) +
                    "/" + props.getProperty("DBName"));
        }
        else if (dbType.equals("oracle"))
        {
            dbProperties.put("javax.persistence.jdbc.driver",
                    RAGEConst.ORACLE_DRIVER);
        }
        else if (dbType.equals("oracle9"))
        {
            dbProperties.put("javax.persistence.jdbc.driver",
                    RAGEConst.ORACLE_DRIVER);
        }
        else if (dbType.equals("hsqldb"))
        {
            dbProperties.put("javax.persistence.jdbc.driver",
                    RAGEConst.HSQLDB_DRIVER);
            dbProperties.put("javax.persistence.jdbc.url",
                    "jdbc:hsqldb:hsql://" + props.getProperty("DBHost") + ":" +
                    props.getProperty("DBPort", Integer.toString(RAGEConst.HSQLDB_PORT)) +
                    "/" + props.getProperty("DBName"));
        }
        else if (dbType.equals("pgsql"))
        {
            dbProperties.put("javax.persistence.jdbc.driver",
                    RAGEConst.POSTGRESQL_DRIVER);
            dbProperties.put("javax.persistence.jdbc.url",
                    "jdbc:postgresql://" + props.getProperty("DBHost") + ":" +
                    props.getProperty("DBPort", 
                    Integer.toString(RAGEConst.POSTGRESQL_PORT)) +
                    "/" + props.getProperty("DBName"));
        }
        dbProperties.put("javax.persistence.jdbc.user", props.getProperty("DBUser"));
        dbProperties.put("javax.persistence.jdbc.password", props.getProperty("DBPass"));
        LOGGER.debug("Database Server: " +
                dbProperties.get("javax.persistence.jdbc.url"));
        LOGGER.debug("Database Driver: " +
                dbProperties.get("javax.persistence.jdbc.driver"));
        LOGGER.debug("Database User: " +
                dbProperties.get("javax.persistence.jdbc.user"));
        LOGGER.debug("Database Password: " +
                dbProperties.get("javax.persistence.jdbc.password"));
    }

    /**
     * The main AutoGraderServer application.  Spawns an AGSThread to handle the
     * connection
     * 
     * @param args the command line arguments
     * 
     */
    public static void main(String[] args)
    {

        System.out.println("RAGE AutoGrader Server (v2.1)");
        System.out.println("=============================");
        LOGGER.info("RAGE AutoGrader Server (v2.1)");
        LOGGER.info("=============================");
        LOGGER.debug("Number of arguments: " + args.length);

        Properties props = new Properties();

        if (args.length == 2)
        {
            if (args[0].equals("-c"))
            {
                try
                {
                    LOGGER.info("Loading server configuration information from " +
                            "config file");
                    props.load(new FileInputStream(args[1]));
                }
                catch (IOException ex)
                {
                    LOGGER.error("ERROR: " + ex.getLocalizedMessage());
                    usage(args);
                }
            }
            else
            {
                // Display error message and exit program
                usage(args);
            }
        }
        else if (args.length == 4)
        {
            if (args[0].equals("-c") && (args[2].equals("-p") || args[2].equals("--port")))
            {
                try
                {
                    LOGGER.info("Loading server configuration information from " +
                            "config file");
                    props.load(new FileInputStream(args[1]));
                }
                catch (IOException ex)
                {
                    LOGGER.error("ERROR: " + ex.getLocalizedMessage());
                    usage(args);
                }
                try
                {
                    port = Integer.parseInt(args[3]);
                }
                catch (NumberFormatException ex)
                {
                    LOGGER.info("Problem parsing port number. " +
                            ex.getLocalizedMessage());
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
        }
        else if (args.length == 12)
        {
            // Parse the Database Type
            if (args[0].equals("-t") || args[0].equals("--type"))
            {
                if (!args[1].startsWith("-"))
                {
                    dbProperties.put("DBType", args[1]);
                }
                else
                {
                    // Display error message and exit program
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
            // Parse the Database Hostname
            if (args[2].equals("-h") || args[2].equals("--host"))
            {
                if (!args[3].startsWith("-"))
                {
                    props.setProperty("DBHost", args[3]);
                }
                else
                {
                    // Display error message and exit program
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
            // Parse the Database Port arguments
            if (args[4].equals("-dbp"))
            {
                if (!args[5].startsWith("-"))
                {
                    props.setProperty("DBPort", args[5]);
                }
                else
                {
                    // Display error message and exit program
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
            // Parse the Database name
            if (args[6].equals("-db") || args[6].equals("--database"))
            {
                if (!args[7].startsWith("-"))
                {
                    props.setProperty("DBName", args[7]);
                }
                else
                {
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
            // Parse the Database Username
            if (args[8].equals("-u") || args[8].equals("--user"))
            {
                if (!args[9].startsWith("-"))
                {
                    props.setProperty("DBUser", args[9]);
                }
                else
                {
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
            // Parse the Database Password
            if (args[10].equals("-P") || args[10].equals("--password"))
            {
                if (!args[11].startsWith("-"))
                {
                    props.setProperty("DBPass", args[11]);
                }
                else
                {
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
        }
        else if (args.length == 14)
        {
            // Parse the Database Port
            if (args[0].equals("-p") || args[0].equals("--port"))
            {
                try
                {
                    port = Integer.parseInt(args[1]);
                }
                catch (NumberFormatException ex)
                {
                    LOGGER.info("Problem parsing port number. " +
                            ex.getLocalizedMessage());
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
            // Parse the Database Type
            if (args[2].equals("-t") || args[2].equals("--type"))
            {
                if (!args[3].startsWith("-"))
                {
                    dbProperties.put("DBType", args[3]);
                }
                else
                {
                    // Display error message and exit program
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
            // Parse the Database Hostname
            if (args[4].equals("-h") || args[4].equals("--host"))
            {
                if (!args[5].startsWith("-"))
                {
                    props.setProperty("DBHost", args[5]);
                }
                else
                {
                    // Display error message and exit program
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
            // Parse the Database Port arguments
            if (args[6].equals("-dbp"))
            {
                if (!args[7].startsWith("-"))
                {
                    props.setProperty("DBPort", args[7]);
                }
                else
                {
                    // Display error message and exit program
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
            // Parse the Database name
            if (args[8].equals("-db") || args[8].equals("--database"))
            {
                if (!args[9].startsWith("-"))
                {
                    props.setProperty("DBName", args[9]);
                }
                else
                {
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
            // Parse the Database Username
            if (args[10].equals("-u") || args[10].equals("--user"))
            {
                if (!args[11].startsWith("-"))
                {
                    props.setProperty("DBUser", args[11]);
                }
                else
                {
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
            // Parse the Database Password
            if (args[12].equals("-P") || args[12].equals("--password"))
            {
                if (!args[13].startsWith("-"))
                {
                    props.setProperty("DBPass", args[13]);
                }
                else
                {
                    usage(args);
                }
            }
            else
            {
                usage(args);
            }
        }
        else
        {
            // Display error message and exit program
            usage(args);
        }

        // Build the connection Map from the properties
        buildConnectionString(props);

        // Connect to the database
        try
        {
            LOGGER.debug("Connecting to the database");
            // Log Database connection info (sans password)
            LOGGER.info("URL: " + dbProperties.get("javax.persistence.jdbc.url"));
            LOGGER.info("User: " + dbProperties.get("javax.persistence.jdbc.user"));
            LOGGER.debug("Driver: "
                    + dbProperties.get("javax.persistence.jdbc.driver"));
            emf = Persistence.createEntityManagerFactory("rage-ags", dbProperties);
            if (emf.isOpen())
            {
                // Verify the Application version is compatible with the database
                EntityManager em = emf.createEntityManager();
                LOGGER.debug("Comparing the version of the database to the App");
                VersionDAO versionDAO = new VersionDAO(em);
                if (versionDAO.checkDbVersion(version))
                {
                    try
                    {
                        int i = 1;
                        ServerSocket srv_sock = new ServerSocket(port);
                        LOGGER.info("Listening for connections on Port " + port);

                        for (;;)
                        {
                            Socket incoming = srv_sock.accept();
                            LOGGER.info("Connection: " +
                                    incoming.getInetAddress().toString());
                            Runnable r = new AGSThread(incoming,
                                    emf.createEntityManager());
                            Thread client_thread = new Thread(r);
                            client_thread.start();
                            i++;
                        }

                    }
                    catch (Exception e)
                    {
                        LOGGER.error(e.getLocalizedMessage());
                        e.printStackTrace();
                    }
                }
                else
                {
                    LOGGER.error("Application version is not compatible with " +
                            "the database.  Exiting server.");
                    em.close();
                    emf.close();
                    System.exit(-1);
                }
            }
            else
            {
                LOGGER.error("Could not connect to database.  Server exiting...");
            }
        }
        catch (Exception ex)
        {
            LOGGER.error("Exception Details: " + ex.getLocalizedMessage());
            LOGGER.error("Not connected to database.  Exiting server...");
        }
        
    }

    private static final Logger LOGGER = Logger.getLogger(Server.class);
    private static EntityManagerFactory emf;
    private static final Map<String,String> dbProperties = new HashMap<>();

    // Variable used to keep track of the Database version used by AGS
    private static final Version version = new Version(2.1);

}
