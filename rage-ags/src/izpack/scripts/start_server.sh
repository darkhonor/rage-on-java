#!/bin/sh
#
# Shell script to start the RAGE AutoGrader Server
#
#

JAVA=`which java`
# Use the following if the JRE executable is not on your path and which won't find it
#JAVA=/usr/local/jvm/etc  # replace with the actual location

$JAVA -jar AutoGraderServer.jar NOHUP &
