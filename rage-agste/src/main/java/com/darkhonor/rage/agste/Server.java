/**
 * Copyright 2011-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.agste;

import com.darkhonor.rage.libs.AGSThread;
import com.darkhonor.rage.libs.dataaccess.VersionDAO;
import com.darkhonor.rage.model.Version;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import org.apache.log4j.Logger;

/**
 * AutoGrader Server (Test Environment) main class.  Since this server uses a
 * fixed database source, there are no arguments that are parsed.  Spawns
 * {@link AGSThread} objects to handle client connections.
 * 
 * @author Alex Ackerman
 */
public class Server
{

    /**
     * The main Server application.  Spawns an AGSThread to handle the connection
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            emf = Persistence.createEntityManagerFactory("rage-agste");
            if (emf.isOpen())
            {
                // Verify the Application version is compatible with the database
                EntityManager em = emf.createEntityManager();
                LOGGER.debug("Comparing the version of the database to the App");
                VersionDAO versionDAO = new VersionDAO(em);
                if (versionDAO.checkDbVersion(version))
                {
                    try
                    {
                        LOGGER.debug("Creating EntifyManagerFactory");
                        LOGGER.debug("EntityManagerFactory created.  Starting threads");
                        Runnable r = new AGSThread(System.in, System.out,
                                emf.createEntityManager());
                        Thread client_thread = new Thread(r);
                        client_thread.start();
                        while (client_thread.isAlive())
                        {
                        }
                    } catch (PersistenceException e)
                    {
                        LOGGER.error("DATABASE ERROR: " + e.getMessage());
                    }
                } else
                {
                    LOGGER.error("Application version is not compatible with "
                            + "the database.  Exiting server.");
                    em.close();
                    emf.close();
                    System.exit(-1);
                }
            }
        } catch (Exception ex)
        {
            LOGGER.error("ERROR: Not connected to database.  Exiting server...");
        }
        if (emf.isOpen())
        {
            emf.close();
        }
    }
    private static final Logger LOGGER = Logger.getLogger(Server.class);
    private static EntityManagerFactory emf;
    // Variable used to keep track of the Database version used by AGS
    private static final Version version = new Version(2.1);
}
