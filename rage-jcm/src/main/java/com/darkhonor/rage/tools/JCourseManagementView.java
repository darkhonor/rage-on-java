/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.tools;

import com.darkhonor.rage.libs.listmodel.CourseListModel;
import com.darkhonor.rage.libs.listmodel.InstructorListModel;
import com.darkhonor.rage.libs.RAGEConst;
import com.darkhonor.rage.libs.listmodel.SectionListModel;
import com.darkhonor.rage.libs.listmodel.StudentListModel;
import com.darkhonor.rage.libs.dataaccess.CourseDAO;
import com.darkhonor.rage.libs.dataaccess.VersionDAO;
import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.GradedEvent;
import com.darkhonor.rage.model.Instructor;
import com.darkhonor.rage.model.Section;
import com.darkhonor.rage.model.Student;
import com.darkhonor.rage.model.Version;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import org.jdesktop.application.Action;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

/**
 * The application's main frame.
 *
 * @author Alex Ackerman
 */
public class JCourseManagementView extends FrameView
{

    public JCourseManagementView(SingleFrameApplication app)
    {
        super(app);

        initComponents();

        lstCourses.setModel(clm);
        lstInstructors.setModel(ilm);
        lstSections.setModel(slm);
        lstStudents.setModel(studentListModel);

        // Set the default DDL Gen option to False
        node.put("DDLGen", RAGEConst.DEFAULT_DDL_OPTION);

        Toolkit kit = Toolkit.getDefaultToolkit();
        Image icon = kit.getImage("icons/rage32.png");
        this.getFrame().setIconImage(icon);
        // Set the initial menu state
        databaseConnectionHelper(false);
        btnMoveSection.setEnabled(false);
        btnRemoveFromCourse.setEnabled(false);

        //Features not enabled yet
        loadGradedEventsMenuItem.setEnabled(false);
        loadInstructorsMenuItem.setEnabled(false);
    }

    @Action
    public void showAboutBox()
    {
        if (aboutBox == null)
        {
            JFrame mainFrame = JCourseManagementApp.getApplication().getMainFrame();
            aboutBox = new JCourseManagementAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        JCourseManagementApp.getApplication().show(aboutBox);
    }

    @Action
    public void moveSection()
    {
        if (lstStudents.getSelectedIndex() >= 0)
        {
            if (selectSectionDialog == null)
            {
                LOGGER.debug("Creating SelectSectionDialog");
                JFrame mainFrame = JCourseManagementApp.getApplication().getMainFrame();
                selectSectionDialog = new SelectSectionDialog(mainFrame, true);
                selectSectionDialog.setLocationRelativeTo(mainFrame);
            }
            LOGGER.debug("Getting selected course");
            Course selectedCourse = (Course) lstCourses.getSelectedValue();
            LOGGER.debug("Selected course: " + selectedCourse.getName());
            EntityManager em = emf.createEntityManager();
            Course course = em.find(Course.class, selectedCourse.getId());
            LOGGER.debug("Setting SelectSectionDialog course");
            selectSectionDialog.setCourse(course);
            LOGGER.debug("Showing dialog");
            JCourseManagementApp.getApplication().show(selectSectionDialog);

            if (selectSectionDialog.getReturnStatus() == SelectSectionDialog.RET_OK)
            {
                LOGGER.debug("Dialog returned Ok. Getting section from Database");
                Section section = em.find(Section.class, selectSectionDialog.getSection().getId());
                LOGGER.debug("New Section: " + section.getName());
                Section selectedSection = (Section) lstSections.getSelectedValue();
                Section oldSection = em.find(Section.class, selectedSection.getId());
                LOGGER.debug("Old Section: " + oldSection.getName());
                Student selectedStudent = (Student) lstStudents.getSelectedValue();
                Student student = em.find(Student.class, selectedStudent.getId());
                LOGGER.debug("Removing from old Section: " + oldSection);
                try
                {
                    student.removeFromSection(oldSection);
                } catch (NullPointerException ex)
                {
                    LOGGER.error("NullPointException: " + ex.getLocalizedMessage());
                } catch (IllegalArgumentException ex)
                {
                    LOGGER.error("IllegalArgumentException: " + ex.getLocalizedMessage());
                }

                LOGGER.debug("Adding to new Section: " + section);
                student.addSection(section);
                oldSection.removeStudent(student);
                section.addStudent(student);
                EntityTransaction tx = em.getTransaction();
                try
                {
                    tx.begin();
                    em.merge(student);
                    em.merge(section);
                    em.merge(oldSection);
                    tx.commit();
                } catch (IllegalStateException ex)
                {
                    LOGGER.error("Connection to Database has been closed.  Change not saved");
                } catch (IllegalArgumentException ex)
                {
                    LOGGER.error("Invalid Student object");
                }
                studentListModel.remove(lstStudents.getSelectedIndex());
            }
            em.close();
        } else
        {
            LOGGER.warn("No Student selected");
        }
    }

    @Action
    public void exitApplication()
    {
        LOGGER.info("Attempting to disconnect from database");
        try
        {
            if ((emf != null) && (emf.isOpen()))
            {
                emf.close();
            }
        } catch (Exception ex)
        {
        }
        LOGGER.info("Exiting application");
        System.exit(0);
    }

    @Action
    public void showOptionDialog()
    {
        if (optionDialog == null)
        {
            JFrame mainFrame = JCourseManagementApp.getApplication().getMainFrame();
            optionDialog = new OptionsDialog(mainFrame, true);
            optionDialog.setPreferences(node);
            optionDialog.setLocationRelativeTo(mainFrame);
        }
        JCourseManagementApp.getApplication().show(optionDialog);

        /**
         * Update db preferences if they were changed
         */
        if (optionDialog.getReturnStatus() == OptionsDialog.RET_OK)
        {
            if (!dbProperties.get("javax.persistence.jdbc.url").equals(node.get("DBURI", RAGEConst.DEFAULT_DBURI))
                    || !dbProperties.get("javax.persistence.jdbc.driver").equals(node.get("DBDriver", RAGEConst.DEFAULT_DBDRIVER))
                    || !dbProperties.get("javax.persistence.jdbc.user").equals(node.get("DBUsername", RAGEConst.DEFAULT_DBUSER))
                    || !dbProperties.get("javax.persistence.jdbc.password").equals(node.get("DBPassword", RAGEConst.DEFAULT_DBPASS)))
            {
                /**
                 * A change was made to one of the database properties.  Update the
                 * database properties and reconnect to the database.  Also, update
                 * the course listing
                 */
                JOptionPane.showMessageDialog(null, "Database connection preferences change. "
                        + "Reconnecting to database using updated details.",
                        "Database Options Changed", JOptionPane.INFORMATION_MESSAGE);
                clm.removeAll();

                dbProperties.put("javax.persistence.jdbc.driver",
                        node.get("DBDriver", RAGEConst.DEFAULT_DBDRIVER));
                dbProperties.put("javax.persistence.jdbc.url",
                        node.get("DBURI", RAGEConst.DEFAULT_DBURI));
                dbProperties.put("javax.persistence.jdbc.user",
                        node.get("DBUsername", RAGEConst.DEFAULT_DBUSER));
                dbProperties.put("javax.persistence.jdbc.password",
                        node.get("DBPassword", RAGEConst.DEFAULT_DBPASS));
                if (!node.get("DDLGen", RAGEConst.DEFAULT_DDL_OPTION).equals(RAGEConst.DEFAULT_DDL_OPTION))
                {
                    dbProperties.put("eclipselink.ddl-generation",
                            node.get("DDLGen", RAGEConst.EMDEDDED_DDL_OPTION));
                    LOGGER.debug("Setting dbProperties('eclipselink.ddl-generation': "
                            + node.get("DDLGen", "Error getting value"));
                } else
                {
                    dbProperties.put("eclipselink.ddl-generation", "none");
                }
                if ((emf != null) && (emf.isOpen()))
                {
                    emf.close();
                }
            }
        }
    }

    @Action
    public void clearStudents()
    {
        if (lstCourses.getSelectedIndex() < 0)
        {
            JOptionPane.showMessageDialog(null, "ERROR: No Course Selected",
                    "Error", JOptionPane.ERROR_MESSAGE);
        } else
        {
            EntityManager em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            Course selectedCourse = (Course) lstCourses.getSelectedValue();
            Course course = em.find(Course.class, selectedCourse.getId());
            try
            {
                if (course.getSections().isEmpty())
                {
                    LOGGER.warn("INFO: No sections listed for Course: "
                            + course.getName());
                } else
                {
                    for (Section section : course.getSections())
                    {
                        // Remove the Instructor from the Section
                        Instructor instructor = section.getInstructor();
                        instructor.removeSection(section);
                        section.removeInstructor();
                        tx.begin();
                        em.persist(section);
                        em.persist(instructor);
                        tx.commit();

                        // Remove each of the students from the section
                        for (Student student : section.getStudents())
                        {
                            student.removeFromSection(section);
                            tx.begin();
                            em.persist(student);
                            tx.commit();
                        }
                        section.clearStudents();
                        tx.begin();
                        em.persist(section);
                        tx.commit();

                        // Finally, remove the section
                        tx.begin();
                        em.remove(section);
                        tx.commit();
                    }
                }

                /**
                 * NOTE: This will leave all students and instructors in the database.
                 * this may not be desirable...have yet to decide.  However, if a 
                 * student or instructor is enrolled in another course, this would
                 * inadvertently remove them from that course too.
                 */
                // Now remove all the students from the database
//                CriteriaQuery cq = em.getCriteriaBuilder().createQuery(Student.class);
//                cq.select(cq.from(Student.class));
//                try
//                {
//                    ArrayList<Student> students =
//                            (ArrayList<Student>)em.createQuery(cq).getResultList();
//                    if (students.isEmpty())
//                    {
//                        LOGGER.warn("INFO: No students found in the database");
//                    }
//                    else
//                    {
//                        for (Student student : students)
//                        {
//                            tx.begin();
//                            em.remove(student);
//                            tx.commit();
//                        }
//                    }
//                }
//                catch (NoResultException ex)
//                {
//                    LOGGER.warn("INFO: No students found in database");
//                }
//
//                // Now remove all instructors who are teaching the course
//                if (course.getInstructors().isEmpty())
//                {
//                    LOGGER.warn("INFO: No instructors listed for Course: " +
//                            course.getName());
//                }
//                else
//                {
//                    course.clearInstructors();
//                    tx.begin();
//                    em.persist(course);
//                    tx.commit();
//                }
                lstInstructors.setEnabled(false);
                lstSections.setEnabled(false);
                slm.removeAll();
                ilm.removeAll();
                JOptionPane.showMessageDialog(null, "All Students and Sections "
                        + "removed for Course: " + course.getName(), "Success",
                        JOptionPane.INFORMATION_MESSAGE);
            } catch (NoResultException ex)
            {
                LOGGER.error("ERROR: No course found in database");
            }
            em.close();
        }
    }

    @Action
    public void connectDatabase()
    {
        LOGGER.info("Connecting to database.");
        if (JOptionPane.showConfirmDialog(null, "Connect to the database?",
                "Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
        {
            /**
             * Load the preferences from the System Registry
             */
            dbProperties.put("javax.persistence.jdbc.driver",
                    node.get("DBDriver", RAGEConst.DEFAULT_DBDRIVER));
            dbProperties.put("javax.persistence.jdbc.url",
                    node.get("DBURI", RAGEConst.DEFAULT_DBURI));
            dbProperties.put("javax.persistence.jdbc.user",
                    node.get("DBUsername", RAGEConst.DEFAULT_DBUSER));
            dbProperties.put("javax.persistence.jdbc.password",
                    node.get("DBPassword", RAGEConst.DEFAULT_DBPASS));
            LOGGER.debug("DDL: " + node.get("DDLGen", ""));
            boolean schemaCreated = false;
            if (!node.get("DDLGen", RAGEConst.DEFAULT_DDL_OPTION).equals(RAGEConst.DEFAULT_DDL_OPTION))
            {
                dbProperties.put("eclipselink.ddl-generation",
                        node.get("DDLGen", RAGEConst.EMDEDDED_DDL_OPTION));
                LOGGER.debug("Setting dbProperties('eclipselink.ddl-generation': "
                        + node.get("DDLGen", "Error getting value"));
                schemaCreated = true;
            } else
            {
                dbProperties.put("eclipselink.ddl-generation", "none");
            }

            /**
             * Create the EntityManagerFactory based upon current
             * preferences
             */
            LOGGER.info("URL: " + dbProperties.get("javax.persistence.jdbc.url"));
            LOGGER.info("User: " + dbProperties.get("javax.persistence.jdbc.user"));
            LOGGER.debug("Driver: " + dbProperties.get("javax.persistence.jdbc.driver"));
            LOGGER.debug("eclipselink.ddl-generation: "
                    + dbProperties.get("eclipselink.ddl-generation"));
            emf = Persistence.createEntityManagerFactory("rage-coursemgt",
                    dbProperties);
            LOGGER.info("Connected to the database");
            LOGGER.debug("Checking application version (" + version.getId()
                    + ") against database");
            checkVersion(schemaCreated);
            LOGGER.info("(connectDatabase) JCourseManagement version verified");
            EntityManager em = emf.createEntityManager();
            if (!em.isOpen())
            {
                LOGGER.error("Not connected to the database.");
                JOptionPane.showMessageDialog(null, "ERROR: Not connected to the"
                        + " database.  Check your database connection options.",
                        "ERROR", JOptionPane.ERROR_MESSAGE);
            } else
            {
//                if (schemaCreated)
//                {
//                    // Insert the version number in the database
//                    LOGGER.debug("We created schema...storing version");
//                    EntityTransaction tx = em.getTransaction();
//                    tx.begin();
//                    em.persist(version);
//                    tx.commit();
//                } else
//                {
//                    LOGGER.debug("Schema not created.");
//                }
//                if (RageLib.checkDBVersion(em, version))
//                {
//                    LOGGER.debug("Database version matches application version.");
//                    // Set the various tabs and button activity state
//                } else
//                {
//                    LOGGER.error("Database version does not match.  Exiting program.");
//                    JOptionPane.showMessageDialog(null, "ERROR: This version of"
//                            + " JTestGen is not compatible with the RAGE database."
//                            + " Exiting program.", "ERROR", JOptionPane.ERROR_MESSAGE);
//                    exitApplication();
//                }
                /**
                 * Load the courses from the Database
                 */
                LOGGER.debug("Loading courses from the database");
                clm.removeAll();
//                EntityManager em = emf.createEntityManager();
                try
                {
                    CriteriaBuilder cb = em.getCriteriaBuilder();
                    CriteriaQuery<Course> cq = cb.createQuery(Course.class);
                    Root<Course> courseRoot = cq.from(Course.class);
                    cq.select(courseRoot);
                    cq.orderBy(cb.asc(courseRoot.get("name")));
                    TypedQuery<Course> query = em.createQuery(cq);
                    LOGGER.debug("Query Created");
                    List<Course> courseList = query.getResultList();
                    LOGGER.debug("Courses Found: " + courseList.size());
                    for (int i = 0; i < courseList.size(); i++)
                    {
                        clm.add(courseList.get(i));
                    }
                } catch (Exception ex)
                {
                    LOGGER.error("Exception Caught: " + ex.getLocalizedMessage());
                }
                em.close();
            }

            // Enable/disable the appropriate elements
            databaseConnectionHelper(true);
        }
    }

    @Action
    public void disconnectDatabase()
    {
        btnMoveSection.setEnabled(false);
        btnRemoveFromCourse.setEnabled(false);
        btnBuildFolders.setEnabled(false);
        ilm.removeAll();
        slm.removeAll();
        studentListModel.removeAll();
        clm.removeAll();
        LOGGER.info("Disconnecting from the database");
        try
        {
            if ((emf != null) && (emf.isOpen()))
            {
                emf.close();
            }
            // Enable/Disable the appropriate elements
            databaseConnectionHelper(false);
        } catch (Exception ex)
        {
            LOGGER.error("Exception caught: " + ex.getLocalizedMessage());
        }

    }

    @Action
    public void loadInstructorsFromXLSX()
    {
        Course course = (Course) lstCourses.getSelectedValue();
        LOGGER.info("Loading list of instructors for Course ("
                + course.getName() + ") from a file");

        if (loadInstructorDialog == null)
        {
            JFrame mainFrame = JCourseManagementApp.getApplication().getMainFrame();
            loadInstructorDialog = new LoadInstructorFromXLSXDialog(mainFrame, true);
            loadInstructorDialog.setLocationRelativeTo(mainFrame);
        }
        JCourseManagementApp.getApplication().show(loadInstructorDialog);
    }

//    @Action
//    public void loadCourseDialogResponse()
//    {
//        LOGGER.info("Loading Course Dialog");
//        if (loadCourseD2 == null)
//        {
//            JFrame mainFrame = JCourseManagementApp.getApplication().getMainFrame();
//            loadCourseD2 = new LoadCoursesGUI(emf);
//            loadCourseD2.setLocationRelativeTo(mainFrame);
//        }
//        JCourseManagementApp.getApplication().show(loadCourseD2);
//    }

    @Action
    public void loadCourseFromFile()
    {
        LOGGER.info("Loading course from File.");
        if (loadCourseDialog == null)
        {
            JFrame mainFrame = JCourseManagementApp.getApplication().getMainFrame();
            loadCourseDialog = new LoadCourseFromFileDialog(mainFrame, true);
            loadCourseDialog.setLocationRelativeTo(mainFrame);
        }
        JCourseManagementApp.getApplication().show(loadCourseDialog);
        if (loadCourseDialog.getReturnStatus() == LoadCourseFromXLSXDialog.RET_OK)
        {
            LOGGER.info("Successfully received good response");
            Course newCourse = loadCourseDialog.getCourse();
            LOGGER.debug("Course Info: " + newCourse.getName());
            LOGGER.debug("- # Instructors: " + newCourse.getInstructors().size());
            for (int i = 0; i < newCourse.getInstructors().size(); i++)
            {
                LOGGER.debug("-- Instructor: " + newCourse.getInstructor(i).getDomainAccount());
            }
            LOGGER.debug("- # Sections: " + newCourse.getSections().size());
            int courseStudentCount = 0;
            for (int i = 0; i < newCourse.getSections().size(); i++)
            {
                int sectionStudentCount = newCourse.getSection(i).getStudents().size();
                LOGGER.debug("-- Section: " + newCourse.getSection(i).getName()
                        + " (" + sectionStudentCount + " students)");
                courseStudentCount += sectionStudentCount;
            }
            LOGGER.debug("- # Students: " + courseStudentCount);
            CourseDAO courseDAO = new CourseDAO(emf.createEntityManager());

            LOGGER.debug("Saving Course to Database");
            Long newId = courseDAO.create(newCourse);
            LOGGER.info("Course Saved to Database: " + newId);

            if (courseDAO.isOpen())
            {
                LOGGER.debug("Closing EntityManager.");
                courseDAO.closeConnection();
            } else
            {
                LOGGER.debug("EntityManager previously closed");
            }

            /**
             * Update the Window to show the new Course.  Clear all other lists.
             */
            LOGGER.debug("Updating window with all courses in the DB");
            // Clear Course List Model
            clm.removeAll();
            // Clear Instructor List Model
            ilm.removeAll();
            // Clear Section List Model
            slm.removeAll();
            // Clear Student List Model
            studentListModel.removeAll();

            // Get a full list of courses from the Data Source
            List<Course> courses = courseDAO.getAllCoursesByName();
            LOGGER.debug("# Courses Found: " + courses.size());
            for (int i = 0; i < courses.size(); i++)
            {
                clm.add(courses.get(i));
            }
            LOGGER.debug("Added " + clm.getSize() + " Courses");
        } else
        {
            LOGGER.info("Positive response not received from Dialog");
        }
    }

    @Action
    public void removeCourse()
    {
        if (lstCourses.getSelectedIndex() < 0)
        {
            JOptionPane.showMessageDialog(null, "ERROR: No Course Selected",
                    "Error", JOptionPane.ERROR_MESSAGE);
        } else
        {
            EntityManager em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            Course selectedCourse = (Course) lstCourses.getSelectedValue();
            Course course = em.find(Course.class, selectedCourse.getId());
            lstInstructors.removeAll();
            lstSections.removeAll();
            lstStudents.removeAll();
            try
            {
                LOGGER.debug("Checking whether there are Instructors in Course");
                // Clear the Instructors
                if (!course.getInstructors().isEmpty())
                {
                    course.clearInstructors();
                }
                tx.begin();
                em.persist(course);
                tx.commit();

                LOGGER.debug("Removed Instructors from Course.");

                // Clear the student enrollment from the course
                LOGGER.debug("Removing student enrollment");
                List<Section> sections = course.getSections();
                for (int i = 0; i < sections.size(); i++)
                {
                    Section section = sections.get(i);
                    section.clearStudents();
                    course.removeSection(section);
                    tx.begin();
                    em.merge(section);
                    em.remove(section);
                    tx.commit();
                }

                LOGGER.debug("Students removed from course");
                
                LOGGER.debug("Removing Graded Events from Course.");
                // Clear all graded events for the course
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery<GradedEvent> cq = cb.createQuery(GradedEvent.class);
                Root<GradedEvent> gradedEventRoot = cq.from(GradedEvent.class);
                cq.where(cb.equal(gradedEventRoot.get("course"), course));
                TypedQuery<GradedEvent> query = em.createQuery(cq);
                try
                {
                    List<GradedEvent> events = query.getResultList();
                    for (int i = 0; i < events.size(); i++)
                    {
                        GradedEvent gevent = events.get(i);
                        gevent.clearQuestions();
                        tx.begin();
                        em.persist(gevent);
                        em.remove(gevent);
                        tx.commit();
                    }
                } catch (NoResultException ex2)
                {
                    LOGGER.warn("ERROR: No graded events found for course");
                }

                LOGGER.debug("Graded Events removed.  Removing Course");
                // Remove the course
                tx.begin();
                em.merge(course);
                em.remove(course);
                tx.commit();
                clm.remove(lstCourses.getSelectedIndex());
                JOptionPane.showMessageDialog(null, "Successfully removed "
                        + course.getName(), "Success", JOptionPane.INFORMATION_MESSAGE);
            } catch (NoResultException ex)
            {
                LOGGER.error("ERROR: Course not found in the database");
            } catch (NullPointerException ex)
            {
                LOGGER.error("ERROR: Null pointer exception. " + ex.getLocalizedMessage());

            }
            em.close();
        }
    }

    @Action
    public void removeStudentFromCourse()
    {
        if (lstStudents.getSelectedIndex() >= 0)
        {
            LOGGER.info("Removing selected Student from Course");
            Student selectedStudent = (Student) lstStudents.getSelectedValue();
            Section selectedSection = (Section) lstSections.getSelectedValue();
            EntityManager em = emf.createEntityManager();
            Student student = em.find(Student.class, selectedStudent.getId());
            Section section = em.find(Section.class, selectedSection.getId());
            LOGGER.info("Removing " + student.getLastName() + ", "
                    + student.getFirstName() + " (" + student.getClassYear()
                    + ") from Section " + section.getName());
            student.removeFromSection(section);
            section.removeStudent(student);
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.merge(section);
            em.merge(student);
            tx.commit();
            studentListModel.remove(lstStudents.getSelectedIndex());
            LOGGER.info("Student removed from Section.");
            em.close();
        }
    }

    @Action
    public void removeUnenrolledStudents()
    {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Student> cq = cb.createQuery(Student.class);
        Root<Student> stuRoot = cq.from(Student.class);
        cq.select(stuRoot);
        TypedQuery<Student> query = em.createQuery(cq);
        LOGGER.debug("Query Created");
        List<Student> students = query.getResultList();
        List<Student> unenrolledStudents = new ArrayList<>();
        
        LOGGER.debug(students.size() + " students found");
        for (int i = 0; i < students.size(); i++)
        {
            if (students.get(i).getSections().isEmpty())
            {
                unenrolledStudents.add(students.get(i));
            }
        }
        int numRemove = unenrolledStudents.size();
        LOGGER.info("Found " + numRemove + " unenrolled students");
        int decision = JOptionPane.showConfirmDialog(null, "Remove " + numRemove
                + " unenrolled students from the database?", "Confirm deletion",
                JOptionPane.YES_NO_OPTION);
        if (decision == JOptionPane.YES_OPTION)
        {
            EntityTransaction tx = em.getTransaction();
            for (int i = 0; i < numRemove; i++)
            {
                tx.begin();
                em.remove(unenrolledStudents.get(i));
                tx.commit();
            }
        }

        em.close();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        lblCourses = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstCourses = new javax.swing.JList();
        lblInstructors = new javax.swing.JLabel();
        lblSections = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstInstructors = new javax.swing.JList();
        jScrollPane3 = new javax.swing.JScrollPane();
        lstSections = new javax.swing.JList();
        jScrollPane4 = new javax.swing.JScrollPane();
        lstStudents = new javax.swing.JList();
        lblStudents = new javax.swing.JLabel();
        btnRemoveFromCourse = new javax.swing.JButton();
        btnMoveSection = new javax.swing.JButton();
        btnBuildFolders = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        connectMenuItem = new javax.swing.JMenuItem();
        disconnectMenuItem = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JSeparator();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        toolsMenu = new javax.swing.JMenu();
        loadCourseMenuItem = new javax.swing.JMenuItem();
        loadCourseGUI = new javax.swing.JMenuItem();
        loadInstructorsMenuItem = new javax.swing.JMenuItem();
        loadGradedEventsMenuItem = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JSeparator();
        removeUnenrolledStudentsMenuItem = new javax.swing.JMenuItem();
        clearStudentsMenuItem = new javax.swing.JMenuItem();
        removeCourseMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        optionsMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();

        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setPreferredSize(new java.awt.Dimension(470, 290));

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(com.darkhonor.rage.tools.JCourseManagementApp.class).getContext().getResourceMap(JCourseManagementView.class);
        lblCourses.setText(resourceMap.getString("lblCourses.text")); // NOI18N
        lblCourses.setName("lblCourses"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        lstCourses.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstCourses.setEnabled(false);
        lstCourses.setName("lstCourses"); // NOI18N
        lstCourses.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstCoursesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lstCourses);

        lblInstructors.setText(resourceMap.getString("lblInstructors.text")); // NOI18N
        lblInstructors.setName("lblInstructors"); // NOI18N

        lblSections.setText(resourceMap.getString("lblSections.text")); // NOI18N
        lblSections.setName("lblSections"); // NOI18N

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        lstInstructors.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstInstructors.setEnabled(false);
        lstInstructors.setName("lstInstructors"); // NOI18N
        lstInstructors.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstInstructorsMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(lstInstructors);

        jScrollPane3.setName("jScrollPane3"); // NOI18N

        lstSections.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstSections.setEnabled(false);
        lstSections.setName("lstSections"); // NOI18N
        lstSections.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstSectionsMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(lstSections);

        jScrollPane4.setName("jScrollPane4"); // NOI18N

        lstStudents.setEnabled(false);
        lstStudents.setName("lstStudents"); // NOI18N
        lstStudents.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstStudentsMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(lstStudents);

        lblStudents.setText(resourceMap.getString("lblStudents.text")); // NOI18N
        lblStudents.setName("lblStudents"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(com.darkhonor.rage.tools.JCourseManagementApp.class).getContext().getActionMap(JCourseManagementView.class, this);
        btnRemoveFromCourse.setAction(actionMap.get("removeStudentFromCourse")); // NOI18N
        btnRemoveFromCourse.setText(resourceMap.getString("btnRemoveFromCourse.text")); // NOI18N
        btnRemoveFromCourse.setName("btnRemoveFromCourse"); // NOI18N

        btnMoveSection.setAction(actionMap.get("moveSection")); // NOI18N
        btnMoveSection.setText(resourceMap.getString("btnMoveSection.text")); // NOI18N
        btnMoveSection.setName("btnMoveSection"); // NOI18N

        btnBuildFolders.setText(resourceMap.getString("btnBuildFolders.text")); // NOI18N
        btnBuildFolders.setEnabled(false);
        btnBuildFolders.setName("btnBuildFolders"); // NOI18N

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, mainPanelLayout.createSequentialGroup()
                            .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblCourses)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(mainPanelLayout.createSequentialGroup()
                                    .addGap(14, 14, 14)
                                    .addComponent(lblInstructors))
                                .addGroup(mainPanelLayout.createSequentialGroup()
                                    .addGap(62, 62, 62)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(mainPanelLayout.createSequentialGroup()
                                    .addGap(12, 12, 12)
                                    .addComponent(lblSections))
                                .addGroup(mainPanelLayout.createSequentialGroup()
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addComponent(lblStudents)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(btnRemoveFromCourse)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnMoveSection)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuildFolders)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCourses)
                    .addComponent(lblInstructors)
                    .addComponent(lblSections))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblStudents)
                .addGap(7, 7, 7)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRemoveFromCourse)
                    .addComponent(btnMoveSection)
                    .addComponent(btnBuildFolders))
                .addGap(12, 12, 12))
        );

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        connectMenuItem.setAction(actionMap.get("connectDatabase")); // NOI18N
        connectMenuItem.setText(resourceMap.getString("connectMenuItem.text")); // NOI18N
        connectMenuItem.setName("connectMenuItem"); // NOI18N
        fileMenu.add(connectMenuItem);

        disconnectMenuItem.setAction(actionMap.get("disconnectDatabase")); // NOI18N
        disconnectMenuItem.setText(resourceMap.getString("disconnectMenuItem.text")); // NOI18N
        disconnectMenuItem.setName("disconnectMenuItem"); // NOI18N
        fileMenu.add(disconnectMenuItem);

        jSeparator2.setName("jSeparator2"); // NOI18N
        fileMenu.add(jSeparator2);

        exitMenuItem.setAction(actionMap.get("exitApplication")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        toolsMenu.setAction(actionMap.get("loadCourseFromFile")); // NOI18N
        toolsMenu.setText(resourceMap.getString("toolsMenu.text")); // NOI18N
        toolsMenu.setName("toolsMenu"); // NOI18N

        loadCourseMenuItem.setAction(actionMap.get("loadCourseFromFile")); // NOI18N
        loadCourseMenuItem.setText(resourceMap.getString("loadCourseMenuItem.text")); // NOI18N
        loadCourseMenuItem.setName("loadCourseMenuItem"); // NOI18N
        toolsMenu.add(loadCourseMenuItem);

        loadCourseGUI.setAction(actionMap.get("loadCourseDialogResponse")); // NOI18N
        loadCourseGUI.setText(resourceMap.getString("loadCourseGUI.text")); // NOI18N
        loadCourseGUI.setName("loadCourseGUI"); // NOI18N
        toolsMenu.add(loadCourseGUI);

        loadInstructorsMenuItem.setAction(actionMap.get("loadInstructorsFromXLSX")); // NOI18N
        loadInstructorsMenuItem.setText(resourceMap.getString("loadInstructorsMenuItem.text")); // NOI18N
        loadInstructorsMenuItem.setName("loadInstructorsMenuItem"); // NOI18N
        toolsMenu.add(loadInstructorsMenuItem);

        loadGradedEventsMenuItem.setText(resourceMap.getString("loadGradedEventsMenuItem.text")); // NOI18N
        loadGradedEventsMenuItem.setEnabled(false);
        loadGradedEventsMenuItem.setName("loadGradedEventsMenuItem"); // NOI18N
        toolsMenu.add(loadGradedEventsMenuItem);

        jSeparator3.setName("jSeparator3"); // NOI18N
        toolsMenu.add(jSeparator3);

        removeUnenrolledStudentsMenuItem.setAction(actionMap.get("removeUnenrolledStudents")); // NOI18N
        removeUnenrolledStudentsMenuItem.setText(resourceMap.getString("removeUnenrolledStudentsMenuItem.text")); // NOI18N
        removeUnenrolledStudentsMenuItem.setName("removeUnenrolledStudentsMenuItem"); // NOI18N
        toolsMenu.add(removeUnenrolledStudentsMenuItem);

        clearStudentsMenuItem.setAction(actionMap.get("clearStudents")); // NOI18N
        clearStudentsMenuItem.setText(resourceMap.getString("clearStudentsMenuItem.text")); // NOI18N
        clearStudentsMenuItem.setName("clearStudentsMenuItem"); // NOI18N
        toolsMenu.add(clearStudentsMenuItem);

        removeCourseMenuItem.setAction(actionMap.get("removeCourse")); // NOI18N
        removeCourseMenuItem.setText(resourceMap.getString("removeCourseMenuItem.text")); // NOI18N
        removeCourseMenuItem.setName("removeCourseMenuItem"); // NOI18N
        toolsMenu.add(removeCourseMenuItem);

        jSeparator1.setName("jSeparator1"); // NOI18N
        toolsMenu.add(jSeparator1);

        optionsMenuItem.setAction(actionMap.get("showOptionDialog")); // NOI18N
        optionsMenuItem.setText(resourceMap.getString("optionsMenuItem.text")); // NOI18N
        optionsMenuItem.setName("optionsMenuItem"); // NOI18N
        toolsMenu.add(optionsMenuItem);

        menuBar.add(toolsMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setComponent(mainPanel);
        setMenuBar(menuBar);
    }// </editor-fold>//GEN-END:initComponents

    private void lstCoursesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstCoursesMouseClicked

        if (lstCourses.getSelectedIndex() >= 0)
        {
            LOGGER.debug("Disabling Student Management Buttons");
            btnMoveSection.setEnabled(false);
            btnRemoveFromCourse.setEnabled(false);
            lstInstructors.setEnabled(true);
            loadInstructorsMenuItem.setEnabled(true);
            studentListModel.removeAll();
            slm.removeAll();
            Course selectedCourse = (Course) lstCourses.getSelectedValue();
            EntityManager em = emf.createEntityManager();
            Course course = em.find(Course.class, selectedCourse.getId());
            LOGGER.debug("Finding Instructors for Course: " + course.getName());
            if (course.getInstructors().size() > 0)
            {
                LOGGER.debug("Found " + course.getInstructors().size() + " instructors");
                ilm.removeAll();
                for (int i = 0; i < course.getInstructors().size(); i++)
                {
                    ilm.add(course.getInstructor(i));
                }

                LOGGER.debug("Added " + ilm.getSize() + " instructors to the ListModel");
            }
            em.close();
            loadInstructorsMenuItem.setEnabled(true);
        } else
        {
            lstInstructors.setEnabled(false);
            ilm.removeAll();
            loadInstructorsMenuItem.setEnabled(false);
        }
    }//GEN-LAST:event_lstCoursesMouseClicked

    private void lstInstructorsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstInstructorsMouseClicked

        if (lstInstructors.getSelectedIndex() >= 0)
        {
            LOGGER.debug("Instructor List clicked.  Disabling Student Management Buttons");
            btnMoveSection.setEnabled(false);
            btnRemoveFromCourse.setEnabled(false);
            lstSections.setEnabled(true);
            studentListModel.removeAll();
            Instructor selectedInstructor = (Instructor) lstInstructors.getSelectedValue();
            EntityManager em = emf.createEntityManager();
            Instructor instructor = em.find(Instructor.class, selectedInstructor.getId());
            LOGGER.debug("Finding Sections for Instructor " + instructor.getDomainAccount());
            if (instructor.getSections().size() > 0)
            {
                LOGGER.debug("Found " + instructor.getSections().size() + " sections");
                slm.removeAll();
                for (int i = 0; i < instructor.getSections().size(); i++)
                {
                    slm.add(instructor.getSection(i));
                }
                LOGGER.debug("Added " + slm.getSize() + " sections to the ListModel");
            } else
            {
                LOGGER.warn("No sections found for Instructor "
                        + instructor.getDomainAccount());
            }
            em.close();
        } else
        {
            lstSections.setEnabled(false);
            slm.removeAll();
        }
    }//GEN-LAST:event_lstInstructorsMouseClicked

    private void lstSectionsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstSectionsMouseClicked
        // Section clicked...fill in the list of students for that section
        if (lstSections.getSelectedIndex() >= 0)
        {
            LOGGER.debug("Section selected");
            lstStudents.setEnabled(true);
            btnRemoveFromCourse.setEnabled(false);
            btnMoveSection.setEnabled(false);
            Section selectedSection = (Section) lstSections.getSelectedValue();
            EntityManager em = emf.createEntityManager();
            LOGGER.debug("Loading selected section from database");
            Section section = em.find(Section.class, selectedSection.getId());
            LOGGER.debug("Clearing Student List Model");
            studentListModel.removeAll();
            LOGGER.debug("Adding all students to Student List Model");
            for (Student student : section.getStudents())
            {
                studentListModel.add(student);
            }
            studentListModel.sort();
            lstStudents.setSelectedIndex(-1);
            LOGGER.info("Added " + studentListModel.getSize()
                    + " students to Student List Model");
            em.close();
            LOGGER.debug("EntityManager closed.");
        }
    }//GEN-LAST:event_lstSectionsMouseClicked

    private void lstStudentsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstStudentsMouseClicked
        if (lstStudents.getSelectedIndex() >= 0)
        {
            LOGGER.debug("Student selected at index " + lstStudents.getSelectedIndex());
            btnMoveSection.setEnabled(true);
            btnRemoveFromCourse.setEnabled(true);
        }
    }//GEN-LAST:event_lstStudentsMouseClicked

    private void databaseConnectionHelper(boolean connected)
    {
        // Enable/disable the appropriate elements
        disconnectMenuItem.setEnabled(connected);
        connectMenuItem.setEnabled(!connected);
        lstCourses.setEnabled(connected);
        loadCourseMenuItem.setEnabled(connected);
        btnBuildFolders.setEnabled(connected);
        loadCourseMenuItem.setEnabled(connected);
        //loadGradedEventsMenuItem.setEnabled(connected);
        //loadInstructorsMenuItem.setEnabled(connected);
        removeUnenrolledStudentsMenuItem.setEnabled(connected);
        removeCourseMenuItem.setEnabled(connected);
        clearStudentsMenuItem.setEnabled(connected);

        if (!connected)
        {
            lstInstructors.setEnabled(connected);
            lstSections.setEnabled(connected);
            lstStudents.setEnabled(connected);
        }
    }
    
    private void checkVersion(boolean createSchema)
    {
        try
        {
            LOGGER.debug("(checkVersion) Creating VersionDAO object");
            VersionDAO versionDAO = new VersionDAO(emf.createEntityManager());
            LOGGER.debug("(checkVersion) VersionDAO object created");
            // If this is a new install and the option to create the database
            // schema is set, do so here
            if (createSchema)
            {
                // Insert the version number in the database
                LOGGER.debug("(checkVersion) We created schema...Version stored "
                        + "on connect");
                versionDAO.create(version);
            } else
            {
                LOGGER.debug("(checkVersion) Schema not created.");
            }
            if (versionDAO.checkDbVersion(version))
            {
                LOGGER.debug("(checkVersion) Database version matches "
                        + "application version.");
            } else
            {
                LOGGER.error("(checkVersion) Database version does not match. "
                        + "Exiting program.");
                JOptionPane.showMessageDialog(JCourseManagementApp.getApplication()
                        .getMainFrame(), "ERROR: This version of "
                        + "JCourseManagements is not compatible with the RAGE "
                        + "database. Exiting program.",
                        "ERROR", JOptionPane.ERROR_MESSAGE);
                exitApplication();
            }
            LOGGER.debug("(checkVersion) Closing VersionDAO connection");
            versionDAO.closeConnection();
        } catch (IllegalStateException ex)
        {
            LOGGER.warn("(checkVersion) Problem connecting to the database "
                    + "with the VersionDAO object");
        } 
    }    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuildFolders;
    private javax.swing.JButton btnMoveSection;
    private javax.swing.JButton btnRemoveFromCourse;
    private javax.swing.JMenuItem clearStudentsMenuItem;
    private javax.swing.JMenuItem connectMenuItem;
    private javax.swing.JMenuItem disconnectMenuItem;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel lblCourses;
    private javax.swing.JLabel lblInstructors;
    private javax.swing.JLabel lblSections;
    private javax.swing.JLabel lblStudents;
    private javax.swing.JMenuItem loadCourseGUI;
    private javax.swing.JMenuItem loadCourseMenuItem;
    private javax.swing.JMenuItem loadGradedEventsMenuItem;
    private javax.swing.JMenuItem loadInstructorsMenuItem;
    private javax.swing.JList<Course> lstCourses;
    private javax.swing.JList<Instructor> lstInstructors;
    private javax.swing.JList<Section> lstSections;
    private javax.swing.JList<Student> lstStudents;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem optionsMenuItem;
    private javax.swing.JMenuItem removeCourseMenuItem;
    private javax.swing.JMenuItem removeUnenrolledStudentsMenuItem;
    private javax.swing.JMenu toolsMenu;
    // End of variables declaration//GEN-END:variables
    private JDialog aboutBox;
    private OptionsDialog optionDialog;
    private LoadInstructorFromXLSXDialog loadInstructorDialog;
    private LoadCourseFromFileDialog loadCourseDialog;
//    private LoadCoursesGUI loadCourseD2;
    private SelectSectionDialog selectSectionDialog;
    private final Preferences rootPref = Preferences.userRoot();
    private final Preferences node = rootPref.node("/com/darkhonor/rage");
    private EntityManagerFactory emf;
    private final Map<String,String> dbProperties = new HashMap<>();
    private final CourseListModel clm = new CourseListModel();
    private final InstructorListModel ilm = new InstructorListModel();
    private final SectionListModel slm = new SectionListModel();
    private final StudentListModel studentListModel = new StudentListModel();
    private static final Logger LOGGER = Logger.getLogger(JCourseManagementView.class);
    private static final Version version = new Version(2.1);

    public class RageWindowAdapter extends WindowAdapter
    {

        @Override
        public void windowClosing(WindowEvent e)
        {
            try
            {
                if ((emf != null) && (emf.isOpen()))
                {
                    LOGGER.debug("Disconnecting from the database for Window Closing");
                    emf.close();
                    LOGGER.info("Disconnected from the database.  Exiting program.");
                }
            } catch (Exception ex)
            {
            }
            System.exit(0);
        }
    }
}
