/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.tools;

import com.darkhonor.rage.libs.listmodel.GradedEventListModel;
import com.darkhonor.rage.libs.listmodel.QuestionListModel;
import com.darkhonor.rage.libs.RAGEConst;
import com.darkhonor.rage.libs.RageLib;
import com.darkhonor.rage.libs.listmodel.StringListModel;
import com.darkhonor.rage.libs.listmodel.TestCaseListModel;
import com.darkhonor.rage.libs.TestCaseGenThread;
import com.darkhonor.rage.libs.dataaccess.CategoryDAO;
import com.darkhonor.rage.libs.dataaccess.CourseDAO;
import com.darkhonor.rage.libs.dataaccess.GradedEventDAO;
import com.darkhonor.rage.libs.dataaccess.TestCaseDAO;
import com.darkhonor.rage.libs.dataaccess.VersionDAO;
import com.darkhonor.rage.model.Category;
import com.darkhonor.rage.model.Course;
import com.darkhonor.rage.model.GradedEvent;
import com.darkhonor.rage.model.Question;
import com.darkhonor.rage.model.TestCase;
import com.darkhonor.rage.model.Version;
import java.awt.Dialog.ModalityType;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.persistence.EntityManager;
import java.util.HashMap;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

/**
 * The JTestGen application provides a means to create Questions that can be loaded
 * into a RAGE database.  At this time, it will create an XML file containing
 * the Test Case information for only a single question.  Later versions will
 * allow for direct loading into the database as well as creation of GradedEvent
 * objects.
 *
 * @author Alexander Ackerman
 * @author James Maher
 *
 * @version 2.0.0
 * 
 */
public class JTestGen extends javax.swing.JFrame
{

    /**
     * Constructor for the JTestGen class.  Creates new form JTestGen and
     * and initializes the principle variables to their default values.  
     *
     */
    public JTestGen()
    {
        initComponents();
        this.addWindowListener(new RageWindowAdapter());
        lstTestCases.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lstTestCases.setModel(modTCList);
        lstInputs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lstInputs.setModel(modInputList);
        lstExclusions.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lstExclusions.setModel(modExclusionList);
        lstQuestions.setModel(modQuestionList);
        lstGradedEvents.setModel(modGradedEvents);
        lstGradedEventQuestions.setModel(modGradedEventQuestions);
        txtValue.setText(questionValue.toPlainString());
        btnRemoveQuestion.setEnabled(false);
        btnEditQuestion.setEnabled(false);
        disconnectMenuItem.setEnabled(false);

        // Set the default DDL Gen option to False
        node.put("DDLGen", RAGEConst.DEFAULT_DDL_OPTION);

        Toolkit kit = Toolkit.getDefaultToolkit();
        Image icon = kit.getImage("icons/rage32.png");
        this.setIconImage(icon);

        txtRefProgram.getDocument().addDocumentListener(new DocumentListener()
        {

            @Override
            public void changedUpdate(DocumentEvent de)
            {
                if ((txtDestFilename.getText().length() != 0)
                        && (txtRefProgram.getText().length() != 0)
                        && (txtCategory.getText().length() != 0))
                {
                    btnGenerateXML.setEnabled(true);
                    btnLoadDB.setEnabled(true);
                } else
                {
                    btnGenerateXML.setEnabled(false);
                    btnLoadDB.setEnabled(false);
                }
            }

            @Override
            public void insertUpdate(DocumentEvent de)
            {
                if ((txtDestFilename.getText().length() != 0)
                        && (txtRefProgram.getText().length() != 0)
                        && (txtCategory.getText().length() != 0))
                {
                    btnGenerateXML.setEnabled(true);
                    btnLoadDB.setEnabled(true);
                } else
                {
                    btnGenerateXML.setEnabled(false);
                    btnLoadDB.setEnabled(false);
                }
            }

            @Override
            public void removeUpdate(DocumentEvent de)
            {
                if ((txtDestFilename.getText().length() != 0)
                        && (txtRefProgram.getText().length() != 0)
                        && (txtCategory.getText().length() != 0))
                {
                    btnGenerateXML.setEnabled(true);
                    btnLoadDB.setEnabled(true);
                } else
                {
                    btnGenerateXML.setEnabled(false);
                    btnLoadDB.setEnabled(false);
                }
            }
        });

        txtDestFilename.getDocument().addDocumentListener(new DocumentListener()
        {

            @Override
            public void changedUpdate(DocumentEvent de)
            {
                if ((txtDestFilename.getText().length() != 0)
                        && (txtRefProgram.getText().length() != 0)
                        && (txtCategory.getText().length() != 0))
                {
                    btnGenerateXML.setEnabled(true);
                    btnLoadDB.setEnabled(true);
                } else
                {
                    btnGenerateXML.setEnabled(false);
                    btnLoadDB.setEnabled(false);
                }
            }

            @Override
            public void insertUpdate(DocumentEvent de)
            {
                if ((txtDestFilename.getText().length() != 0)
                        && (txtRefProgram.getText().length() != 0)
                        && (txtCategory.getText().length() != 0))
                {
                    btnGenerateXML.setEnabled(true);
                    btnLoadDB.setEnabled(true);
                } else
                {
                    btnGenerateXML.setEnabled(false);
                    btnLoadDB.setEnabled(false);
                }
            }

            @Override
            public void removeUpdate(DocumentEvent de)
            {
                if ((txtDestFilename.getText().length() != 0)
                        && (txtRefProgram.getText().length() != 0)
                        && (txtCategory.getText().length() != 0))
                {
                    btnGenerateXML.setEnabled(true);
                    btnLoadDB.setEnabled(true);
                } else
                {
                    btnGenerateXML.setEnabled(false);
                    btnLoadDB.setEnabled(false);
                }
            }
        });

        txtCategory.getDocument().addDocumentListener(new DocumentListener()
        {

            @Override
            public void changedUpdate(DocumentEvent de)
            {
                if ((txtDestFilename.getText().length() != 0)
                        && (txtRefProgram.getText().length() != 0)
                        && (txtCategory.getText().length() != 0))
                {
                    btnGenerateXML.setEnabled(true);
                    btnLoadDB.setEnabled(true);
                } else
                {
                    btnGenerateXML.setEnabled(false);
                    btnLoadDB.setEnabled(false);
                }
            }

            @Override
            public void insertUpdate(DocumentEvent de)
            {
                if ((txtDestFilename.getText().length() != 0)
                        && (txtRefProgram.getText().length() != 0)
                        && (txtCategory.getText().length() != 0))
                {
                    btnGenerateXML.setEnabled(true);
                    btnLoadDB.setEnabled(true);
                } else
                {
                    btnGenerateXML.setEnabled(false);
                    btnLoadDB.setEnabled(false);
                }
            }

            @Override
            public void removeUpdate(DocumentEvent de)
            {
                if ((txtDestFilename.getText().length() != 0)
                        && (txtRefProgram.getText().length() != 0)
                        && (txtCategory.getText().length() != 0))
                {
                    btnGenerateXML.setEnabled(true);
                    btnLoadDB.setEnabled(true);
                } else
                {
                    btnGenerateXML.setEnabled(false);
                    btnLoadDB.setEnabled(false);
                }
            }
        });

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        pnlReference = new javax.swing.JPanel();
        lblRefInstructions = new javax.swing.JLabel();
        rBtnRaptor = new javax.swing.JRadioButton();
        rBtnProcessing = new javax.swing.JRadioButton();
        lblRefProgram = new javax.swing.JLabel();
        txtRefProgram = new javax.swing.JTextField();
        btnBrowseProgram = new javax.swing.JButton();
        lblDestFilename = new javax.swing.JLabel();
        txtDestFilename = new javax.swing.JTextField();
        btnDestBrowse = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstInputs = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstExclusions = new javax.swing.JList();
        lblTestCases = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        lstTestCases = new javax.swing.JList();
        chkVerbatim = new javax.swing.JCheckBox();
        lblValue = new javax.swing.JLabel();
        txtValue = new javax.swing.JTextField();
        btnTCAdd = new javax.swing.JButton();
        btnTCRemove = new javax.swing.JButton();
        btnInputAdd = new javax.swing.JButton();
        btnInputRemove = new javax.swing.JButton();
        btnExcRemove = new javax.swing.JButton();
        lblInputs = new javax.swing.JLabel();
        lblExclusions = new javax.swing.JLabel();
        btnExcAdd = new javax.swing.JButton();
        btnGenerateXML = new javax.swing.JButton();
        btnLoadDB = new javax.swing.JButton();
        lblDescription = new javax.swing.JLabel();
        txtDescription = new javax.swing.JTextField();
        lblCategory = new javax.swing.JLabel();
        txtCategory = new javax.swing.JTextField();
        chkOrdered = new javax.swing.JCheckBox();
        pnlQuestionMgt = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        lstQuestions = new javax.swing.JList();
        btnRemoveQuestion = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnLoadQuestions = new javax.swing.JButton();
        btnEditQuestion = new javax.swing.JButton();
        pnlGradedEventMgt = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lblGradedEvents = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        lstGradedEvents = new javax.swing.JList();
        lblTerm = new javax.swing.JLabel();
        txtTermYear = new javax.swing.JTextField();
        cboCourse = new javax.swing.JComboBox();
        lblCourse = new javax.swing.JLabel();
        lblAssignment = new javax.swing.JLabel();
        txtAssignment = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtVersion = new javax.swing.JTextField();
        chkPartialCredit = new javax.swing.JCheckBox();
        jScrollPane6 = new javax.swing.JScrollPane();
        lstGradedEventQuestions = new javax.swing.JList();
        lblGradedEventQuestions = new javax.swing.JLabel();
        btnAddGradedEvent = new javax.swing.JButton();
        btnRemoveGradedEvent = new javax.swing.JButton();
        btnLoadGradedEvents = new javax.swing.JButton();
        btnRemoveEventQuestion = new javax.swing.JButton();
        btnAddGEQuestion = new javax.swing.JButton();
        lblDueDate = new javax.swing.JLabel();
        txtDueDate = new javax.swing.JTextField();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        connectDBMenuItem = new javax.swing.JMenuItem();
        disconnectMenuItem = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();
        toolsMenu = new javax.swing.JMenu();
        loadGradedEventMenuItem = new javax.swing.JMenuItem();
        loadQuestionMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        optionsMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("JTestGen");

        jTabbedPane1.setName("jTabbedPane1"); // NOI18N

        pnlReference.setMinimumSize(new java.awt.Dimension(597, 509));
        pnlReference.setName("pnlReference"); // NOI18N

        lblRefInstructions.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblRefInstructions.setText("<html><body>This module will help you to generate test cases for a given program.  You will provide a reference program for the program to use.  You will then need to specify the inputs to each of the test cases.  The program will generate an XML document with the completed test cases for that question.<p></P><p>You do not need to connect to the database before using this module.  When you click the \"Load to DB\" button, it will connect to the database, load the question, and disconnect again.</p></body></html>");
        lblRefInstructions.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        lblRefInstructions.setAutoscrolls(true);
        lblRefInstructions.setName("lblRefInstructions"); // NOI18N

        buttonGroup1.add(rBtnRaptor);
        rBtnRaptor.setSelected(true);
        rBtnRaptor.setText("RAPTOR");
        rBtnRaptor.setName("rBtnRaptor"); // NOI18N

        buttonGroup1.add(rBtnProcessing);
        rBtnProcessing.setText("Processing");
        rBtnProcessing.setName("rBtnProcessing"); // NOI18N

        lblRefProgram.setText("Reference Program:");
        lblRefProgram.setName("lblRefProgram"); // NOI18N

        txtRefProgram.setMinimumSize(new java.awt.Dimension(315, 20));
        txtRefProgram.setName("txtRefProgram"); // NOI18N
        txtRefProgram.setPreferredSize(new java.awt.Dimension(315, 20));

        btnBrowseProgram.setText("Browse...");
        btnBrowseProgram.setName("btnBrowseProgram"); // NOI18N
        btnBrowseProgram.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBrowseProgramActionPerformed(evt);
            }
        });

        lblDestFilename.setText("Destination File: ");
        lblDestFilename.setName("lblDestFilename"); // NOI18N

        txtDestFilename.setMinimumSize(new java.awt.Dimension(315, 20));
        txtDestFilename.setName("txtDestFilename"); // NOI18N
        txtDestFilename.setPreferredSize(new java.awt.Dimension(315, 20));

        btnDestBrowse.setText("Browse...");
        btnDestBrowse.setName("btnDestBrowse"); // NOI18N
        btnDestBrowse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDestBrowseActionPerformed(evt);
            }
        });

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        lstInputs.setEnabled(false);
        lstInputs.setName("lstInputs"); // NOI18N
        jScrollPane1.setViewportView(lstInputs);

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        lstExclusions.setEnabled(false);
        lstExclusions.setName("lstExclusions"); // NOI18N
        jScrollPane2.setViewportView(lstExclusions);

        lblTestCases.setText("Test Case:");
        lblTestCases.setName("lblTestCases"); // NOI18N

        jScrollPane3.setName("jScrollPane3"); // NOI18N

        lstTestCases.setName("lstTestCases"); // NOI18N
        lstTestCases.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstTestCasesMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(lstTestCases);

        chkVerbatim.setText("Verbatim Response Expected");
        chkVerbatim.setName("chkVerbatim"); // NOI18N

        lblValue.setText("Question Value: ");
        lblValue.setName("lblValue"); // NOI18N

        txtValue.setEditable(false);
        txtValue.setName("txtValue"); // NOI18N

        btnTCAdd.setText("Add");
        btnTCAdd.setName("btnTCAdd"); // NOI18N
        btnTCAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTCAddActionPerformed(evt);
            }
        });

        btnTCRemove.setText("Remove");
        btnTCRemove.setEnabled(false);
        btnTCRemove.setName("btnTCRemove"); // NOI18N
        btnTCRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTCRemoveActionPerformed(evt);
            }
        });

        btnInputAdd.setText("Add");
        btnInputAdd.setEnabled(false);
        btnInputAdd.setName("btnInputAdd"); // NOI18N
        btnInputAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInputAddActionPerformed(evt);
            }
        });

        btnInputRemove.setText("Remove");
        btnInputRemove.setEnabled(false);
        btnInputRemove.setName("btnInputRemove"); // NOI18N
        btnInputRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInputRemoveActionPerformed(evt);
            }
        });

        btnExcRemove.setText("Remove");
        btnExcRemove.setEnabled(false);
        btnExcRemove.setName("btnExcRemove"); // NOI18N
        btnExcRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcRemoveActionPerformed(evt);
            }
        });

        lblInputs.setText("Inputs:");
        lblInputs.setName("lblInputs"); // NOI18N

        lblExclusions.setText("Exclusions:");
        lblExclusions.setName("lblExclusions"); // NOI18N

        btnExcAdd.setText("Add");
        btnExcAdd.setEnabled(false);
        btnExcAdd.setName("btnExcAdd"); // NOI18N
        btnExcAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcAddActionPerformed(evt);
            }
        });

        btnGenerateXML.setText("Generate XML");
        btnGenerateXML.setEnabled(false);
        btnGenerateXML.setName("btnGenerateXML"); // NOI18N
        btnGenerateXML.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateXMLActionPerformed(evt);
            }
        });

        btnLoadDB.setText("Load to DB");
        btnLoadDB.setEnabled(false);
        btnLoadDB.setName("btnLoadDB"); // NOI18N
        btnLoadDB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadDBActionPerformed(evt);
            }
        });

        lblDescription.setText("Description (50 char max):");
        lblDescription.setName("lblDescription"); // NOI18N

        txtDescription.setMinimumSize(new java.awt.Dimension(350, 20));
        txtDescription.setName("txtDescription"); // NOI18N
        txtDescription.setPreferredSize(new java.awt.Dimension(350, 20));

        lblCategory.setText("Category:");
        lblCategory.setName("lblCategory"); // NOI18N

        txtCategory.setName("txtCategory"); // NOI18N

        chkOrdered.setText("Ordered Output Expected");
        chkOrdered.setName("chkOrdered"); // NOI18N

        javax.swing.GroupLayout pnlReferenceLayout = new javax.swing.GroupLayout(pnlReference);
        pnlReference.setLayout(pnlReferenceLayout);
        pnlReferenceLayout.setHorizontalGroup(
            pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlReferenceLayout.createSequentialGroup()
                .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(lblRefInstructions, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlReferenceLayout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(pnlReferenceLayout.createSequentialGroup()
                                    .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(rBtnRaptor)
                                        .addComponent(rBtnProcessing))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(pnlReferenceLayout.createSequentialGroup()
                                            .addComponent(lblRefProgram)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtRefProgram, javax.swing.GroupLayout.PREFERRED_SIZE, 312, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(pnlReferenceLayout.createSequentialGroup()
                                            .addComponent(lblDestFilename)
                                            .addGap(18, 18, 18)
                                            .addComponent(txtDestFilename, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGroup(pnlReferenceLayout.createSequentialGroup()
                                    .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(lblCategory)
                                        .addComponent(lblDescription))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtCategory, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnBrowseProgram)
                                .addComponent(btnDestBrowse))))
                    .addGroup(pnlReferenceLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(chkVerbatim)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chkOrdered))
                    .addGroup(pnlReferenceLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblTestCases))
                    .addGroup(pnlReferenceLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pnlReferenceLayout.createSequentialGroup()
                                .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(pnlReferenceLayout.createSequentialGroup()
                                        .addComponent(btnTCAdd)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnTCRemove)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(pnlReferenceLayout.createSequentialGroup()
                                .addComponent(lblValue)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtValue, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblInputs)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnlReferenceLayout.createSequentialGroup()
                                .addComponent(btnInputAdd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnInputRemove)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblExclusions)
                            .addGroup(pnlReferenceLayout.createSequentialGroup()
                                .addComponent(btnExcAdd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnExcRemove))
                            .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlReferenceLayout.createSequentialGroup()
                                    .addComponent(btnGenerateXML)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnLoadDB))
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(25, 25, 25))
        );
        pnlReferenceLayout.setVerticalGroup(
            pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlReferenceLayout.createSequentialGroup()
                .addComponent(lblRefInstructions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rBtnRaptor)
                    .addComponent(lblRefProgram)
                    .addComponent(txtRefProgram, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBrowseProgram))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rBtnProcessing)
                    .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblDestFilename)
                        .addComponent(txtDestFilename, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnDestBrowse)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDescription)
                    .addComponent(txtDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCategory, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCategory))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chkVerbatim)
                    .addComponent(chkOrdered))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTestCases)
                    .addComponent(lblInputs)
                    .addComponent(lblExclusions))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlReferenceLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnInputAdd)
                                .addComponent(btnInputRemove))
                            .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnTCAdd)
                                .addComponent(btnTCRemove)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblValue)
                            .addComponent(txtValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlReferenceLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnExcAdd)
                            .addComponent(btnExcRemove))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addGroup(pnlReferenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGenerateXML)
                    .addComponent(btnLoadDB))
                .addGap(26, 26, 26))
        );

        jTabbedPane1.addTab("Question Generation", pnlReference);

        pnlQuestionMgt.setName("pnlQuestionMgt"); // NOI18N

        jScrollPane4.setName("jScrollPane4"); // NOI18N

        lstQuestions.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstQuestions.setName("lstQuestions"); // NOI18N
        jScrollPane4.setViewportView(lstQuestions);

        btnRemoveQuestion.setText("Remove Question");
        btnRemoveQuestion.setName("btnRemoveQuestion"); // NOI18N
        btnRemoveQuestion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveQuestionActionPerformed(evt);
            }
        });

        jLabel1.setText("<html><body><p>This module will allow you to remove a question from the database.  However, you will not be able to remove the question if it has already been added to a graded event.  You will need to remove the question from the graded event first.</p><p></P><p>You will need to connect to the database before using this module.</p></body></html>");
        jLabel1.setName("jLabel1"); // NOI18N

        jLabel4.setText("Questions:");
        jLabel4.setName("jLabel4"); // NOI18N

        btnLoadQuestions.setText("Load Questions");
        btnLoadQuestions.setEnabled(false);
        btnLoadQuestions.setName("btnLoadQuestions"); // NOI18N
        btnLoadQuestions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadQuestionsActionPerformed(evt);
            }
        });

        btnEditQuestion.setText("Edit Question");
        btnEditQuestion.setName("btnEditQuestion"); // NOI18N
        btnEditQuestion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditQuestionAction(evt);
            }
        });

        javax.swing.GroupLayout pnlQuestionMgtLayout = new javax.swing.GroupLayout(pnlQuestionMgt);
        pnlQuestionMgt.setLayout(pnlQuestionMgtLayout);
        pnlQuestionMgtLayout.setHorizontalGroup(
            pnlQuestionMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlQuestionMgtLayout.createSequentialGroup()
                .addGroup(pnlQuestionMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlQuestionMgtLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnRemoveQuestion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnEditQuestion))
                    .addGroup(pnlQuestionMgtLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlQuestionMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlQuestionMgtLayout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnLoadQuestions))
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 573, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 558, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(88, Short.MAX_VALUE))
        );
        pnlQuestionMgtLayout.setVerticalGroup(
            pnlQuestionMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlQuestionMgtLayout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlQuestionMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addComponent(btnLoadQuestions))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlQuestionMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRemoveQuestion)
                    .addComponent(btnEditQuestion))
                .addContainerGap(250, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Question Management", pnlQuestionMgt);

        pnlGradedEventMgt.setName("pnlGradedEventMgt"); // NOI18N

        jLabel2.setText("<html><body><p>This module enables you to manage the Graded Events that are currently loaded in the database.  You can create and delete graded events as well as add or delete questions from each graded event.</p><p></P><p>You will need to connect to the database before using this module.</p></body></html>");
        jLabel2.setName("jLabel2"); // NOI18N

        lblGradedEvents.setText("Graded Events:");
        lblGradedEvents.setName("lblGradedEvents"); // NOI18N

        jScrollPane5.setName("jScrollPane5"); // NOI18N

        lstGradedEvents.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstGradedEvents.setName("lstGradedEvents"); // NOI18N
        lstGradedEvents.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstGradedEventsMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(lstGradedEvents);

        lblTerm.setText("Term: ");
        lblTerm.setName("lblTerm"); // NOI18N

        txtTermYear.setName("txtTermYear"); // NOI18N

        cboCourse.setName("cboCourse"); // NOI18N

        lblCourse.setText("Course: ");
        lblCourse.setName("lblCourse"); // NOI18N

        lblAssignment.setText("Assignment: ");
        lblAssignment.setName("lblAssignment"); // NOI18N

        txtAssignment.setName("txtAssignment"); // NOI18N

        jLabel3.setText("Version:");
        jLabel3.setName("jLabel3"); // NOI18N

        txtVersion.setName("txtVersion"); // NOI18N

        chkPartialCredit.setText("Partial Credit Available");
        chkPartialCredit.setName("chkPartialCredit"); // NOI18N

        jScrollPane6.setName("jScrollPane6"); // NOI18N

        lstGradedEventQuestions.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstGradedEventQuestions.setName("lstGradedEventQuestions"); // NOI18N
        lstGradedEventQuestions.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstGradedEventQuestionsMouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(lstGradedEventQuestions);

        lblGradedEventQuestions.setText("Questions:");
        lblGradedEventQuestions.setName("lblGradedEventQuestions"); // NOI18N

        btnAddGradedEvent.setText("Add Graded Event");
        btnAddGradedEvent.setEnabled(false);
        btnAddGradedEvent.setName("btnAddGradedEvent"); // NOI18N
        btnAddGradedEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddGradedEventActionPerformed(evt);
            }
        });

        btnRemoveGradedEvent.setText("Remove Graded Event");
        btnRemoveGradedEvent.setEnabled(false);
        btnRemoveGradedEvent.setName("btnRemoveGradedEvent"); // NOI18N
        btnRemoveGradedEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveGradedEventActionPerformed(evt);
            }
        });

        btnLoadGradedEvents.setText("Load Graded Events");
        btnLoadGradedEvents.setEnabled(false);
        btnLoadGradedEvents.setName("btnLoadGradedEvents"); // NOI18N
        btnLoadGradedEvents.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadGradedEventsActionPerformed(evt);
            }
        });

        btnRemoveEventQuestion.setText("Remove Question");
        btnRemoveEventQuestion.setEnabled(false);
        btnRemoveEventQuestion.setName("btnRemoveEventQuestion"); // NOI18N
        btnRemoveEventQuestion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveEventQuestionActionPerformed(evt);
            }
        });

        btnAddGEQuestion.setText("Add Question");
        btnAddGEQuestion.setEnabled(false);
        btnAddGEQuestion.setName("btnAddGEQuestion"); // NOI18N
        btnAddGEQuestion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddGEQuestionActionPerformed(evt);
            }
        });

        lblDueDate.setText("Due Date:");
        lblDueDate.setName("lblDueDate"); // NOI18N

        txtDueDate.setName("txtDueDate"); // NOI18N

        javax.swing.GroupLayout pnlGradedEventMgtLayout = new javax.swing.GroupLayout(pnlGradedEventMgt);
        pnlGradedEventMgt.setLayout(pnlGradedEventMgtLayout);
        pnlGradedEventMgtLayout.setHorizontalGroup(
            pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlGradedEventMgtLayout.createSequentialGroup()
                .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(pnlGradedEventMgtLayout.createSequentialGroup()
                            .addGap(10, 10, 10)
                            .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btnLoadGradedEvents)
                                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 577, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(pnlGradedEventMgtLayout.createSequentialGroup()
                                        .addComponent(lblCourse)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cboCourse, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(111, 111, 111)
                                        .addComponent(lblTerm)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtTermYear, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(pnlGradedEventMgtLayout.createSequentialGroup()
                                        .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(pnlGradedEventMgtLayout.createSequentialGroup()
                                                .addComponent(lblAssignment)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtAssignment, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(pnlGradedEventMgtLayout.createSequentialGroup()
                                                .addGap(63, 63, 63)
                                                .addComponent(chkPartialCredit)))
                                        .addGap(32, 32, 32)
                                        .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel3)
                                            .addComponent(lblDueDate))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtDueDate, javax.swing.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
                                            .addComponent(txtVersion))))))
                        .addGroup(pnlGradedEventMgtLayout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(lblGradedEvents))
                        .addComponent(jLabel2, 0, 0, Short.MAX_VALUE))
                    .addGroup(pnlGradedEventMgtLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblGradedEventQuestions)
                            .addGroup(pnlGradedEventMgtLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlGradedEventMgtLayout.createSequentialGroup()
                                        .addComponent(btnAddGEQuestion)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnRemoveEventQuestion)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnAddGradedEvent)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnRemoveGradedEvent))
                                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 567, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlGradedEventMgtLayout.setVerticalGroup(
            pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlGradedEventMgtLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnLoadGradedEvents)
                    .addGroup(pnlGradedEventMgtLayout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblGradedEvents)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTermYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCourse)
                    .addComponent(cboCourse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTerm))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAssignment)
                    .addComponent(txtAssignment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtVersion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDueDate)
                    .addComponent(txtDueDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkPartialCredit))
                .addGap(4, 4, 4)
                .addComponent(lblGradedEventQuestions)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlGradedEventMgtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRemoveGradedEvent)
                    .addComponent(btnAddGradedEvent)
                    .addComponent(btnAddGEQuestion)
                    .addComponent(btnRemoveEventQuestion))
                .addGap(47, 47, 47))
        );

        jTabbedPane1.addTab("Graded Event Management", pnlGradedEventMgt);

        fileMenu.setText("File");

        connectDBMenuItem.setText("Connect to Database");
        connectDBMenuItem.setName("connectDBMenuItem"); // NOI18N
        connectDBMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectDBMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(connectDBMenuItem);

        disconnectMenuItem.setText("Disconnect");
        disconnectMenuItem.setName("disconnectMenuItem"); // NOI18N
        disconnectMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                disconnectMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(disconnectMenuItem);

        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        toolsMenu.setText("Tools");
        toolsMenu.setName("toolsMenu"); // NOI18N

        loadGradedEventMenuItem.setText("Load Graded Event...");
        loadGradedEventMenuItem.setName("loadGradedEventMenuItem"); // NOI18N
        loadGradedEventMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadGradedEventMenuItemActionPerformed(evt);
            }
        });
        toolsMenu.add(loadGradedEventMenuItem);

        loadQuestionMenuItem.setText("Load Question...");
        loadQuestionMenuItem.setName("loadQuestionMenuItem"); // NOI18N
        loadQuestionMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadQuestionMenuItemActionPerformed(evt);
            }
        });
        toolsMenu.add(loadQuestionMenuItem);

        jSeparator1.setName("jSeparator1"); // NOI18N
        toolsMenu.add(jSeparator1);

        optionsMenuItem.setText("Options...");
        optionsMenuItem.setName("optionsMenuItem"); // NOI18N
        optionsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optionsMenuItemActionPerformed(evt);
            }
        });
        toolsMenu.add(optionsMenuItem);

        menuBar.add(toolsMenu);

        helpMenu.setText("Help");

        aboutMenuItem.setText("About");
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 702, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 631, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * ActionPerformed event handler for the Exit menu item.  Attempts to closes
     * the EntityManagerFactory before exiting the program.  If an exception is
     * thrown, it is ignored since we are exiting the program.
     *
     * @param evt   The event to handle
     */
    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        try
        {
            if ((emf != null) && (emf.isOpen()))
            {
                LOGGER.debug("Disconnecting from the database");
                emf.close();
                LOGGER.info("Disconnected from the database.  Exiting program.");
            }
        } catch (Exception ex)
        {
        }
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    /**
     * ActionPerfomed event handler for the Exclusion Add button.  Adds an
     * exclusion specified by the user to the {@link TestCase}.  It adds the
     * exclusion to both the TestCase and the JList.
     *
     * @param evt   The event to handle
     */
    private void btnExcAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcAddActionPerformed

        if (lstTestCases.getSelectedIndex() != -1)
        {
            TestCase tc = (TestCase) lstTestCases.getSelectedValue();
            String input = "zzzz";
            do  // Error Check to make sure a value is entered
            {
                if ((input != null) && (input.equals("zzzz")))
                {
                    input = JOptionPane.showInputDialog(null,
                            "Enter exclusion for Test Case", "Enter Exclusion",
                            JOptionPane.QUESTION_MESSAGE);
                } else // This is an error time through
                {
                    input = JOptionPane.showInputDialog(null,
                            "ERROR: Enter valid exclusion for test case",
                            "ERROR: Enter Exclusion",
                            JOptionPane.ERROR_MESSAGE);
                }
            } while ((input == null) || (input.equals("")));
            tc.addExclusion(input);
            modExclusionList.add(input);
            if (modExclusionList.getSize() > 0)
            {
                btnExcRemove.setEnabled(true);
            }
        }
}//GEN-LAST:event_btnExcAddActionPerformed
    /**
     * ActionPerformed event handler for the Exclusion Remove button.  Removes the
     * selected exclusion from the list (and from the selected test case).  If the
     * last exclusion has been removed, the button is disabled.
     *
     * @param evt   The event to handle
     */
    private void btnExcRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcRemoveActionPerformed

        if (lstExclusions.getSelectedIndex() < 0)
        {
            JOptionPane.showMessageDialog(null, "ERROR: No exclusion selected",
                    "Error", JOptionPane.ERROR_MESSAGE);
        } else
        {
            TestCase tc = (TestCase) lstTestCases.getSelectedValue();
            tc.removeExclusion((String) lstExclusions.getSelectedValue());
            modExclusionList.remove(lstExclusions.getSelectedIndex());
        }
        if (modExclusionList.getSize() < 1)
        {
            btnExcRemove.setEnabled(false);
        }
    }//GEN-LAST:event_btnExcRemoveActionPerformed

    /**
     * ActionPerformed event handler for Input remove button.  Removes the selected
     * input from the list (and from the selected test case).  If the last input
     * is removed, the input remove button is disabled.
     *
     * @param evt   The event to handle
     */
    private void btnInputRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInputRemoveActionPerformed

        if (lstInputs.getSelectedIndex() < 0)
        {
            JOptionPane.showMessageDialog(null, "ERROR: No input selected",
                    "Error", JOptionPane.ERROR_MESSAGE);
        } else
        {
            TestCase tc = (TestCase) lstTestCases.getSelectedValue();
            tc.removeInput((String) lstInputs.getSelectedValue());
            modInputList.remove(lstInputs.getSelectedIndex());
        }
        if (modInputList.getSize() < 1)
        {
            btnInputRemove.setEnabled(false);
        }
    }//GEN-LAST:event_btnInputRemoveActionPerformed

    /**
     * ActionPerformed event handler for the Input Add button.  Adds an input
     * provided by the user to the selected {@link TestCase}.  The input is added
     * to both the TestCase and the JList.
     *
     * @param evt   The event to handle
     */
    private void btnInputAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInputAddActionPerformed

        if (lstTestCases.getSelectedIndex() != -1)
        {
            TestCase tc = (TestCase) lstTestCases.getSelectedValue();
            String input = "zzzz";
            do  // Error Check to make sure a value is entered
            {
                if ((input != null) && (input.equals("zzzz")))
                {
                    input = JOptionPane.showInputDialog(null,
                            "Enter input for Test Case", "Enter Input",
                            JOptionPane.QUESTION_MESSAGE);
                } else // This is an error time through
                {
                    input = JOptionPane.showInputDialog(null,
                            "ERROR: Enter valid input for test case",
                            "ERROR: Enter Input",
                            JOptionPane.ERROR_MESSAGE);
                }
            } while ((input == null) || (input.equals("")));
            tc.addInput(input);
            modInputList.add(input);
            if (modInputList.getSize() > 0)
            {
                btnInputRemove.setEnabled(true);
            }
        }
}//GEN-LAST:event_btnInputAddActionPerformed

    /**
     * The ActionPerformed event handler for the TestCase Remove Button.  Removes
     * the selected item from the JList.  If the last item has been removed, the
     * Remove button is disabled as well as the Inputs and Exclusions windows.
     *
     * @param evt   The event to handle
     */
    private void btnTCRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTCRemoveActionPerformed

        if (lstTestCases.getSelectedIndex() < 0)
        {
            JOptionPane.showMessageDialog(null, "ERROR: No test case selected",
                    "Error", JOptionPane.ERROR_MESSAGE);
        } else
        {
            questionValue = questionValue.subtract(modTCList.remove(lstTestCases.getSelectedIndex()).getValue());
            txtValue.setText(questionValue.toPlainString());
        }
        if (modTCList.getSize() < 1)
        {
            btnTCRemove.setEnabled(false);
            // Since there are no more test cases, ensure the Input and Exclusion
            // windows are deactivated as well
            lstInputs.setEnabled(false);
            btnInputAdd.setEnabled(false);
            btnInputRemove.setEnabled(false);
            lstExclusions.setEnabled(false);
            btnExcAdd.setEnabled(false);
            btnExcRemove.setEnabled(false);
        }
}//GEN-LAST:event_btnTCRemoveActionPerformed

    /**
     * ActionPerformed event handler for the Add Test Case button.  Creates a new
     * {@link TestCase} object and adds it to the {@link Question}.  A new item
     * is added to the TestCase List object and the Input and Exclusion methods
     * are enabled if they are not already enabled.
     *
     * @param evt   The event to handle
     */
    private void btnTCAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTCAddActionPerformed
        TestCase testCase = new TestCase();
        index++;
        testCase.setId(index);
        String temp;
        do  // Error Check to make sure a value is entered
        {
            temp = JOptionPane.showInputDialog(null,
                    "Enter value for Test Case", "Enter Test Case Value",
                    JOptionPane.QUESTION_MESSAGE);
        } while ((temp == null) || (temp.equals("")));
        BigDecimal value = new BigDecimal(temp);
        testCase.setValue(value);
        questionValue = questionValue.add(value);
        txtValue.setText(questionValue.toPlainString());
        modTCList.addTestCase(testCase);
        if (modTCList.getSize() > 0)
        {
            btnTCRemove.setEnabled(true);
        }
    }//GEN-LAST:event_btnTCAddActionPerformed

    /**
     * The MouseClicked event handler for the TestCase List object.  Activates
     * the inputs and exclusions windows if a user has selected a test case.  If
     * there are test cases loaded, it will load the correct list of inputs and
     * exclusions for the selected test case.
     *
     * @param evt   The event to handle
     */
    private void lstTestCasesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstTestCasesMouseClicked

        // If the user clicks on the TestCase List and there is an item selected,
        // go ahead and activate the Inputs and Exclusions windows
        if (lstTestCases.getSelectedIndex() != -1)
        {
            // Activate the Input window
            lstInputs.setEnabled(true);
            btnInputAdd.setEnabled(true);
            // Activate the Exclusions window
            lstExclusions.setEnabled(true);
            btnExcAdd.setEnabled(true);
            btnTCRemove.setEnabled(true);

            /**
             * Find the selected test case and load the inputs and exclusions to
             * the other windows (clearing the windows first)
             */
            TestCase tc = (TestCase) lstTestCases.getSelectedValue();
            List<String> inputs = tc.getInputs();
            List<String> excludes = tc.getExcludes();
            modInputList.removeAll();
            modInputList.addAll(inputs);
            modExclusionList.removeAll();
            modExclusionList.addAll(excludes);
        }
}//GEN-LAST:event_lstTestCasesMouseClicked

    /**
     * ActionPerformed event handler for the Browse Destination File button.
     * Opens a FileChooser dialog with a FileFilter set for XML files.
     *
     * @param evt   The event to handle
     */
    private void btnDestBrowseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDestBrowseActionPerformed
        if (chooser == null)
        {
            chooser = new JFileChooser();
        }
        // Take from currently listed property
        if (txtDestFilename.getText().isEmpty())
        {
            chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        } else
        {
            chooser.setCurrentDirectory(new File(txtDestFilename.getText()));
        }
        // We're only after directories, so set the chooser to only list directories
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        chooser.setMultiSelectionEnabled(false);
        chooser.resetChoosableFileFilters();
        chooser.setFileFilter(new FileNameExtensionFilter("Graded Event Files", "xml"));
        int res = chooser.showOpenDialog(this);
        if (res == JFileChooser.APPROVE_OPTION)
        {
            try
            {
                txtDestFilename.setText(chooser.getSelectedFile().getCanonicalPath());
            } catch (IOException e)
            {
                txtDestFilename.setText("");

            }
        }
}//GEN-LAST:event_btnDestBrowseActionPerformed

    /**
     * ActionPerformed event handler for the Browse Program button. Opens a
     * FileChooser dialog with a FileFilter set for Raptor files (*.rap) if the
     * Raptor radio button is selected.  Otherwise, the FileFilter is set to look
     * for Java Class files (*.class).
     *
     * @param evt   The event to handle
     */
    private void btnBrowseProgramActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBrowseProgramActionPerformed
        if (chooser == null)
        {
            chooser = new JFileChooser();
        }
        // Take from currently listed property
        if (txtRefProgram.getText().isEmpty())
        {
            chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        } else
        {
            chooser.setCurrentDirectory(new File(txtRefProgram.getText()));
        }
        // We're only after directories, so set the chooser to only list directories
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        chooser.setMultiSelectionEnabled(false);
        chooser.resetChoosableFileFilters();
        if (rBtnRaptor.isSelected())
        {
            chooser.setFileFilter(new FileNameExtensionFilter("RAPTOR Files", "rap"));
        } else
        {
            chooser.setFileFilter(new FileNameExtensionFilter("Java Archive Files", "jar"));
        }
        int res = chooser.showOpenDialog(this);
        if (res == JFileChooser.APPROVE_OPTION)
        {
            try
            {
                txtRefProgram.setText(chooser.getSelectedFile().getCanonicalPath());
            } catch (IOException e)
            {
                txtRefProgram.setText("");

            }
        }
}//GEN-LAST:event_btnBrowseProgramActionPerformed

    /**
     * ActionPerformed event handler for the Generate XML button.  Creates a
     * {@link Question} object and populates it with the details included.  The
     * resulting question is written to a Graded Event XML document with a dummy
     * Graded Event listed.
     *
     * @param evt   The event to handle.
     */
    private void btnGenerateXMLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateXMLActionPerformed

        /**
         * Since Send to DB and Generate XML will both do this first part (create
         * Question object, this should probably be a static function in RageLib.
         */
        // Set the question name first...if the question name isn't a valid name
        // no further processing should happen and an error should be displayed
        Question question = new Question();
        File refFileName = new File(txtRefProgram.getText());
        // Retrieve the question name based upon the filename
        // TODO: RAGE-98
        String name = RageLib.getQuestionNameFromFilename(refFileName.getName());
        if ((name != null) && (!name.startsWith("ERROR in Filename:")))
        {
            LOGGER.debug("Creating Question for " + name);
            // This is a valid question name (not null or "ERROR in Filename:"
            question.setName(name);

            // Set whether question requires a verbatim response
            question.setVerbatim(chkVerbatim.isSelected());

            // Set whether question requires ordered output
            question.setOrderedOutput(chkOrdered.isSelected());

            // Set the description
            try
            {
                question.setDescription(txtDescription.getText(0, 50));
            } catch (BadLocationException ex)
            {
                question.setDescription(txtDescription.getText());

            } catch (IllegalArgumentException ex)
            {
                // Description is blank so just ignore...not required for question
            }

            /** Set the question category--this is not a nullable field
             *  This button will not activate if the txtCategory field is null or
             *  empty, so we can safely not check for it here.
             */
            question.setCategory(new Category(txtCategory.getText()));

            /**
             * As long as there are test cases, we can continue...otherwise stop
             */
            if (modTCList.getSize() > 0)
            {
                // Store the test cases in the question
                try
                {
                    question.setTestCases(modTCList.cloneTestCases());
                } catch (CloneNotSupportedException ex)
                {
                    LOGGER.error("Clone not supported");
                }

                /**
                 * Run Raptor and store the returned outputs in the TestCase object
                 */
                ProcessBuilder launcher = new ProcessBuilder();
                Map<String, String> environment = launcher.environment();
                launcher.redirectErrorStream(true);
                launcher.directory(refFileName.getParentFile());
                int type = 0;
                if (rBtnRaptor.isSelected())
                {
                    type = 0;
                } else if (rBtnProcessing.isSelected())
                {
                    type = 1;
                }
                Runnable r = new TestCaseGenThread(node, launcher, refFileName, question, type);

                Thread testCaseGenThread = new Thread(r);
                testCaseGenThread.run();
                while (testCaseGenThread.isAlive())
                {
                }

                /**
                 * Build the XML document--move this to a static function in RageLib
                 */
                // TODO: RAGE-99 - Epic
                Document doc = DocumentHelper.createDocument();
                doc.setXMLEncoding("UTF-8");
                doc.addDocType("GradedEvent", RAGEConst.DEFAULT_PUBLIC_URI,
                        RAGEConst.DEFAULT_SYSTEM_URI);
                Element root = doc.addElement("GradedEvent");
                Element term = root.addElement("Term").addAttribute("name", node.get("Term", "No Term"));
                Element course = root.addElement("Course").addAttribute("name", "CS110");
                Element assignment = root.addElement("Assignment").addAttribute("name", "Dummy");
                Element versionElement = root.addElement("Version").addAttribute("id", "ALL");
                Element dueDate = root.addElement("DueDate").addText("25 Jan 2020");
                Element pc = root.addElement("PartialCredit").addAttribute("value", "false");
                Element question1 = root.addElement("Question").addAttribute("category", question.getCategory().getName());
                Element qName = question1.addElement("Name").addAttribute("name", question.getName());
                Element qDesc = question1.addElement("Description").addText(question.getDescription());
                if (question.getVerbatim())
                {
                    Element qVerbatim = question1.addElement("Verbatim").addAttribute("value", "true");
                } else
                {
                    Element qVerbatim = question1.addElement("Verbatim").addAttribute("value", "false");
                }
                if (question.getOrderedOutput())
                {
                    Element qOrdered = question1.addElement("OrderedOutput").addAttribute("value", "true");
                } else
                {
                    Element qOrdered = question1.addElement("OrderedOutput").addAttribute("value", "false");
                }
                for (TestCase tc : question.getTestCases())
                {
                    Element qTestCase = question1.addElement("TestCase");
                    Element tcValue = qTestCase.addElement("Value").addAttribute("value", tc.getValue().toPlainString());
                    LOGGER.debug("Inputs / Outputs: " + tc.getInputs().size()
                            + " / " + tc.getOutputs().size());
                    for (String input : tc.getInputs())
                    {
                        Element qInput = qTestCase.addElement("input").addText(input);
                    }
                    for (String output : tc.getOutputs())
                    {
                        Element qOutput = qTestCase.addElement("output").addText(output);
                    }
                    for (String exclude : tc.getExcludes())
                    {
                        Element qExclude = qTestCase.addElement("exclude").addText(exclude);
                    }
                }

                try
                {
                    // Format the output with line breaks and tabs
                    OutputFormat format = OutputFormat.createPrettyPrint();
                    XMLWriter writer = new XMLWriter(new FileWriter(txtDestFilename.getText()),
                            format);
                    writer.write(doc);
                    writer.close();
                    JOptionPane.showMessageDialog(null, "File created successfully",
                            "Success", JOptionPane.INFORMATION_MESSAGE);
                    LOGGER.info("XML File created successfully");
                } catch (IOException ex)
                {
                    LOGGER.error("Error writing to " + txtDestFilename.getText());
                }

            } else
            {
                JOptionPane.showMessageDialog(null, "ERROR: No Test Cases added",
                        "ERROR", JOptionPane.ERROR_MESSAGE);
                LOGGER.error("No Test Cases added");
            }
        } else
        {
            JOptionPane.showMessageDialog(null,
                    "ERROR: Invalid Reference File Name."
                    + "  Please correct the reference filename and resubmit.",
                    "Error", JOptionPane.ERROR_MESSAGE);
            LOGGER.error("Invalid Reference File Name.");
        }
}//GEN-LAST:event_btnGenerateXMLActionPerformed

    /**
     * The ActionPerformed event handler for the Load DB Button.  Connects to the
     * configured database and saves the {@link Question} and it's {@link TestCase}
     * data to the database.
     *
     * @param evt   The event to handle
     */
    private void btnLoadDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoadDBActionPerformed

        if ((emf != null) && (emf.isOpen()))
        {
            File refFileName = new File(txtRefProgram.getText());
            // Retrieve the question name based upon the filename
            // TODO: RAGE-98
            String name = RageLib.getQuestionNameFromFilename(refFileName.getName());
            if ((name != null) && (!name.startsWith("ERROR in Filename:")))
            {
                if (JOptionPane.showConfirmDialog(this, "Connect to Database and "
                        + "load question into Database?", "Confirm Connect to DB",
                        JOptionPane.YES_NO_OPTION)
                        == JOptionPane.YES_OPTION)
                {
                    EntityManager em = emf.createEntityManager();
                    // TODO: RAGE-24 - Migrate to QuestionDAO class
                    CriteriaBuilder cb = em.getCriteriaBuilder();
                    CriteriaQuery<Question> cq = cb.createQuery(Question.class);
                    Root<Question> questionRoot = cq.from(Question.class);
                    cq.where(cb.equal(questionRoot.get("name"), name));
                    TypedQuery<Question> questionQuery = em.createQuery(cq);
                    EntityTransaction tx = em.getTransaction();
                    //See if we are editing a question or creating a new question
                    try
                    {
                        Question question = questionQuery.getSingleResult();
                        LOGGER.debug("Question" + question.getName()
                                + "already exists in the database");
                        tx.begin();
                        em.remove(question);
                        tx.commit();

                    } catch (NoResultException e)
                    {
                        LOGGER.error("That question doesn't exist in the database. "
                                + e.getLocalizedMessage());
                    } catch (IllegalStateException ex)
                    {
                        LOGGER.error("Illegal State Exception: "
                                + ex.getLocalizedMessage());
                    } catch (IllegalArgumentException ex)
                    {
                        LOGGER.error("Illegal Argument Exception: "
                                + ex.getLocalizedMessage());
                    } catch (TransactionRequiredException ex)
                    {
                        LOGGER.error("Transaction Required Exception: "
                                + ex.getLocalizedMessage());
                    } catch (Exception ex)
                    {
                        LOGGER.error("Exception Caught: "
                                + ex.getLocalizedMessage());
                    }
                    // This is a valid question name (not null or "ERROR in Filename:"
                    Question question = new Question();
                    question.setName(name);

                    // Set whether question requires a verbatim response
                    question.setVerbatim(chkVerbatim.isSelected());

                    // Set whether question requires ordered output
                    question.setOrderedOutput(chkOrdered.isSelected());

                    // Set the description
                    try
                    {
                        question.setDescription(txtDescription.getText(0, 50));
                    } catch (BadLocationException ex)
                    {
                        question.setDescription(txtDescription.getText());

                    } catch (IllegalArgumentException ex)
                    {
                        // Description is blank so just ignore...not required for question
                    }

                    /**
                     * As long as there are test cases, we can continue...otherwise stop
                     */
                    if (modTCList.getSize() > 0)
                    {
                        try
                        {
                            // Store the test cases in the question
                            question.setTestCases(modTCList.cloneTestCases());
                        } catch (CloneNotSupportedException ex)
                        {
                            LOGGER.error("Clone not supported");
                        }

                        /**
                         * Run Raptor and store the returned outputs in the TestCase object
                         */
                        ProcessBuilder launcher = new ProcessBuilder();
                        Map<String, String> environment = launcher.environment();
                        launcher.redirectErrorStream(true);

                        launcher.directory(refFileName.getParentFile());
                        int type = 0;
                        if (rBtnRaptor.isSelected())
                        {
                            type = 0;
                        } else if (rBtnProcessing.isSelected())
                        {
                            type = 1;
                        }
                        Runnable r = new TestCaseGenThread(node, launcher, 
                                refFileName, question, type);

                        Thread testCaseGenThread = new Thread(r);
                        testCaseGenThread.run();
                        while (testCaseGenThread.isAlive())
                        {
                        }

                        /** Set the question category--this is not a nullable field
                         *  This button will not activate if the txtCategory field is 
                         *  null or empty, so we can safely not check for it here.
                         *
                         * First check to see if category already exists in database
                         */
                        //cq = cb.createQuery(Category.class);
                        //Root<Category> categoryRoot = cq.from(Category.class);
                        //cq.where(cb.equal(categoryRoot.get("name"), 
                        //      txtCategory.getText().trim()));
                        //TypedQuery<Category> categoryQuery = em.createQuery(cq);
                        CategoryDAO catDAO = new CategoryDAO(emf.createEntityManager());
                        Category queryCat;
                        try
                        {
                            //queryCat = categoryQuery.getSingleResult();
                            queryCat = catDAO.find(txtCategory.getText().trim());
                            LOGGER.info("Category already exists in database: "
                                    + queryCat.getName());
                        } catch (NoResultException ex)
                        {
                            queryCat = new Category(txtCategory.getText().trim());
                            //tx.begin();
                            //em.persist(queryCat);
                            //tx.commit();
                            Long newCatId = catDAO.create(queryCat);
                            queryCat.setId(newCatId);
                            LOGGER.info("Added Category: " + queryCat.getName());
                        }
                        question.setCategory(queryCat);
                        if (catDAO.isOpen())
                        {
                            catDAO.closeConnection();
                        }
                        
                        // TODO: RAGE-24 - Migrate to the QuestionDAO class
                        cq = cb.createQuery(Question.class);
                        questionRoot = cq.from(Question.class);
                        cq.where(cb.and(cb.equal(questionRoot.get("name"), 
                                question.getName()), cb.equal(questionRoot.get("category"), 
                                question.getCategory())));
                        questionQuery = em.createQuery(cq);

                        Question quest;
                        try
                        {
                            quest = questionQuery.getSingleResult();
                            JOptionPane.showMessageDialog(this, "ERROR: Question " +
                                    "already exists.", "Error", 
                                    JOptionPane.ERROR_MESSAGE);
                            LOGGER.warn("Question already exists: "
                                    + quest.getName());
                        } catch (NoResultException ex)
                        {
                            LOGGER.debug("Question not in database.  Inserting "
                                    + "new question");

                            // Need to persist each of the test cases before the question
                            TestCaseDAO testCaseDAO = new TestCaseDAO(emf.createEntityManager());
                            for (TestCase tc : question.getTestCases())
                            {
                                if (tc.getId() != null)
                                {
                                    try
                                    {
                                        TestCase dbTestCase = testCaseDAO.find(tc.getId());
                                        // TestCase exists in the database, update
                                        tc = testCaseDAO.update(tc);
                                    } catch (IllegalArgumentException exTestCase)
                                    {
                                        // TestCase not in data source, just save
                                        Long newId = testCaseDAO.create(tc);
                                    }
                                } else
                                {
                                    // New TestCase with no Id
                                    Long newId = testCaseDAO.create(tc);
                                    tc.setId(newId);
                                }
//                                tx.begin();
//                                tc.setId(null); // Reset the TestCase Id for persisting
//                                LOGGER.debug("Inputs / Outputs: " + tc.getInputs().size()
//                                        + " / " + tc.getOutputs().size());
//                                em.persist(tc);
//                                tx.commit();
                            }
                            testCaseDAO.closeConnection();
                            // Can persist the question now that all components are saved
                            // TODO: RAGE-24 - Migrate to the QuestionDAO class
                            tx.begin();
                            em.persist(question);
                            tx.commit();
                            JOptionPane.showMessageDialog(this, "Question added: "
                                    + question.getName(), "Success", 
                                    JOptionPane.INFORMATION_MESSAGE);
                            LOGGER.debug("Question added: " + question.getName());
                        }
                        em.close();
                    } else
                    {
                        JOptionPane.showMessageDialog(this, "ERROR: No Test Cases added",
                                "ERROR", JOptionPane.ERROR_MESSAGE);
                        LOGGER.error("No Test Cases added");
                    }
                    em.close();
                } else
                {
                    JOptionPane.showMessageDialog(this, "Question not loaded to database",
                            "Information", JOptionPane.INFORMATION_MESSAGE);
                    LOGGER.error("Question not loaded to database");
                }

            } else
            {
                JOptionPane.showMessageDialog(this,
                        "ERROR: Invalid Reference File Name."
                        + "  Please correct the reference filename and resubmit.",
                        "Error", JOptionPane.ERROR_MESSAGE);
                LOGGER.error("Invalid Reference File Name.");
            }
        }
    }//GEN-LAST:event_btnLoadDBActionPerformed

    /**
     * The ActionPerformed event handler for the Options menu item.  Opens the
     * Option Dialog where the user can specify and save his preferences.
     * 
     * @param evt   The event to handle
     */
    private void optionsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optionsMenuItemActionPerformed

        if (optionDialog == null)
        {
            optionDialog = new OptionsDialog(this, true);
            optionDialog.setPreferences(node);
            optionDialog.setLocationRelativeTo(this);
        }
        optionDialog.setVisible(true);

    }//GEN-LAST:event_optionsMenuItemActionPerformed

    private void btnRemoveQuestionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveQuestionActionPerformed

        if (emf.isOpen() && btnRemoveQuestion.isEnabled())
        {
            LOGGER.debug("Creating EntityManager");
            EntityManager em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();

            Question selectedQuestion = (Question) lstQuestions.getSelectedValue();
            // TODO: RAGE-24 - Migrate to the QuestionDAO class
            Question question = em.find(Question.class, selectedQuestion.getId());
            if (question != null)
            {

                GradedEventDAO gradedEventDAO = 
                    new GradedEventDAO(emf.createEntityManager());
                try
                {
                    // Remove the Question from any GradedEvents it's current part of
                    int count = gradedEventDAO.removeQuestionFromAllGradedEvents(question);
                    LOGGER.info("Question (" + question.getName() + ") removed from " 
                        + count + " Graded Events");
                } catch (Exception ex)
                {
                    LOGGER.warn("(btnRemoveQuestionActionPerformed) Error removing "
                        + "Question (" + question.getName() + ") from all Graded"
                        + " Events");
                } finally
                {
                    gradedEventDAO.closeConnection();
                }
            
                try
                {
                    tx.begin();
                    em.remove(question);
                    tx.commit();
                    // Remove from the list
                    modQuestionList.remove(lstQuestions.getSelectedIndex());
                    JOptionPane.showMessageDialog(this, "Question removed",
                            "Success", JOptionPane.INFORMATION_MESSAGE);
                    LOGGER.info("Question removed");
                } catch (RollbackException ex)
                {
                    JOptionPane.showMessageDialog(this, "ERROR: Unable to delete question",
                            "ERROR", JOptionPane.ERROR_MESSAGE);
                    LOGGER.error("Unble to delete question");
                }
            }

            LOGGER.debug("Closing EntityManager");
            em.close();
        }
    }//GEN-LAST:event_btnRemoveQuestionActionPerformed

    private void connectDBMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connectDBMenuItemActionPerformed

        if (JOptionPane.showConfirmDialog(this, "Connect to the database?",
                "Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
        {
            LOGGER.debug("Setting database properties map based upon current "
                    + "Preferences");
            // Set the database properties from those in the Preferences
            dbProperties.put("javax.persistence.jdbc.driver",
                    node.get("DBDriver", RAGEConst.DEFAULT_DBDRIVER));
            dbProperties.put("javax.persistence.jdbc.url",
                    node.get("DBURI", RAGEConst.DEFAULT_DBURI));
            dbProperties.put("javax.persistence.jdbc.user",
                    node.get("DBUsername", RAGEConst.DEFAULT_DBUSER));
            dbProperties.put("javax.persistence.jdbc.password",
                    node.get("DBPassword", RAGEConst.DEFAULT_DBPASS));
            LOGGER.debug("DDL: " + node.get("DDLGen", ""));
            boolean schemaCreated = false;
            if (!node.get("DDLGen", RAGEConst.DEFAULT_DDL_OPTION)
                    .equals(RAGEConst.DEFAULT_DDL_OPTION))
            {
                dbProperties.put("eclipselink.ddl-generation",
                        node.get("DDLGen", RAGEConst.EMDEDDED_DDL_OPTION));
                LOGGER.debug("Setting dbProperty 'eclipselink.ddl-generation': "
                        + node.get("DDLGen", "Error getting value"));
                schemaCreated = true;
            }

            LOGGER.info("URL: " + dbProperties.get("javax.persistence.jdbc.url"));
            LOGGER.info("User: " + dbProperties.get("javax.persistence.jdbc.user"));
            LOGGER.debug("Driver: " + dbProperties.get("javax.persistence.jdbc.driver"));
            LOGGER.debug("eclipselink.ddl-generation: "
                    + dbProperties.get("eclipselink.ddl-generation"));
            emf = Persistence.createEntityManagerFactory("rage-testgen", dbProperties);
            // Conduct version check
            LOGGER.debug("Checking application version (" + version.getId()
                    + ") against database");
            checkVersion(schemaCreated);
            //EntityManager em = emf.createEntityManager();
            //if (!em.isOpen())
            //{
            //    LOGGER.error("Not connected to the database.");
            //    JOptionPane.showMessageDialog(this, "ERROR: Not connected to the"
            //            + " database.  Check your database connection options.",
            //            "ERROR", JOptionPane.ERROR_MESSAGE);
            //} else
            //{
            //    LOGGER.debug("Creating VersionDAO Variable to use");
            //    VersionDAO versionDAO = new VersionDAO(em);
                
            //    if (schemaCreated)
            //    {
            //        // Insert the version number in the database
            //        LOGGER.debug("We created schema...Version stored on connect");
            //        versionDAO.create(version);
//                    EntityTransaction tx = em.getTransaction();
//                    tx.begin();
//                    em.persist(version);
//                    tx.commit();
            //    } else
            //    {
            //        LOGGER.debug("Schema not created.");
            //    }
//                if (RageLib.checkDBVersion(em, version))
            //    if (versionDAO.checkDbVersion(version))
            //    {
            
            LOGGER.debug("Database version matches application version.");
            // Set the various tabs and button activity state
            databaseActionHelper(true);
            
            //    } else
            //    {
            //        LOGGER.error("Database version does not match.  Exiting program.");
            //        JOptionPane.showMessageDialog(this, "ERROR: This version of"
            //                + " JTestGen is not compatible with the RAGE database."
            //                + " Exiting program.", "ERROR", JOptionPane.ERROR_MESSAGE);
            //        exitMenuItemActionPerformed(evt);
            //    }
            //}
            //em.close();
        }
    }//GEN-LAST:event_connectDBMenuItemActionPerformed

    private void disconnectMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_disconnectMenuItemActionPerformed

        LOGGER.debug("Disconnecting from the database");
        if (emf.isOpen())
        {
            emf.close();
            databaseActionHelper(false);
            LOGGER.info("Disconnected from the database");
        }
        LOGGER.debug("Removing the DDL property.  This will be set on connect.");
        dbProperties.remove("hibernate.hbm2ddl.auto");
    }//GEN-LAST:event_disconnectMenuItemActionPerformed

    private void btnLoadGradedEventsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoadGradedEventsActionPerformed

        if (emf.isOpen())
        {
            LOGGER.debug("Loading GradedEvents");
            //EntityManager em = emf.createEntityManager();
            GradedEventDAO gradedEventDAO = 
                    new GradedEventDAO(emf.createEntityManager());
            try
            {
                modGradedEvents.removeAll();
                List<GradedEvent> events = 
                        gradedEventDAO.getGradedEventsByCourseAndAssignment();
                LOGGER.debug("Found " + events.size() + " Graded Events");
                int count = 0;
                for (int i = 0; i < events.size(); i++)
                {
                    modGradedEvents.add(events.get(i));
                    count++;
                }
                LOGGER.debug("Loaded " + count + " Graded Events to the model");
            } catch (NoResultException ex)
            {
                JOptionPane.showMessageDialog(this, "WARNING: No Graded Events Loaded",
                        "ERROR", JOptionPane.ERROR_MESSAGE);
                LOGGER.warn("No Graded Events loaded");
            } catch (Exception ex)
            {
                LOGGER.error("Exception Caught: " + ex.getLocalizedMessage());
            } finally
            {
                gradedEventDAO.closeConnection();
            }
            //if (em.isOpen())
            //    em.close();
            btnAddGradedEvent.setEnabled(true);
            btnRemoveGradedEvent.setEnabled(true);
        }
    }//GEN-LAST:event_btnLoadGradedEventsActionPerformed

    private void lstGradedEventsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstGradedEventsMouseClicked

        //EntityManager em = emf.createEntityManager();
        CourseDAO courseDAO = new CourseDAO(emf.createEntityManager());
        GradedEventDAO gradedEventDAO = new GradedEventDAO(emf.createEntityManager());
        //EntityTransaction tx = em.getTransaction();

        GradedEvent event = (GradedEvent) lstGradedEvents.getSelectedValue();
        if (event != null)
        {
            txtAssignment.setText(event.getAssignment());
            txtTermYear.setText(event.getTerm());
            txtVersion.setText(event.getVersion());
            chkPartialCredit.setSelected(event.getPartialCredit());
            txtDueDate.setText(event.getDueDate());

            // Get the course for this GradedEvent
            cboCourse.removeAllItems();
            //CriteriaBuilder cb = em.getCriteriaBuilder();
            //CriteriaQuery cq = cb.createQuery(Course.class);
            //Root<Course> courseRoot = cq.from(Course.class);
            //cq.orderBy(cb.asc(courseRoot.get("name")));
            //TypedQuery<Course> courseQuery = em.createQuery(cq);
            //List<Course> courses = courseQuery.getResultList();
            List<Course> courses = courseDAO.getAllCoursesByName();
            for (int i = 0; i < courses.size(); i++)
            {
                cboCourse.addItem(courses.get(i).getName());
                if (courses.get(i).equals(event.getCourse()))
                {
                    cboCourse.setSelectedIndex(i);
                }
            }

            // Get the list of questions for this event
            //GradedEvent gevent = em.find(GradedEvent.class, event.getId());
            GradedEvent gevent = gradedEventDAO.find(event.getId());
            
            modGradedEventQuestions.removeAll();
            for (int i = 0; i < gevent.getQuestions().size(); i++)
            {
                modGradedEventQuestions.add(gevent.getQuestions().get(i));
            }
        }
        //em.close();
        courseDAO.closeConnection();
        gradedEventDAO.closeConnection();
    }//GEN-LAST:event_lstGradedEventsMouseClicked

    private void btnRemoveEventQuestionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveEventQuestionActionPerformed

        if ((lstGradedEventQuestions.getSelectedIndex() >= 0)
                && (lstGradedEvents.getSelectedIndex() >= 0))
        {
            //EntityManager em = emf.createEntityManager();
            //EntityTransaction tx = em.getTransaction();

            GradedEvent selectedEvent = (GradedEvent) lstGradedEvents.getSelectedValue();
            GradedEventDAO gradedEventDAO = new GradedEventDAO(emf.createEntityManager());
            GradedEvent gevent = gradedEventDAO.find(selectedEvent.getId());
            //GradedEvent gevent = em.find(GradedEvent.class, selectedEvent.getId());

            // We were able to pull the GradedEvent object from the DB...now
            // we need to pull it's list of questions and then remove the selected
            // one from it's list and re-persist
            if (gevent != null)
            {
                Question quest = (Question) lstGradedEventQuestions.getSelectedValue();
                if (gevent.removeQuestion(quest))
                {
                    JOptionPane.showMessageDialog(this, "Question " + quest.getName()
                            + " removed from Graded Event " + gevent.getAssignment(),
                            "Success", JOptionPane.INFORMATION_MESSAGE);
                    modGradedEventQuestions.remove(lstGradedEventQuestions.getSelectedIndex());
                    LOGGER.info("Question " + quest.getName() + " removed from "
                            + "Graded Event.");
                } else
                {
                    JOptionPane.showMessageDialog(this, "ERROR: Question not removed",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    LOGGER.warn("Question not removed.");
                }
                //tx.begin();
                //gevent = em.merge(gevent);
                gevent = gradedEventDAO.update(gevent);
                //tx.commit();
                modGradedEvents.update(gevent);
            }
            gradedEventDAO.closeConnection();
            //em.close();
        } else
        {
            LOGGER.debug("No question or event selected.  Selected Indices:");
            LOGGER.debug(" - lstGradedEvents: " + lstGradedEvents.getSelectedIndex());
            LOGGER.debug(" - lstGradedEventQuestions: " + lstGradedEventQuestions.getSelectedIndex());
        }
    }//GEN-LAST:event_btnRemoveEventQuestionActionPerformed

    private void lstGradedEventQuestionsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstGradedEventQuestionsMouseClicked

        btnRemoveEventQuestion.setEnabled(true);
    }//GEN-LAST:event_lstGradedEventQuestionsMouseClicked

    private void btnLoadQuestionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoadQuestionsActionPerformed

        if (emf.isOpen())
        {
            EntityManager em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            LOGGER.debug("Loading Questions in the Database");
            // TODO: RAGE-24 - Migrate to the QuestionDAO class
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Question> cq = cb.createQuery(Question.class);
            Root<Question> questionRoot = cq.from(Question.class);
            cq.orderBy(cb.asc(questionRoot.get("name")));
            TypedQuery<Question> questionQuery = em.createQuery(cq);
            try
            {
                modQuestionList.removeAll();
                List<Question> questions = questionQuery.getResultList();
                LOGGER.debug("Found " + questions.size() + " Questions");
                int count = 0;
                for (int i = 0; i < questions.size(); i++)
                {
                    modQuestionList.add(questions.get(i));
                    count++;
                }
                LOGGER.debug("Loaded " + count + " Questions");

                //Code to clear the Question Generator if the user was prviously
                //editing a question
                btnRemoveQuestion.setEnabled(true);
                btnEditQuestion.setEnabled(true);
                modTCList.removeAll();
                modInputList.removeAll();
                modExclusionList.removeAll();
                questionValue = questionValue.multiply(BigDecimal.ZERO);
                txtValue.setText(questionValue.toPlainString());
                chkOrdered.setSelected(false);
                chkVerbatim.setSelected(false);
                txtDescription.setText(null);
                txtCategory.setText(null);
                btnInputAdd.setEnabled(false);
                btnInputRemove.setEnabled(false);
                btnExcAdd.setEnabled(false);
                btnExcRemove.setEnabled(false);
                btnTCRemove.setEnabled(false);
                //.setEnabled(false);
            } catch (NoResultException ex)
            {
                JOptionPane.showMessageDialog(this, "ERROR: No Questions Loaded",
                        "ERROR", JOptionPane.ERROR_MESSAGE);
            }
            em.close();
        }
    }//GEN-LAST:event_btnLoadQuestionsActionPerformed

    private void loadGradedEventMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadGradedEventMenuItemActionPerformed

        if (emf != null && emf.isOpen())
        {
            if (loadGradedEvent == null)
            {
                loadGradedEvent = new LoadGradedEventDialog(this, false);
                loadGradedEvent.setLocationRelativeTo(this);
                loadGradedEvent.setTitle("Load Graded Event");
            }
            loadGradedEvent.setEntityManager(emf.createEntityManager());
            loadGradedEvent.setVisible(true);

        } else
        {
            JOptionPane.showMessageDialog(this, "ERROR: Not connected to the database",
                    "ERROR", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_loadGradedEventMenuItemActionPerformed

    private void loadQuestionMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadQuestionMenuItemActionPerformed

        if (emf != null && emf.isOpen())
        {
            if (loadQuestionDlg == null)
            {
                loadQuestionDlg = new LoadQuestionDialog(this, false);
                loadQuestionDlg.setLocationRelativeTo(this);
                loadQuestionDlg.setTitle("Load Question");
            }
            loadQuestionDlg.setEntityManager(emf.createEntityManager());
            loadQuestionDlg.setVisible(true);

        } else
        {
            JOptionPane.showMessageDialog(this, "ERROR: Not connected to the database",
                    "ERROR", JOptionPane.ERROR_MESSAGE);
            LOGGER.error("Not connected to the database.");
        }

    }//GEN-LAST:event_loadQuestionMenuItemActionPerformed

    private void btnAddGEQuestionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddGEQuestionActionPerformed

        if (emf != null && emf.isOpen())
        {
            LOGGER.debug("Adding Questions to Graded Event");
            if (selectQuestionDialog == null)
            {
                LOGGER.debug("selectQuestionDialog is null");
                selectQuestionDialog = new SelectQuestionDialog(this, false);
                LOGGER.debug("Created new SelectQuestioNDialog");
                selectQuestionDialog.setLocationRelativeTo(this);
                LOGGER.debug("Location set.  Setting title");
                selectQuestionDialog.setTitle("Select Questions");
                LOGGER.debug("Title set: " + selectQuestionDialog.getTitle());
            }
            LOGGER.debug("Initializing SelectQuestionDialog");
            try
            {
                selectQuestionDialog.initialize(emf,
                        (GradedEvent) lstGradedEvents.getSelectedValue());
            } catch (IllegalArgumentException ex)
            {
                LOGGER.error("IllegalArgumentException: " + ex.getLocalizedMessage());
            }
            selectQuestionDialog.setModalityType(ModalityType.APPLICATION_MODAL);
            selectQuestionDialog.setVisible(true);
            if (selectQuestionDialog.getReturnStatus() == SelectQuestionDialog.RET_OK)
            {
                LOGGER.debug("Updating list of questions for the GradedEvent");
                GradedEvent gradedEvent = (GradedEvent) lstGradedEvents.getSelectedValue();
                //EntityManager em = emf.createEntityManager();
                //EntityTransaction tx = em.getTransaction();
                LOGGER.debug("Merging GradedEvent with Persistence Context");
                GradedEventDAO gradedEventDAO = new GradedEventDAO(emf.createEntityManager());
                //tx.begin();
                //gradedEvent = em.find(GradedEvent.class, gradedEvent.getId());
                //tx.commit();
                gradedEvent = gradedEventDAO.find(gradedEvent.getId());
                LOGGER.debug("Adding " + gradedEvent.getQuestions().size()
                        + " Questions to GradedEvent Question List Model");
                int actual = 0;
                modGradedEventQuestions.removeAll();
                for (int i = 0; i < gradedEvent.getQuestions().size(); i++)
                {
                    modGradedEventQuestions.add(gradedEvent.getQuestions().get(i));
                    actual++;
                }
                LOGGER.debug("Added " + actual + " Questions to List Model");
                // Update the Graded Event List Model
                modGradedEvents.update(gradedEvent);
                //em.close();
                gradedEventDAO.closeConnection();
            } else
            {
                LOGGER.debug("Questions not added");
            }
        } else
        {
            JOptionPane.showMessageDialog(this, "ERROR: Not connected to the database",
                    "ERROR", JOptionPane.ERROR_MESSAGE);
            LOGGER.error("Not connected to the database");
        }
    }//GEN-LAST:event_btnAddGEQuestionActionPerformed

    private void btnAddGradedEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddGradedEventActionPerformed

        if (emf != null && emf.isOpen())
        {
            LOGGER.debug("Adding Graded Event");
            if (newGradedEventDialog == null)
            {
                newGradedEventDialog = new NewGradedEventDialog(this, false);
                newGradedEventDialog.setLocationRelativeTo(this);
                newGradedEventDialog.setTitle("Add New Graded Event");
            }
            newGradedEventDialog.initialize(emf);
            newGradedEventDialog.setModalityType(ModalityType.APPLICATION_MODAL);
            newGradedEventDialog.setVisible(true);
            if (newGradedEventDialog.getReturnStatus() == NewGradedEventDialog.RET_OK)
            {
                JOptionPane.showMessageDialog(this, "Graded Event Added Successfully",
                        "Success", JOptionPane.INFORMATION_MESSAGE);
                LOGGER.debug("Clearing GradedEvent model to reload values");
                modGradedEvents.removeAll();
                GradedEventDAO gradedEventDAO =
                        new GradedEventDAO(emf.createEntityManager());
                List<GradedEvent> gradedEvents =
                        gradedEventDAO.getGradedEventsByCourseAndAssignment();

                int count = 0;
                for (int i = 0; i < gradedEvents.size(); i++)
                {
                    modGradedEvents.add(gradedEvents.get(i));
                    count++;
                }
                LOGGER.debug("Added " + count + " Graded Events to the model");
                gradedEventDAO.closeConnection();
                LOGGER.info("Graded Event added");
            } else
            {
                LOGGER.info("Graded Event not added");
            }
        } else
        {
            JOptionPane.showMessageDialog(this, "ERROR: Not connected to the database",
                    "ERROR", JOptionPane.ERROR_MESSAGE);
            LOGGER.error("Not connected to the database");
        }
    }//GEN-LAST:event_btnAddGradedEventActionPerformed

    private void btnEditQuestionAction(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditQuestionAction
        if (lstQuestions.getSelectedIndex() == -1)
        {
            JOptionPane.showMessageDialog(this, "ERROR: No Question Selected",
                    "WARNING", JOptionPane.WARNING_MESSAGE);
            LOGGER.warn("Quesiton Not Selected");
        } else
        {
            EntityManager em = emf.createEntityManager();
            Question quest = (Question) lstQuestions.getSelectedValue();
            // TODO: RAGE-24 - Migrate to the QuestionDAO class
            Question question = em.find(Question.class, quest.getId());

            JOptionPane.showMessageDialog(null, "You must select a Reference "
                    + "Program and Destination File before reloading this question to"
                    + " the database", "Warning", JOptionPane.WARNING_MESSAGE);

            List<TestCase> testCaseList = question.getTestCases();
            modTCList.removeAll();

            //Reset the total value of the testcases to zero before calculating
            //the new value.

            questionValue = questionValue.multiply(BigDecimal.ZERO);

            for (int i = 0; i < testCaseList.size(); i++)
            {

                modTCList.addTestCase(testCaseList.get(i));
                questionValue = questionValue.add(testCaseList.get(i).getValue());
            }
            //Check the question to see if we are expecting an ordered output
            // If we are, turn on the Ordered Output Checkbox

            if (question.getOrderedOutput() == true)
            {
                chkOrdered.setSelected(true);
            }

            //Check the question to see if we are expecting verbatim output
            //If so, turn on the Verbatium checkbox

            if (question.getVerbatim() == true)
            {
                chkVerbatim.setSelected(true);
            }

            //Set the description field with the description of the question
            txtDescription.setText(question.getDescription());
            //Use a tempory category object to grab the name of the category as
            // a string and set the category text box to that catgory name
            Category tempCategory = question.getCategory();
            txtCategory.setText(tempCategory.getName());
            txtValue.setText(questionValue.toPlainString());
            em.close();
        }

    }//GEN-LAST:event_btnEditQuestionAction

    private void btnRemoveGradedEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveGradedEventActionPerformed
        // Remove GradedEvent from the database
        if (emf != null && emf.isOpen() && lstGradedEvents.getSelectedIndex() >= 0)
        {
            //EntityManager em = emf.createEntityManager();
            GradedEvent selectedEvent = (GradedEvent) lstGradedEvents.getSelectedValue();
            GradedEventDAO gradedEventDAO = new GradedEventDAO(emf.createEntityManager());
            //GradedEvent gradedEvent = em.find(GradedEvent.class, selectedEvent.getId());
            GradedEvent gradedEvent = gradedEventDAO.find(selectedEvent.getId());
            gradedEvent.clearQuestions();
            //EntityTransaction tx = em.getTransaction();
            //tx.begin();
            gradedEvent = gradedEventDAO.update(gradedEvent);
            //gradedEvent = em.merge(gradedEvent);
            gradedEventDAO.delete(gradedEvent.getId());
            //em.remove(gradedEvent);
            //tx.commit();
            //em.close();
            gradedEventDAO.closeConnection();
        }
    }//GEN-LAST:event_btnRemoveGradedEventActionPerformed

    private void databaseActionHelper(boolean status)
    {
        // Activates or deactivates buttons and menu items depending on status
        connectDBMenuItem.setEnabled(!status);
        disconnectMenuItem.setEnabled(status);
        btnRemoveQuestion.setEnabled(status);
        btnLoadGradedEvents.setEnabled(status);
        btnLoadQuestions.setEnabled(status);
        btnLoadDB.setEnabled(status);
        btnAddGEQuestion.setEnabled(status);

        // Clear the lists of Graded Events and Questions when you disconnect
        if (!status)
        {
            modQuestionList.removeAll();
            modGradedEvents.removeAll();
            modGradedEventQuestions.removeAll();
            dlgQuestionList.removeAll();
        }
    }

    private void checkVersion(boolean createSchema)
    {
        try
        {
            LOGGER.debug("(checkVersion) Creating VersionDAO object");
            VersionDAO versionDAO = new VersionDAO(emf.createEntityManager());
            LOGGER.debug("(checkVersion) VersionDAO object created");
            // If this is a new install and the option to create the database
            // schema is set, do so here
            if (createSchema)
            {
                // Insert the version number in the database
                LOGGER.debug("We created schema...Version stored on connect");
                versionDAO.create(version);
//                    EntityTransaction tx = em.getTransaction();
//                    tx.begin();
//                    em.persist(version);
//                    tx.commit();
            } else
            {
                LOGGER.debug("Schema not created.");
            }
            if (versionDAO.checkDbVersion(version))
            {
                LOGGER.debug("(checkVersion) Database version matches "
                        + "application version.");
            } else
            {
                LOGGER.error("(checkVersion) Database version does not match. "
                        + "Exiting program.");
                JOptionPane.showMessageDialog(this, "ERROR: This version of "
                        + "JTestGen is not compatible with the RAGE database. "
                        + "Exiting program.",
                        "ERROR", JOptionPane.ERROR_MESSAGE);
                exitMenuItemActionPerformed(null);
            }
            LOGGER.debug("(checkVersion) Closing VersionDAO connection");
            versionDAO.closeConnection();
        } catch (IllegalStateException ex)
        {
            LOGGER.warn("(checkVersion) Problem connecting to the database "
                    + "with the VersionDAO object");
        } 
    }
    
    /**
     * The main method to the JTestGen program.  Sets the Look And Feel for the
     * program to be the system default.  This will help to ensure a more native
     * feel for the user.
     *
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {

            @Override
            public void run()
            {
                try
                {
                    System.setProperty("apple.laf.useScreenMenuBar", "true");
                    System.setProperty("com.apple.mrj.application.apple.menu.about.name",
                            "JTestGen");
                    // Set the default look and feel to be whatever the system default is
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | 
                        IllegalAccessException | UnsupportedLookAndFeelException e)
                {
                    LOGGER.error("Error setting native look and feel");
                }
                new JTestGen().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JButton btnAddGEQuestion;
    private javax.swing.JButton btnAddGradedEvent;
    private javax.swing.JButton btnBrowseProgram;
    private javax.swing.JButton btnDestBrowse;
    private javax.swing.JButton btnEditQuestion;
    private javax.swing.JButton btnExcAdd;
    private javax.swing.JButton btnExcRemove;
    private javax.swing.JButton btnGenerateXML;
    private javax.swing.JButton btnInputAdd;
    private javax.swing.JButton btnInputRemove;
    private javax.swing.JButton btnLoadDB;
    private javax.swing.JButton btnLoadGradedEvents;
    private javax.swing.JButton btnLoadQuestions;
    private javax.swing.JButton btnRemoveEventQuestion;
    private javax.swing.JButton btnRemoveGradedEvent;
    private javax.swing.JButton btnRemoveQuestion;
    private javax.swing.JButton btnTCAdd;
    private javax.swing.JButton btnTCRemove;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cboCourse;
    private javax.swing.JCheckBox chkOrdered;
    private javax.swing.JCheckBox chkPartialCredit;
    private javax.swing.JCheckBox chkVerbatim;
    private javax.swing.JMenuItem connectDBMenuItem;
    private javax.swing.JMenuItem disconnectMenuItem;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblAssignment;
    private javax.swing.JLabel lblCategory;
    private javax.swing.JLabel lblCourse;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblDestFilename;
    private javax.swing.JLabel lblDueDate;
    private javax.swing.JLabel lblExclusions;
    private javax.swing.JLabel lblGradedEventQuestions;
    private javax.swing.JLabel lblGradedEvents;
    private javax.swing.JLabel lblInputs;
    private javax.swing.JLabel lblRefInstructions;
    private javax.swing.JLabel lblRefProgram;
    private javax.swing.JLabel lblTerm;
    private javax.swing.JLabel lblTestCases;
    private javax.swing.JLabel lblValue;
    private javax.swing.JMenuItem loadGradedEventMenuItem;
    private javax.swing.JMenuItem loadQuestionMenuItem;
    private javax.swing.JList<String> lstExclusions;
    private javax.swing.JList<Question> lstGradedEventQuestions;
    private javax.swing.JList<GradedEvent> lstGradedEvents;
    private javax.swing.JList<String> lstInputs;
    private javax.swing.JList<Question> lstQuestions;
    private javax.swing.JList<TestCase> lstTestCases;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem optionsMenuItem;
    private javax.swing.JPanel pnlGradedEventMgt;
    private javax.swing.JPanel pnlQuestionMgt;
    private javax.swing.JPanel pnlReference;
    private javax.swing.JRadioButton rBtnProcessing;
    private javax.swing.JRadioButton rBtnRaptor;
    private javax.swing.JMenu toolsMenu;
    private javax.swing.JTextField txtAssignment;
    private javax.swing.JTextField txtCategory;
    private javax.swing.JTextField txtDescription;
    private javax.swing.JTextField txtDestFilename;
    private javax.swing.JTextField txtDueDate;
    private javax.swing.JTextField txtRefProgram;
    private javax.swing.JTextField txtTermYear;
    private javax.swing.JTextField txtValue;
    private javax.swing.JTextField txtVersion;
    // End of variables declaration//GEN-END:variables
    // Additional variables declaration
    private EntityManagerFactory emf;
    private JFileChooser chooser;
    private final TestCaseListModel modTCList = new TestCaseListModel();
    private final StringListModel modInputList = new StringListModel();
    private final StringListModel modExclusionList = new StringListModel();
    private final QuestionListModel modQuestionList = new QuestionListModel();
    private final GradedEventListModel modGradedEvents = new GradedEventListModel();
    private final QuestionListModel modGradedEventQuestions = new QuestionListModel();
    private Long index = 0L;
    private BigDecimal questionValue = new BigDecimal("0.0");
    private final Preferences rootPref = Preferences.userRoot();
    private final Preferences node = rootPref.node("/com/darkhonor/rage");
    private OptionsDialog optionDialog;
    private final Map<String,String> dbProperties = new HashMap<>();
    private LoadGradedEventDialog loadGradedEvent;
    private LoadQuestionDialog loadQuestionDlg;
    private final QuestionListModel dlgQuestionList = new QuestionListModel();
    private SelectQuestionDialog selectQuestionDialog;
    private NewGradedEventDialog newGradedEventDialog;
    private static final Version version = new Version(2.1);
    private static final Logger LOGGER = Logger.getLogger(JTestGen.class);

    public class RageWindowAdapter extends WindowAdapter
    {

        @Override
        public void windowClosing(WindowEvent e)
        {
            try
            {
                if ((emf != null) && (emf.isOpen()))
                {
                    LOGGER.debug("Disconnecting from the database");
                    emf.close();
                    LOGGER.info("Disconnected from the database.  Exiting program.");
                }
            } catch (Exception ex)
            {
            }
            System.exit(0);
        }
    }
}
