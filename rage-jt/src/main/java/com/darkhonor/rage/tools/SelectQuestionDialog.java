/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.tools;

import com.darkhonor.rage.libs.dataaccess.GradedEventDAO;
import com.darkhonor.rage.libs.listmodel.QuestionListModel;
import com.darkhonor.rage.model.GradedEvent;
import com.darkhonor.rage.model.Question;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

/**
 *
 * @author Alexander.Ackerman
 */
public class SelectQuestionDialog extends javax.swing.JDialog
{

    /** A return status code - returned if Cancel button has been pressed */
    public static final int RET_CANCEL = 0;
    /** A return status code - returned if OK button has been pressed */
    public static final int RET_OK = 1;

    /** Creates new form SelectQuestionDialog */
    public SelectQuestionDialog(java.awt.Frame parent, boolean modal)
    {
        super(parent, modal);
        try
        {
            initComponents();
            lstQuestions.setModel(modQuestionList);
            okButton.setEnabled(false);
            cancelButton.setEnabled(true);
        } catch (Exception ex)
        {
            LOGGER.error("Exception: " + ex.getLocalizedMessage());
        }
    }

    /** @return the return status of this dialog - one of RET_OK or RET_CANCEL */
    public int getReturnStatus()
    {
        return returnStatus;
    }

    public void initialize(EntityManagerFactory emf, GradedEvent gradedEvent)
            throws IllegalArgumentException
    {
        if (emf == null)
        {
            throw new IllegalArgumentException("EntityManagerFactory is Null");
        } else if (gradedEvent == null)
        {
            throw new IllegalArgumentException("GradedEvent is Null");
        } else if (!emf.isOpen())
        {
            throw new IllegalArgumentException("Connection to database is not active");
        } else
        {
            this.emf = emf;
            EntityManager em = this.emf.createEntityManager();
            GradedEventDAO gradedEventDAO = new GradedEventDAO(emf.createEntityManager());
            gradedEvent = gradedEventDAO.find(gradedEvent.getId());
            //this.gradedEvent = em.find(GradedEvent.class, gradedEvent.getId());

            lblGradedEvent.setText(gradedEvent.getCourse().getName() + " ("
                    + gradedEvent.getTerm() + ") " + gradedEvent.getAssignment()
                    + ", Version: " + gradedEvent.getVersion());

            // Generate list of Question Id's to pass to Query
            ArrayList<Long> questionIds = new ArrayList<Long>();
            for (int i = 0; i < this.gradedEvent.getQuestions().size(); i++)
            {
                questionIds.add(this.gradedEvent.getQuestions().get(i).getId());
            }
            LOGGER.debug("Adding Questions to QuestionListModel");
            // Build the Database Query
            // TODO: RAGE-24 - Migrate to the QuestionDAO class
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Question> cq = cb.createQuery(Question.class);
            Root<Question> questionRoot = cq.from(Question.class);
            // Restrict result to those not found in the list of Question Id's
            cq.where(cb.not(questionRoot.get("id").in(questionIds)));
            cq.orderBy(cb.asc(questionRoot.get("name")));
            TypedQuery<Question> questionQuery = em.createQuery(cq);
            try
            {
                LOGGER.debug("Getting list of Questions");
                List<Question> result = questionQuery.getResultList();
                LOGGER.debug("Found " + result.size() + " Questions");

                // Fill the QuestionListModel on the page
                modQuestionList.removeAll();
                int actual = 0;
                for (int i = 0; i < result.size(); i++)
                {
                    Question question = result.get(i);
                    LOGGER.debug("Adding Question: " + question.getName());
                    modQuestionList.add(question);
                    actual++;
                }
                LOGGER.debug("Added " + actual + " Questions");
                okButton.setEnabled(true);
            } catch (NoResultException ex)
            {
                JOptionPane.showMessageDialog(this, "ERROR: No Questions Found",
                        "ERROR", JOptionPane.ERROR_MESSAGE);
                LOGGER.error("No Questions found");
                okButton.setEnabled(false);
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, "ERROR: There is an error "
                        + "loading questions. ", "ERROR", JOptionPane.ERROR_MESSAGE);
                LOGGER.error("Error adding questions.  Exception: "
                        + ex.getLocalizedMessage());
                okButton.setEnabled(false);
            } finally
            {
                em.close();
                gradedEventDAO.closeConnection();
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstQuestions = new javax.swing.JList();
        lblInstructions = new javax.swing.JLabel();
        lblGradedEvent = new javax.swing.JLabel();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        lstQuestions.setName("lstQuestions"); // NOI18N
        jScrollPane1.setViewportView(lstQuestions);

        lblInstructions.setText("Select all questions you want to add to Graded Event:");
        lblInstructions.setName("lblInstructions"); // NOI18N

        lblGradedEvent.setText("<Graded Event Name>");
        lblGradedEvent.setName("lblGradedEvent"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cancelButton))
                            .addComponent(lblInstructions))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lblGradedEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14))))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cancelButton, okButton});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblInstructions)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblGradedEvent, javax.swing.GroupLayout.DEFAULT_SIZE, 16, Short.MAX_VALUE)
                .addGap(12, 12, 12)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(okButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed

        if (lstQuestions.getSelectedIndex() == -1)
        {
            JOptionPane.showMessageDialog(this, "ERROR: Please select at least "
                    + "one question before continuing or Cancel to exit.", "ERROR",
                    JOptionPane.ERROR_MESSAGE);
            LOGGER.error("No Questions selected");
        } else
        {
            EntityManager em = emf.createEntityManager();
            //LOGGER.debug("Pull GradedEvent from Database");
            //gradedEvent = em.find(GradedEvent.class, gradedEvent.getId());
            List<Question> questions = lstQuestions.getSelectedValuesList();
            LOGGER.debug("Adding " + questions.size() + " Questions to GradedEvent");
            int actual = 0;
            for (int i = 0; i < questions.size(); i++)
            {
                Question question = questions.get(i);
                try
                {
                    // TODO: RAGE-24 - Migrate to the QuestionDAO class
                    question = em.find(Question.class, question.getId());
                    gradedEvent.addQuestion(question);
                    actual++;
                } catch (IllegalArgumentException ex)
                {
                    LOGGER.error("Exception: " + ex.getLocalizedMessage());
                }
            }
            LOGGER.debug("Added " + actual + " Questions to GradedEvent");
            LOGGER.debug("Saving GradedEvent");
            // TODO: RAGE-69 - Migrate to the GradedEventDAO class
            GradedEventDAO gradedEventDAO = new GradedEventDAO(emf.createEntityManager());
//            EntityTransaction tx = em.getTransaction();
            try
            {
                gradedEventDAO.update(gradedEvent);
//                tx.begin();
//                em.merge(gradedEvent);
//                tx.commit();
            } catch (IllegalStateException ex)
            {
                LOGGER.error("Error saving Graded Event to database.  Exception: "
                        + ex.getLocalizedMessage());
//                tx.rollback();
            } catch (NullPointerException ex)
            {
                LOGGER.error("Error saving Graded Event to database.  Exception: "
                        + ex.getLocalizedMessage());
//                tx.rollback();
            } catch (IllegalArgumentException ex)
            {
                LOGGER.error("Error saving Graded Event to database.  Exception: "
                        + ex.getLocalizedMessage());
//                tx.rollback();
            } finally
            {
                gradedEventDAO.closeConnection();
            }
            em.close();
        }

        doClose(RET_OK);
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        doClose(RET_CANCEL);
    }//GEN-LAST:event_cancelButtonActionPerformed

    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        doClose(RET_CANCEL);
    }//GEN-LAST:event_closeDialog

    private void doClose(int retStatus)
    {
        returnStatus = retStatus;
        setVisible(false);
        LOGGER.debug("Notifying waiting parent thread");
        this.notifyAll();
        dispose();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {

            @Override
            public void run()
            {
                SelectQuestionDialog dialog = new SelectQuestionDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter()
                {

                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e)
                    {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblGradedEvent;
    private javax.swing.JLabel lblInstructions;
    private javax.swing.JList<Question> lstQuestions;
    private javax.swing.JButton okButton;
    // End of variables declaration//GEN-END:variables
    private int returnStatus = RET_CANCEL;
    private QuestionListModel modQuestionList = new QuestionListModel();
    private EntityManagerFactory emf;
    private GradedEvent gradedEvent;
    private static final Logger LOGGER = Logger.getLogger(SelectQuestionDialog.class);
}
