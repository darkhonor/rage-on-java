
    alter table COURSE_INSTRUCTOR 
        drop constraint FKFFAB6F41D5C20AB1;

    alter table COURSE_INSTRUCTOR 
        drop constraint FKFFAB6F41A8E2C695;

    alter table Course 
        drop constraint FK_CD_ID;

    alter table GRADED_EVENT_QUESTION 
        drop constraint FKA83BE31DD5349B3E;

    alter table GRADED_EVENT_QUESTION 
        drop constraint FKA83BE31D21C75FC9;

    alter table GradedEvent 
        drop constraint FK_COURSE_ID;

    alter table INSTRUCTOR_SECTIONS 
        drop constraint FKD1E6390EE8FE0A3;

    alter table INSTRUCTOR_SECTIONS 
        drop constraint FKD1E6390B4D5AF45;

    alter table Instructor 
        drop constraint FKC9FAECBDD42E469C;

    alter table QUESTION_TEST_CASES 
        drop constraint FK1D1B3B2F1B115888;

    alter table QUESTION_TEST_CASES 
        drop constraint FK1D1B3B2F7F13BBC3;

    alter table Question 
        drop constraint FK_Category_ID;

    alter table SECTIONS_COURSE 
        drop constraint FKC081D34CB4D5AF45;

    alter table SECTIONS_COURSE 
        drop constraint FKC081D34C28DFCF63;

    alter table Student 
        drop constraint FKF3371A1B4466CA04;

    alter table Student 
        drop constraint FKF3371A1BA99834D3;

    alter table TEST_CASE_EXCLUSION 
        drop constraint FKF9043AAC1B1158DE;

    alter table TEST_CASE_INPUT 
        drop constraint FKA357FBA81B1158DE;

    alter table TEST_CASE_OUTPUT 
        drop constraint FKD248F0C31B1158DE;

    drop table CLASS_SECTION;

    drop table COURSE_INSTRUCTOR;

    drop table Category;

    drop table Course;

    drop table GRADED_EVENT_QUESTION;

    drop table GradedEvent;

    drop table INSTRUCTOR_SECTIONS;

    drop table Instructor;

    drop table Person;

    drop table QUESTION_TEST_CASES;

    drop table Question;

    drop table SECTIONS_COURSE;

    drop table Student;

    drop table TEST_CASE_EXCLUSION;

    drop table TEST_CASE_INPUT;

    drop table TEST_CASE_OUTPUT;

    drop table TestCase;

    drop table Version;

    drop table hibernate_unique_key;

    create table CLASS_SECTION (
        SECTION_INDEX bigint not null,
        SECTION_NAME varchar(255),
        primary key (SECTION_INDEX)
    );

    create table COURSE_INSTRUCTOR (
        CRS_INDEX bigint not null,
        INST_INDEX bigint not null
    );

    create table Category (
        CAT_INDEX bigint not null,
        CAT_NAME varchar(255),
        primary key (CAT_INDEX)
    );

    create table Course (
        COURSE_INDEX bigint not null,
        COURSE_NAME varchar(255),
        PERSON_INDEX bigint,
        primary key (COURSE_INDEX)
    );

    create table GRADED_EVENT_QUESTION (
        GRADED_EVENT_INDEX bigint not null,
        QUESTION_INDEX bigint not null
    );

    create table GradedEvent (
        GRADED_EVENT_INDEX bigint not null,
        ASSIGNMENT varchar(255),
        PARTIAL_CREDIT smallint,
        TERM varchar(255),
        VERSION varchar(255),
        COURSE_INDEX bigint not null,
        primary key (GRADED_EVENT_INDEX)
    );

    create table INSTRUCTOR_SECTIONS (
        Instructor_fk bigint,
        Section_fk bigint not null,
        primary key (Section_fk)
    );

    create table Instructor (
        DOMAIN_ACNT varchar(255),
        PASSWORD varchar(255),
        PERSON_INDEX bigint not null,
        primary key (PERSON_INDEX)
    );

    create table Person (
        PERSON_INDEX bigint not null,
        FIRST_NAME varchar(255),
        LAST_NAME varchar(255),
        WEBID varchar(255),
        primary key (PERSON_INDEX)
    );

    create table QUESTION_TEST_CASES (
        QUESTION_fk bigint not null,
        TEST_CASE_fk bigint not null,
        POSITION integer not null,
        primary key (QUESTION_fk, POSITION),
        unique (TEST_CASE_fk)
    );

    create table Question (
        QUESTION_ID bigint not null,
        DESCRIPTION varchar(255),
        NAME varchar(255),
        Ordered_Output smallint,
        VERBATIM smallint,
        CAT_INDEX bigint not null,
        primary key (QUESTION_ID)
    );

    create table SECTIONS_COURSE (
        Course_fk bigint,
        Section_fk bigint not null,
        primary key (Section_fk)
    );

    create table Student (
        CLASS_YEAR integer,
        STUDENT_ID bigint not null,
        section_SECTION_INDEX bigint,
        primary key (STUDENT_ID)
    );

    create table TEST_CASE_EXCLUSION (
        TEST_CASE_ID bigint not null,
        EXCLUDES varchar(255)
    );

    create table TEST_CASE_INPUT (
        TEST_CASE_ID bigint not null,
        INPUT_VALUE varchar(255),
        POSITION integer not null,
        primary key (TEST_CASE_ID, POSITION)
    );

    create table TEST_CASE_OUTPUT (
        TEST_CASE_ID bigint not null,
        OUTPUT_VALUE varchar(255),
        POSITION integer not null,
        primary key (TEST_CASE_ID, POSITION)
    );

    create table TestCase (
        TEST_CASE_ID bigint not null,
        TEST_CASE_VALUE numeric(19,2),
        primary key (TEST_CASE_ID)
    );

    create table Version (
        id double not null,
        primary key (id)
    );

    alter table COURSE_INSTRUCTOR 
        add constraint FKFFAB6F41D5C20AB1 
        foreign key (CRS_INDEX) 
        references Course;

    alter table COURSE_INSTRUCTOR 
        add constraint FKFFAB6F41A8E2C695 
        foreign key (INST_INDEX) 
        references Instructor;

    alter table Course 
        add constraint FK_CD_ID 
        foreign key (PERSON_INDEX) 
        references Instructor;

    alter table GRADED_EVENT_QUESTION 
        add constraint FKA83BE31DD5349B3E 
        foreign key (QUESTION_INDEX) 
        references Question;

    alter table GRADED_EVENT_QUESTION 
        add constraint FKA83BE31D21C75FC9 
        foreign key (GRADED_EVENT_INDEX) 
        references GradedEvent;

    alter table GradedEvent 
        add constraint FK_COURSE_ID 
        foreign key (COURSE_INDEX) 
        references Course;

    alter table INSTRUCTOR_SECTIONS 
        add constraint FKD1E6390EE8FE0A3 
        foreign key (Instructor_fk) 
        references Instructor;

    alter table INSTRUCTOR_SECTIONS 
        add constraint FKD1E6390B4D5AF45 
        foreign key (Section_fk) 
        references CLASS_SECTION;

    alter table Instructor 
        add constraint FKC9FAECBDD42E469C 
        foreign key (PERSON_INDEX) 
        references Person;

    alter table QUESTION_TEST_CASES 
        add constraint FK1D1B3B2F1B115888 
        foreign key (TEST_CASE_fk) 
        references TestCase;

    alter table QUESTION_TEST_CASES 
        add constraint FK1D1B3B2F7F13BBC3 
        foreign key (QUESTION_fk) 
        references Question;

    alter table Question 
        add constraint FK_Category_ID 
        foreign key (CAT_INDEX) 
        references Category;

    alter table SECTIONS_COURSE 
        add constraint FKC081D34CB4D5AF45 
        foreign key (Section_fk) 
        references CLASS_SECTION;

    alter table SECTIONS_COURSE 
        add constraint FKC081D34C28DFCF63 
        foreign key (Course_fk) 
        references Course;

    alter table Student 
        add constraint FKF3371A1B4466CA04 
        foreign key (section_SECTION_INDEX) 
        references CLASS_SECTION;

    alter table Student 
        add constraint FKF3371A1BA99834D3 
        foreign key (STUDENT_ID) 
        references Person;

    alter table TEST_CASE_EXCLUSION 
        add constraint FKF9043AAC1B1158DE 
        foreign key (TEST_CASE_ID) 
        references TestCase;

    alter table TEST_CASE_INPUT 
        add constraint FKA357FBA81B1158DE 
        foreign key (TEST_CASE_ID) 
        references TestCase;

    alter table TEST_CASE_OUTPUT 
        add constraint FKD248F0C31B1158DE 
        foreign key (TEST_CASE_ID) 
        references TestCase;

    create table hibernate_unique_key (
         next_hi integer 
    );

    insert into hibernate_unique_key values ( 0 );
