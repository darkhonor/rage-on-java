
    alter table COURSE_INSTRUCTOR 
        drop 
        foreign key FKFFAB6F41D5C20AB1;

    alter table COURSE_INSTRUCTOR 
        drop 
        foreign key FKFFAB6F41A8E2C695;

    alter table Course 
        drop 
        foreign key FK_CD_ID;

    alter table GRADED_EVENT_QUESTION 
        drop 
        foreign key FKA83BE31DD5349B3E;

    alter table GRADED_EVENT_QUESTION 
        drop 
        foreign key FKA83BE31D21C75FC9;

    alter table GradedEvent 
        drop 
        foreign key FK_COURSE_ID;

    alter table INSTRUCTOR_SECTIONS 
        drop 
        foreign key FKD1E6390EE8FE0A3;

    alter table INSTRUCTOR_SECTIONS 
        drop 
        foreign key FKD1E6390B4D5AF45;

    alter table Instructor 
        drop 
        foreign key FKC9FAECBDD42E469C;

    alter table QUESTION_TEST_CASES 
        drop 
        foreign key FK1D1B3B2F1B115888;

    alter table QUESTION_TEST_CASES 
        drop 
        foreign key FK1D1B3B2F7F13BBC3;

    alter table Question 
        drop 
        foreign key FK_Category_ID;

    alter table SECTIONS_COURSE 
        drop 
        foreign key FKC081D34CB4D5AF45;

    alter table SECTIONS_COURSE 
        drop 
        foreign key FKC081D34C28DFCF63;

    alter table Student 
        drop 
        foreign key FKF3371A1B4466CA04;

    alter table Student 
        drop 
        foreign key FKF3371A1BA99834D3;

    alter table TEST_CASE_EXCLUSION 
        drop 
        foreign key FKF9043AAC1B1158DE;

    alter table TEST_CASE_INPUT 
        drop 
        foreign key FKA357FBA81B1158DE;

    alter table TEST_CASE_OUTPUT 
        drop 
        foreign key FKD248F0C31B1158DE;

    drop table if exists CLASS_SECTION;

    drop table if exists COURSE_INSTRUCTOR;

    drop table if exists Category;

    drop table if exists Course;

    drop table if exists GRADED_EVENT_QUESTION;

    drop table if exists GradedEvent;

    drop table if exists INSTRUCTOR_SECTIONS;

    drop table if exists Instructor;

    drop table if exists Person;

    drop table if exists QUESTION_TEST_CASES;

    drop table if exists Question;

    drop table if exists SECTIONS_COURSE;

    drop table if exists Student;

    drop table if exists TEST_CASE_EXCLUSION;

    drop table if exists TEST_CASE_INPUT;

    drop table if exists TEST_CASE_OUTPUT;

    drop table if exists TestCase;

    drop table if exists Version;

    create table CLASS_SECTION (
        SECTION_INDEX bigint not null auto_increment,
        SECTION_NAME varchar(255),
        primary key (SECTION_INDEX)
    );

    create table COURSE_INSTRUCTOR (
        CRS_INDEX bigint not null,
        INST_INDEX bigint not null
    );

    create table Category (
        CAT_INDEX bigint not null auto_increment,
        CAT_NAME varchar(255),
        primary key (CAT_INDEX)
    );

    create table Course (
        COURSE_INDEX bigint not null auto_increment,
        COURSE_NAME varchar(255),
        PERSON_INDEX bigint,
        primary key (COURSE_INDEX)
    );

    create table GRADED_EVENT_QUESTION (
        GRADED_EVENT_INDEX bigint not null,
        QUESTION_INDEX bigint not null
    );

    create table GradedEvent (
        GRADED_EVENT_INDEX bigint not null auto_increment,
        ASSIGNMENT varchar(255),
        PARTIAL_CREDIT bit,
        TERM varchar(255),
        VERSION varchar(255),
        COURSE_INDEX bigint not null,
        primary key (GRADED_EVENT_INDEX)
    );

    create table INSTRUCTOR_SECTIONS (
        Instructor_fk bigint,
        Section_fk bigint not null,
        primary key (Section_fk)
    );

    create table Instructor (
        DOMAIN_ACNT varchar(255),
        PASSWORD varchar(255),
        PERSON_INDEX bigint not null,
        primary key (PERSON_INDEX)
    );

    create table Person (
        PERSON_INDEX bigint not null auto_increment,
        FIRST_NAME varchar(255),
        LAST_NAME varchar(255),
        WEBID varchar(255),
        primary key (PERSON_INDEX)
    );

    create table QUESTION_TEST_CASES (
        QUESTION_fk bigint not null,
        TEST_CASE_fk bigint not null,
        POSITION integer not null,
        primary key (QUESTION_fk, POSITION),
        unique (TEST_CASE_fk)
    );

    create table Question (
        QUESTION_ID bigint not null auto_increment,
        DESCRIPTION varchar(255),
        NAME varchar(255),
        Ordered_Output bit,
        VERBATIM bit,
        CAT_INDEX bigint not null,
        primary key (QUESTION_ID)
    );

    create table SECTIONS_COURSE (
        Course_fk bigint,
        Section_fk bigint not null,
        primary key (Section_fk)
    );

    create table Student (
        CLASS_YEAR integer,
        STUDENT_ID bigint not null,
        section_SECTION_INDEX bigint,
        primary key (STUDENT_ID)
    );

    create table TEST_CASE_EXCLUSION (
        TEST_CASE_ID bigint not null,
        EXCLUDES varchar(255)
    );

    create table TEST_CASE_INPUT (
        TEST_CASE_ID bigint not null,
        INPUT_VALUE varchar(255),
        POSITION integer not null,
        primary key (TEST_CASE_ID, POSITION)
    );

    create table TEST_CASE_OUTPUT (
        TEST_CASE_ID bigint not null,
        OUTPUT_VALUE varchar(255),
        POSITION integer not null,
        primary key (TEST_CASE_ID, POSITION)
    );

    create table TestCase (
        TEST_CASE_ID bigint not null auto_increment,
        TEST_CASE_VALUE decimal(19,2),
        primary key (TEST_CASE_ID)
    );

    create table Version (
        id double precision not null,
        primary key (id)
    );

    alter table COURSE_INSTRUCTOR 
        add index FKFFAB6F41D5C20AB1 (CRS_INDEX), 
        add constraint FKFFAB6F41D5C20AB1 
        foreign key (CRS_INDEX) 
        references Course (COURSE_INDEX);

    alter table COURSE_INSTRUCTOR 
        add index FKFFAB6F41A8E2C695 (INST_INDEX), 
        add constraint FKFFAB6F41A8E2C695 
        foreign key (INST_INDEX) 
        references Instructor (PERSON_INDEX);

    alter table Course 
        add index FK_CD_ID (PERSON_INDEX), 
        add constraint FK_CD_ID 
        foreign key (PERSON_INDEX) 
        references Instructor (PERSON_INDEX);

    alter table GRADED_EVENT_QUESTION 
        add index FKA83BE31DD5349B3E (QUESTION_INDEX), 
        add constraint FKA83BE31DD5349B3E 
        foreign key (QUESTION_INDEX) 
        references Question (QUESTION_ID);

    alter table GRADED_EVENT_QUESTION 
        add index FKA83BE31D21C75FC9 (GRADED_EVENT_INDEX), 
        add constraint FKA83BE31D21C75FC9 
        foreign key (GRADED_EVENT_INDEX) 
        references GradedEvent (GRADED_EVENT_INDEX);

    alter table GradedEvent 
        add index FK_COURSE_ID (COURSE_INDEX), 
        add constraint FK_COURSE_ID 
        foreign key (COURSE_INDEX) 
        references Course (COURSE_INDEX);

    alter table INSTRUCTOR_SECTIONS 
        add index FKD1E6390EE8FE0A3 (Instructor_fk), 
        add constraint FKD1E6390EE8FE0A3 
        foreign key (Instructor_fk) 
        references Instructor (PERSON_INDEX);

    alter table INSTRUCTOR_SECTIONS 
        add index FKD1E6390B4D5AF45 (Section_fk), 
        add constraint FKD1E6390B4D5AF45 
        foreign key (Section_fk) 
        references CLASS_SECTION (SECTION_INDEX);

    alter table Instructor 
        add index FKC9FAECBDD42E469C (PERSON_INDEX), 
        add constraint FKC9FAECBDD42E469C 
        foreign key (PERSON_INDEX) 
        references Person (PERSON_INDEX);

    alter table QUESTION_TEST_CASES 
        add index FK1D1B3B2F1B115888 (TEST_CASE_fk), 
        add constraint FK1D1B3B2F1B115888 
        foreign key (TEST_CASE_fk) 
        references TestCase (TEST_CASE_ID);

    alter table QUESTION_TEST_CASES 
        add index FK1D1B3B2F7F13BBC3 (QUESTION_fk), 
        add constraint FK1D1B3B2F7F13BBC3 
        foreign key (QUESTION_fk) 
        references Question (QUESTION_ID);

    alter table Question 
        add index FK_Category_ID (CAT_INDEX), 
        add constraint FK_Category_ID 
        foreign key (CAT_INDEX) 
        references Category (CAT_INDEX);

    alter table SECTIONS_COURSE 
        add index FKC081D34CB4D5AF45 (Section_fk), 
        add constraint FKC081D34CB4D5AF45 
        foreign key (Section_fk) 
        references CLASS_SECTION (SECTION_INDEX);

    alter table SECTIONS_COURSE 
        add index FKC081D34C28DFCF63 (Course_fk), 
        add constraint FKC081D34C28DFCF63 
        foreign key (Course_fk) 
        references Course (COURSE_INDEX);

    alter table Student 
        add index FKF3371A1B4466CA04 (section_SECTION_INDEX), 
        add constraint FKF3371A1B4466CA04 
        foreign key (section_SECTION_INDEX) 
        references CLASS_SECTION (SECTION_INDEX);

    alter table Student 
        add index FKF3371A1BA99834D3 (STUDENT_ID), 
        add constraint FKF3371A1BA99834D3 
        foreign key (STUDENT_ID) 
        references Person (PERSON_INDEX);

    alter table TEST_CASE_EXCLUSION 
        add index FKF9043AAC1B1158DE (TEST_CASE_ID), 
        add constraint FKF9043AAC1B1158DE 
        foreign key (TEST_CASE_ID) 
        references TestCase (TEST_CASE_ID);

    alter table TEST_CASE_INPUT 
        add index FKA357FBA81B1158DE (TEST_CASE_ID), 
        add constraint FKA357FBA81B1158DE 
        foreign key (TEST_CASE_ID) 
        references TestCase (TEST_CASE_ID);

    alter table TEST_CASE_OUTPUT 
        add index FKD248F0C31B1158DE (TEST_CASE_ID), 
        add constraint FKD248F0C31B1158DE 
        foreign key (TEST_CASE_ID) 
        references TestCase (TEST_CASE_ID);

insert into Version (id) values (1.3);
