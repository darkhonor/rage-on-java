
    drop table CLASS_SECTION cascade constraints;

    drop table COURSE_INSTRUCTOR cascade constraints;

    drop table Category cascade constraints;

    drop table Course cascade constraints;

    drop table GRADED_EVENT_QUESTION cascade constraints;

    drop table GradedEvent cascade constraints;

    drop table INSTRUCTOR_SECTIONS cascade constraints;

    drop table Instructor cascade constraints;

    drop table Person cascade constraints;

    drop table QUESTION_TEST_CASES cascade constraints;

    drop table Question cascade constraints;

    drop table SECTIONS_COURSE cascade constraints;

    drop table Student cascade constraints;

    drop table TEST_CASE_EXCLUSION cascade constraints;

    drop table TEST_CASE_INPUT cascade constraints;

    drop table TEST_CASE_OUTPUT cascade constraints;

    drop table TestCase cascade constraints;

    drop table Version cascade constraints;

    drop sequence hibernate_sequence;

    create table CLASS_SECTION (
        SECTION_INDEX number(19,0) not null,
        SECTION_NAME varchar2(255 char),
        primary key (SECTION_INDEX)
    );

    create table COURSE_INSTRUCTOR (
        CRS_INDEX number(19,0) not null,
        INST_INDEX number(19,0) not null
    );

    create table Category (
        CAT_INDEX number(19,0) not null,
        CAT_NAME varchar2(255 char),
        primary key (CAT_INDEX)
    );

    create table Course (
        COURSE_INDEX number(19,0) not null,
        COURSE_NAME varchar2(255 char),
        PERSON_INDEX number(19,0),
        primary key (COURSE_INDEX)
    );

    create table GRADED_EVENT_QUESTION (
        GRADED_EVENT_INDEX number(19,0) not null,
        QUESTION_INDEX number(19,0) not null
    );

    create table GradedEvent (
        GRADED_EVENT_INDEX number(19,0) not null,
        ASSIGNMENT varchar2(255 char),
        PARTIAL_CREDIT number(1,0),
        TERM varchar2(255 char),
        VERSION varchar2(255 char),
        COURSE_INDEX number(19,0) not null,
        primary key (GRADED_EVENT_INDEX)
    );

    create table INSTRUCTOR_SECTIONS (
        Instructor_fk number(19,0),
        Section_fk number(19,0) not null,
        primary key (Section_fk)
    );

    create table Instructor (
        DOMAIN_ACNT varchar2(255 char),
        PASSWORD varchar2(255 char),
        PERSON_INDEX number(19,0) not null,
        primary key (PERSON_INDEX)
    );

    create table Person (
        PERSON_INDEX number(19,0) not null,
        FIRST_NAME varchar2(255 char),
        LAST_NAME varchar2(255 char),
        WEBID varchar2(255 char),
        primary key (PERSON_INDEX)
    );

    create table QUESTION_TEST_CASES (
        QUESTION_fk number(19,0) not null,
        TEST_CASE_fk number(19,0) not null,
        POSITION number(10,0) not null,
        primary key (QUESTION_fk, POSITION),
        unique (TEST_CASE_fk)
    );

    create table Question (
        QUESTION_ID number(19,0) not null,
        DESCRIPTION varchar2(255 char),
        NAME varchar2(255 char),
        Ordered_Output number(1,0),
        VERBATIM number(1,0),
        CAT_INDEX number(19,0) not null,
        primary key (QUESTION_ID)
    );

    create table SECTIONS_COURSE (
        Course_fk number(19,0),
        Section_fk number(19,0) not null,
        primary key (Section_fk)
    );

    create table Student (
        CLASS_YEAR number(10,0),
        STUDENT_ID number(19,0) not null,
        section_SECTION_INDEX number(19,0),
        primary key (STUDENT_ID)
    );

    create table TEST_CASE_EXCLUSION (
        TEST_CASE_ID number(19,0) not null,
        EXCLUDES varchar2(255 char)
    );

    create table TEST_CASE_INPUT (
        TEST_CASE_ID number(19,0) not null,
        INPUT_VALUE varchar2(255 char),
        POSITION number(10,0) not null,
        primary key (TEST_CASE_ID, POSITION)
    );

    create table TEST_CASE_OUTPUT (
        TEST_CASE_ID number(19,0) not null,
        OUTPUT_VALUE varchar2(255 char),
        POSITION number(10,0) not null,
        primary key (TEST_CASE_ID, POSITION)
    );

    create table TestCase (
        TEST_CASE_ID number(19,0) not null,
        TEST_CASE_VALUE number(19,2),
        primary key (TEST_CASE_ID)
    );

    create table Version (
        id double precision not null,
        primary key (id)
    );

    alter table COURSE_INSTRUCTOR 
        add constraint FKFFAB6F41D5C20AB1 
        foreign key (CRS_INDEX) 
        references Course;

    alter table COURSE_INSTRUCTOR 
        add constraint FKFFAB6F41A8E2C695 
        foreign key (INST_INDEX) 
        references Instructor;

    alter table Course 
        add constraint FK_CD_ID 
        foreign key (PERSON_INDEX) 
        references Instructor;

    alter table GRADED_EVENT_QUESTION 
        add constraint FKA83BE31DD5349B3E 
        foreign key (QUESTION_INDEX) 
        references Question;

    alter table GRADED_EVENT_QUESTION 
        add constraint FKA83BE31D21C75FC9 
        foreign key (GRADED_EVENT_INDEX) 
        references GradedEvent;

    alter table GradedEvent 
        add constraint FK_COURSE_ID 
        foreign key (COURSE_INDEX) 
        references Course;

    alter table INSTRUCTOR_SECTIONS 
        add constraint FKD1E6390EE8FE0A3 
        foreign key (Instructor_fk) 
        references Instructor;

    alter table INSTRUCTOR_SECTIONS 
        add constraint FKD1E6390B4D5AF45 
        foreign key (Section_fk) 
        references CLASS_SECTION;

    alter table Instructor 
        add constraint FKC9FAECBDD42E469C 
        foreign key (PERSON_INDEX) 
        references Person;

    alter table QUESTION_TEST_CASES 
        add constraint FK1D1B3B2F1B115888 
        foreign key (TEST_CASE_fk) 
        references TestCase;

    alter table QUESTION_TEST_CASES 
        add constraint FK1D1B3B2F7F13BBC3 
        foreign key (QUESTION_fk) 
        references Question;

    alter table Question 
        add constraint FK_Category_ID 
        foreign key (CAT_INDEX) 
        references Category;

    alter table SECTIONS_COURSE 
        add constraint FKC081D34CB4D5AF45 
        foreign key (Section_fk) 
        references CLASS_SECTION;

    alter table SECTIONS_COURSE 
        add constraint FKC081D34C28DFCF63 
        foreign key (Course_fk) 
        references Course;

    alter table Student 
        add constraint FKF3371A1B4466CA04 
        foreign key (section_SECTION_INDEX) 
        references CLASS_SECTION;

    alter table Student 
        add constraint FKF3371A1BA99834D3 
        foreign key (STUDENT_ID) 
        references Person;

    alter table TEST_CASE_EXCLUSION 
        add constraint FKF9043AAC1B1158DE 
        foreign key (TEST_CASE_ID) 
        references TestCase;

    alter table TEST_CASE_INPUT 
        add constraint FKA357FBA81B1158DE 
        foreign key (TEST_CASE_ID) 
        references TestCase;

    alter table TEST_CASE_OUTPUT 
        add constraint FKD248F0C31B1158DE 
        foreign key (TEST_CASE_ID) 
        references TestCase;

    create sequence hibernate_sequence;
