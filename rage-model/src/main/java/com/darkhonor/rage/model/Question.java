/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.*;

/**
 * The Question class represents a question in the RAGE system.  The Question has
 * a name, {@link Category}, a description, whether or not it has to be answered 
 * verbatim, and a collection of {@link TestCase}s that can be used to evaluate 
 * a {@link Student}'s response.  This is a persistent class using the Java
 * Persistence Annotations (JPA) annotations to reflect the persisted elements.
 * 
 * @author Alexander Ackerman
 * 
 * @version 1.2.1
 */
@Entity
public class Question implements Serializable, Comparable<Question> 
{
    /**
     * The Id field for the persistent class.  The value will be generated using
     * the database's normal generation strategy.  The data will be stored in the
     * "QUESTION_ID" column of the database table.
     */
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "QUESTION_ID")
    private Long id;

    /**
     * The name of the question.  This name of the question is used as a file name
     * filter for the student submissions.  The data is stored in the "NAME" column
     * of the database table.
     */
    @Column(name = "NAME")
    private String name;
    
    /**
     * The {@link Category} of question.  This is a unidirectional relationship
     * with the {@link Category} class.  The data will be stored in the "CAT_INDEX"
     * column of the database table.  This is not a nullable field.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="CAT_INDEX", nullable = false)
    private Category category;
    
    /**
     * A short description for the question.  The data will be stored in the 
     * "DESCRIPTION" column of the database table.
     */
    @Column(name = "DESCRIPTION", length = 255)
    private String description;
    
    /**
     * A boolean value indicating whether or not the question's response must be
     * a verbatim response.  The default value is <code>false</code>.  The data 
     * is stored in the "VERBATIM" column of the database table.
     */
    @Column(name = "VERBATIM")
    private boolean verbatim = false;
    
    /**
     * A boolean value indicating whether or not teh question's response must be
     * received in a specific order.  The default value is <code>false</code>.
     * The data is stored in the "ORDERED_OUTPUT" column of the database table.
     *
     * @since 1.2.0
     */
    @Column(name = "Ordered_Output")
    private boolean orderedOutput = false;

    /**
     * The {@link Set} of test cases for the question.  This is a unidirectional
     * relationship with the {@link TestCase} class.  The data will be stored in
     * a join table called "QUESTION_TEST_CASES".  The Question Id is stored in
     * the "QUESTION_FK" column and the Test Case Id is stored in the "TEST_CASE_FK"
     * column.
     */
    // Unidirectional relationship
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "QUESTION_TEST_CASES",
        joinColumns = {@JoinColumn(name = "QUESTION_fk")},
        inverseJoinColumns = {@JoinColumn(name = "TEST_CASE_fk")}
    )
    @OrderColumn(name = "POSITION")
    private List<TestCase> testCases = new ArrayList<TestCase>();
    
    /**
     * No argument constructor for the Question class.
     */
    public Question()
    {
    }
    
    /**
     * Constructor for the Question class.
     * 
     * @param id            The Id for the question
     * @param name          The question's name
     * @param category      The {@link Category} of question
     * @param description   A short description of the question
     */
    public Question(Long id, String name, Category category, String description)
    {
        this.id = id;
        this.name = name;
        this.category = category;
        this.description = description;
    }
    
    /** 
     * Constructor for the Question class.
     * 
     * @param name          The question's name
     * @param category      The {@link Category} of question
     * @param description   A short description of the question
     */
    public Question(String name, Category category, String description)
    {
        this.name = name;
        this.category = category;
        this.description = description;
    }
    
    /**
     * Constructor for the Question class
     * 
     * @param name          The question's name
     * @param category      The {@link Category} of question
     */
    public Question(String name, Category category)
    {
        this.name = name;
        this.category = category;
    }
    
    /**
     * Returns the Id field of the Question object
     * 
     * @return      The Id represented as a Long value
     */
    public Long getId() 
    {
        return id;
    }

    /**
     * Sets the Id field of the Question object
     * 
     * @param id    The new Id for the Question object
     */
    public void setId(Long id) 
    {
        this.id = id;
    }

    /**
     * Returns the name of the question in String format.
     * 
     * @return  The name of the question
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * Sets the name of the Question object.  
     * 
     * @param name      The new question name
     * @throws java.lang.IllegalArgumentException   When the new name is either
     *              <code>null</code> or blank
     */
    public void setName(String name) throws IllegalArgumentException
    {
        if ((name == null) || name.equals(""))
            throw new IllegalArgumentException();
        this.name = name;
    }
    
    /**
     * Returns the {@link Category} for the question.
     * 
     * @return  The category of the question
     */
    public Category getCategory() 
    {
        return category;
    }

    /**
     * Sets the {@link Category} of the question
     * 
     * @param category      The new question category
     * @throws java.lang.IllegalArgumentException   When the category is <code>null</code>
     */
    public void setCategory(Category category) throws IllegalArgumentException
    {
        if (category == null)
            throw new IllegalArgumentException();
        this.category = category;
    }

    /**
     * Returns a short description of the question.  The description should identify
     * the basic characteristics and what is expected for this question.
     * 
     * @return  A description of the question
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * Sets the description of the question.  The description should identify the
     * basic characteristics and what is expected for this question.
     * 
     * @param description       The new description
     * @throws java.lang.IllegalArgumentException   When the description is 
     *              <code>null</code>
     */
    public void setDescription(String description) throws IllegalArgumentException
    {
        if (description == null)
            throw new IllegalArgumentException();
        this.description = description;
    }
    
    /**
     * Returns whether or not the question must be answered verbatim. 
     * 
     * @return  <code>true</code> if the question must be answered verbatim, 
     *          <code>false</code> otherwise.
     */
    public boolean getVerbatim()
    {
        return this.verbatim;
    }
    
    /**
     * Sets whether or not the question must be answered verbatim.
     * 
     * @param verbatim  Whether or not the question must be answered verbatim
     */
    public void setVerbatim(boolean verbatim)
    {
        this.verbatim = verbatim;
    }

    /**
     * Returns whether or not the question requires ordered output.
     *
     * @return  <code>true</code> if the question must have ordered output,
     *          <code>false</code> otherwise.
     */
    public boolean getOrderedOutput()
    {
        return this.orderedOutput;
    }

    /**
     * Sets whether or not the question must have ordered output.
     *
     * @param ordered   Whether or not the question must have ordered output
     */
    public void setOrderedOutput(boolean ordered)
    {
        this.orderedOutput = ordered;
    }

    /**
     * Returns the {@link Set} of {@link TestCase} objects associated with this question.
     * These tests will all be run against the student's response.
     * 
     * @return      The set of test cases
     */
    public List<TestCase> getTestCases()
    {
        return testCases;
    }
    
    /**
     * Assigns the {@link Set} of {@link TestCase} objects that are associated
     * with this question.
     * 
     * @param testCases     The set of test cases
     * @throws java.lang.IllegalArgumentException   When the set is <code>null</code>
     */
    public void setTestCases(List<TestCase> testCases) throws IllegalArgumentException
    {
        if (testCases == null)
        {
            throw new IllegalArgumentException();
        }
        this.testCases = testCases;
    }
    
    /**
     * Adds a {@link TestCase} object to the set of test cases for this question.
     * It is possible to add duplicate test cases to a question.
     * 
     * @param test      The test case to add to the question
     * @return          <code>true</code> if the test case was added successfully, 
     *                  <code>false</code> otherwise
     * @throws java.lang.IllegalArgumentException   When the test case is <code>null</code>
     */
    public boolean addTestCase(TestCase test) throws IllegalArgumentException
    {
        if (test == null)
            throw new IllegalArgumentException();
        return this.testCases.add(test);
    }
    
    /**
     * Removes a {@link TestCase} object from the set of test cases for this 
     * question.  If a test case is not part of the set of test cases, the method
     * will return <code>false</code>
     * 
     * @param test      The test case to remove from the question
     * @return          <code>true</code> if the test case was removed successfully,
     *                  <code>false</code> otherwise
     * @throws java.lang.IllegalArgumentException   When the test case is <code>null</code>
     */
    public boolean removeTestCase(TestCase test) throws IllegalArgumentException
    {
        if (test == null)
            throw new IllegalArgumentException();
        return this.testCases.remove(test);
    }
    
    /**
     * Compares two Question objects.  The comparison is based upon the name of the 
     * Question.  They are alphabetically compared.
     * 
     * @param other The other Question to compare
     * @return      0 if the Questions are equal, greater than 1 if this Question
     *              is greater than the other Question, less than 1 if this Question
     *              is less than the other Question.
     */
    @Override
    public int compareTo(Question other)
    {
        if (this.name.equals(other.name))
            return 0;
        else
            return this.name.compareTo(other.name);
    }
    
    /**
     * Returns a hash code for the Question.  The hash code is based upon
     * the name of the Question
     * 
     * @return  A hash code for the Question
     */
    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }
    
    /**
     * Indicates whether the object specified is equal to this one.  Another Question
     * object is equal if the names are equal
     * 
     * @param object    The object to compare
     * @return          <code>true</code> if the objects are equal, <code>false</code>
     *                  otherwise
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object) return true;
        if (object == null) return false;
        if (object.getClass() != this.getClass()) {
            return false;
        }
        Question other = (Question) object;
        if ((this.name == null && other.name != null) ||
                (this.name != null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    /**
     * Returns a {@link String} representation of the Question.  The String has
     * the text "Question: " followed by the Id, ", ", the name, " (", the category
     * of the question, "): ", and the description for the question.
     * 
     * @return  A string representation of the Question
     */
    @Override
    public String toString()
    {
        return "Question " + name + ": " + description;
    }

}
