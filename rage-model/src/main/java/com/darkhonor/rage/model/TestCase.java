/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 * The TestCase class represents an individual Test Case used by the RAGE system.
 * Test cases may have an input and some expected output.  In some cases, the output
 * must not contain certain words or phrases.  These are referred to as exclusions.
 * This is a persistent class using the Java Persistent Annotations (JPA) annotations
 * to reflect the persisted elements.
 * 
 * @author Alexander Ackerman
 * 
 * @version 1.1.0
 */
@Entity
public class TestCase implements Serializable, Cloneable {
    /**
     * The Id field for the persistent class.  The value will be generated using
     * the database's normal generation strategy.  The data will be stored in the 
     * "TEST_CASE_ID" column of the database table.
     */
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "TEST_CASE_ID")
    private Long id;

    /**
     * The numerical value of the test case.  The value will be stored in the 
     * "TEST_CASE_VALUE" column of the database table.
     */
    @Column(name = "TEST_CASE_VALUE", precision = 10, scale = 4)
    private BigDecimal value;
    
    /**
     * The list of {@link String} inputs to the test case.   Since we are 
     * persisting a basic class, we have to use a Hibernate specific annotation.
     * The data will be stored in a join table called "TEST_CASE_INPUT".  The 
     * TestCase Id will be stored in the "TEST_CASE_ID" column.  There will be a 
     * "POSITION" column to maintain the position of the entry in the List (0-based).
     * Finally, the actual input will be stored in the "INPUT_VALUE" column.
     * 
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(
        name = "TEST_CASE_INPUT",
        joinColumns = @JoinColumn(name = "TEST_CASE_ID")
        )
    @OrderColumn(name = "POSITION")
    @Column(name = "INPUT_VALUE")
    private List<String> inputs = new ArrayList<String>();

    /**
     * The list of {@link String} outputs to the test case.   Since we are 
     * persisting a basic class, we have to use a Hibernate specific annotation.
     * The data will be stored in a join table called "TEST_CASE_OUTPUT".  The 
     * TestCase Id will be stored in the "TEST_CASE_ID" column.  There will be a 
     * "POSITION" column to maintain the position of the entry in the List (0-based).
     * Finally, the actual output will be stored in the "OUTPUT_VALUE" column.
     * 
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(
        name = "TEST_CASE_OUTPUT",
        joinColumns = @JoinColumn(name = "TEST_CASE_ID")
    )
    @OrderColumn(name = "POSITION")
    @Column(name = "OUTPUT_VALUE")
    private List<String> outputs = new ArrayList<String>();
    
    /**
     * The list of {@link String} exclusions to the test case.   Since we are 
     * persisting a basic class, we have to use a Hibernate specific annotation.
     * The data will be stored in a join table called "TEST_CASE_EXCLUSION".  The 
     * TestCase Id will be stored in the "TEST_CASE_ID" column.  There will be a 
     * "POSITION" column to maintain the position of the entry in the List (0-based).
     * Finally, the actual exclusion will be stored in the "EXCLUDES" column.
     * 
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(
        name = "TEST_CASE_EXCLUSION",
        joinColumns = @JoinColumn(name = "TEST_CASE_ID")
    )
    @OrderColumn(name = "POSITION")
    @Column(name = "EXCLUDES")
    private List<String> excludes = new ArrayList<String>();
    
    /**
     * No argument constructor for the TestCase class
     */
    public TestCase()
    {
    }
    
    /**
     * Constructor for the TestCase class
     * 
     * @param value The value of the test case
     */
    public TestCase(BigDecimal value)
    {
        this.value = value;
    }
    
    /**
     * Returns the Id field of the TestCase object
     * 
     * @return  The Id represented as a Long value
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the Id field of the TestCase object
     * 
     * @param id    The new Id of the TestCase
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the numerical value of the test case.
     * 
     * @return  A BigDecimal value of the test case
     */
    public BigDecimal getValue()
    {
        return value;
    }
    
    /**
     * Sets the numerical value of the test case
     * 
     * @param value The numerical value for the Test Case
     * @throws java.lang.IllegalArgumentException   When the value is <code>null</code>
     *              or less than 0
     */
    public void setValue(BigDecimal value) throws IllegalArgumentException
    {
        if (value == null)
            throw new IllegalArgumentException();
        if (value.compareTo(BigDecimal.ZERO) < 0)
            throw new IllegalArgumentException();
        this.value = value;
    }
    
    /**
     * Returns a {@link List} of {@link String} objects that represent inputs
     * to the test case.
     * 
     * @return  The list of Inputs
     */
    public List<String> getInputs()
    {
        return inputs;
    }
    
    /**
     * Saves the {@link List} of inputs to the test case
     * 
     * @param inputs    The list of inputs
     * @throws java.lang.IllegalArgumentException   When the List is <code>null</code>
     */
    public void setInputs(List<String> inputs) throws IllegalArgumentException
    {
        if (inputs == null)
            throw new IllegalArgumentException();
        this.inputs = inputs;
    }
    
    /**
     * Adds a {@link String} input to the Test Case
     * 
     * @param input     The input to add to the test case
     * @return          <code>true</code> if the input was added successfully, 
     *                  <code>false</code> otherwise.
     * @throws java.lang.IllegalArgumentException   When the input is either blank
     *              or <code>null</code>
     */
    public boolean addInput(String input) throws IllegalArgumentException
    {
        if ((input == null) || input.equals(""))
            throw new IllegalArgumentException();
        return this.inputs.add(input);
    }
    
    /**
     * Removes a {@link String} input to the test case.  If the input does not
     * exist within the {@link List}, the method returns <code>false</code>.
     * 
     * @param input     The input to remove from the test case
     * @return          <code>true</code> if the input was removed successfully, 
     *                  <code>false</code> otherwise
     * @throws java.lang.IllegalArgumentException   When the input is either blank
     *              or <code>null</code>
     */
    public boolean removeInput(String input) throws IllegalArgumentException
    {
        if ((input == null) || input.equals(""))
            throw new IllegalArgumentException();
        return this.inputs.remove(input);
    }
    
    /**
     * Returns a {@link List} of {@link String} objects that represent expected 
     * outputs to the test case.
     * 
     * @return  The list of Outputs
     */
    public List<String> getOutputs()
    {
        return outputs;
    }
    
    /**
     * Saves the {@link List} of outputs to the test case
     * 
     * @param outputs       The list of outputs
     * @throws java.lang.IllegalArgumentException   When the List is <code>null</code>
     */
    public void setOutputs(List<String> outputs) throws IllegalArgumentException
    {
        if (outputs == null)
            throw new IllegalArgumentException();
        this.outputs = outputs;
    }
    
    /**
     * Adds a {@link String} output to the Test Case
     * 
     * @param output    The output to add to the test case
     * @return          <code>true</code> if the input was added successfully, 
     *                  <code>false</code> otherwise.
     * @throws java.lang.IllegalArgumentException   When the output is either blank
     *              or <code>null</code>
     */
    public boolean addOutput(String output) throws IllegalArgumentException
    {
        if ((output == null) || output.equals(""))
            throw new IllegalArgumentException();
        return this.outputs.add(output);
    }
    
    /**
     * Removes a {@link String} output to the test case.  If the output does not
     * exist within the {@link List}, the method returns <code>false</code>.
     * 
     * @param output    The output to remove from the test case
     * @return          <code>true</code> if the output was removed successfully, 
     *                  <code>false</code> otherwise
     * @throws java.lang.IllegalArgumentException   When the output is either blank
     *              or <code>null</code>
     */
    public boolean removeOutput(String output) throws IllegalArgumentException
    {
        if ((output == null) || output.equals(""))
            throw new IllegalArgumentException();
        return this.outputs.remove(output);
    }
    
    /**
     * Returns a {@link List} of {@link String} objects that represent exclusions
     * to the test case.
     * 
     * @return The list of exclusions
     */
    public List<String> getExcludes()
    {
        return excludes;
    }
    
    /**
     * Saves the {@link List} of exclusions to the test case
     * 
     * @param excludes  The list of exclusions
     * @throws java.lang.IllegalArgumentException   When the List is 
     *              <code>null</code>
     */
    public void setExcludes(List<String> excludes) throws IllegalArgumentException
    {
        if (excludes == null)
            throw new IllegalArgumentException();
        this.excludes = excludes;
    }
    
    /**
     * Adds a {@link String} exclusion to the Test Case
     * 
     * @param exclude   The exclusion to add to the test case
     * @return          <code>true</code> if the input was added successfully, 
     *                  <code>false</code> otherwise.
     * @throws java.lang.IllegalArgumentException   When the exclusion is either blank
     *              or <code>null</code>
     */
    public boolean addExclusion(String exclude) throws IllegalArgumentException
    {
        if ((exclude == null) || exclude.equals(""))
            throw new IllegalArgumentException();
        return this.excludes.add(exclude);
    }
    
    /**
     * Removes a {@link String} exclusion to the test case.  If the exclusion does not
     * exist within the {@link List}, the method returns <code>false</code>.
     * 
     * @param exclude   The exclusion to remove from the test case
     * @return          <code>true</code> if the exclusion was removed successfully, 
     *                  <code>false</code> otherwise
     * @throws java.lang.IllegalArgumentException   When the exclusion is either blank
     *              or <code>null</code>
     */
    public boolean removeExclusion(String exclude) throws IllegalArgumentException
    {
        if ((exclude == null) || exclude.equals(""))
            throw new IllegalArgumentException();
        return this.excludes.remove(exclude);
    }
    
    /**
     * Returns a hash code for the Test Case.  The hash code is based upon
     * the Id and value
     * 
     * @return  A hash code for the Test Case
     */
    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        hash += (value != null ? value.hashCode() : 0);
        return hash;
    }
    
    /**
     * Indicates whether the object specified is equal to this one.  Another Test Case
     * object is equal if the Id and value are both equal
     * 
     * @param object    The object to compare
     * @return          <code>true</code> if the objects are equal, <code>false</code>
     *                  otherwise
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object) return true;
        if (object == null) return false;
        if (object.getClass() != this.getClass()) {
            return false;
        }
        TestCase other = (TestCase) object;
        if ((this.id == null && other.id != null) || 
                (this.id != null && !this.id.equals(other.id)) ||
                (this.value == null && other.value != null) ||
                (this.value != null && !this.value.equals(other.value))) {
            return false;
        }
        return true;
    }

    @Override
    public TestCase clone() throws CloneNotSupportedException
    {
        try
        {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bout);
            out.writeObject(this);
            out.close();

            ByteArrayInputStream bin = new ByteArrayInputStream(bout.toByteArray());
            ObjectInputStream in = new ObjectInputStream(bin);
            Object ret = in.readObject();
            in.close();
            return (TestCase)ret;
        }
        catch (IOException ex)
        {
            return null;
        } catch (ClassNotFoundException ex)
        {
            return null;
        }
    }

    /**
     * Returns a {@link String} representation of the TestCase.  The String has
     * the text "Test Case: " followed by the Id and the test case value
     * 
     * @return  A string representation of the Test Case
     */
    @Override
    public String toString()
    {
        return "Test Case: " + id + "; Value: " + value.toString();
    }

}
