/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.*; 

/**
 * The Section class represents a Section used by the RAGE system.  The section
 * names are course dependent and are used to filter assignment versions.  Sample
 * sections at USAFA are "M1A", "T3B", and "M7A".  This is a persistent class using 
 * the Java Persistence Annotations (JPA) annotations to reflect the persisted
 * elements.  The class data will be stored in the "CLASS_SECTION" database table.
 * 
 * @author Alexander Ackerman
 * 
 * @version 2.0.0
 */
@Entity
@Table(name="CLASS_SECTION")
public class Section implements Serializable, Comparable<Section> 
{
    /**
     * The Id field for the persistent class.  The value will be generated using
     * the database's normal generation strategy.  The data will be stored in the
     * "SECTION_INDEX" column of the database table.
     */
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column
    private Long id;

    /**
     * The {@link Course} this section is a part of.  This is a bidirectional
     * relationship with the {@link Course} class.  The data will be stored in
     * a join table called "SECTIONS_COURSE".  The Course Id will be stored in 
     * the "COURSE_FK" column and the Section Id will be stored in the "SECTION_FK"
     * column. 
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(
        name = "SECTION_COURSE",
        joinColumns= {@JoinColumn(name = "section_id")},
        inverseJoinColumns = {@JoinColumn(name = "course_id")}
    )
    @OrderColumn(name = "Section_Order")
    private Course course;
    
    /**
     * The name of the section.  The value will be stored in the database in the
     * "SECTION_NAME" column of the database table.
     */
    @Column(name = "SECTION_NAME")
    private String name;
    
    /**
     * The {@link Instructor} that teaches this section.  This is a bidirectional 
     * relationship with the {@link Instructor} class.  The data will be stored 
     * in a join table called "INSTRUCTOR_SECTIONS".  The Instructor Id
     * will be stored in the "INSTRUCTOR_FK" column and the Section Id will be stored
     * in the "SECTION_FK" column.  
     */
    @ManyToOne(fetch = FetchType.EAGER//,
        //cascade = {CascadeType.PERSIST, CascadeType.MERGE}
    )
    @JoinTable(
        name = "SECTION_INSTRUCTOR",
        joinColumns= {@JoinColumn(name = "section_id")},
        inverseJoinColumns = {@JoinColumn(name = "instructor_id")}
    )
    @OrderColumn(name = "Instructor_Section_Num")
//    @IndexColumn(
//        name = "Instructor_Section_Num", base = 0
//    )
    private Instructor instructor;
            
    /**
     * The {@link Set} of students enrolled in the section.  This is a bidirectional 
     * relationship with the {@link Student} class.  The data is currently stored 
     * in the "section_SECTION_INDEX" column of the Student database table.
     */
    @ManyToMany(fetch = FetchType.EAGER//,
    //    cascade = {CascadeType.MERGE, CascadeType.PERSIST}
    )
    @JoinTable(name = "SECTION_STUDENTS",
        joinColumns = {@JoinColumn(name = "Section_FK")},
        inverseJoinColumns = {@JoinColumn(name = "Student_FK")})
    @OrderColumn(name = "POSITION")
    private List<Student> students = new ArrayList<Student>();
    
    /**
     * No argument constructor for the Section class
     */
    public Section()
    {
    }
    
    /**
     * Constructor for the Section class
     * 
     * @param name      The name of the new Section
     * 
     * @throws java.lang.IllegalArgumentException   When name is either blank or
     *              <code>null</code>
     */
    public Section(String name) throws IllegalArgumentException
    {
        if ((name == null) || (name.equals("")))
            throw new IllegalArgumentException();
        this.name = name;
    }
    
    /**
     * Constructor for the Section class
     * 
     * @param course        The {@link Course} this section is a part of
     * @param name          The name of the new section
     * @param instructor    The {@link Instructor} teaching the section
     * 
     * @throws java.lang.IllegalArgumentException   When either name, course, or 
     *          instructor are <code>null</code> or when name is blank
     */
    public Section(Course course, String name, Instructor instructor) 
            throws IllegalArgumentException
    {
        if ((name == null) || (course == null) || (instructor == null) || 
                (name.equals("")))
            throw new IllegalArgumentException();
        this.course = course;
        this.name = name;
        this.instructor = instructor;
    }
    
    /**
     * Returns the Id field of the Section object
     * 
     * @return  The Id represented as a Long value
     */
    public Long getId()
    {
        return id;
    }
    
    /**
     * Sets the Id field of the Section object
     * 
     * @param id    The new Id for  the section
     */
    public void setId(Long id)
    {
        this.id = id;
    }
    
    /**
     * Returns the {@link Course} this section is a part of.
     * 
     * @return      The course for this section
     */
    public Course getCourse()
    {
        return course;
    }
    
    /**
     * Sets the {@link Course} for this section.
     * 
     * @param course    The new course
     * @throws NullPointerException   When the course is <code>null</code>
     */
    public void setCourse(Course course) throws NullPointerException
    {
        if (course == null)
            throw new NullPointerException();
        this.course = course;
    }
    
    /**
     * Removes the {@link com.darkhonor.rage.model.Course} from the Section.  This
     * should only be used when clearing sections from the data source
     */
    public void removeCourse()
    {
        this.course = null;
    }

    /** Returns a String representation of the Section name
     * 
     * @return  The name of the section
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * Sets the name of the Section
     * 
     * @param name  The new Section name
     * @throws java.lang.IllegalArgumentException   When the name is <code>null</code>
     *          or blank
     */
    public void setName(String name) throws IllegalArgumentException
    {
        if ((name == null) || name.equals(""))
            throw new IllegalArgumentException();
        this.name = name;
    }
    
    /**
     * Returns the {@link Instructor} for the section
     * 
     * @return  The instructor
     */
    public Instructor getInstructor()
    {
        return instructor;
    }
    
    /**
     * Sets the {@link Instructor} for the section
     * 
     * @param instructor    The new instructor
     * @throws java.lang.IllegalArgumentException   When the instructor is <code>null</code>
     */
    public void setInstructor(Instructor instructor) throws NullPointerException
    {
        if (instructor == null)
            throw new NullPointerException();
        this.instructor = instructor;
    }

    /**
     * Removes the {@link Instructor} from the section.
     *
     * @since 1.1.0
     */
    public void removeInstructor()
    {
        this.instructor = null;
    }
    
    /**
     * Returns the {@link List} of {@link com.darkhonor.rage.model.Student}
     * enrolled in the section
     * 
     * @return  The list of students
     */
    public List<Student> getStudents()
    {
        return students;
    }
    
    /**
     * Returns the {@link com.darkhonor.rage.model.Student} at the given index.
     * 
     * @param index     The Index of the Student to return
     * @return          The Student object in the list
     * @throws IllegalArgumentException     When the index is out of range
     */
    public Student getStudent(int index) throws IllegalArgumentException
    {
        if (index < 0 || index >= students.size())
            throw new IllegalArgumentException();
        return students.get(index);
    }

    /**
     * Sets the {@link Set} of {@link Student} enrolled in the course
     * 
     * @param students      The set of students
     * @throws NullPointerException   When the set is <code>null</code>
     */
    public void setStudents(List<Student> students) throws NullPointerException
    {
        if (students == null)
            throw new NullPointerException();
        this.students = students;
    }
    
    /**
     * Adds a {@link Student} to the set of students in the section
     * 
     * @param student   The student to add to the section
     * @return          <code>true</code> if the student was added successfully,
     *                  <code>false</code> otherwise
     * @throws NullPointerException   When the student is <code>null</code>
     */
    public boolean addStudent(Student student) throws NullPointerException
    {
        if (student == null)
            throw new NullPointerException();
        return this.students.add(student);
    }
    
    /**
     * Removes a {@link Student} from the set of students in the section.  If the 
     * student is not a member of the section, the method will return <code>false</code>.
     * 
     * @param student   The student to remove
     * @return          <code>true</code> if the student was removed, <code>false</code>
     *                  otherwise
     * @throws NullPointerException   When the student is <code>null</code>
     */
    public boolean removeStudent(Student student) throws NullPointerException
    {
        if (student == null)
            throw new NullPointerException();
        return this.students.remove(student);
    }

    /**
     * Removes the {@link com.darkhonor.rage.model.Student} at the given index
     * from the List of Students.  When the call returns, the student will not 
     * be in the List.
     * 
     * @param index     The index of the Student to remove
     * @return          The removed Student object
     * @throws IllegalArgumentException     When the index is out of range
     */
    public Student removeStudent(int index) throws IllegalArgumentException
    {
        if (index < 0 || index >= students.size())
            throw new IllegalArgumentException();
        return students.remove(index);
    }

    /**
     * Clears all {@link Student} from the section.
     *
     * @since 1.1.0
     */
    public void clearStudents()
    {
        this.students.clear();
    }

    @Override
    public Section clone()
    {
        Section clonedSection = new Section();
        clonedSection.setId(new Long(id.longValue()));
        clonedSection.setName(new String(name));
        Instructor copiedInstructor = new Instructor(new String(instructor.getFirstName()),
                new String(instructor.getLastName()),
                new String(instructor.getWebID()));
        copiedInstructor.setId(new Long(instructor.getId().longValue()));

        for (Student student : students)
        {
            clonedSection.addStudent(student.clone());
        }
        clonedSection.setCourse(new Course(new String(course.getName())));
        return clonedSection;
    }
    
    /**
     * Compares two Section objects.  The comparison is based upon the course and
     * section name.  For the name, they are alphabetically compared.
     * 
     * @param other The other Section to compare
     * @return      0 if the Sections are equal, greater than 1 if this Section
     *              is greater than the other Section, less than 1 if this Section
     *              is less than the other Section.
     */
    @Override
    public int compareTo(Section other)
    {
        if (course.equals(other.course))
        {
            if (name.equals(other.name))
                return 0;
            else
                return name.compareTo(other.name);
        }
        else
            return course.compareTo(other.course);
    }
    
    /**
     * Returns a hash code for the Section.  The hash code is based upon
     * the course and section name
     * 
     * @return  A hash code for the Section
     */
    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (course != null ? course.hashCode() : 0);
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }
    
    /**
     * Indicates whether the object specified is equal to this one.  Another Section
     * object is equal if course and name are both equal
     * 
     * @param object    The object to compare
     * @return          <code>true</code> if the objects are equal, <code>false</code>
     *                  otherwise
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object) return true;
        if (object == null)
        {
            return false;
        }
        if (object.getClass() != this.getClass()) 
        {
            return false;
        }
        Section other = (Section) object;
        if ((course == null && other.course != null) ||
                (course != null && !course.equals(other.course)) ||
                (name == null && other.name != null) || 
                (name != null && !this.name.equals(other.name))) 
        {
            return false;
        }
        return true;
     }
    
    /**
     * Returns a {@link String} representation of the Section.  The String has
     * the section name.
     * 
     * @return  A string representation of the Section
     */
    @Override
    public String toString()
    {
        return this.name;
    }
}
