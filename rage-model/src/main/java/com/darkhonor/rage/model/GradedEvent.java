/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

/**
 * The GradedEvent class represents a Graded Event in the RAGE system.  The Graded
 * Event is a collection of {@link Question}s that have a specified value.  The 
 * event could be an exam, a homework assignment, or anything that could be 
 * evaluated using the RAGE environment.  This is a persistent class using the
 * Java Persistence Annotations (JPA) annotations to reflect the persisted
 * elements.
 * 
 * @author Alexander Ackerman
 * 
 * @version 1.2.0
 */
@Entity
public class GradedEvent implements Serializable, Comparable<GradedEvent> {
    /**
     * The Id field for the persistent class.  The value will be generated using
     * the database's normal generation strategy.  The data will be stored in the
     * "GRADED_EVENT_INDEX" column of the database table.
     */
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "GRADED_EVENT_INDEX")
    private Long id;
    
    /**
     * The term the course is offered.  This field is used to distinguish between
     * Graded Events that happen between semesters/quarters.  This allows for a 
     * historical record of past events that could be used to generate new events.
     * The value will be stored in the database in the "TERM" column of the database
     * table.
     * 
     * Sample terms could be "Fall 2008" or "Summer 2009"
     */
    @Column(name = "TERM")
    private String term;
    
    /**
     * The {@link Course} this Graded Event is associated with.  This is a 
     * unidirectional relationship with the {@link Course} class.  The data will be
     * stored in the "COURSE_INDEX" column of the database table.  This is not a
     * nullable field.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="COURSE_INDEX", nullable = false)
    private Course course;
    
    /**
     * A {@link String} representation of the Assignment.  The value must match
     * the directory name given by the Webpost software for the system to be able
     * to locate the proper directory.  The value will be stored in the "ASSIGNMENT"
     * column of the database table.
     * 
     * Sample assignments are "HW#1" or "Assessment #1"
     */
    @Column(name = "ASSIGNMENT")
    private String assignment;
    
    /**
     * A {@link String} representing which version of the assignment this
     * Graded Event represents.  This is to enable multiple versions of an event
     * across sections.  Currently this version is keyed to either "ALL" or the
     * first two characters of the section identifier.  The value will be stored
     * in the "VERSION" column of the database table.
     * 
     * Sample versions include "M5" or "T2"
     */
    @Column(name = "VERSION")
    private String version;

    /**
     * A Date representing the due date for the graded event.  The primary
     * interface for users is via a String value.  This was added to the 1.5
     * database structure.
     * 
     * @since 1.2
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "DUE_DATE")
    private Date dueDate;

    /**
     * A boolean value indicating whether or not partial credit is allowed on 
     * this Graded Event.  Partial credit cannot be automatically given for part
     * of a Graded Event.  Either the entire event has partial credit available 
     * or it doesn't.  The value will be stored in the "PARTIAL_CREDIT" column
     * of the database table.
     */
    @Column(name = "PARTIAL_CREDIT")
    private boolean partialCredit;
    
    /**
     * The {@link List} of {@link Question}s that make up the Graded Event.  This
     * is a unidirectional relationship with the {@link Question} class.  The data
     * will be stored in a join table called "GRADED_EVENT_QUESTION".  The Graded
     * Event Id will be stored in the "GRADED_EVENT_INDEX" column and the Question
     * Id will be stored in the "QUESTION_INDEX" column.
     */
    @ManyToMany( cascade = {CascadeType.PERSIST, CascadeType.MERGE,
        CascadeType.REFRESH}, fetch = FetchType.EAGER )
    @JoinTable(
        name = "GRADED_EVENT_QUESTION",
        joinColumns = {@JoinColumn(name = "GRADED_EVENT_INDEX")},
        inverseJoinColumns = {@JoinColumn(name = "QUESTION_INDEX")}
    )
    @OrderColumn(name = "POSITION")
    private List<Question> questions = new ArrayList<Question>();
    
    /**
     * No argument constructor for the GradedEvent class.
     */
    public GradedEvent()
    {
    }
    
    /**
     * Constructor for the GradedEvent class.
     * 
     * @param course        The {@link Course} for this Graded Event
     * @param assignment    The assignment name
     * @param version       Which version of the assignment does this event refer to
     */
    public GradedEvent(Course course, String assignment, String version)
    {
        this.course = course;
        this.assignment = assignment;
        this.version = version;
    }
    
    /**
     * Returns the Id field of the GradedEvent object.
     * 
     * @return  The Id represented as a Long value
     */
    public Long getId() 
    {
        return id;
    }

    /**
     * Sets the Id field of the GradedEvent object.
     * 
     * @param id    The new Id of the GradedEvent
     */
    public void setId(Long id) 
    {
        this.id = id;
    }
    
    /**
     * Returns the term in which this GradedEvent takes place.
     * 
     * @return      A string representation of the term
     */
    public String getTerm()
    {
        return term;
    }
    
    /**
     * Sets the term for the GradedEvent.
     * 
     * @param term  The new term for the Graded Event
     * @throws java.lang.IllegalArgumentException   When the term is either <code>
     *          null</code> or blank
     */
    public void setTerm(String term) throws IllegalArgumentException
    {
        if ((term == null) || term.equals(""))
            throw new IllegalArgumentException();
        this.term = term;
    }
    
    /**
     * Returns the {@link Course} for which this Graded Event was part of
     * 
     * @return  The course for the Graded Event
     */
    public Course getCourse()
    {
        return course;
    }
    
    /**
     * Sets the {@link Course} for the Graded Event
     * 
     * @param course    The new course
     * @throws java.lang.IllegalArgumentException   When the course is <code>null</code>
     */
    public void setCourse(Course course) throws IllegalArgumentException
    {
        if (course == null)
            throw new IllegalArgumentException();
        this.course = course;
    }
    
    /**
     * The name of the assignment for this Graded Event.
     *
     * @return      A string representation of the assignment name
     */
    public String getAssignment()
    {
        return assignment;
    }
    
    /**
     * Sets the name of the assignment for this Graded Event
     * 
     * @param assignment    The new assignment name
     * @throws java.lang.IllegalArgumentException   When the new name is either
     *          <code>null</code> or blank
     */
    public void setAssignment(String assignment) throws IllegalArgumentException
    {
        if ((assignment == null) || assignment.equals(""))
            throw new IllegalArgumentException();
        this.assignment = assignment;
    }
    
    /**
     * Returns a string for the version of this Graded Event
     * 
     * @return      A string representation for the version of the event
     */
    public String getVersion()
    {
        return version;
    }

    /**
     * Returns a String representation of the due date in the format DD MMM YYYY.
     * An example would be 21 Feb 2010.
     *
     * @return  The Due Date for the Graded Event
     *
     * @since 1.2.0
     */
    public String getDueDate()
    {
        DateFormat formattedDate = new SimpleDateFormat("dd MMM yyyy");
        return formattedDate.format(dueDate);
    }

    /**
     * Sets the due date for the assignment to the specified DD MMM YYYY.
     *
     * @param dueDate   String representation of the due date
     * @throws IllegalArgumentException If date is not in format dd mmm yyyy
     *
     * @since 1.2.0
     */
    public void setDueDate(String dueDate) throws IllegalArgumentException
    {
        if (dueDate == null)
            throw new IllegalArgumentException();
        SimpleDateFormat formattedDate = new SimpleDateFormat("dd MMM yyyy");
        formattedDate.setLenient(false);
        try
        {
            this.dueDate = formattedDate.parse(dueDate);
        }
        catch (ParseException ex)
        {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Sets the version for this Graded Event.  
     * 
     * @param version   The new version for the Graded Event
     * @throws java.lang.IllegalArgumentException   When the version is either blank 
     *              or <code>null</code>
     */
    public void setVersion(String version) throws IllegalArgumentException
    {
        if ((version == null) || version.equals(""))
            throw new IllegalArgumentException();
        this.version = version;
    }
    
    /**
     * Returns whether or not partial credit is available for this graded event
     * 
     * @return  <code>true</code> if partial credit is available, <code>false</code>
     *          otherwise
     */
    public boolean getPartialCredit()
    {
        return partialCredit;
    }
    
    /**
     * Sets whether or not partial credit is available for this graded event
     * 
     * @param partialCredit     Whether or not partial credit is available (<code>true</code>
     *                          or <code>false</code>)
     */
    public void setPartialCredit(boolean partialCredit)
    {
        this.partialCredit = partialCredit;
    }
    
    /**
     * Returns a {@link List} of {@link Question} objects that represent the questions
     * that make up this Graded Event.
     * 
     * @return  The list of questions
     */
    public List<Question> getQuestions()
    {
        return questions;
    }

    /**
     * Returns the Question at the given index.  If the provided index is less than
     * 0 or greater than or equal to the size of the List, an exception will be
     * thrown.  The contents of the List are not modified by this method.
     *
     * @param index     The index of the Question to return.
     * @return          The Question at the specified index
     * @throws IndexOutOfBoundsException    Thrown when the index is less than 0 or
     *                                      greater than or equal to the size of
     *                                      the List
     */
    public Question getQuestion(int index) throws IndexOutOfBoundsException
    {
        return questions.get(index);
    }

    /**
     * Saves the {@link List} of {@link Question}s that make up this Graded Event.
     * 
     * @param questions     The questions for this graded event
     * @throws java.lang.IllegalArgumentException   When the list is <code>null</code>
     */
    public void setQuestions(List<Question> questions) throws IllegalArgumentException
    {
        if (questions == null)
            throw new IllegalArgumentException();
        this.questions = questions;
    }
    
    /**
     * Adds a {@link Question} to the list of questions for this Graded Event.  If
     * the question already is in the list of questions, the method will return
     * <code>false</code>
     * 
     * @param question      The question to add to the list of questions
     * @return              <code>true</code> if the question was added successfully,
     *                      <code>false</code> otherwise
     * @throws java.lang.IllegalArgumentException   When the question is <code>null</code>
     */
    public boolean addQuestion(Question question) throws IllegalArgumentException
    {
        if (question == null)
            throw new IllegalArgumentException();
        if (this.questions.contains(question))
        {
            return false;
        }
        else
        {
            return this.questions.add(question);
        }
    }
    
    /**
     * Removes a {@link Question} from the list of questions for this Graded Event.
     * If the question is not in the List of questions, the method will returns
     * <code>false</code>
     * 
     * @param question  The question to remove
     * @return          <code>true</code> if the question was removed successfully,
     *                  <code>false</code> otherwise
     * @throws java.lang.IllegalArgumentException   When the question is <code>null</code>
     */
    public boolean removeQuestion(Question question) throws IllegalArgumentException
    {
        if (question == null)
            throw new IllegalArgumentException();
        return this.questions.remove(question);
    }

    /**
     * Removes all {@link Question} from the Graded Event.
     *
     * @since 1.1.0
     */
    public void clearQuestions()
    {
        this.questions.clear();
    }
    
    /**
     * Compares two GradedEvent objects.  The comparison is based upon the Course,
     * then the term, then the assignment name, then the version for the assignment.
     * For the String values, they are alphabetically compared.
     * 
     * @param other The other GradedEvent to compare
     * @return      0 if the GradedEvents are equal, greater than 1 if this GradedEvent
     *              is greater than the other GradedEvent, less than 1 if this GradedEvent
     *              is less than the other GradedEvent.
     */
    @Override
    public int compareTo(GradedEvent other)
    {
        if (this.course.equals(other.course))
        {
            if (this.term.equals(other.term))
            {
                if (this.assignment.equals(other.assignment))
                {
                    if (this.version.equals(other.version))
                        return 0;
                    else 
                        return this.version.compareTo(other.version);
                }
                else
                    return this.assignment.compareTo(other.assignment);
            }
            else
                return this.term.compareTo(other.term);
        } 
        else
            return this.course.compareTo(other.course);
    }

    /**
     * Returns a hash code for the GradedEvent.  The hash code is based upon
     * the course, term, assignment, and version.
     * 
     * @return  A hash code for the GradedEvent
     */
    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (course != null ? course.hashCode() : 0);
        hash += (term != null ? term.hashCode() : 0);
        hash += (assignment != null ? assignment.hashCode() : 0);
        hash += (version != null ? version.hashCode() : 0);
        return hash;
    }
    
    /**
     * Indicates whether the object specified is equal to this one.  Another GradedEvent
     * object is equal if the course, term, assignment, and version are all equal
     * 
     * @param object    The object to compare
     * @return          <code>true</code> if the objects are equal, <code>false</code>
     *                  otherwise
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object) return true;
        if (object == null) return false;
        if (object.getClass() != this.getClass()) {
            return false;
        }
        GradedEvent other = (GradedEvent) object;
        if ((this.term == null && other.term != null) ||
                (this.term != null && !this.term.equals(other.term)) ||
                (this.course == null && other.course != null) || 
                (this.course != null && !this.course.equals(other.course)) ||
                (this.assignment == null && other.assignment != null) ||
                (this.assignment != null && !this.assignment.equals(other.assignment)) ||
                (this.version == null && other.version != null) ||
                (this.version != null && !this.version.equals(other.version))) {
            return false;
        }
        return true;
    }

    /**
     * Returns a {@link String} representation of the GradedEvent.  The String has
     * the text "Graded Event: " followed by the assignment, "; Version: ", and
     * the version of the assignment.
     * 
     * @return  A string representation of the Graded Event
     */
    @Override
    public String toString()
    {
        return "Graded Event: " + assignment + "; Version: " + version +
                " (" + term + ")";
    }
    
}
