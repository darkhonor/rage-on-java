/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * 
 * @author Alexander Ackerman
 * 
 * @version 1.0.0
 */
public class Response 
{

    private boolean result;
    private BigDecimal pointsEarned;
    private List<String> answers;
    
    public Response()
    {
        answers = new ArrayList<String>();
        pointsEarned = new BigDecimal(BigDecimal.ZERO.toPlainString());
    }
    
    public boolean getResult()
    {
        return result;
    }
    public void setResult(boolean result)
    {
        this.result = result;
    }
    
    public BigDecimal getPointsEarned()
    {
        return pointsEarned;
    }
    public void setPointsEarned(BigDecimal points)
    {
        this.pointsEarned = points;
    }
    public void addPoints(BigDecimal points)
    {
        this.pointsEarned = this.pointsEarned.add(points);
    }
    
    public List<String> getAnswers()
    {
        return answers;
    }
    public void addAnswer(String answer)
    {
        this.answers.add(answer);
    }
    public void clearAnswers()
    {
        this.answers.clear();
    }
    
}
