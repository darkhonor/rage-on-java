/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 * The Student class extends the {@link Person} class and represents a student
 * in the RAGE system.  The distinction is made between Students and Instructors
 * in order to provide a mechanism to limit access to the RAGE system to only 
 * instructors.  Students have a class year associated with their entity that 
 * distinguishes them from instructors.
 * 
 * @author Alexander Ackerman
 * 
 * @version 2.0.0
 */
@Entity
@PrimaryKeyJoinColumn(name = "STUDENT_ID")
public class Student extends Person implements Serializable, Comparable<Student> {

    /**
     * The class year for the Student.  The value is stored in the "CLASS_YEAR" 
     * column of the database table.
     */
    @Column(name = "CLASS_YEAR")
    private Integer classYear;
    
    /**
     * The set of {@link Section} the student is in.  This allows for a student
     * to be enrolled in multiple courses at the same time.  However, there is
     * no direct relationship to the Course the student is enrolled in.  The
     * user will need to ensure the student cannot be enrolled in two sections
     * in the same course.
     */
    @ManyToMany(mappedBy = "students", fetch = FetchType.EAGER
    //,
    //    cascade = { CascadeType.MERGE, CascadeType.PERSIST}
    )
    private List<Section> sections = new ArrayList<Section>();
    
    /**
     * No argument constructor for the Student class.
     */
    public Student()
    {
    }
    
    /**
     * Constructor for the Student class.
     * 
     * @param firstName     The first name of the student
     * @param lastName      The last name of the student
     * @param webID         The WebID of the Student from the Webpost application
     * @param classYear     The class year for the student
     */
    public Student(String firstName, String lastName, String webID, Integer classYear)
    {
        super.setFirstName(firstName);
        super.setLastName(lastName);
        super.setWebID(webID);
        this.classYear = classYear;
    }
    
    /**
     * Returns an Integer representing the class year for the student
     * 
     * @return  The class year of the student
     */
    public Integer getClassYear()
    {
        return classYear;
    }
    
    /**
     * Sets the class year for the student.
     * 
     * @param classYear     The new class year
     * @throws NullPointerException   When the class year is <code>null</code>
     */
    public void setClassYear(Integer classYear) throws NullPointerException
    {
        if (classYear == null)
        {
            throw new NullPointerException();
        }
        this.classYear = classYear;
    }
    
    /**
     * Returns the {@link Section} the student is a part of.
     * 
     * @return  The section the student belongs to
     */
    public List<Section> getSections()
    {
        return sections;
    }

    /**
     * Returns the number of {@link com.darkhonor.rage.model.Section} the Student
     * is enrolled in.
     *
     * @return      The number of Sections the student is enrolled in
     */
    public int numEnrolledSections()
    {
        return sections.size();
    }

    /**
     * Sets the {@link Section} the student belongs to.
     * 
     * @param sections   The set of sections for the student
     * @throws NullPointerException If sections is <code>null</code>
     */
    public void setSections(List<Section> sections) throws NullPointerException
    {
        if (sections == null)
            throw new NullPointerException();
        this.sections = sections;
    }

    /**
     * Adds the Section to the list of sections the Student is enrolled in.
     * Does not verify the section is part of a course the student is already
     * enrolled in.
     *
     * @param   section The section to add the student to
     * @return  boolean <code>true</code> if the student is successfully added,
     *                  <code>false</code> otherwise.
     * @throws IllegalArgumentException If the student is already enrolled in
     *                                  the section
     * @throws NullPointerException     If the Student is <code>null</code>
     * 
     * @since 1.5.0
     */
    public boolean addSection(Section section) throws IllegalArgumentException,
            NullPointerException
    {
        if (section == null)
            throw new NullPointerException("Section is null");
        else if (sections.indexOf(section) >= 0)
            throw new IllegalArgumentException("Adding a student to a section they are in");
        else
            return sections.add(section);
    }
    
    /**
     * Returns whether or not the Student is enrolled in the given Section.
     * 
     * @param section       The Section to check
     * @return              <code>true</code> if the Student is enrolled in the
     *                      section, <code>false</code> otherwise
     * @throws NullPointerException     If the Section is <code>null</code>
     */
    public boolean isEnrolledIn(Section section) throws NullPointerException
    {
        if (section == null)
        {
            throw new NullPointerException("Section is null");
        }
        return sections.contains(section);
    }

    /**
     * Removes the student from the provided {@link Section}.
     *
     * @since 1.5.0
     * @param section   The section to remove the student from
     * @return          <code>true</code> if the student was removed from the
     *                  section, <code>false</code> otherwise
     * @throws IllegalArgumentException When section is <code>null</code> or if
     *                                  the student is not enrolled in the
     *                                  chosen section
     */
    public boolean removeFromSection(Section section) throws IllegalArgumentException,
            NullPointerException
    {
        if (section == null)
        {
            throw new NullPointerException("Section is null");
        }
        else if (!isEnrolledIn(section))
        {
            System.err.println("Enrolled Sections");
            for (Section enrolledSection : sections)
            {
                System.err.println("Enrolled Section = Section: " + enrolledSection.equals(section));
                System.err.println("Section: " + enrolledSection);
                System.err.println("Section Course: " + enrolledSection.getCourse());
            }
            throw new IllegalArgumentException("Student is not enrolled in section " +
                    section);
        }
        else
        {
            return sections.remove(section);
        }
    }

    @Override
    public Student clone()
    {
        Student clonedStudent = new Student(new String(super.getFirstName()),
                new String(super.getLastName()), new String(super.getWebID()),
                new Integer(classYear.intValue()));
        clonedStudent.setId(new Long(super.getId().longValue()));
//        for (Section section : sections)
//        {
//            Course copiedCourse = new Course(section.getCourse().getName());
//            //copiedCourse.setId(new Long(section.getCourse().getId().longValue()));
//            Instructor copiedInstructor = new Instructor(new String(section.getInstructor().getFirstName()),
//                    new String(section.getInstructor().getLastName()),
//                    new String(section.getInstructor().getWebID()));
//            //copiedInstructor.setId(new Long(section.getInstructor().getId().longValue()));
//            Section copiedSection = new Section(copiedCourse, new String(section.getName()), copiedInstructor);
//            copiedSection.setId(new Long(section.getId().longValue()));
//            clonedStudent.addSection(copiedSection);
//        }
        return clonedStudent;
    }
    
    /**
     * Compares two Student objects.  The comparison is based upon the class year,
     * last name, first name, and then Id.  For the String values, they are alphabetically 
     * compared.
     * 
     * @param other The other Student to compare
     * @return      0 if the Students are equal, greater than 1 if this Student
     *              is greater than the other Student, less than 1 if this Student
     *              is less than the other Student.
     */
    @Override
    public int compareTo(Student other)
    {
        if (this.classYear.compareTo(other.classYear) == 0)
        {
            if (super.getLastName().equals(other.getLastName()))
            {
                if (super.getFirstName().equals(other.getFirstName()))
                {
                    if (super.getWebID().equals(other.getWebID()))
                        return 0;
                    else
                        return super.getWebID().compareTo(other.getWebID());
                }
                else return super.getFirstName().compareTo(other.getFirstName());
            }
            else
                return super.getLastName().compareTo(other.getLastName());
        } 
        else
            return this.classYear.compareTo(other.classYear);
    }

    /**
     * Returns a hash code for the Student.  The hash code is based upon
     * the Id, last name, first name, and class year.
     * 
     * @return  A hash code for the Student
     */
    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (super.getWebID() != null ? super.getWebID().hashCode() : 0);
        hash += (super.getLastName() != null ? super.getLastName().hashCode() : 0);
        hash += (super.getFirstName() != null ? super.getFirstName().hashCode() : 0);
        hash += (this.classYear != null ? this.classYear.hashCode() : 0);
        return hash;
    }
    
    /**
     * Indicates whether the object specified is equal to this one.  Another Student
     * object is equal if the Id, last name, first name, and class year are all equal
     * 
     * @param object    The object to compare
     * @return          <code>true</code> if the objects are equal, <code>false</code>
     *                  otherwise
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object) return true;
        if (object == null) return false;
        if (object.getClass() != this.getClass()) {
            return false;
        }
        Student other = (Student) object;
        if ((super.getWebID() == null && other.getWebID() != null) ||
                (super.getWebID() != null && !super.getWebID().equals(other.getWebID())) ||
                (super.getLastName() == null && other.getLastName() != null) || 
                (super.getLastName() != null && !this.getLastName().equals(other.getLastName())) ||
                (super.getFirstName() == null && other.getFirstName() != null) || 
                (super.getFirstName() != null && !this.getFirstName().equals(other.getFirstName())) ||
                (this.classYear == null && other.classYear != null) ||
                (this.classYear != null && !this.classYear.equals(other.classYear))) {
            
            return false;
        }
        return true;
    }
    
    /**
     * Returns a {@link String} representation of the Student.  The String has
     * the student's last name, ", ", their first name, then " (" their class year
     * and ")".
     * 
     * @return  A string representation of the Student
     */
    @Override
    public String toString()
    {
        return super.getLastName() + ", " + super.getFirstName() + " (" + classYear + ")";
    }

}
