/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.persistence.*;

/**
 * The Course class represents a Course used by the RAGE system.  Sample courses
 * include "CS110", "CS210", and "CS359".  This is a persistent class using the 
 * Java Persistent Annotations (JPA) annotations to reflect the persisted 
 * elements.
 * 
 * @author Alexander Ackerman
 * 
 * @version 2.0.0
 */
@Entity
public class Course implements Serializable, Comparable<Course> 
{
    /**
     * The Id field for the persistent class.  The value will be generated using
     * the database's normal generation strategy.  The data will be stored in the 
     * "COURSE_INDEX" column of the database table.
     */
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column
    private Long id;

    /**
     * The name of the course.  The value will be stored in the database in the
     * "COURSE_NAME" column of the database table.
     */
    @Column(name = "COURSE_NAME")
    private String name;
    
    /**
     * The course director for this course.  This is a unidirectional relationship
     * with the {@link Instructor} class.  The data will be stored in the "PERSON_INDEX"
     * column of the database table.  This is a nullable field.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="CD_INDEX", nullable = true)
    private Instructor courseDirector;
    
    /**
     * The list of instructors who teach this course.  This is a bi-directional
     * relationship with the {@link Instructor} class.  The data will be stored
     * in a join table called "COURSE_INSTRUCTOR".  The Course Id will be stored
     * in the "CRS_INDEX" column and the Instructor Id will be stored in the 
     * "INST_INDEX" column.  
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "COURSE_INSTRUCTOR",
        joinColumns= {@JoinColumn(name = "course_id")},
        inverseJoinColumns = {@JoinColumn(name = "instructor_id")}
    )
    @OrderColumn(name="Instructor_Order")
    private List<Instructor> instructors = new ArrayList<Instructor>();

    /**
     * The set of sections that make up this course.  This is a bidirectional
     * relationship with the {@link Section} class.  The data will be stored in
     * a join table called "SECTIONS_COURSE".  The Course Id will be stored in 
     * the "COURSE_FK" column and the Section Id will be stored in the "SECTION_FK"
     * column. The details for this mapping can be found in the {@link Section}
     * class.
     */
    @OneToMany(mappedBy="course", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private List<Section> sections = new ArrayList<Section>();
        
    /**
     * No argument constructor for the Course class
     */
    public Course()
    {
    }
    
    /**
     * Constructor for the Course class.
     * 
     * @param name  The name of the new Course
     */
    public Course(String name)
    {
        this.name = name;
    }
    
    /**
     * Returns the Id field of the Course object
     * 
     * @return  The Id represented as a Long value
     */
    public Long getId() 
    {
        return id;
    }

    /**
     * Sets the Id field of the Course object
     * 
     * @param id    The new Id of the Course
     */
    public void setId(Long id) 
    {
        this.id = id;
    }

    /**
     * Returns the name of the Course in String format
     * 
     * @return  A string representation of the Course name
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * Sets the name of the Course object
     * 
     * @param name  The new name of the Course object
     * @throws java.lang.IllegalArgumentException   When the new name is either 
     *              <code>null</code> or blank.
     */
    public void setName(String name) throws IllegalArgumentException
    {
        if ((name == null) || name.equals(""))
            throw new IllegalArgumentException();
        this.name = name;
    }
    
    /**
     * Returns the Course Director for the Course.
     * 
     * @return  An {@link Instructor} object representing the Course Director
     */
    public Instructor getCourseDirector()
    {
        return courseDirector;
    }
    
    /**
     * Sets the Course Director for the Course.  This can be <code>null</code>.
     * 
     * @param courseDirector    The new Course Director
     */
    public void setCourseDirector(Instructor courseDirector)
    {
        this.courseDirector = courseDirector;
    }
     
    /**
     * Returns a {@link Set} of {@link Section} objects that represent the sections
     * in the Course.
     * 
     * @return  The set of Sections
     */
    public List<Section> getSections()
    {
        return sections;
    }
    
    /**
     * Saves the {@link Set} of {@link Section}s that are part of the course.
     * 
     * @param sections  The sections for the course
     * @throws java.lang.IllegalArgumentException   When the list has a size of 0.
     * @throws NullPointerException             When the List is <code>null</code> 
     */
    public void setSections(List<Section> sections) 
            throws IllegalArgumentException, NullPointerException
    {
        if (sections == null)
            throw new NullPointerException();
        else if (sections.isEmpty())
            throw new IllegalArgumentException();
        this.sections = sections;
    }
    
    /**
     * Adds a {@link Section} to the course.  
     * 
     * @param section   The new section to add to the course
     * @return          <code>true</code> if the section is added, <code>false</code>
     *                  otherwise
     * @throws NullPointerException   When the section is <code>null</code>
     */
    public boolean addSection(Section section) throws NullPointerException
    {
        if (section == null)
            throw new NullPointerException();
        else if (this.sections.contains(section))
            return false;
        return this.sections.add(section);
    }
    
    /**
     * Removes a {@link Section} from the course.  If the {@link Section} is 
     * not in the set, the method returns <code>false</code>.
     * 
     * @param section   The section to remove
     * @return          <code>true</code> if the section is removed from the course, 
     *                  <code>false</code> otherwise
     * @throws NullPointerException   When the section is <code>null</code>
     */
    public boolean removeSection(Section section) throws NullPointerException
    {
        if (section == null)
            throw new NullPointerException();
        return this.sections.remove(section);
    }
    
    /**
     * Returns the {@link com.darkhonor.rage.model.Section} at the given index.
     * 
     * @param index     The index for the given Section
     * @return          The selected Section
     * @throws IllegalArgumentException     If the index is out of range
     */
    public Section getSection(int index) throws IllegalArgumentException
    {
        if (index < 0 || index >= sections.size())
            throw new IllegalArgumentException();
        return sections.get(index);
    }

    /**
     * Returns the {@link List} of instructors for the course
     * 
     * @return  The List of instructors
     */
    public List<Instructor> getInstructors()
    {
        return instructors;
    }

    /**
     * Returns the {@link com.darkhonor.rage.model.Instructor} at the given index.
     *
     * @param index The index for the given Instructor
     * @return      The selected Instructor
     * @throws IllegalArgumentException     If the index is out of range
     */
    public Instructor getInstructor(int index) throws IllegalArgumentException
    {
        if (index < 0 || index >= instructors.size())
            throw new IllegalArgumentException();
        return instructors.get(index);
    }

    /**
     * Set the {@link Collection} of instructors for the course.
     * 
     * @param instructors   The collection of instructors
     * @throws java.lang.IllegalArgumentException   When the List has a size of 0
     * @throws NullPointerException     When the List is <code>null</code>
     */
    public void setInstructors(List<Instructor> instructors) 
            throws IllegalArgumentException, NullPointerException
    {
        if (instructors == null)
            throw new NullPointerException();
        else if (instructors.isEmpty())
            throw new IllegalArgumentException();
        this.instructors = instructors;
    }
    
    /**
     * Adds an {@link Instructor} to the set of Instructors.  
     * 
     * @param instructor    The new instructor to add
     * @return              <code>true</code> if the instructor is added, <code>false
     *                      </code> otherwise
     * @throws NullPointerException   When the instructor is <code>null</code>
     */
    public boolean addInstructor(Instructor instructor) throws NullPointerException
    {
        if (instructor == null)
            throw new NullPointerException();
        else if (this.instructors.contains(instructor))
            return false;
        return this.instructors.add(instructor);
    }
    
    /**
     * Removes an {@link Instructor} from the Course's set of Instructors.  If the {@link 
     * Instructor} is not in the set, the method returns <code>false</code>.
     * 
     * @param instructor    The {@link Instructor} to remove
     * @return              <code>true</code> if the Instructor is removed, 
     *                      <code>false</code> otherwise.
     * @throws java.lang.NullPointerException   When the instructor is <code>null</code>
     */
    public boolean removeInstructor(Instructor instructor) throws NullPointerException
    {
        if (instructor == null)
            throw new NullPointerException();
        return this.instructors.remove(instructor);
    }

    /**
     * Removes the {@link com.darkhonor.rage.model.Instructor} at the given index
     * from the list of Instructos for the Course.  The removed Instructor is
     * returned.
     *
     * @param index     The index of the instructor to remove
     * @return          The removed Instructor object
     * @throws IllegalArgumentException     When the index is out of range
     */
    public Instructor removeInstructor(int index) throws IllegalArgumentException
    {
        if (index < 0 || index >= instructors.size())
            throw new IllegalArgumentException();
        return instructors.remove(index);
    }

    /**
     * Removes all {@link Instructor} from the Course.
     *
     * @since 1.1.0
     */
    public void clearInstructors()
    {
        this.instructors.clear();
    }

    /**
     * Removes all {@link Section} from the Course.
     * 
     * @since 2.0.0
     */
    public void clearSections()
    {
        this.sections.clear();
    }

    /**
     * Compares two Course objects.  The comparison is based upon the Course
     * names.  They are alphabetically compared.
     * 
     * @param other The other Course to compare
     * @return      0 if the Courses are equal, greater than 1 if this Course
     *              is greater than the other Course, less than 1 if this Course
     *              is less than the other Course.
     */
    @Override
    public int compareTo(Course other) 
    {
        if (this.name.equals(other.name))
           return 0;
        else
           return this.name.compareTo(other.name);
    }

    /**
     * Returns a hash code for the Course.  The hash code is based only upon
     * the Course name.
     * 
     * @return  A hash code for the Course
     */
    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }
    
    /**
     * Indicates whether the object specified is equal to this one.  Another Course
     * object is equal if the Course names are equal.
     * 
     * @param object    The object to compare
     * @return          <code>true</code> if the objects are equal, <code>false</code>
     *                  otherwise
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object) return true;
        if (object == null) return false;
        if (object.getClass() != this.getClass()) {
            return false;
        }
        Course other = (Course) object;
        if ((this.name == null && other.name != null) || 
                (this.name!= null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    /**
     * Returns a {@link String} representation of the Course.  The String has
     * the text "Course: " followed by the Course name.
     * 
     * @return  A string representation of the Course
     */
    @Override
    public String toString()
    {
        return name;
    }

}
