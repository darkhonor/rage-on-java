/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * The Version class is used to simply manage the version of the database for
 * application access.  This table/field will be checked on application startup
 * for compatibility with the database.
 *
 * @author Alex Ackerman
 */
@Entity
public class Version implements Serializable
{

    /**
     * The Version ID of the database.  It is stored in the ID field in the
     * database table Version.
     */
    @Id
    private Double id;

    /**
     * No Argument constructor required by JPA
     */
    public Version()
    {
    }

    /**
     * Constructor.
     * @param version   The version of the database
     */
    public Version(Double version) throws NullPointerException, IllegalArgumentException
    {
        if (version == null)
        {
            throw new NullPointerException("Version cannot be null");
        } else if (version <= 0.0)
        {
            throw new IllegalArgumentException("Version cannot be less than or "
                    + "equal to 0");
        } else
        {
            id = version;
        }
    }

    /**
     * Returns the Version id for the database.
     *
     * @return  The Database Version Id
     */
    public Double getId()
    {
        return id;
    }

    /**
     * Sets the Version Id for the database.
     *
     * @param id    The new Database Version Id
     */
    public void setId(Double id) throws NullPointerException, IllegalArgumentException
    {
        if (id == null)
        {
            throw new NullPointerException("Version cannot be null");
        } else if (id <= 0.0)
        {
            throw new IllegalArgumentException("Version cannot be less than or "
                    + "equal to 0");
        } else
        {
            this.id = id;
        }
    }

    /**
     * Compares two objects to determine equality.
     *
     * @param object    The object to compare
     * @return          <code>true</code> if the object is equal to this one,
     *                  <code>false</code> otherwise.
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (object == null)
        {
            return false;
        }
        if (object.getClass() != this.getClass())
        {
            return false;
        }
        Version other = (Version) object;
        if ((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }
}
