/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * 
 * @author Alexander Ackerman
 * 
 * @version 1.1.0
 */
public class Result 
{

    private Question question;
    private List<Response> responses;
    private BigDecimal score;
    private BigDecimal value;
    private boolean partialCredit;
    
    public Result()
    {
        responses = new ArrayList<Response>();
        score = new BigDecimal(BigDecimal.ZERO.toPlainString());
        value = new BigDecimal(BigDecimal.ZERO.toPlainString());
    }
    
    public Result(Question question)
    {
        this.question = question;
        responses = new ArrayList<Response>();
        score = new BigDecimal(BigDecimal.ZERO.toPlainString());
        value = new BigDecimal(BigDecimal.ZERO.toPlainString());
        for (TestCase tc : this.question.getTestCases())
        {
            value = value.add(tc.getValue());
        }
    }

    public Result(Question question, boolean partialCredit)
    {
        this.question = question;
        responses = new ArrayList<Response>();
        score = new BigDecimal(BigDecimal.ZERO.toPlainString());
        value = new BigDecimal(BigDecimal.ZERO.toPlainString());
        for (TestCase tc : this.question.getTestCases())
        {
            value = value.add(tc.getValue());
        }
        this.partialCredit = partialCredit;
    }
    
    public Question getQuestion()
    {
        return question;
    }
    public void setQuestion(Question question)
    {
        this.question = question;
    }
    
    public List<Response> getResponses()
    {
        return responses;
    }
    public void addResponse(Response response)
    {
        this.responses.add(response);
    }
    
    public void calculateScore()
    {
        BigDecimal temp = new BigDecimal(BigDecimal.ZERO.toPlainString());
        
        for (Response r : responses)
        {
            temp = temp.add(r.getPointsEarned());
        }
        if (partialCredit)
        {
            this.score = temp;
            LOGGER.debug("Partial Credit available");
        }
        else
        {
            LOGGER.debug("Partial Credit not available");
            if (!temp.equals(this.value))
            {
                this.score = BigDecimal.ZERO;
            }
            else
            {
                this.score = this.value;
            }
        }
    }
    public BigDecimal getScore()
    {
        return score;
    }
    
    public BigDecimal getValue()
    {
        return value;
    }
    public void addValuePoints(BigDecimal points)
    {
        this.value = this.value.add(points);
    }
    
    private void setValue(BigDecimal value)
    {
        this.value = value;
    }
    
    public boolean getPartialCredit()
    {
        return partialCredit;
    }
    public void setPartialCredit(boolean partialCredit)
    {
        this.partialCredit = partialCredit;
    }
    private Logger LOGGER = Logger.getLogger(Result.class);
    
}
