/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * 
 * @author Alexander Ackerman
 * 
 * @version 1.0.0
 */
public class SectionReport 
{
    private String course;
    private String assignment;
    private Instructor instructor;
    private String section;
    private List<StudentReport> studentReports;
    
    public SectionReport(String course, Instructor instructor, String section,
            String assignment)
    {
        this.course = course;
        this.instructor = instructor;
        this.section = section;
        this.assignment = assignment;
        studentReports = new ArrayList<StudentReport>();
    }

    public SectionReport(String course, String section, String assignment)
    {
        this.course = course;
        this.section = section;
        this.assignment = assignment;
        studentReports = new ArrayList<StudentReport>();
    }
    
    public String getCourse()
    {
        return course;
    }
    public void setCourse(String course) throws IllegalArgumentException
    {
        if ((course == null) || (course.equals("")))
        {
            throw new IllegalArgumentException();
        }
        this.course = course;
    }
    
    public String getAssignment()
    {
        return assignment;
    }
    public void setAssignment(String assignment) throws IllegalArgumentException
    {
        if ((assignment == null) || (assignment.equals("")))
        {
            throw new IllegalArgumentException();
        }
        this.assignment = assignment;
    }
    
    public Instructor getInstructor()
    {
        return instructor;
    }
    public void setInstructor(Instructor instructor) throws IllegalArgumentException
    {
        if (instructor == null)
        {
            throw new IllegalArgumentException();
        }
        this.instructor = instructor;
    }

    public String getSection()
    {
        return section;
    }
    public void setSection(String section) throws IllegalArgumentException
    {
        if ((section == null) || (section.equals("")))
        {
            throw new IllegalArgumentException();
        }
        this.section = section;
    }
     
    public List<StudentReport> getStudentReports()
    {
        return studentReports;
    }
    public void setStudentReports(List<StudentReport> reports)
    {
        this.studentReports = reports;
    }
    public void addStudentReport(StudentReport report)
    {
        this.studentReports.add(report);
    }
    
    public void sortStudentReports()
    {
        Collections.sort(studentReports);
    }
}
