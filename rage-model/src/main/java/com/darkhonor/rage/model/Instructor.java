/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package com.darkhonor.rage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 * The Instructor class extends the {@link Person} class and represents an 
 * instructor in the RAGE system.  The distinction is made between Students and 
 * Instructors in order to provide a mechanism to limit access to the RAGE system
 * to only instructors.  Password authentication is supported by this class.
 * 
 * @author Alexander Ackerman
 * 
 * @version 1.0.0
 */
@Entity
public class Instructor extends Person implements Serializable, Comparable<Instructor>
{

    /**
     * The Windows Active Directory Domain account Id for the instructor.  This
     * field is what is used to determine whether or not the user of the RAGE 
     * system is authorized to use it.  The value will be stored in the "DOMAIN_ACNT"
     * column of the database table.
     */
    @Column(name = "DOMAIN_ACNT")
    private String domainAccount;
    /**
     * The instructor's password for the RAGE system.  The value will be stored in 
     * the "PASSWORD" column of the database table.  The password is not encrypted 
     * at this time in the database.  Is is planned to make a SHA1 hash of the 
     * password before persisting it in the database.
     */
    @Column(name = "PASSWORD")
    private String password;
    /**
     * The {@link List} of sections taught by the instructor.  This is a
     * bidirectional relationship with the {@link Section} class.  The data will
     * be stored in a join table called "INSTRUCTOR_SECTIONS".  The Instructor Id
     * will be stored in the "INSTRUCTOR_FK" column and the Section Id will be stored
     * in the "SECTION_FK" column.  The details for this mapping can be found in the 
     * {@link Section} class.
     */
    @OneToMany(mappedBy = "instructor", cascade = CascadeType.REMOVE)
    private List<Section> sections = new ArrayList<Section>();

    /**
     * No argument constructor for the Instructor class
     */
    public Instructor()
    {
    }

    /**
     * Constructor for the Instructor class.
     * 
     * @param firstName     The first name of the instructor
     * @param lastName      The last name of the instructor
     * @param webID         The Windows Active Directory domain account Id
     */
    public Instructor(String firstName, String lastName, String webID)
    {
        super.setFirstName(firstName);
        super.setLastName(lastName);
        super.setWebID(webID);
    }

    /**
     * Returns a string of the instructors Windows Active Directory domain account
     * Id.
     * 
     * @return      The instructor's domain account Id
     */
    public String getDomainAccount()
    {
        return domainAccount;
    }

    /**
     * Sets the Windows Active Directory domain account Id for the instructor.
     * 
     * @param domainAccount     The new domain account Id
     * @throws java.lang.IllegalArgumentException   When the account is blank
     * @throws NullPointerException     When the account is <code>null</code>
     */
    public void setDomainAccount(String domainAccount) throws IllegalArgumentException,
            NullPointerException
    {
        if (domainAccount == null)
        {
            throw new NullPointerException();
        } else if (domainAccount.equals(""))
        {
            throw new IllegalArgumentException();
        }
        this.domainAccount = domainAccount;
    }

    /**
     * Returns the unencrypted password for the instructor.
     * 
     * @return  A string representation of the Instructor's password
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * Sets the password for the Instructor.  The password is not encrypted at 
     * this time.  Is is planned to make a SHA1 hash of the password before 
     * persisting it in the database.
     * 
     * @param password      The new password
     * @throws java.lang.IllegalArgumentException   When the password is blank.
     * @throws NullPointerException     When the password is <code>null</code>
     */
    public void setPassword(String password) throws IllegalArgumentException,
            NullPointerException
    {
        if (password == null)
        {
            throw new NullPointerException();
        } else if (password.equals(""))
        {
            throw new IllegalArgumentException();
        }
        this.password = password;
    }

    /**
     * Returns a {@link List} of {@link Section} objects that represent the
     * sections taught by the instructor.
     * 
     * @return  The set of Sections
     */
    public List<Section> getSections()
    {
        return sections;
    }

    /**
     * Returns the Section at the specified index.  The value cannot be less than
     * 0 or greater than or equal to the number of sections in the List.
     *
     * @param index The index of the Section
     * @return      The Section at the given index
     * @throws IllegalArgumentException Thrown when the index less than 0 or 
     *                                  greater than sections.size
     */
    public Section getSection(int index) throws IllegalArgumentException
    {
        if (index < 0 || index >= sections.size())
        {
            throw new IllegalArgumentException();
        }
        return sections.get(index);
    }

    /**
     * Saves the {@link List} of {@link Section}s that are taught by the instructor.
     * 
     * @param sections  The sections for the instructors
     * @throws java.lang.IllegalArgumentException   When the list has a size of 0.
     * @throws NullPointerException     When the List is <code>null</code>
     */
    public void setSections(List<Section> sections) throws IllegalArgumentException,
            NullPointerException
    {
        if (sections == null)
        {
            throw new NullPointerException();
        } else if (sections.size() == 0)
        {
            throw new IllegalArgumentException();
        }
        this.sections = sections;
    }

    /**
     * Adds a {@link Section} to the course.  
     * 
     * @param section   The new section to add to the instructor
     * @return          <code>true</code> if the section is added, <code>false</code>
     *                  otherwise
     * @throws java.lang.IllegalArgumentException   When the section is exists
     * @throws NullPointerException     When the section is <code>null</code>
     */
    public boolean addSection(Section section) throws IllegalArgumentException,
            NullPointerException
    {
        if (section == null)
        {
            throw new NullPointerException();
        }
        if (this.sections.indexOf(section) >= 0)
        {
            throw new IllegalArgumentException();
        } else
        {
            return this.sections.add(section);
        }
    }

    /**
     * Removes a {@link Section} from the instructor.  If the {@link Section} is 
     * not in the set, the method returns <code>false</code>.
     * 
     * @param section   The section to remove
     * @return          <code>true</code> if the section is removed from the instructor, 
     *                  <code>false</code> otherwise
     * @throws java.lang.IllegalArgumentException   When the section is <code>null</code>
     */
    public boolean removeSection(Section section) throws IllegalArgumentException,
            NullPointerException
    {
        if (section == null)
        {
            throw new NullPointerException();
        } //else if (sections.indexOf(section) < 0)
        else if (!sections.contains(section))
        {
            for (int i = 0; i < sections.size(); i++)
            {
                System.err.println("Section (" + sections.get(i).getId() + "): "
                        + sections.get(i).getName());

            }
            throw new IllegalArgumentException("Error finding Section ("
                    + section + ":" + section.getId() + ") for Instructor #" + this.getId()
                    + ": " + sections.indexOf(section));
        }
        return this.sections.remove(section);
    }

    /**
     * Compares two Instructor objects.  The comparison is based upon the Web ID,
     * last name, and first name.  For the String values, they are alphabetically 
     * compared.
     * 
     * @param other The other Instructor to compare
     * @return      0 if the Instructors are equal, greater than 1 if this Instructor
     *              is greater than the other Instructor, less than 1 if this Instructor
     *              is less than the other Instructor.
     */
    @Override
    public int compareTo(Instructor other)
    {
        if (super.getLastName().equals(other.getLastName()))
        {
            if (super.getFirstName().equals(other.getFirstName()))
            {
                if (super.getWebID().equals(other.getWebID()))
                {
                    return 0;
                } else
                {
                    return super.getWebID().compareTo(other.getWebID());
                }
            } else
            {
                return super.getFirstName().compareTo(other.getFirstName());
            }
        } else
        {
            return super.getLastName().compareTo(other.getLastName());
        }
    }

    /**
     * Returns a hash code for the Instructor.  The hash code is based upon
     * the Id, the last name, and the first name.
     * 
     * @return  A hash code for the Instructor
     */
    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (super.getWebID() != null ? super.getWebID().hashCode() : 0);
        hash += (super.getLastName() != null ? super.getLastName().hashCode() : 0);
        hash += (super.getFirstName() != null ? super.getFirstName().hashCode() : 0);
        return hash;
    }

    /**
     * Indicates whether the object specified is equal to this one.  Another Instructor
     * object is equal if the Web ID, last name, and first name are all equal
     * 
     * @param object    The object to compare
     * @return          <code>true</code> if the objects are equal, <code>false</code>
     *                  otherwise
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (object == null)
        {
            return false;
        }
        if (object.getClass() != this.getClass())
        {
            return false;
        }
        Instructor other = (Instructor) object;
        if ((super.getWebID() == null && other.getWebID() != null)
                || (super.getWebID() != null && !super.getWebID().equals(other.getWebID()))
                || (super.getLastName() == null && other.getLastName() != null)
                || (super.getLastName() != null && !this.getLastName().equals(other.getLastName()))
                || (super.getFirstName() == null && other.getFirstName() != null)
                || (super.getFirstName() != null && !this.getFirstName().equals(other.getFirstName())))
        {

            return false;
        }
        return true;
    }

    /**
     * Returns a {@link String} representation of the Instructor.  The String has
     * the instructor's last name, ", ", then their first name.
     * 
     * @return  A string representation of the Instructor
     */
    @Override
    public String toString()
    {
        return super.getLastName() + ", " + super.getFirstName();
    }
}
