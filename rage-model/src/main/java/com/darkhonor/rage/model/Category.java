/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The Category class represents a category of question used by the RAGE system.
 * Sample categories include "Simple Calculation" or "Complex Loop with Array".
 * This is a persistent class using the Java Persistent Annotations (JPA) 
 * annotations to reflect the persisted criteria.
 * 
 * @author Alexander Ackerman
 * 
 * @version 1.0.0
 */
@Entity
public class Category implements Serializable, Comparable<Category> 
{
    /**
     * The Id field for the persistent class.  The value will be generated using 
     * the database's normal generation strategy.  The data will be stored in the 
     * "CAT_INDEX" column of the database table.
     */
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "CAT_INDEX")
    private Long id;

    /**
     * The name of the category.  The value will be stored in the database in 
     * the "CAT_NAME" column of the database table.
     */
    @Column(name = "CAT_NAME")
    private String name;
    
    /**
     * No argument constructor for the Category class
     */
    public Category()
    {
    }
    
    /**
     * Constructor for the Category class.  
     * 
     * @param name  The name of the new Category
     */
    public Category(String name)
    {
        this.name = name;
    }
    
    /**
     * Returns the Id field of the Category object 
     * 
     * @return  The Id represented as a Long value
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the ID field of the Category object
     * 
     * @param id    The new Id of the Category
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the name of the Category in String format
     * 
     * @return  A string representation of the Category name
     */
    public String getName() 
    {
        return name;
    }
    
    /**
     * Sets the name of the Category object.
     * 
     * @param name      The new name of the Category object
     * 
     * @throws java.lang.IllegalArgumentException   When the new name is either 
     *              <code>null</code> or blank.
     */
    public void setName(String name) throws IllegalArgumentException
    {
        if ((name == null) || name.equals(""))
            throw new IllegalArgumentException();
        this.name = name;
    }
    
    /**
     * Compares two Category objects.  The comparison is based upon the Category
     * names.  They are alphabetically compared.
     * 
     * @param other The other Category to compare
     * @return      0 if the Categories are equal, greater than 1 if this Category
     *              is greater than the other Category, less than 1 if this Category
     *              is less than the other Category.
     */
    @Override
    public int compareTo(Category other)
    {
        if (this.name.equals(other.name))
            return 0;
        else
            return this.name.compareTo(other.name);
    }
    
    /**
     * Returns a hash code for the Category.  The hash code is based only upon
     * the Category name.
     * 
     * @return  A hashCode value for the Category
     */
    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }
    
    /**
     * Indicates whether the object specified is equal to this one.  Another
     * Category object is equal if the Category names are equal.
     * 
     * @param object    The object to compare
     * @return          <code>true</code> if the objects are equal, <code>false</code>
     *                  otherwise
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object) return true;
        if (object == null) return false;
        if (object.getClass() != this.getClass()) {
            return false;
        }
        Category other = (Category) object;
        if ((this.name == null && other.name != null) || 
                (this.name!= null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    /**
     * Returns a {@link String} representation of the Category.  The String has
     * the text "Category: " and the Category name.
     * 
     * @return  A string representation of the Category
     */
    @Override
    public String toString()
    {
        return "Category: " + name;
    }
    
}
