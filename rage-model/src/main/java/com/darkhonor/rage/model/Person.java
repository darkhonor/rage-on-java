/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The Person class is an abstract class representing a person in the RAGE System.
 * This is an abstract class and has been implemented by the {@link Instructor} and
 * {@link Student} classes.
 * 
 * @author Alexander Ackerman
 * 
 * @version 1.0.0
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Person implements Serializable {
    
    /**
     * The Id field for the persistent class.  The value will be generated using
     * the database's normal generation strategy.  The data will be stored in the
     * "PERSON_INDEX" column of the database table.
     */
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "PERSON_INDEX")
    private Long id;
    
    /**
     * The first name for the person.  The data will be stored in the "FIRST_NAME"
     * column of the database table.
     */
    @Column(name = "FIRST_NAME")
    private String firstName;
    
    /**
     * The last name for the person.  The data will be stored in the "LAST_NAME"
     * column of the database table.
     */
    @Column(name = "LAST_NAME")
    private String lastName;

    /**
     * The Webpost WebId for the person.  This value must match the person's 
     * entry in the Webpost system in order to ensure success.  The data will
     * be stored in the "WEBID" column of the database table.
     */
    @Column(name = "WEBID")
    private String webID;
    
    /**
     * Returns the Id fielf of the Person object.
     * 
     * @return      The Id represented as a Long value
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the Id field fo the Person object.
     * 
     * @param id    The new Id of the Person
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the first name of the person
     * 
     * @return  A string representation of the person's first name
     */
    public String getFirstName()
    {
        return firstName;
    }
    
    /**
     * Sets the first name for the person
     * 
     * @param name  The new first name
     * @throws java.lang.IllegalArgumentException   When the name is blank
     * @throws NullPointerException     When the name is <code>null</code>
     */
    public void setFirstName(String name) throws IllegalArgumentException,
            NullPointerException
    {
        if (name == null)
            throw new NullPointerException();
        else if (name.equals(""))
            throw new IllegalArgumentException();
        this.firstName = name;
    }
    
    /**
     * Returns the last name of the person
     * 
     * @return  A string representation of the person's last name
     */
    public String getLastName()
    {
        return lastName;
    }
    
    /**
     * Sets the last name of the person
     * 
     * @param name      The new last name
     * @throws java.lang.IllegalArgumentException   When the name is blank
     * @throws NullPointerException     When the name is <code>null</code>
     */
    public void setLastName(String name) throws IllegalArgumentException,
            NullPointerException
    {
        if (name == null)
            throw new NullPointerException();
        else if (name.equals(""))
            throw new IllegalArgumentException();
        this.lastName = name;
    }
    
    /**
     * Returns the Webpost ID for the person.
     * 
     * @return  A string representation of the person's Webpost ID
     */
    public String getWebID()
    {
        return webID;
    }
    
    /**
     * Sets the Webpost ID for the person
     * 
     * @param webID     The new Webpost ID
     * @throws java.lang.IllegalArgumentException   When the new Id is blank
     * @throws NullPointerException     When the name is <code>null</code>
     */
    public void setWebID(String webID) throws IllegalArgumentException,
            NullPointerException
    {
        if (webID == null)
            throw new NullPointerException();
        else if (webID.equals(""))
            throw new IllegalArgumentException();
        this.webID = webID;
    }
    
    /**
     * Abstract method to return a hash code for the Person object.
     * 
     * @return  A hash code for a person
     */
    @Override
    public abstract int hashCode();

    /**
     * Abstract method to determine equality between two Person objects
     * 
     * @param object    The object to compare
     * @return          <code>true</code> if the objects are equal, <code>false</code>
     *                  otherwise
     */
    @Override
    public abstract boolean equals(Object object);

    /**
     * Abstract method to return a string representation of the person object
     * 
     * @return  A string representation of a person
     */
    @Override
    public abstract String toString();

}
