/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit 4 tests of the {@link Question} class and its methods
 *
 * @author Alexander Ackerman
 * 
 * @version 1.0.0
 */
public class QuestionTest 
{

    /**
     * No argument constructor
     */
    public QuestionTest() 
    {
    }

    /**
     * Set up method for all of the method tests
     */
    @Before
    public void setUp() 
    {
        instance = new Question();
        instance.setName("Sounds");
        instance.setId(42L);
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        instance.setCategory(cat);
        instance.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        instance.setOrderedOutput(false);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        instance.setTestCases(testCases);
    }

    /**
     * Test of getId method, of class Question.
     */
    @Test
    public void getId() 
    {
        System.out.println("getId");
        Long expResult = 42L;
        Long result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Question.
     */
    @Test
    public void setId() 
    {
        System.out.println("setId");
        Long id = 69L;
        instance.setId(id);
        assertEquals(id, instance.getId());
    }

    /**
     * Test of getName method.
     */
    @Test
    public void getName()
    {
        System.out.println("getName");
        String expected = "Sounds";
        assertEquals(expected, instance.getName());
    }
    
    /**
     * Test of setName method
     */
    @Test
    public void setName()
    {
        System.out.println("setName");
        String newName = "Practice-InchCM";
        instance.setName(newName);
        assertEquals(newName, instance.getName());
    }
    
    /**
     * Test of setName method (<code>null</code> name)
     * 
     * @throws java.lang.IllegalArgumentException   When the name is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void setNullName()
    {
        System.out.println("setNullName");
        String nullName = null;
        instance.setName(nullName);
    }
    
    /**
     * Test of setName method (blank name)
     * 
     * @throws java.lang.IllegalArgumentException   When the name is blank
     */
    @Test(expected=IllegalArgumentException.class)
    public void setBlankName()
    {
        System.out.println("setBlankName");
        String blankName = "";
        instance.setName(blankName);
    }
    
    /**
     * Test of getCategory method
     */    
    @Test
    public void getCategory()
    {
        System.out.println("getCategory");
        Category expected = new Category("SimpleCalculation");
        expected.setId(12L);
        assertEquals(expected, instance.getCategory());
    }
    
    /**
     * Test of setCategory method
     */
    @Test
    public void setCategory()
    {
        System.out.println("setCategory");
        Category newCat = new Category("SimpleSelection");
        newCat.setId(12L);
        instance.setCategory(newCat);
        assertEquals(newCat, instance.getCategory());
    }
    
    /**
     * Test of setCategory method (<code>null</code> {@link Category})
     * 
     * @throws java.lang.IllegalArgumentException   When category is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void setNullCategory()
    {
        System.out.println("setNullCategory");
        Category nullCat = null;
        instance.setCategory(nullCat);
    }
    
    /**
     * Test of getDescription method
     */
    @Test
    public void getDescription()
    {
        System.out.println("getDescription");
        String expected = "This is a simple test question";
        assertEquals(expected, instance.getDescription());
    }
    
    /**
     * Test of setDescription method
     */
    @Test
    public void setDescription()
    {
        System.out.println("setDescription");
        String newDesc = "This is a new test question";
        instance.setDescription(newDesc);
        assertEquals(newDesc, instance.getDescription());
    }
    
    /**
     * Test of setDescription method (<code>null</code> description)
     * 
     * @throws java.lang.IllegalArgumentException   When description is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void setNullDescription()
    {
        System.out.println("setNullDescription");
        String nullDesc = null;
        instance.setDescription(nullDesc);
    }
    
    /**
     * Test of getVerbatim method
     */
    @Test
    public void getVerbatim()
    {
        System.out.println("getVerbatim");
        assertTrue(instance.getVerbatim());
    }
    
    /**
     * Test of setVerbatim method
     */
    @Test
    public void setVerbatim()
    {
        System.out.println("setVerbatim");
        boolean newVal = false;
        instance.setVerbatim(newVal);
        assertFalse(instance.getVerbatim());
    }

    /**
     * Test of getOrderedOutput method
     */
    @Test
    public void getOrderedOutput()
    {
        System.out.println("getOrderedOutput");
        assertFalse(instance.getOrderedOutput());
    }

    /**
     * Test of setOrderedOutput method
     */
    @Test
    public void setOrderedOutput()
    {
        System.out.println("setOrderedOutput");
        boolean newVal = true;
        assertFalse(instance.getOrderedOutput());
        instance.setOrderedOutput(newVal);
        assertTrue(instance.getOrderedOutput());
    }

    /**
     * Test of getTestCases method
     */
    @Test
    public void getTestCases()
    {
        System.out.println("getTestCases");
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        assertEquals(testCases, instance.getTestCases());
    }
    
    /**
     * Test of setTestCases method
     */
    @Test
    public void setTestCases()
    {
        System.out.println("setTestCases");
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(34L);
        test1.setValue(new BigDecimal("15.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(10L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22100");
        inputSet.add("2");
        inputSet.add("32");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        instance.setTestCases(testCases);
        assertEquals(testCases, instance.getTestCases());
    }
    
    /**
     * Test of setTestCases method (<code>null</code> set of testCases)
     * 
     * @throws java.lang.IllegalArgumentException   When set is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void setNullTestCases()
    {
        System.out.println("setNullTestCases");
        instance.setTestCases(null);
    }
    
    /**
     * Test of addTestCase method
     */
    @Test
    public void addTestCase()
    {
        System.out.println("addTestCase");
        TestCase test1 = new TestCase();
        test1.setId(34L);
        test1.setValue(new BigDecimal("1.6"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("16000");
        inputSet.add("2");
        inputSet.add("8");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        assertTrue(instance.addTestCase(test1));
        // Started with 2 test cases, added a third
        assertEquals(3, instance.getTestCases().size());
        assertTrue(instance.getTestCases().contains(test1));
    }
    
    /**
     * Test of addTestCase method (<code>null</code> {@link TestCase})
     * 
     * @throws java.lang.IllegalArgumentException   When testCase is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void addNullTestCase()
    {
        System.out.println("addNullTestCase");
        TestCase nullTestCase = null;
        instance.addTestCase(nullTestCase);
    }
    
    /**
     * Test of addTestCase method (duplicate test case).  This is possible.
     */
    @Test
    public void addDuplicateTestCase()
    {
        System.out.println("addDuplicateTestCase");
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        assertTrue(instance.addTestCase(test1));
    }
    
    /**
     * Test of removeTestCase method
     */
    @Test
    public void removeTestCase()
    {
        System.out.println("removeTestCase");
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        assertTrue(instance.removeTestCase(test1));
    }
    
    /**
     * Test of removeTestCase method (<code>null</code> {@link TestCase})
     * 
     * @throws java.lang.IllegalArgumentException   When testCase is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void removeNullTestCase()
    {
        System.out.println("removeNullTestCase");
        TestCase nullTestCase = null;
        instance.removeTestCase(nullTestCase);
    }
    
    /**
     * Test of removeTestCase method (non-existent {@link TestCase}
     * 
     * @throws java.lang.IllegalArgumentException   When test case doesn't exist in set
     */
    @Test
    public void removeNonExistentTestCase()
    {
        System.out.println("removeNonExistentTestCase");
        TestCase test1 = new TestCase();
        test1.setId(22L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("22100");
        inputSet.add("1");
        inputSet.add("8");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("729380000");
        outputSet.add("723519.5313");
        outputSet.add("767.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        assertFalse(instance.removeTestCase(test1));
    }
    
    /**
     * Test of compareTo method (equal objects)
     */
    @Test
    public void compareToEquals()
    {
        System.out.println("compareToEquals");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
    
    /**
     * Test of compareTo method (larger id)
     */
    @Test
    public void compareToLargerId()
    {
        System.out.println("compareToLargerId");
        Question other = new Question();
        other.setId(43L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
    
    /**
     * Test of compareTo method (smaller id)
     */
    @Test
    public void compareToSmallerId()
    {
        System.out.println("compareToSmallerId");
        Question other = new Question();
        other.setId(40L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
    
    /**
     * Test of compareTo method (larger name)
     */
    @Test
    public void compareToLargerName()
    {
        System.out.println("compareToLargerName");
        Question other = new Question();
        other.setId(42L);
        other.setName("TraceRoute");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertTrue(instance.compareTo(other) < 0);
        assertTrue(other.compareTo(instance) > 0);
    }
    
    /**
     * Test of compareTo method (smaller name)
     */
    @Test
    public void compareToSmallerName()
    {
        System.out.println("compareToSmallerName");
        Question other = new Question();
        other.setId(42L);
        other.setName("Practice-InchCM");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertTrue(instance.compareTo(other) > 0);
        assertTrue(other.compareTo(instance) < 0);
    }
    
    /**
     * Test of compareTo method (larger {@link Category})
     */
    @Test
    public void compareToLargerCategory()
    {
        System.out.println("compareToLargerCategory");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("SimpleSubstitution");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
    
    /**
     * Test of compareTo method (smaller {@link Category})
     */
    @Test
    public void compareToSmallerCategory()
    {
        System.out.println("compareToSmallerCategory");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("Graphics");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
     
    /**
     * Test of compareTo method (different verbatim)
     */
    @Test
    public void compareToDiffVerbatim()
    {
        System.out.println("compareToDiffVerbatim");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(false);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
    
    /**
     * Test of hashCode method (equal objects)
     */
    @Test
    public void hashCodeSame()
    {
        System.out.println("hashCodeSame");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertEquals(instance.hashCode(), other.hashCode());
    }

    /**
     * Test of hashCode method (different id)
     */
    @Test
    public void hashCodeDiffId()
    {
        System.out.println("hashCodeDiffId");
        Question other = new Question();
        other.setId(69L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertTrue(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (different name)
     */
    @Test
    public void hashCodeDiffName()
    {
        System.out.println("hashCodeDiffName");
        Question other = new Question();
        other.setId(42L);
        other.setName("Practice-InchCM");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertFalse(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (different {@link Category})
     */
    @Test
    public void hashCodeDiffCat()
    {
        System.out.println("hashCodeDiffCat");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("SimpleSelection");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertTrue(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (different description)
     */
    @Test
    public void hashCodeDiffDescription()
    {
        System.out.println("hashCodeDiffDescription");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a another simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertTrue(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (different verbatim)
     */
    @Test
    public void hashCodeDiffVerbatim()
    {
        System.out.println("hashCodeDiffVerbatim");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(false);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertTrue(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (1 different {@link TestCase})
     */
    @Test
    public void hashCodeDiffTestCases1Diff()
    {
        System.out.println("hashCodeDiffTestCases1Diff");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(43L);
        test1.setValue(new BigDecimal("1.6"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("16230");
        inputSet.add("2");
        inputSet.add("8");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("380000");
        outputSet.add("519.5313");
        outputSet.add("5.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertEquals(instance.hashCode(), other.hashCode());
    }
    
    /**
     * Test of equals method (equals method)
     */
    @Test
    public void equals()
    {
        System.out.println("equals");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertTrue(instance.equals(other));
    }
    
    /**
     * Test of equals method (different Id)
     */
    @Test
    public void equalsDiffId()
    {
        System.out.println("equalsDiffId");
        Question other = new Question();
        other.setId(69L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertTrue(instance.equals(other));
    }
    
    /**
     * Test of equals method (different name)
     */
    @Test
    public void equalsDiffName()
    {
        System.out.println("equalsDiffName");
        Question other = new Question();
        other.setId(42L);
        other.setName("Practice-InchCM");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertFalse(instance.equals(other));
    }
    
    /**
     * Test of equals method (different {@link Category})
     */
    @Test
    public void equalsDiffCat()
    {
        System.out.println("equalsDiffCat");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("SimpleSelection");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertTrue(instance.equals(other));
    }
    
    /**
     * Test of equals method (different description)
     */
    @Test
    public void equalsDiffDescription()
    {
        System.out.println("equalsDiffDescription");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a another simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertTrue(instance.equals(other));
    }
    
    /**
     * Test of equals method (different verbatim)
     */
    @Test
    public void equalsDiffVerbatim()
    {
        System.out.println("equalsDiffDescription");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(false);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(42L);
        test1.setValue(new BigDecimal("1.4"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertTrue(instance.equals(other));
    }
    
    /**
     * Test of equals method (1 different test case)
     */
    @Test
    public void equalsDiffTestCases1Diff()
    {
        System.out.println("equalsDiffTestCases1Diff");
        Question other = new Question();
        other.setId(42L);
        other.setName("Sounds");
        Category cat = new Category("SimpleCalculation");
        cat.setId(12L);
        other.setCategory(cat);
        other.setDescription("This is a simple test question");
        instance.setVerbatim(true);
        List<TestCase> testCases = new ArrayList<TestCase>();
        TestCase test1 = new TestCase();
        test1.setId(16L);
        test1.setValue(new BigDecimal("1.7"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("16300");
        inputSet.add("2");
        inputSet.add("8");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("80000");
        outputSet.add("519.5313");
        outputSet.add("5.7027");
        test1.setInputs(inputSet);
        test1.setOutputs(outputSet);
        TestCase test2 = new TestCase();
        test2.setId(12L);
        test2.setValue(new BigDecimal("2.5"));
        List<String> inputSet2 = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("4");
        inputSet.add("16");
        List<String> outputSet2 = new ArrayList<String>();
        outputSet.add("4252300");
        outputSet.add("41223.2433");
        outputSet.add("40.2234");
        test2.setInputs(inputSet2);
        test2.setOutputs(outputSet2);
        testCases.add(test1);
        testCases.add(test2);
        other.setTestCases(testCases);
        assertTrue(instance.equals(other));
    }

    /**
     * Test of toString method
     */
    @Test
    public void testToString()
    {
        System.out.println("testToString");
        assertEquals("Question Sounds: This is a simple test question", 
                instance.toString());
    }

    private Question instance;
}