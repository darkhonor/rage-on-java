/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit 4 tests of the {@link Category} class and its methods.
 *
 * @author Alexander Ackerman
 * 
 * @version 1.0.0
 */
public class CategoryTest 
{

    /**
     * No argument constructor
     */
    public CategoryTest() 
    {
    }

    /**
     * Set up method for all of the method tests
     */
    @Before
    public void setUp() 
    {
        instance = new Category();
        instance.setId(42L);
        instance.setName("SimpleCalculation");
    }

    /**
     * Test of getId method, of class Category.
     */
    @Test
    public void getId() 
    {
        System.out.println("getId");
        Long expResult = 42L;
        Long result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Category.
     */
    @Test
    public void setId() 
    {
        System.out.println("setId");
        Long id = 69L;
        instance.setId(id);
        assertEquals(id, instance.getId());
    }

    /**
     * Test of getName method.
     */
    @Test
    public void getName()
    {
        System.out.println("getName");
        String expResult = "SimpleCalculation";
        assertEquals(expResult, instance.getName());
    }
    
    /**
     * Test of setName method
     */
    @Test
    public void setName()
    {
        System.out.println("setName");
        String newName = "SimpleSelection";
        instance.setName(newName);
        assertEquals(newName, instance.getName());
    }
    
    /**
     * Test of setName method (<code>null</code> name)
     * 
     * @throws java.lang.IllegalArgumentException   When the name is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void setNullName()
    {
        System.out.println("setNullName");
        String nullName = null;
        instance.setName(nullName);
    }
    
    /**
     * Test of setName method (blank name)
     * 
     * @throws java.lang.IllegalArgumentException   When the name is blank.
     */
    @Test(expected=IllegalArgumentException.class)
    public void setBlankName()
    {
        System.out.println("setBlankName");
        String blankName = "";
        instance.setName(blankName);
    }
    
    /**
     * Test of compareTo method (equal <code>Category</code> objects
     */
    @Test
    public void compareToEquals()
    {
        System.out.println("compareToEquals");
        Category other = new Category();
        other.setId(42L);
        other.setName("SimpleCalculation");
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
    
    /**
     * Test of compareTo method (larger <code>Category.id</code>)
     */
    @Test
    public void compareToLargerId()
    {
        System.out.println("compareToLargerId");
        Category other = new Category();
        other.setId(43L);
        other.setName("SimpleCalculation");
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
    
    /**
     * Test of compareTo method (smaller <code>Category.id</code>)
     */
    @Test
    public void compareToSmallerId()
    {
        System.out.println("compareToSmallerId");
        Category other = new Category();
        other.setId(40L);
        other.setName("SimpleCalculation");
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
    
    /**
     * Test of compareTo method (larger <code>Category.name</code>)
     */
    @Test
    public void compareToLargerName()
    {
        System.out.println("compareToLargerName");
        Category other = new Category();
        other.setId(42L);
        other.setName("SimpleSubstitution");
        assertTrue(instance.compareTo(other) < 0);
        assertTrue(other.compareTo(instance) > 0);
    }
    
    /**
     * Test of compareTo method (smaller <code>Category.name</code>)
     */
    @Test
    public void compareToSmallerName()
    {
        System.out.println("compareToSmallerName");
        Category other = new Category();
        other.setId(42L);
        other.setName("Graphics");
        assertTrue(instance.compareTo(other) > 0);
        assertTrue(other.compareTo(instance) < 0);
    }
    
    /**
     * Test of hashCode method (equal <code>Category</code> objects)
     */
    @Test
    public void hashCodeSame()
    {
        System.out.println("hashCodeSame");
        Category other = new Category();
        other.setId(42L);
        other.setName("SimpleCalculation");
        assertEquals(instance.hashCode(), other.hashCode());
    }
    
    /**
     * Test of hashCode method (different <code>Category.id</code>)
     */
    @Test
    public void hashCodeDiffId()
    {
        System.out.println("hashCodeDiffId");
        Category other = new Category();
        other.setId(69L);
        other.setName("SimpleCalculation");
        assertEquals(instance.hashCode(), other.hashCode());
    }
    
    /**
     * Test of hashCode method (different <code>Category.name</code>)
     */
    @Test
    public void hashCodeDiffName()
    {
        System.out.println("hashCodeDiffName");
        Category other = new Category();
        other.setId(42L);
        other.setName("SimpleSelection");
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertFalse(hash1.equals(hash2));
    }
    
    /**
     * Test of equals method (equal objects)
     */
    @Test
    public void equals()
    {
        System.out.println("equals");
        Category other = new Category();
        other.setId(42L);
        other.setName("SimpleCalculation");
        assertTrue(instance.equals(other));
    }
    
    /**
     * Test of equals method (different <code>Category.id</code>)
     */
    @Test
    public void equalsDiffId()
    {
        System.out.println("equalsDiffId");
        Category other = new Category();
        other.setId(69L);
        other.setName("SimpleCalculation");
        assertTrue(instance.equals(other));
    }
    
    /**
     * Test of equals method (different <code>Category.name</code>)
     */
    @Test
    public void equalsDiffName()
    {
        System.out.println("equalsDiffName");
        Category other = new Category();
        other.setId(42L);
        other.setName("SimpleSelection");
        assertFalse(instance.equals(other));
    }
    
    /**
     * Test of toString method
     */
    @Test
    public void testToString()
    {
        System.out.println("testToString");
        assertEquals("Category: SimpleCalculation", instance.toString());
    }

    private Category instance;
    
}