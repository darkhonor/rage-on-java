/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit 4 tests of the {@link GradedEvent} class and its methods
 *
 * @author Alexander Ackerman
 * 
 * @version 1.0.0
 */
public class GradedEventTest 
{

    /**
     * No argument constructor
     */
    public GradedEventTest() 
    {
    }

    /**
     * Set up method for all of the method tests
     */
    @Before
    public void setUp() 
    {
        Course course = new Course("CS110");
        course.setId(110L);
        instance = new GradedEvent(course, "HW2", "F08-M5");
        instance.setId(42L);
        instance.setDueDate("21 Feb 2010");
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        instance.setQuestions(questions);
        instance.setPartialCredit(true);
        instance.setTerm("Fall 2008");
    }

    /**
     * Test of getId method, of class GradedEvent.
     */
    @Test
    public void testGetId() 
    {
        System.out.println("getId");
        Long expResult = 42L;
        Long result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class GradedEvent.
     */
    @Test
    public void testSetId() 
    {
        System.out.println("setId");
        Long id = 69L;
        instance.setId(id);
        assertEquals(id, instance.getId());
    }

    /**
     * Test of getCourse method
     */
    @Test
    public void getCourse()
    {
        System.out.println("getCourse");
        Course expected = new Course("CS110");
        expected.setId(110L);
        assertEquals(expected, instance.getCourse());
    }
    
    /**
     * Test of setCourse method
     */
    @Test
    public void setCourse()
    {
        System.out.println("setCourse");
        Course newCourse = new Course("CS483");
        instance.setCourse(newCourse);
        assertEquals(newCourse, instance.getCourse());
    }
    
    /**
     * Test of setCourse method (<code>null</code> {@link Course})
     * 
     * @throws java.lang.IllegalArgumentException   When the course is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void setNullCourse()
    {
        System.out.println("setNullCourse");
        Course nullCourse = null;
        instance.setCourse(nullCourse);
    }
    
    /**
     * Test of getTerm method
     */
    @Test
    public void getTerm()
    {
        System.out.println("getTerm");
        String expected = "Fall 2008";
        assertEquals(expected, instance.getTerm());
    }
    
    /**
     * Test of setTerm method
     */
    @Test
    public void setTerm()
    {
        System.out.println("setTerm");
        String newTerm = "Spring 2009";
        assertEquals("Fall 2008", instance.getTerm());
        instance.setTerm(newTerm);
        assertEquals(newTerm, instance.getTerm());
    }
    
    /**
     * Test of setTerm method (<code>null</code> term {@link String})
     * 
     * @throws java.lang.IllegalArgumentException   When term is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void setNullTerm()
    {
        System.out.println("setNullTerm");
        instance.setTerm(null);
    }
    
    /**
     * Test of setTerm method(blank term {@link String})
     * 
     * @throws java.lang.IllegalArgumentException   When term is blank
     */
    @Test(expected=IllegalArgumentException.class)
    public void setBlankTerm()
    {
        System.out.println("setBlankTerm");
        instance.setTerm("");
    }
    
    /**
     * Test of getAssignment method
     */
    @Test
    public void getAssignment()
    {
        System.out.println("getAssignment");
        String expected = "HW2";
        assertEquals(expected, instance.getAssignment());
    }
    
    /**
     * Test of setAssignment method
     */
    @Test
    public void setAssignment()
    {
        System.out.println("setAssignment");
        String newAssignment = "HW1";
        instance.setAssignment(newAssignment);
        assertEquals(newAssignment, instance.getAssignment());
    }
    
    /**
     * Test of setAssignment method (<code>null</code> assignment {@link String})
     * 
     * @throws java.lang.IllegalArgumentException   When the assignment is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void setNullAssignment()
    {
        System.out.println("setNullAssignment");
        String nullAssignment = null;
        instance.setAssignment(nullAssignment);
    }
    
    /**
     * Test of setAssignment method (blank assignment {@link String})
     * 
     * @throws java.lang.IllegalArgumentException   When the assignment is blank
     */
    @Test(expected=IllegalArgumentException.class)
    public void setBlankAssignment()
    {
        System.out.println("setBlankAssignment");
        String blankAssignment = "";
        instance.setAssignment(blankAssignment);
    }
    
    /**
     * Test of getVersion method
     */
    @Test
    public void getVersion()
    {
        System.out.println("getVersion");
        String expected = "F08-M5";
        assertEquals(expected, instance.getVersion());
    }
    
    /**
     * Test of setVersion method
     */
    @Test
    public void setVersion()
    {
        System.out.println("setVersion");
        String newVersion = "F08-T6";
        instance.setVersion(newVersion);
        assertEquals(newVersion, instance.getVersion());
    }
    
    /**
     * Test of setVersion method (<code>null</code> version {@link String})
     * 
     * @throws java.lang.IllegalArgumentException   When the version is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void setNullVersion()
    {
        System.out.println("setNullVersion");
        String nullVersion = null;
        instance.setVersion(nullVersion);
    }
    
    /**
     * Test of setVersion method (blank version {@link String})
     * 
     * @throws java.lang.IllegalArgumentException   When the version is version
     */
    @Test(expected=IllegalArgumentException.class)
    public void setBlankVersion()
    {
        System.out.println("setBlankVersion");
        String blankVersion = "";
        instance.setVersion(blankVersion);
    }

    /**
     * Test of getDueDate method
     */
    @Test
    public void getDueDate()
    {
        System.out.println("getDueDate");
        String expected = "21 Feb 2010";
        assertEquals(expected, instance.getDueDate());
    }

    /**
     * Test of setDueDate method
     */
    @Test
    public void setDueDate()
    {
        System.out.println("setDueDate");
        String newDueDate = "21 Mar 2010";
        instance.setDueDate(newDueDate);
        assertEquals(newDueDate, instance.getDueDate());
    }

    /**
     * Test of setDueDate with blank due date
     */
    @Test(expected=IllegalArgumentException.class)
    public void setBlankDueDate()
    {
        System.out.println("setBlankDueDate");
        String blankDueDate = "";
        instance.setDueDate(blankDueDate);
    }

    /**
     * Test of setDueDate with <code>null</code> due date
     */
    @Test(expected=IllegalArgumentException.class)
    public void setNullDueDate()
    {
        System.out.println("setNullDueDate");
        instance.setDueDate(null);
    }

    /**
     * Test of setDueDate with incorrectly formatted date
     */
    @Test(expected=IllegalArgumentException.class)
    public void setBadDueDate()
    {
        System.out.println("setBadDueDate");
        String newDueDate = "2010 Mar 21";
        instance.setDueDate(newDueDate);
    }
    
    /**
     * Test of getQuestions method
     */
    @Test
    public void getQuestions()
    {
        System.out.println("getQuestions");
        List<Question> expected = new ArrayList<Question>();
        Question question = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        expected.add(question);
        assertEquals(expected, instance.getQuestions());
    }

    @Test
    public void testGetQuestion()
    {
        System.out.println("getQuestion");
        Question expected = new Question("Sounds", new Category("SimpleSelection"),
                "This is a test question");
        assertEquals(expected, instance.getQuestion(0));
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void testGetQuestionNegativeIndex()
    {
        System.out.println("getQuestionNegativeIndex");
        Question result = instance.getQuestion(-1);
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void testGetQuestionLargeIndex()
    {
        System.out.println("getQuestionLargeIndex");
        Question result = instance.getQuestion(42);
    }

    /**
     * Test of setQuestions method
     */
    @Test
    public void setQuestions()
    {
        System.out.println("setQuestions");
    }
    
    /**
     * Test of setQuestions method (<code>null</code> {@link List} of {@link Question}s)
     * 
     * @throws java.lang.IllegalArgumentException   When the {@link List} is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void setNullQuestions()
    {
        System.out.println("setNullQuestions");
        List<Question> nullQuestions = null;
        instance.setQuestions(nullQuestions);
    }
    
    /**
     * Test of addQuestion method
     */
    @Test
    public void addQuestion()
    {
        System.out.println("addQuestion");
        Question newQuest = new Question("Penguin", new Category("Simple Selection"), 
                "This is another test question");
        assertEquals(1, instance.getQuestions().size());
        assertTrue(instance.addQuestion(newQuest));
        assertEquals(2, instance.getQuestions().size());
        assertTrue(instance.getQuestions().contains(newQuest));
    }
    
    /**
     * Test of addQuestion method (<code>null</code> {@link Question})
     * 
     * @throws java.lang.IllegalArgumentException   When the question is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void addNullQuestion()
    {
        System.out.println("addNullQuestion");
        Question nullQuest = null;
        instance.addQuestion(nullQuest);
    }
    
    /**
     * Test of addQuestion method (duplicate {@link Question})
     */
    @Test
    public void addDuplicateQuestion()
    {
        System.out.println("addDuplicateQuestion");
        Question question = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        assertFalse(instance.addQuestion(question));
        assertEquals(1, instance.getQuestions().size());
    }
    
    /**
     * Test of removeQuestion method
     */
    @Test
    public void removeQuestion()
    {
        System.out.println("removeQuestion");
        Question question = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        assertEquals(1, instance.getQuestions().size());
        assertTrue(instance.removeQuestion(question));
        assertEquals(0, instance.getQuestions().size());
    }
    
    /**
     * Test of removeQuestion method (<code>null</code> {@link Question})
     * 
     * @throws java.lang.IllegalArgumentException   When the question is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void removeNullQuestion()
    {
        System.out.println("removeNullQuestion");
        Question nullQuestion = null;
        instance.removeQuestion(nullQuestion);
    }
    
    /**
     * Test of removeQuestion method (non-existent {@link question})
     */
    @Test
    public void removeNonExistantQuestion()
    {
        System.out.println("removeNonExistantQuestion");
        Question question = new Question("Penguin", new Category("SimpleCalculation"), 
                "This is another test question");
        assertEquals(1, instance.getQuestions().size());
        assertFalse(instance.removeQuestion(question));
        assertEquals(1, instance.getQuestions().size());
    }
    
    /**
     * Test of getPartialCredit method
     */
    @Test
    public void getPartialCredit()
    {
        System.out.println("getPartialCredit");
        assertTrue(instance.getPartialCredit());
    }
    
    /**
     * Test of setPartialCredit method
     */
    @Test
    public void setPartialCredit()
    {
        System.out.println("setPartialCredit");
        assertTrue(instance.getPartialCredit());
        instance.setPartialCredit(false);
        assertFalse(instance.getPartialCredit());
    }
    
    /** 
     * Test of compareTo method (equal objects)
     */
    @Test
    public void compareToEquals()
    {
        System.out.println("compareToEquals");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
    
    /** 
     * Test of compareTo method (larger id)
     */
    @Test
    public void compareToLargerId()
    {
        System.out.println("compareToLargerId");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(43L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
    
    /** 
     * Test of compareTo method (smaller id)
     */
    @Test
    public void compareToSmallerId()
    {
        System.out.println("compareToSmallerId");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(41L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
    
    /** 
     * Test of compareTo method (larger {@link Course})
     */
    @Test
    public void compareToLargerCourse()
    {
        System.out.println("compareToLargerCourse");
        Course course = new Course("CS483");
        course.setId(483L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertTrue(instance.compareTo(other) < 0);
        assertTrue(other.compareTo(instance) > 0);
    }
    
    /** 
     * Test of compareTo method (smaller {@link Course})
     */
    @Test
    public void compareToSmallerCourse()
    {
        System.out.println("compareToSmallerCourse");
        Course course = new Course("AERO215");
        course.setId(105L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertTrue(instance.compareTo(other) > 0);
        assertTrue(other.compareTo(instance) < 0);
    }
    
    /**
     * Test of compareTo method (larger Term)
     */
    @Test
    public void compareToLargerTerm()
    {
        System.out.println("compareToLargerTerm");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Spring 2009");
        other.setPartialCredit(true);
        assertTrue(instance.compareTo(other) < 0);
        assertTrue(other.compareTo(instance) > 0);
    }
    
    /**
     * Test of compareTo method (smaller Term)
     */
    @Test
    public void compareToSmallerTerm()
    {
        System.out.println("compareToLargerTerm");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2007");
        other.setPartialCredit(true);
        System.out.println("compareToSmallerTerm");
        assertTrue(instance.compareTo(other) > 0);
        assertTrue(other.compareTo(instance) < 0);
    }
    
    /** 
     * Test of compareTo method (larger assignment)
     */
    @Test
    public void compareToLargerAssignment()
    {
        System.out.println("compareToLargerAssignment");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW3", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertTrue(instance.compareTo(other) < 0);
        assertTrue(other.compareTo(instance) > 0);
    }
    
    /** 
     * Test of compareTo method (smaller assignment)
     */
    @Test
    public void compareToSmallerAssignment()
    {
        System.out.println("compareToSmallerAssignment");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW1", "F08-M7");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertTrue(instance.compareTo(other) > 0);
        assertTrue(other.compareTo(instance) < 0);
    }
    
    /** 
     * Test of compareTo method (larger version)
     */
    @Test
    public void compareToLargerVersion()
    {
        System.out.println("compareToLargerVersion");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M7");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertTrue(instance.compareTo(other) < 0);
        assertTrue(other.compareTo(instance) > 0);
    }
    
    /** 
     * Test of compareTo method (smaller version)
     */
    @Test
    public void compareToSmallerVersion()
    {
        System.out.println("compareToSmallerVersion");
        Course testCourse = new Course("CS110");
        testCourse.setId(110L);
        GradedEvent other = new GradedEvent(testCourse, "HW2", "F08-M4");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertTrue(instance.compareTo(other) > 0);
        assertTrue(other.compareTo(instance) < 0);
    }
    
    /** 
     * Test of compareTo method (larger {@link Question})
     */
    @Test
    public void compareToLargerQuestion()
    {
        System.out.println("compareToLargerQuestion");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("ZebrasRunning", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
    
    /** 
     * Test of compareTo method (smaller {@link Question})
     */
    @Test
    public void compareToSmallerQuestion()
    {
        System.out.println("compareToSmallerQuestion");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Penguins", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        other.setQuestions(questions);
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }

    /**
     * Test of compareTo method (different partial Credit)
     */
    @Test
    public void compareToDiffPartialCredit()
    {
        System.out.println("compareToDiffPartialCredit");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(false);
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }

    /**
     * Test of hashCode method (equal objects)
     */
    @Test
    public void hashCodeSame()
    {
        System.out.println("hashCodeSame");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertEquals(instance.hashCode(), other.hashCode());
    }
    
    /**
     * Test of hashCode method (Different id)
     */
    @Test
    public void hashCodeDiffId()
    {
        System.out.println("hashCodeDiffId");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(40L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertEquals(instance.hashCode(), other.hashCode());
    }
    
    /**
     * Test of hashCode method (Different {@link Course})
     */
    @Test
    public void hashCodeDiffCourse()
    {
        System.out.println("hashCodeDiffName");
        Course course = new Course("CS483");
        course.setId(483L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertFalse(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (Different term)
     */
    @Test
    public void hashCodeDiffTerm()
    {
        System.out.println("hashCodeDiffTerm");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Spring 2009");
        other.setPartialCredit(true);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertFalse(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (Different assignment)
     */
    @Test
    public void hashCodeDiffAssignment()
    {
        System.out.println("hashCodeDiffAssignment");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW3", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertFalse(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (Different version)
     */
    @Test
    public void hashCodeDiffVersion()
    {
        System.out.println("hashCodeVersion");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-T5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertFalse(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (Different {@link List} of questions)
     */
    @Test
    public void hashCodeDiffQuestion()
    {
        System.out.println("hashCodeSame");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Penguin", new Category("SimpleSelection"), 
                "This is another test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertEquals(instance.hashCode(), other.hashCode());
    }
    
    /**
     * Test of hashCode method (different Partial Credit)
     */
    @Test
    public void hashCodeDiffPartialCredit()
    {
        System.out.println("hashCodeDiffPartialCredit");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(false);
        assertEquals(instance.hashCode(), other.hashCode());
    }
    
    /**
     * Test of equals method
     */
    @Test
    public void equals()
    {
        System.out.println("equals");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertTrue(instance.equals(other));
        assertTrue(other.equals(instance));
    }
    
    /**
     * Test of equals method (Different id)
     */
    @Test
    public void equalsDiffId()
    {
        System.out.println("equalsDiffId");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(43L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertTrue(instance.equals(other));
        assertTrue(other.equals(instance));
    }
    
    /**
     * Test of equals method (Different {@link Course})
     */
    @Test
    public void equalsDiffCourse()
    {
        System.out.println("equalsDiffCourse");
        Course course = new Course("CS483");
        course.setId(483L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertFalse(instance.equals(other));
    }
    
    /**
     * Test of equals method (different Term)
     */
    @Test
    public void equalsDiffTerm()
    {
        System.out.println("equals");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Spring 2009");
        other.setPartialCredit(true);
        assertFalse(instance.equals(other));
        assertFalse(other.equals(instance));
    }
    
    /**
     * Test of equals method (Different assignment)
     */
    @Test
    public void equalsDiffAssignment()
    {
        System.out.println("equalsDiffAssignment");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW3", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertFalse(instance.equals(other));
    }
    
    /**
     * Test of equals method (Different version)
     */
    @Test
    public void equalsDiffVersion()
    {
        System.out.println("equalsDiffVersion");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-T5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertFalse(instance.equals(other));
    }
    
    /**
     * Test of equals method (Different {@link List} of questions)
     */
    @Test
    public void equalsDiffQuestions()
    {
        System.out.println("equalsDiffQuestions");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is another test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(true);
        assertTrue(instance.equals(other));
        assertTrue(other.equals(instance));
    }
    
    /**
     * Test of equals method (different partialCredit)
     */
    @Test
    public void equalsDiffPartialCredit()
    {
        System.out.println("equalsDiffPartialCredit");
        Course course = new Course("CS110");
        course.setId(110L);
        GradedEvent other = new GradedEvent(course, "HW2", "F08-M5");
        other.setId(42L);
        List<Question> questions = new ArrayList<Question>();
        Question q1 = new Question("Sounds", new Category("SimpleSelection"), 
                "This is a test question");
        questions.add(q1);
        other.setQuestions(questions);
        other.setTerm("Fall 2008");
        other.setPartialCredit(false);
        assertTrue(instance.equals(other));
        assertTrue(other.equals(instance));       
    }
    
    /**
     * Test of toString method
     */
    @Test
    public void testToString()
    {
        System.out.println("testToString");
        assertEquals("Graded Event: HW2; Version: F08-M5 (Fall 2008)",
                instance.toString());
    }

    private GradedEvent instance;
}