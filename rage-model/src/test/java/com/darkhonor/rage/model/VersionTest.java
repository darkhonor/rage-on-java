/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit Test Cases for the Version class
 * 
 * @author Alex Ackerman
 */
public class VersionTest
{

    public VersionTest()
    {
    }

    @Before
    public void setUp()
    {
        instance = new Version(2.0);
    }

    @Test(expected=NullPointerException.class)
    public void testConstructorNullId()
    {
        System.out.println("constructorNullVersion");
        Double id = null;
        instance = new Version(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testConstructorNegativeId()
    {
        System.out.println("constructorNegativeId");
        Double id = new Double(-1.0);
        instance = new Version(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testConstructorZeroId()
    {
        System.out.println("constructorZeroId");
        Double id = new Double(0.0);
        instance = new Version(id);
    }

    /**
     * Test of getId method, of class Version.
     */
    @Test
    public void testGetId()
    {
        System.out.println("getId");
        Double expResult = 2.0;
        Double result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Version.
     */
    @Test
    public void testSetId()
    {
        System.out.println("setId");
        Double id = 3.0;
        Double expected = 2.0;
        assertEquals(expected, instance.getId());
        instance.setId(id);
        expected = id;
        assertEquals(expected, instance.getId());
    }

    @Test(expected = NullPointerException.class)
    public void testSetNullId()
    {
        System.out.println("setNullId");
        Double id = null;
        instance.setId(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testSetNegativeId()
    {
        System.out.println("setNegativeId");
        Double id = new Double(-1.0);
        instance.setId(id);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testSetZeroId()
    {
        System.out.println("setZeroId");
        Double id = new Double(0.0);
        instance.setId(id);
    }

    /**
     * Test of equals method, of class Version.
     */
    @Test
    public void testEquals()
    {
        System.out.println("equals");
        // null Object
        Object object = null;
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
        // Two equal objects
        Version version = new Version();
        version.setId(2.0);
        expResult = true;
        result = instance.equals(version);
        assertEquals(expResult, result);
        // Two unequal objects
        version.setId(3.0);
        expResult = false;
        result = instance.equals(version);
        assertEquals(expResult, result);
        // Same object
        expResult = true;
        result = instance.equals(instance);
        assertEquals(expResult, result);
    }
    
    private Version instance;
}
