/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit 4 tests of the {@link Student} class and its methods
 *
 * @author Alexander Ackerman
 * 
 * @version 1.0.0
 */
public class StudentTest 
{

    /**
     * No argument constructor
     */
    public StudentTest() 
    {
    }

    /**
     * Set up method for all of the method tests
     */
    @Before
    public void setUp() 
    {
        instance = new Student("John", "Smith", "C98John.Smith", 1998);
        instance.setId(42L);
    }

    /**
     * Test of getId method
     */
    @Test
    public void getId()
    {
        System.out.println("getId");
        Long expResult = 42L;
        Long result = instance.getId();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setId method
     */
    @Test
    public void setId()
    {
        System.out.println("setId");
        Long id = 69L;
        instance.setId(id);
        assertEquals(id, instance.getId());
    }
    
    /**
     * Test of getFirstName method
     */
    @Test
    public void getFirstName()
    {
        System.out.println("getFirstName");
        String expResult = "John";
        assertEquals(expResult, instance.getFirstName());
    }
    
    /**
     * Test of setFirstName method
     */
    @Test
    public void setFirstName()
    {
        System.out.println("setFirstName");
        String newName = "George";
        instance.setFirstName(newName);
        assertEquals(newName, instance.getFirstName());
    }
    
    /**
     * Test of setFirstName method (<code>null</code> first name {@link String})
     * 
     * @throws java.lang.IllegalArgumentException   When first name is <code>null</code>
     */
    @Test(expected=NullPointerException.class)
    public void setNullFirstName()
    {
        System.out.println("setNullFirstName");
        String nullName = null;
        instance.setFirstName(nullName);
    }
    
    /**
     * Test of setFirstName method (blank first name)
     * 
     * @throws java.lang.IllegalArgumentException   When first name is blank
     */
    @Test(expected=IllegalArgumentException.class)
    public void setBlankFirstName()
    {
        System.out.println("setBlankFirstName");
        String blankName = "";
        instance.setFirstName(blankName);
    }

    /**
     * Test of getLastName method
     */
    @Test
    public void getLastName()
    {
        System.out.println("getLastName");
        String expResult = "Smith";
        assertEquals(expResult, instance.getLastName());
    }
    
    /**
     * Test of setLastName method
     */
    @Test
    public void setLastName()
    {
        System.out.println("setLastName");
        String newName = "Washington";
        instance.setLastName(newName);
        assertEquals(newName, instance.getLastName());
    }
    
    /**
     * Test of setLastName method (<code>null</code> last name {@link String})
     * 
     * @throws java.lang.IllegalArgumentException   when lastName is <code>null</code>
     */
    @Test(expected=NullPointerException.class)
    public void setNullLastName()
    {
        System.out.println("setNullLastName");
        String nullName = null;
        instance.setLastName(nullName);
    }
    
    /**
     * Test of setLastName method (blank last name)
     * 
     * @throws java.lang.IllegalArgumentException   When lastName is blank
     */
    @Test(expected=IllegalArgumentException.class)
    public void setBlankLastName()
    {
        System.out.println("setBlankLastName");
        String blankName = "";
        instance.setLastName(blankName);
    }

    /**
     * Test of getWebID method
     */
    @Test
    public void getWebID()
    {
        System.out.println("getWebID");
        String expected = "C98John.Smith";
        assertEquals(expected, instance.getWebID());
    }
    
    /**
     * Test of setWebID method
     */
    @Test
    public void setWebID()
    {
        System.out.println("setWebID");
        String newWebID = "C00John.Smith";
        instance.setWebID(newWebID);
        assertEquals(newWebID, instance.getWebID());
    }
    
    /**
     * Test of setWebID method (<code>null</code> webId {@link String})
     * 
     * @throws java.lang.IllegalArgumentException   When webId is <code>null</code>
     */
    @Test(expected=NullPointerException.class)
    public void setNullWebID()
    {
        System.out.println("setNullWebID");
        String nullWebID = null;
        instance.setWebID(nullWebID);
    }
    
    /**
     * Test of setWebID method (blank webId {@link String})
     * 
     * @throws java.lang.IllegalArgumentException   When webId is blank
     */
    @Test(expected=IllegalArgumentException.class)
    public void setBlankWebID()
    {
        System.out.println("setBlankWebID");
        String blankWebID = "";
        instance.setWebID(blankWebID);
    }

    /**
     * Test of getClassYear method
     */
    @Test
    public void getClassYear()
    {
        System.out.println("getClassYear");
        Integer expected = 1998;
        assertEquals(expected, instance.getClassYear());
    }
    
    /**
     * Test of setClassYear method
     */
    @Test
    public void setClassYear()
    {
        System.out.println("setClassYear");
        Integer newCY = 2000;
        instance.setClassYear(newCY);
        assertEquals(newCY, instance.getClassYear());
    }
    
    /**
     * Test of setClassYear method (<code>null</code> class year)
     * 
     * @throws java.lang.IllegalArgumentException   When class year is null
     */
    @Test(expected=NullPointerException.class)
    public void setNullClassYear()
    {
        System.out.println("setNullClassYear");
        instance.setClassYear(null);
    }
    
    /**
     * Test of getSection method
     */
    @Test
    public void getSections()
    {
        System.out.println("getSections");
        assertEquals(0, instance.getSections().size());
        Section newSection = new Section("M6B");
        assertTrue(instance.addSection(newSection));
        assertEquals(1, instance.getSections().size());
    }
    
    /**
     * Test of setSection method
     */
    @Test
    public void setSections()
    {
        System.out.println("setSection");
        List<Section> newSections = new ArrayList<Section>();
        Section newSection1 = new Section("T6A");
        Section newSection2 = new Section("M5A");
        newSections.add(newSection1);
        newSections.add(newSection2);
        instance.setSections(newSections);
        assertEquals(2, instance.getSections().size());
        assertEquals(newSections, instance.getSections());
    }

    /**
     * Test of setSection method (<code>null</code> {@link Section})
     * 
     */
    @Test(expected=NullPointerException.class)
    public void setNullSections()
    {
        System.out.println("setNullSections");
        List<Section> nullSections = null;
        instance.setSections(nullSections);
    }

    /**
     * Test of addSection method
     */
    @Test
    public void addSection()
    {
        System.out.println("addSection");
        Section newSection = new Section("M5B");
        assertEquals(0, instance.getSections().size());
        assertTrue(instance.addSection(newSection));
        assertEquals(1, instance.getSections().size());
        assertTrue(instance.getSections().contains(newSection));
    }
    
    /**
     * Test of addSection method (<code>null</code> {@link Section})
     */
    @Test(expected=NullPointerException.class)
    public void addNullSection()
    {
        System.out.println("addNullSection");
        Section nullSection = null;
        instance.addSection(nullSection);
    }

    /**
     * Test of addSection method (duplicate {@link Section})
     */
    @Test(expected=IllegalArgumentException.class)
    public void addDuplicateSection()
    {
        System.out.println("addDuplicateSection");
        Section newSection1 = new Section("M5B");
        assertEquals(0, instance.getSections().size());
        assertTrue(instance.addSection(newSection1));
        assertEquals(1, instance.getSections().size());
        Section newSection2 = new Section("M5B");
        assertFalse(instance.addSection(newSection2));
        assertEquals(1, instance.getSections().size());
    }

    /**
     * Test of removeFromCourse method
     */
    @Test
    public void removeFromCourse()
    {
        System.out.println("removeFromCourse");
        Section newSection1 = new Section("M5B");
        assertEquals(0, instance.getSections().size());
        assertTrue(instance.addSection(newSection1));
        assertEquals(1, instance.getSections().size());
        assertTrue(instance.removeFromSection(newSection1));
        assertEquals(0, instance.getSections().size());
    }

    /**
     * Test of removeFromCourse method (<code>null</code> {@link Section})
     */
    @Test(expected=NullPointerException.class)
    public void removeFromCourseNullSection()
    {
        System.out.println("removeFromCourseNullSection");
        assertFalse(instance.removeFromSection(null));
    }

    /**
     * Test of removeFromCourse method ({@link Section} not in set)
     */
    @Test(expected=IllegalArgumentException.class)
    public void removeFromCourseInvalidSection()
    {
        System.out.println("removeFromCourseInvalidSection");
        Section newSection1 = new Section("M5B");
        assertEquals(0, instance.getSections().size());
        assertTrue(instance.addSection(newSection1));
        assertEquals(1, instance.getSections().size());
        Section newSection2 = new Section("M5A");
        assertFalse(instance.removeFromSection(newSection2));
    }
    
    /**
     * Test of compareTo method (equal objects)
     */
    @Test
    public void compareToEquals()
    {
        System.out.println("compareToEquals");
        Student other = new Student("John", "Smith", "C98John.Smith", 1998);
        other.setId(42L);
        assertEquals(0, instance.compareTo(other));
        assertEquals(0, other.compareTo(instance));
    }
    
    /**
     * Test of compareTo method (larger id).  No difference
     */
    @Test
    public void compareToLargerId()
    {
        System.out.println("compareToLargerId");
        Student other = new Student("John", "Smith", "C98John.Smith", 1998);
        other.setId(43L);
        assertTrue(instance.compareTo(other) == 0);
        assertTrue(other.compareTo(instance) == 0);
    }
    
    /**
     * Test of compareTo method (smaller id).  No difference.
     */
    @Test
    public void compareToSmallerId()
    {
        System.out.println("compareToSmallerId");
        Student other = new Student("John", "Smith", "C98John.Smith", 1998);
        other.setId(40L);
        assertTrue(instance.compareTo(other) == 0);
        assertTrue(other.compareTo(instance) == 0);
    }
    
    /**
     * Test of compareTo method (larger first name)
     */
    @Test
    public void compareToLargerFirstName()
    {
        System.out.println("compareToLargerFirstName");
        Student other = new Student("Robert", "Smith", "C98John.Smith", 1998);
        other.setId(42L);
        assertTrue(instance.compareTo(other) < 0);
        assertTrue(other.compareTo(instance) > 0);
    }
    
    /**
     * Test of compareTo method (smaller first name)
     */
    @Test
    public void compareToSmallerFirstName()
    {
        System.out.println("compareToSmallerName");
        Student other = new Student("Albert", "Smith", "C98John.Smith", 1998);
        other.setId(42L);
        assertTrue(instance.compareTo(other) > 0);
        assertTrue(other.compareTo(instance) < 0);
    }
    
    /**
     * Test of compareTo method (larger last name)
     */
    @Test
    public void compareToLargerLastName()
    {
        System.out.println("compareToLargerLastName");
        Student other = new Student("John", "Zhivago", "C98John.Smith", 1998);
        other.setId(42L);
        assertTrue(instance.compareTo(other) < 0);
        assertTrue(other.compareTo(instance) > 0);
    }
    
    /**
     * Test of compareTo method (smaller last name)
     */
    @Test
    public void compareToSmallerLastName()
    {
        System.out.println("compareToSmallerLastName");
        Student other = new Student("John", "Capone", "C98John.Smith", 1998);
        other.setId(42L);
        assertTrue(instance.compareTo(other) > 0);
        assertTrue(other.compareTo(instance) < 0);
    }
    
    /**
     * Test of compareTo method (larger web id)
     */
    @Test
    public void compareToLargerWebID()
    {
        System.out.println("compareToLargerWebID");
        Student other = new Student("John", "Smith", "C99Able.Johnson", 1998);
        other.setId(42L);
        assertTrue(instance.compareTo(other) < 0);
        assertTrue(other.compareTo(instance) > 0);
    }
    
    /**
     * Test of compareTo method (smaller web id)
     */
    @Test
    public void compareToSmallerWebID()
    {
        System.out.println("compareToSmallerWebID");
        Student other = new Student("John", "Smith", "C97Capone, Albert", 1998);
        other.setId(42L);
        assertTrue(instance.compareTo(other) > 0);
        assertTrue(other.compareTo(instance) < 0);
    }
    
    /**
     * Test of compareTo method (larger class year)
     */
    @Test
    public void compareToLargerClassYear()
    {
        System.out.println("compareToSmallerName");
        Student other = new Student("John", "Smith", "C98John.Smith", 2000);
        other.setId(42L);
        assertTrue(instance.compareTo(other) < 0);
        assertTrue(other.compareTo(instance) > 0);
    }
    
    /**
     * Test of compareTo method (smaller class year)
     */
    @Test
    public void compareToSmallerClassYear()
    {
        System.out.println("compareToSmallerName");
        Student other = new Student("John", "Smith", "C98John.Smith", 1996);
        other.setId(42L);
        assertTrue(instance.compareTo(other) > 0);
        assertTrue(other.compareTo(instance) < 0);
    }
    
    /**
     * Test of hashCode method (equal objects)
     */
    @Test
    public void hashCodeEquals()
    {
        System.out.println("hashCodeEquals");
        Student other = new Student("John", "Smith", "C98John.Smith", 1998);
        other.setId(42L);
        assertEquals(instance.hashCode(), other.hashCode());
    }
    
    /**
     * Test of hashCode method (different id)
     */
    @Test
    public void hashCodeDiffId()
    {
        System.out.println("hashCodeDiffId");
        Student other = new Student("John", "Smith", "C98John.Smith", 1998);
        other.setId(69L);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertTrue(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (different first name)
     */
    @Test
    public void hashCodeDiffFirstName()
    {
        System.out.println("hashCodeDiffFirstName");
        Student other = new Student("Alvin", "Smith", "C98John.Smith", 1998);
        other.setId(42L);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertFalse(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (different last name)
     */
    @Test
    public void hashCodeDiffLastName()
    {
        System.out.println("hashCodeDiffLastName");
        Student other = new Student("John", "Capone", "C98John.Smith", 1998);
        other.setId(42L);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertFalse(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (different web id)
     */
    @Test
    public void hashCodeDiffWebID()
    {
        System.out.println("hashCodeDiffWebID");
        Student other = new Student("John", "Smith", "C99Alvin.Capone", 1998);
        other.setId(42L);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertFalse(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (different class year)
     */
    @Test
    public void hashCodeDiffClassYear()
    {
        System.out.println("hashCodeDiffClassYear");
        Student other = new Student("John", "Smith", "C98John.Smith", 1999);
        other.setId(42L);
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertFalse(hash1.equals(hash2));
    }
    
    /**
     * Test of equals method (equal objects)
     */
    @Test
    public void equals()
    {
        System.out.println("equals");
        Student other = new Student("John", "Smith", "C98John.Smith", 1998);
        other.setId(42L);
        assertTrue(instance.equals(other));
        assertTrue(other.equals(instance));
    }
    
    /**
     * Test of equals method (different id).  No difference.
     */
    @Test
    public void equalsDiffId()
    {
        System.out.println("equalsDiffId");
        Student other = new Student("John", "Smith", "C98John.Smith", 1998);
        other.setId(69L);
        assertTrue(instance.equals(other));
        assertTrue(other.equals(instance));
    }
    
    /**
     * Test of equals method (different first name)
     */
    @Test
    public void equalsDiffFirstName()
    {
        System.out.println("equalsDiffFirstName");
        Student other = new Student("Alvin", "Smith", "C98John.Smith", 1998);
        other.setId(42L);
        assertFalse(instance.equals(other));
        assertFalse(other.equals(instance));
    }
    
    /**
     * Test of equals method (different last name)
     */
    @Test
    public void equalsDiffLastName()
    {
        System.out.println("equalsDiffLastName");
        Student other = new Student("John", "Capone", "C98John.Smith", 1998);
        other.setId(42L);
        assertFalse(instance.equals(other));
        assertFalse(other.equals(instance));
    }
    
    /**
     * Test of equals method (different web id)
     */
    @Test
    public void equalsDiffWebID()
    {
        System.out.println("equalsDiffWebID");
        Student other = new Student("John", "Smith", "C99Alvin.Capone", 1998);
        other.setId(42L);
        assertFalse(instance.equals(other));
        assertFalse(other.equals(instance));
    }
    
    /**
     * Test of equals method (different class year)
     */
    @Test
    public void equalsDiffClassYear()
    {
        System.out.println("equalsDiffClassYear");
        Student other = new Student("John", "Smith", "C98John.Smith", 2000);
        other.setId(42L);
        assertFalse(instance.equals(other));
        assertFalse(other.equals(instance));
    }
    
    /**
     * Test of toString method
     */
    @Test
    public void testToString()
    {
        System.out.println("testToString");
        assertEquals("Smith, John (1998)", instance.toString());
    }
    
    Student instance;
    
}