/**
 * Copyright 2010-2014 Darkhonor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.darkhonor.rage.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit 4 tests of the {@link TestCase} class and its methods
 *
 * @author Alexander Ackerman
 * 
 * @version 1.0.0
 */
public class TestCaseTest 
{

    /**
     * No argument constructor
     */
    public TestCaseTest() 
    {
    }

    /**
     * Set up method for all of the method tests
     */
    @Before
    public void setUp() 
    {
        instance = new TestCase();
        instance.setId(42L);
        instance.setValue(new BigDecimal("1.5"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        instance.setInputs(inputSet);
        instance.setOutputs(outputSet);
        instance.addExclusion("1200");
        instance.addExclusion("42");
    }

    /**
     * Test of getId method, of class TestCase.
     */
    @Test
    public void testGetId() 
    {
        System.out.println("getId");
        Long expResult = 42L;
        Long result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class TestCase.
     */
    @Test
    public void testSetId() 
    {
        System.out.println("setId");
        Long id = 69L;
        instance.setId(id);
        assertEquals(id, instance.getId());
    }

    /**
     * Test of getValue method
     */
    @Test
    public void getValue()
    {
        System.out.println("getValue");
        BigDecimal value = new BigDecimal("1.5");
        assertEquals(value, instance.getValue());
    }
    
    /**
     * Test of setValue method
     */
    @Test
    public void setValue()
    {
        System.out.println("setValue");
        BigDecimal newValue = new BigDecimal("2.0");
        instance.setValue(newValue);
        assertEquals(newValue, instance.getValue());
    }
    
    /**
     * Test of setValue method (negative value)
     * 
     * @throws java.lang.IllegalArgumentException   When value is negative
     */
    @Test(expected = IllegalArgumentException.class)
    public void setNegValue()
    {
        System.out.println("setNegValue");
        BigDecimal newValue = new BigDecimal("-1.3");
        instance.setValue(newValue);
    }
    
    /**
     * Test of setValue method (<code>null</code> value)
     * 
     * @throws java.lang.IllegalArgumentException   When value is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void setNullValue()
    {
        System.out.println("setNullValue");
        BigDecimal newValue = null;
        instance.setValue(newValue);
    }
    
    /**
     * Test of getInputs method
     */
    @Test
    public void testGetInputs() 
    {
        System.out.println("getInputs");
        List<String> inputTestSet = new ArrayList<String>();
        inputTestSet.add("44100");
        inputTestSet.add("5");
        inputTestSet.add("24");
        assertEquals(inputTestSet.size(), instance.getInputs().size());
        assertEquals(inputTestSet, instance.getInputs());
    }

    /**
     * Test of setInputs method
     */
    @Test
    public void testSetInputs() 
    {
        System.out.println("setInputs");
        List<String> inputTestSet = new ArrayList<String>();
        inputTestSet.add("23454");
        inputTestSet.add("54");
        inputTestSet.add("48");
        inputTestSet.add("42.3453");
        instance.setInputs(inputTestSet);
        assertEquals(4, instance.getInputs().size());
        assertEquals(inputTestSet, instance.getInputs());
    }
    
    /**
     * Test of setInputs method (<code>null</code> set of inputs)
     * 
     * @throws java.lang.IllegalArgumentException   When the set is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void testSetNullInputs()
    {
        System.out.println("setNullInputs");
        List<String> nullInputs = null;
        instance.setInputs(nullInputs);
        fail("setNullInputs should have thrown an IllegalArgumentException");
    }

    /**
     * Test of addInput method
     */
    @Test
    public void testAddInput() 
    {
        System.out.println("addInput");
        String newInput = "42.0357";
        assertTrue(instance.addInput(newInput));
        assertEquals(4, instance.getInputs().size());
        assertTrue(instance.getInputs().contains(newInput));
    }

    /**
     * Test of addInput method (<code>null</code> input)
     * 
     * @throws java.lang.IllegalArgumentException   When input is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void testAddNullInput()
    {
        System.out.println("addNullInput");
        String nullInput = null;
        instance.addInput(nullInput);
        fail("addNullInput should have thrown an IllegalArgumentException");
    }

    /**
     * Test of addInput method (blank input)
     * 
     * @throws java.lang.IllegalArgumentException   When input is blank
     */
    @Test(expected=IllegalArgumentException.class)
    public void testAddBlankInput()
    {
        System.out.println("addBlankInput");
        String blankInput = "";
        assertFalse(instance.addInput(blankInput));
        fail("addNullInput should have thrown an IllegalArgumentException");
    }

    /**
     * Test of removeInput method
     */
    @Test
    public void testRemoveInput() 
    {
        System.out.println("removeInput");
        String input = "5";
        assertTrue(instance.removeInput(input));
        assertEquals(2, instance.getInputs().size());
        assertFalse(instance.getInputs().contains(5));
    }

    /**
     * Test of removeInput method (<code>null</code> input)
     * 
     * @throws java.lang.IllegalArgumentException   When input is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveNullInput()
    {
        System.out.println("removeNullInput");
        String nullInput = null;
        assertFalse(instance.removeInput(nullInput));
        fail("removeNullInput should have thrown an IllegalArgumentException");
    }

    /**
     * Test of removeInput method (input not in set)
     */
    @Test
    public void testRemoveNonExistentInput()
    {
        System.out.println("removeNonExistentInput");
        String badInput = "6";
        assertFalse(instance.removeInput(badInput));
    }

    /**
     * Tet of getOutputs method
     */
    @Test
    public void testGetOutputs() 
    {
        System.out.println("getOutputs");
        List<String> outputTestSet = new ArrayList<String>();
        outputTestSet.add("79380000");
        outputTestSet.add("77519.5313");
        outputTestSet.add("75.7027");
        assertEquals(outputTestSet.size(), instance.getOutputs().size());
        assertEquals(outputTestSet, instance.getOutputs());
    }

    /**
     * Test of setOutputs method
     */
    @Test
    public void testSetOutputs() 
    {
        System.out.println("setOutputs");
        List<String> outputTestSet = new ArrayList<String>();
        outputTestSet.add("79380000");
        outputTestSet.add("77519.5313");
        outputTestSet.add("75.7027");
        outputTestSet.add("A string value");
        instance.setOutputs(outputTestSet);
        assertEquals(4, instance.getOutputs().size());
        assertEquals(outputTestSet, instance.getOutputs());
    }

    /**
     * Test of setOutputs method (<code>null</code> set)
     * 
     * @throws java.lang.IllegalArgumentException   When set is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void testSetNullOutputs()
    {
        System.out.println("setNullOutputs");
        List<String> nullOutputs = null;
        instance.setOutputs(nullOutputs);
        fail("setNullOutputs should have thrown an IllegalArgumentException");
    }

    /**
     * Test of addOutput method
     */
    @Test
    public void testAddOutput() 
    {
        System.out.println("addOutput");
        String newOutput = "69";
        assertTrue(instance.addOutput(newOutput));
        assertEquals(4, instance.getOutputs().size());
        assertTrue(instance.getOutputs().contains(newOutput));
    }

    /**
     * Test of addOutput method (<code>null</code> output)
     * 
     * @throws java.lang.IllegalArgumentException   When output is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void testAddNullOutput()
    {
        System.out.println("addNullOutput");
        String nullOutput = null;
        assertFalse(instance.addOutput(nullOutput));
        fail("addNullOutput should have thrown an IllegalArgumentException");
    }

    /**
     * Test of addOutput method (blank output)
     * 
     * @throws java.lang.IllegalArgumentException   When output is blank.
     */
    @Test(expected=IllegalArgumentException.class)
    public void testAddBlankOutput()
    {
        System.out.println("addBlankOutput");
        String blankOutput = "";
        assertFalse(instance.addOutput(blankOutput));
        fail("addBlankOutput should have thrown an IllegalArgumentException");
    }

    /**
     * Test of removeOutput method
     */
    @Test
    public void testRemoveOutput() 
    {
        System.out.println("removeOutput");
        String badOutput = "75.7027";
        assertTrue(instance.removeOutput(badOutput));
        assertEquals(2, instance.getOutputs().size());
        assertFalse(instance.getOutputs().contains(badOutput));
    }

    /**
     * Test of removeOutput method (<code>null</code> output)
     * 
     * @throws java.lang.IllegalArgumentException   When output is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveNullOutput()
    {
        System.out.println("removeNullOutput");
        String nullOutput = null;
        assertFalse(instance.removeOutput(nullOutput));
        fail("removeNullOutput should have thrown an IllegalArgumentException");
    }

    /**
     * Test of removeOutput (output not in set)
     */
    @Test
    public void testRemoveNonExistentOutput()
    {
        System.out.println("removeNonExistentOutput");
        String badInput = "76.9865";
        assertFalse(instance.removeInput(badInput));
    }
    
    /**
     * Test of getExcludes method
     */
    @Test
    public void getExcludes()
    {
        System.out.println("getExcludes");
        List<String> expected = new ArrayList<String>();
        expected.add("1200");
        expected.add("42");
        assertEquals(expected.size(), instance.getExcludes().size());
        assertEquals(expected, instance.getExcludes());
    }
    
    /**
     * Test of setExcludes method
     */
    @Test
    public void setExcludes()
    {
        System.out.println("setExcludes");
        List<String> newExcludes = new ArrayList<String>();
        newExcludes.add("12000");
        newExcludes.add("420");
        newExcludes.add("521");
        assertEquals(2, instance.getExcludes().size());
        instance.setExcludes(newExcludes);
        assertEquals(3, instance.getExcludes().size());
        assertEquals(newExcludes, instance.getExcludes());
    }
    
    /**
     * Test of setExcludes method (<code>null</code> set) 
     * 
     * @throws java.lang.IllegalArgumentException   When set is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void setNullExcludes()
    {
        System.out.println("setNullExcludes");
        instance.setExcludes(null);
    }
    
    /**
     * Test of addExclusion method
     */
    @Test
    public void addExclusion()
    {
        System.out.println("addExclusion");
        assertEquals(2, instance.getExcludes().size());
        assertTrue(instance.addExclusion("52000"));
        assertEquals(3, instance.getExcludes().size());
        assertTrue(instance.getExcludes().contains("52000"));
    }
    
    /**
     * Test of addExclusion method (<code>null</code> exclusion)
     * 
     * @throws java.lang.IllegalArgumentException   When exclusion is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void addNullExclusion()
    {
        System.out.println("addNullExclusion");
        instance.addExclusion(null);
    }
    
    /**
     * Test of addExclusion method (blank exclusion)
     * 
     * @throws java.lang.IllegalArgumentException   When exclusion is blank
     */
    @Test(expected=IllegalArgumentException.class)
    public void addBlankExclusion()
    {
        System.out.println("addBlankExclusion");
        instance.addExclusion("");
    }
    
    /**
     * Test of removeExclusion method
     */
    @Test
    public void removeExclusion()
    {
        System.out.println("removeExclusion");
        assertEquals(2, instance.getExcludes().size());
        assertTrue(instance.removeExclusion("42"));
        assertEquals(1, instance.getExcludes().size());
        assertFalse(instance.getExcludes().contains("42"));
    }
    
    /**
     * Test of removeExclusion method (<code>null</code> exclusion)
     * 
     * @throws java.lang.IllegalArgumentException   When exclusion is <code>null</code>
     */
    @Test(expected=IllegalArgumentException.class)
    public void removeNullExclusion()
    {
        System.out.println("removeNullExclusion");
        instance.removeExclusion(null);
    }
    
    /**
     * Test of removeExclusion method (exclusion not in set)
     */
    @Test
    public void removeNonExistentExclusion()
    {
        System.out.println("removeNonExistentExclusion");
        assertEquals(2, instance.getExcludes().size());
        assertFalse(instance.removeExclusion("52"));
        assertEquals(2, instance.getExcludes().size());
    }
    
    /**
     * Test of hashCode method (equal objects)
     */
    @Test
    public void hashCodeSame()
    {
        System.out.println("hashCodeSame");
        TestCase other = new TestCase();
        other.setId(42L);
        other.setValue(new BigDecimal("1.5"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        other.setInputs(inputSet);
        other.setOutputs(outputSet);
        other.addExclusion("1200");
        other.addExclusion("42");
        assertEquals(instance.hashCode(), other.hashCode());
    }
    
    /**
     * Test of hashCode method (different Id)
     */
    @Test
    public void hashCodeDiffId()
    {
        System.out.println("hashCodeDiffId");
        TestCase other = new TestCase();
        other.setId(69L);
        other.setValue(new BigDecimal("1.5"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        other.setInputs(inputSet);
        other.setOutputs(outputSet);
        other.addExclusion("1200");
        other.addExclusion("42");
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertFalse(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (different value)
     */
    @Test
    public void hashCodeDiffValue()
    {
        System.out.println("hashCodeSame");
        TestCase other = new TestCase();
        other.setId(42L);
        other.setValue(new BigDecimal("1.2"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        other.setInputs(inputSet);
        other.setOutputs(outputSet);
        other.addExclusion("1200");
        other.addExclusion("42");
        Integer hash1 = instance.hashCode();
        Integer hash2 = other.hashCode();
        assertFalse(hash1.equals(hash2));
    }
    
    /**
     * Test of hashCode method (different input (by one))
     */
    @Test
    public void hashCodeDiffInputByOne()
    {
        System.out.println("hashCodeDiffInputByOne");
        TestCase other = new TestCase();
        other.setId(42L);
        other.setValue(new BigDecimal("1.5"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        other.setInputs(inputSet);
        other.setOutputs(outputSet);
        other.addExclusion("1200");
        other.addExclusion("42");
        assertEquals(instance.hashCode(), other.hashCode());
    }
    
    /**
     * Test of hashCode method (different output by one)
     */
    @Test
    public void hashCodeDiffOutputByOne()
    {
        System.out.println("hashCodeDiffOutputByOne");
        TestCase other = new TestCase();
        other.setId(42L);
        other.setValue(new BigDecimal("1.5"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("84.2132");
        other.setInputs(inputSet);
        other.setOutputs(outputSet);
        other.addExclusion("1200");
        other.addExclusion("42");
        assertEquals(instance.hashCode(), other.hashCode());
    }
    
    /**
     * Test of equals method (equal objects)
     */
    @Test
    public void equals()
    {
        System.out.println("equals");
        TestCase other = new TestCase();
        other.setId(42L);
        other.setValue(new BigDecimal("1.5"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        other.setInputs(inputSet);
        other.setOutputs(outputSet);
        other.addExclusion("1200");
        other.addExclusion("42");
        assertTrue(instance.equals(other));
    }
    
    /**
     * Test of equals method (different Id)
     */
    @Test
    public void equalsDiffId()
    {
        System.out.println("equalsDiffId");
        TestCase other = new TestCase();
        other.setId(69L);
        other.setValue(new BigDecimal("1.5"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        other.setInputs(inputSet);
        other.setOutputs(outputSet);
        other.addExclusion("1200");
        other.addExclusion("42");
        assertFalse(instance.equals(other));
    }
    
    /**
     * Test of equals method (different value)
     */
    @Test
    public void equalsDiffValue()
    {
        System.out.println("equalsDiffId");
        TestCase other = new TestCase();
        other.setId(42L);
        other.setValue(new BigDecimal("1.3"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        other.setInputs(inputSet);
        other.setOutputs(outputSet);
        other.addExclusion("1200");
        other.addExclusion("42");
        assertFalse(instance.equals(other));
    }
    
    /**
     * Test of equals method (different input by one)
     */
    @Test
    public void equalsDiffInputByOne()
    {
        TestCase other = new TestCase();
        other.setId(42L);
        other.setValue(new BigDecimal("1.5"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("22000");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("75.7027");
        other.setInputs(inputSet);
        other.setOutputs(outputSet);
        other.addExclusion("1200");
        other.addExclusion("42");
        assertTrue(instance.equals(other));
    }
    
    /**
     * Test of equals method (different output by one)
     */
    @Test
    public void equalsDiffOutputByOne()
    {
        System.out.println("equals");
        TestCase other = new TestCase();
        other.setId(42L);
        other.setValue(new BigDecimal("1.5"));
        List<String> inputSet = new ArrayList<String>();
        inputSet.add("44100");
        inputSet.add("5");
        inputSet.add("24");
        List<String> outputSet = new ArrayList<String>();
        outputSet.add("79380000");
        outputSet.add("77519.5313");
        outputSet.add("83.4242");
        other.setInputs(inputSet);
        other.setOutputs(outputSet);
        other.addExclusion("1200");
        other.addExclusion("42");
        assertTrue(instance.equals(other));
    }
    
    /**
     * Test of toString method
     */
    @Test
    public void testToString()
    {
        System.out.println("testToString");
        assertEquals("Test Case: 42; Value: 1.5", instance.toString());
    }
    
    private TestCase instance;
    
}